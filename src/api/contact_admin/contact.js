import express   from 'express';
import ContactController from '../../controllers/contact_admin/contact';

let router = express.Router();
let contactController = new ContactController();

router.post('/contactadmin', contactController.contactAdmin.bind(contactController));

export default router;