exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('users_domain', function(t) {
        t.integer('uid').unsigned().notNull().defaultTo(0).comment('Primary Key: users.uid for user.').alter();
        t.integer('domain_id ').unsigned().notNull().defaultTo(0).comment('Primary Key: domain.domain_id for domain.').alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('users_domain', function(t) {
        
    });
 };