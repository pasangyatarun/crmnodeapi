exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('billwerk_user_customer', function(t) {
        t.string('plan_id',[255]).alter();
        t.string('plan_variant_id',[255]).alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('billwerk_user_customer', function(t) {
       
    });
 };