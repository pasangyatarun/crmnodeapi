import DBConnection from '../../core/db';
let db = new DBConnection().conn.promise();

export default class FilterModel {

    async getUsersByStatus(req){
        const rows = await db.query("SELECT users.uid,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE users.status = ?", [req.body.status]);
        return rows[0];
    }

    async getUsersByRole(req){
        const rows = await db.query("SELECT users.uid,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE role.name = ?", [req.body.role]);
        return rows[0];
    }

    async getUsersByStatusRole(req){
        const rows = await db.query("SELECT users.uid,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE role.name = ? AND users.status =?", [req.body.role,req.body.status]);
        return rows[0];
    }
}