exports.up = async function(knex) {
    return knex.schema.createTable('customer_departments', function(t) {
        t.increments('id').primary();
        t.integer('customer_id',[11]).notNull();
        t.string('department_name',[255]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('customer_departments');
};
