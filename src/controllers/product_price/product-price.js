import async from 'async';
import ProductPriceModel from '../../models/product_price/product-price.js';
import ProductPriceValidation from '../../validators/product_price/product-price.js';
import responseFormat from '../../core/response-format';
let productpriceModel = new ProductPriceModel();
let propriceValid = new ProductPriceValidation();

export default class ProductPriceController {

    /**
    * Function to update product components by id
    * @author  MangoIt Solutions
    * @param   {id} with updated details
    * @return  {message} success message
    */
    proPriceCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = propriceValid.productPriceValidate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productpriceModel.createProPrice(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['product:productSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productPriceFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get product price details 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    productPrices(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productpriceModel.getAllProPrice().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get product price details by Id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} product details
    */
    proPriceById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productpriceModel.getProPriceById(req.params.pid).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get product price details by suid
    * @author  MangoIt Solutions
    * @param   {suid} 
    * @return  {object} product details
    */
    proPriceBysuid(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productpriceModel.getProPriceBysuid(req.params.suid).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to get product price details by cuid
    * @author  MangoIt Solutions
    * @param   {cuid} 
    * @return  {object} product details
    */
    proPriceBycuid(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productpriceModel.getProPriceBycuid(req.params.cuid).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to update product price by product id
    * @author  MangoIt Solutions
    * @param   {pid} 
    * @return  {message} success message
    */
    updateProPrice(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = propriceValid.productPriceValidate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productpriceModel.getProPriceById(req.params.pid).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        done();
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productpriceModel.updateProPrice(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['product:productUpdated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to update product price by product id
    * @author  MangoIt Solutions
    * @param   {pid} 
    * @return  {message} success message
    */
    updateProPriceBypid(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = propriceValid.productPriceValidate(req.body.price);
        if (msgCode && msgCode.length && msgCode.price) {
            response = responseFormat.getResponseMessageByCodes(msgCode.price, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productpriceModel.getProPriceById(req.params.pid).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        done();
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productpriceModel.updateProPricebypid(req.params.pid, req.body.price).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['product:productPriceUpdated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }
    deleteProPrice(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productpriceModel.deleteProPrice(req.params.pid).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                res.status(200).json(response);
            }
            response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
            res.status(200).json(response);
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete multiple product 
    * @author  MangoIt Solutions
    * @param   {array} of product id
    * @return  {message} success message
    */
    deleteMultiProPrice(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        if (req.body.arr) {
            let arrtemp = [];
            let arrTmp = [];
            async.waterfall([
                function (done) {
                    for (let index = 0; index < req.body.arr.length; index++) {
                        const element = req.body.arr[index];
                        productpriceModel.deleteProPrice(element).then((results) => {
                            if (results.affectedRows > 0) {
                                arrtemp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            } else {
                                arrTmp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            }

                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                    res.status(200).json(response);
                }
            ])
        }
    }
}