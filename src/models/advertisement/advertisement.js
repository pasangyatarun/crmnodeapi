import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class AdvertisementModel {

	async getAdvertisement(name) {
		const rows = await db.query(`SELECT * from advertisement WHERE name=?`, [name]);
		return rows[0];
	}

	async CreateAdvertisement(req, Img) {
		const rows = await db.query('INSERT INTO `advertisement`(`name`,`text`,`picture`,`link`,`status`,`start_date`,`end_date`)VALUES(?,?,?,?,?,?,?)', [
			req.body.name,
			req.body.text,
			Img,
			req.body.link,
			req.body.status,
			req.body.start_date,
			req.body.end_date,
		]);
		return rows[0];
	}

	async advertisementUpdate(req, Img) {
		const rows = await db.query("UPDATE advertisement SET name= ?,text=?,picture= ?,link= ?,status=?,start_date=?,end_date=? WHERE id = ?", [
			req.body.name,
			req.body.text,
			Img,
			req.body.link,
			req.body.status,
			req.body.start_date,
			req.body.end_date,
			req.params.id
		])
		return rows[0];
	}

	async getAllAdvertisement() {
		const rows = await db.query(`SELECT * from advertisement`);
		return rows[0];
	}

	async getAdvertisementById(id) {
		const rows = await db.query(`SELECT * from advertisement WHERE id=?`, [id]);
		return rows[0];
	}

	async deleteAdvertisementbyId(id) {
		const rows = await db.query("DELETE  FROM advertisement WHERE `id`=?", [id]);
		return rows[0];
	}
}