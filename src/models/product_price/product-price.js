import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class ProductPriceModel {

    async getProPriceById(pid) {
        const rows = await db.query("SELECT * FROM `sverein_product_price` WHERE pid = ?", [pid]);
        return rows[0];
    }

    async getProPriceBysuid(suid) {
        const rows = await db.query("SELECT * FROM `sverein_product_price` WHERE suid = ?", [suid]);
        return rows[0];
    } 
    
    async getProPriceBycuid(cuid) {
        const rows = await db.query("SELECT * FROM `sverein_product_price` WHERE cuid = ?", [cuid]);
        return rows[0];
    }

    async createProPrice(req, insId) {
        const rows = await db.query('INSERT INTO `sverein_product_price`(`pid`,`suid`,`cuid`,`price`)VALUES(?,?,?,?)', [
            insId,
            25,
            25,
            req.body.productPrice
        ]);
        return rows[0];
    }

    async getAllProPrice(req) {
        const rows = await db.query("SELECT * FROM `sverein_product_price`");
        return rows[0];
    }

    async deleteProPrice(pid) {
        const rows = await db.query("DELETE FROM `sverein_product_price` WHERE `pid`=?", [pid]);
        return rows[0];
    }

    async updateProPrice(req) {
        const rows = await db.query("UPDATE sverein_product_price SET suid = ?,cuid = ?,price = ? WHERE pid = ?", [
            25,
            25,
            req.body.productPrice,
            req.params.pid
        ])
        return rows[0];
    }

    async updateProPricebypid(pid, price) {
        const rows = await db.query("UPDATE sverein_product_price SET price = ? WHERE pid = ?", [
            price,
            pid
        ])
        return rows[0];
    }
}