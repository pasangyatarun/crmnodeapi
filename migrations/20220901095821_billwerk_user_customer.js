exports.up = async function(knex) {
    return knex.schema.createTable('billwerk_user_customer', function(t) {
        t.increments('id').primary();
        t.integer('user_id').notNull();
        t.string('customer_id',[255]).notNull();
        t.string('order_id',[255]).notNull();
        t.string('contract_id',[255]).notNull();
        t.string('plan_id',[255]).notNull();
        t.string('plan_variant_id',[255]).notNull();
        t.text('plan_name').notNull();
        t.text('plan_variant_name').notNull();
        t.integer('type').notNull();
        t.longtext('user_personalized_pdf_url');
        t.longtext('login_info');
        t.longtext('commit_response');
        t.timestamp('created_at').defaultTo(knex.fn.now());
        // t.datetime('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('billwerk_user_customer');
};
