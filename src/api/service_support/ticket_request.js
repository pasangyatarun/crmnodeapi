import express from 'express';
import TicketRequestController from '../../controllers/service_support/ticket_request';
import multerMW from '../../core/multer';

let router = express.Router();
let ticketRequestController = new TicketRequestController();

router.post('/store-request/:req_user_id',multerMW.single('file'), ticketRequestController.createTicket.bind(ticketRequestController));
router.get('/requestbyid/:id', ticketRequestController.requestById.bind(ticketRequestController));
router.get('/requestsbyuser/:uid', ticketRequestController.requestByUserId.bind(ticketRequestController));
router.get('/requestsbyuserpagination/:uid/page/:page/pagesize/:pagesize', ticketRequestController.requestByUserIdPagination.bind(ticketRequestController));
router.get('/requestuserslist', ticketRequestController.requestUsersList.bind(ticketRequestController));
router.get('/requestuserslistbypagination/:page/pagesize/:pagesize', ticketRequestController.requestUsersListByPage.bind(ticketRequestController));
router.put('/requestupdate/:id',multerMW.single('file'),ticketRequestController.updateRequest.bind(ticketRequestController));
router.put('/requestStatusUpdate/:id',ticketRequestController.updateStatusRequest.bind(ticketRequestController));
router.delete('/requestdelete/:id', ticketRequestController.deleteRequest.bind(ticketRequestController));
router.post('/store-ticket-comment/:user_id/ticket/:ticket_id',multerMW.single('file'), ticketRequestController.createTicketComment.bind(ticketRequestController));
router.get('/commentbyticketid/:ticket_id', ticketRequestController.commentbyticketid.bind(ticketRequestController));
router.put('/commentupdate/:id',multerMW.single('file'), ticketRequestController.updateComment.bind(ticketRequestController));
router.delete('/commentdelete/:id', ticketRequestController.deleteComment.bind(ticketRequestController));
router.post('/store-ticket-comment-reply/:user_id/ticket/:ticket_id/comment/:comment_id', ticketRequestController.createTicketCommentReply.bind(ticketRequestController));
//API for all support users
router.get('/getAllSupportUsers', ticketRequestController.getAllSupportUsers.bind(ticketRequestController));
router.post('/store-request-assignedTo', ticketRequestController.addAssignedUser.bind(ticketRequestController));

export default router;