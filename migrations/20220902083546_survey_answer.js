exports.up = async function(knex) {
    return knex.schema.createTable('survey_answer', function(t) {
        t.increments('id').primary();
        t.integer('survey_id',[11]).notNull();
        t.string('survey_answer',[255]).notNull();
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('survey_answer');
};
    