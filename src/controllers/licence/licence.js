import async from 'async';
import LicenceModel from '../../models/licence/licence';
import LicenceValidation from '../../validators//licence/licence';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import qs from 'qs';
import fetch from 'node-fetch';
import UserModel from '../../models/user/user';
let licenceModel = new LicenceModel();
let licenceValidation = new LicenceValidation();
let userModel = new UserModel();
export default class LicenceController {

    /**
    * Function to set the licence status (blocaked/active) of the customer
    * @author  MangoIt Solutions
    * @param   {user_id} with action
    * @return  {message} success message
    */
    licenceClubBlock(req, res, next) {
        let keycloakToken;
        let dbList;
        let item;
        let loginInfo = [];
        let userData;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var data = qs.stringify({
                    'grant_type': 'client_credentials',
                    'client_id': 'client-credentials-crm',
                    'client_secret': configContainer.client_secret
                })
                fetch(configContainer.keyclock, {
                    'method': 'POST',
                    'headers': {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    'body': data
                }).then((res) => {
                    res.text().then((re) => {
                        keycloakToken = JSON.parse(re).access_token;
                        fetch(configContainer.crmClubUrl + 'list-databases', {
                            'method': 'GET',
                            'headers': {
                                'Authorization': 'Bearer ' + keycloakToken + '',
                                'Content-Type': ' application/json'
                            },
                        }).then((res) => {
                            res.text().then((re) => {
                                dbList = JSON.parse(re);
                                done();
                            });
                        }).catch(() => {
                            done();
                        })
                    });
                }).catch(() => {
                    done();
                })

            },
            function (done) {
                userModel.getUserLoginInfo(req.params.userid).then((results) => {
                    if (results.length > 0) {
                        userData = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.userLicenceStatus(req).then((results) => {
                    if (results.affectedRows > 0) {
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                let dbId = JSON.parse(userData[0].login_info).databaseId;
                let clubId = JSON.parse(userData[0].login_info).clubId
                var dataUpdate;
                if (req.body.action == 'active') {
                    dataUpdate = JSON.stringify({
                        "active": true,
                    })
                } else {
                    dataUpdate = JSON.stringify({
                        "active": false,
                    })
                }

                fetch(configContainer.crmClubUrl + 'update-club?database_id=' + dbId + '&club_id=' + clubId, {
                    'method': 'POST',
                    'headers': {
                        'Authorization': 'Bearer ' + keycloakToken + '',
                        'Content-Type': ' application/json'
                    },
                    'body': dataUpdate

                }).then((res) => {
                    res.text().then((re) => {
                        loginInfo[0] = re;
                        done();
                    });
                }).catch(() => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (req.body.action == 'active') {
                    response = responseFormat.getResponseMessageByCodes(['licence:licenceActivated']);
                    res.status(200).json(response);
                } else {
                    response = responseFormat.getResponseMessageByCodes(['licence:licenceBlocked']);
                    res.status(200).json(response);
                }
            }

        ])
    }

    /**
    * Function to create licence for user
    * @author  MangoIt Solutions
    * @param   {object} with licence details
    * @return  {message} success message
    */
    licenceCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = licenceValidation.licenceValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    licenceModel.getlicenceBylicence(req.body.title).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    licenceModel.createlicence(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceCreated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get all license details
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} licence details
    */
    licences(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        licenceModel.getAlllicence().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get license details by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} licence details
    */
    licenceById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        licenceModel.getlicenceById(req.params.id).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['licence:licenceNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to get license details by licence name
    * @author  MangoIt Solutions
    * @param   {title} 
    * @return  {object} licence details
    */
    licenceByLicence(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        licenceModel.getlicenceBylicence(req.params.title).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['licence:licenceNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to update license details 
    * @author  MangoIt Solutions
    * @param   {id}  with updated licence details
    * @return  {message} success message
    */
    updateLicence(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = licenceValidation.licenceValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    licenceModel.getlicenceById(req.params.id).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    licenceModel.getlicenceBylicence(req.body.title).then((results) => {
                        if (results.length > 0 && results.lid != req.params.id) {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    licenceModel.updatelicence(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceUpdateSuccess']);
                            res.status(200).json(response);

                        } else {
                            response = responseFormat.getResponseMessageByCodes(['licence:licenceNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete license details 
    * @author  MangoIt Solutions
    * @param   {id}  
    * @return  {message} success message
    */
    deleteLicence(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        licenceModel.deletelicence(req.params.id).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['licence:licenceDeleted']);
                res.status(200).json(response);
            }
            response = responseFormat.getResponseMessageByCodes(['licence:licenceNotExists'], { code: 417 });
            res.status(200).json(response);
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}