import express from 'express';
import AdvertisementController from '../../controllers/advertisement/advertisement';
import multerMW from '../../core/multer';

let router = express.Router();
let advertisementController =new AdvertisementController();

router.post('/advertisementCreate', multerMW.single('file'), advertisementController.advertisementCreate.bind(advertisementController));
router.put('/advertisementEdit/:id',multerMW.single('file'), advertisementController.advertisementUpdate.bind(advertisementController));
router.get('/getAllAdvertisement', advertisementController.getAllAdvertisement.bind(advertisementController));
router.get('/getAdvertisementById/:id', advertisementController.getAdvertisementById.bind(advertisementController));
router.delete('/advertisementdelete/:id', advertisementController.advertisementDelete.bind(advertisementController));


export default router;