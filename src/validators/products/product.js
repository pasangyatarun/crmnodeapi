export default class ProductsValidation {

    productValidate(details, allowedExt) {
       
        let err = new Array();

        if (!details.productName) {
            err.push('productName:productName');
        } 
        if (!details.productPrice) {
            err.push('productPrice:productPrice');
        }       
        if (!details.description) {
            err.push('description:descriptionReq');
        } 
        if (!details.publish) {
            err.push('publish:publishReq');
        } 
        return err;
    }
}