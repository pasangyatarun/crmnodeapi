import express   from 'express';
import ProfileEditController from '../../controllers/profile_edit/profile-edit';
import multerMW from '../../core/multer';

let router = express.Router();
let profilEditController = new ProfileEditController();

router.post('/profile-edit/:uid', multerMW.single('file'),profilEditController.editProfile.bind(profilEditController));

export default router;