
import responseFormat from '../../core/response-format';
import FaqModel from '../../models/faq_question/faq_question';
import async from 'async';
let faqModel = new FaqModel();

export default class FaqQuestionController {

    /**
    * Function to create faq question by id
    * @author  MangoIt Solutions
    * @param   {object} with question deatils
    * @return  {message} success message
    */
    createFaqQuestion(req, res, next) {
        let arr = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                for (let i = 0; i < req.body.faqList.length; i++) {
                    let questions = req.body.faqList[i].question;
                    arr.push(questions);
                }
                faqModel.checkFaqQuestion(req.body.category_id, arr).then((result) => {
                    if (result.length) {
                        response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionExists']);
                        res.status(200).json(response);
                    } else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < req.body.faqList.length; i++) {
                    faqModel.CreateFaqQuestion(req, req.body.faqList[i]).then((QuestionCreated) => {
                        if (QuestionCreated.affectedRows > 0) {
                            if (i + 1 == req.body.faqList.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                }
            },
            function () {
                response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionCreated']);
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get all  faq question by category
    * @author  MangoIt Solutions
    * @param   {category_id}
    * @return  {object} Faq question details
    */
    getAllFaqQuestionbyCategory(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqModel.AllFaqsByCategoery(req.params.cat_id).then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get faq question by question id
    * @author  MangoIt Solutions
    * @param   {question_id}
    * @return  {object} Faq question details
    */
    getFaqQuestionbyQuestionId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqModel.FaqQuestionByQId(req.params.id).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update faq question by question id
    * @author  MangoIt Solutions
    * @param   {question_id}
    * @return  {message} success message
    */
    updateFaqQuestion(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                faqModel.checkFaqQuestionExistByQId(req).then((result) => {
                    if (result.length) {
                        response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionExists']);
                        res.status(200).json(response);
                    } else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                faqModel.getUpdateFAQ(req).then((result) => {
                    if (result.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionUpdate']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
        ])

    }

    /**
    * Function to delete faq question by question id
    * @author  MangoIt Solutions
    * @param   {question_id}
    * @return  {message} success message
    */
    deleteFaqQuestion(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqModel.deleteFAQById(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionDelete']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Question:FaqQuestionNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to add faq question feedback
    * @author  MangoIt Solutions
    * @param   {object} with feedback details
    * @return  {message} success message
    */
    createFaqCategoryFeedback(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                faqModel.CreateFaqQuestionFeedback(req).then((results) => {
                    if (results.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['Feedback:FaqFeedbakCreated']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['Feedback:FaqFeedbackFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to count total number of feedback on Question
    * @author  MangoIt Solutions
    * @param   {object} with feedback details
    * @return  {object} Total count by question
    */
    getFaqCategoryFeedbackCount(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqModel.FaqQuestionfeedbackCount(req).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Feedback:FaqFeedbackNotExist'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function  to get question status by userId and question id
    * @author  MangoIt Solutions
    * @param   {object} with feedback details
    * @return  {object} Total count by question
    */
    faqQuestionStatusByUserIdAndQuestionId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqModel.faqQuestionStatusByUserIdAndQuestionId(req).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Feedback:FaqFeedbackNotExist'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}