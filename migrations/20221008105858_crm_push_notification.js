exports.up = async function(knex) {
    return knex.schema.createTable('crm_push_notification', function(t) {
        t.increments('id').primary();
        t.string('survey',[264]).notNull();
        t.integer('author').notNull();
        t.longtext('to_user').notNull();
        t.timestamp('timestamp').notNull().defaultTo(knex.fn.now());
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('crm_push_notification');
};
    