import responseFormat from '../../core/response-format';
import CustomerMembersModel from '../../models/customer_members/customer-members';
import CustomerMembersValidation from '../../validators/customer_members/customer-members';
import async from 'async';
import AWSHandler from '../../core/AWSHander';
import { Json } from 'sequelize/dist/lib/utils';

let customerMembersModel = new CustomerMembersModel();
let membersValidation = new CustomerMembersValidation();

export default class UserController {

    /**
    * Function to store department name for loggedin customer
    * @author  MangoIt Solutions
    * @param   {object} customerId and Department name
    * @return  Succes message
    */
    addCustomerDepartment(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.checkDepartmentExists(req).then((data) => {
                    if (data.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.addDepartment(req).then((resData) => {
                    if (resData.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentAddSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentAddFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
   * Function to store created member details by loggedin customer
   * @author  MangoIt Solutions
   * @param   {object} member deatils 
   * @return  Succes message
   */
    addCustomerMember(req, res, next) {
        let file;
        let s3URLofImage;
        let member_id;
        let tagsNum = [];
        let tagsString = [];
        let insertTag = [];
        let tags1 = []
        let tag_id;
        let response = responseFormat.createResponseTemplate(req.heders.lang);
        let msgCode = membersValidation.memberAddValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        }
        else {
            async.waterfall([
                function (done) {
                    if ((req.body.tags)) {
                        let arr_tag = req.body.tags;
                        JSON.parse(arr_tag).forEach(element => {
                            if (typeof (element) == 'number') {
                                tagsNum.push(element)
                            }
                            else if (typeof (element) == 'string') {
                                tagsString.push(element)
                            }
                        });
                        done();
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    if (tagsString.length > 0) {
                        for (let i = 0; i < tagsString.length; i++) {
                            customerMembersModel.addMemberTag(tagsString[i], req.body.customerId).then((resData) => {
                                if (resData.affectedRows > 0) {
                                    tag_id = resData.insertId;
                                    insertTag.push(tag_id)
                                    if (i + 1 == tagsString.length) {
                                        done();
                                    }
                                }
                                else {
                                    response = responseFormat.getResponseMessageByCodes(['member:memberAddFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    let age;
                    tags1 = tagsNum.concat(insertTag);
                    if (req.body.DOB) {
                        let x = new Date() - new Date(req.body.DOB);
                        age = Math.floor(x / 1000 / 60 / 60 / 24 / 365);
                    }
                    customerMembersModel.addMember(req, tags1, s3URLofImage, age).then((resData) => {
                        if (resData.affectedRows > 0) {
                            member_id = resData.insertId;
                            response = responseFormat.getResponseMessageByCodes(['member:memberAddSuccess']);
                            res.status(200).json(response);
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['member:memberAddFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ]);
        }
    }

    /**
     * Function to filter member details for loggedin customer
     * @author  MangoIt Solutions
     * @param   {object} loggedin customer_id and type
     * @return  {object} member details.
     */
    getCustomerMemberFilter(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let type;
        async.waterfall([
            function (done) {
                if (req.params.type == 'all') {
                    customerMembersModel.getMemberByCustomerId(req.params.customer_id).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);

                        } else {
                            response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done()
                }
            },
            function (done) {
                if (req.params.type == 'company') {
                    customerMembersModel.getMemberFilterByCompanyCustomerId(req.params.customer_id).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                customerMembersModel.getMemberFilterByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for listing customer department by depatment id
    * @author  MangoIt Solutions
    * @param   department id
    * @return  {object} department deatils.
    */
    getCustomerDepartmentById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getDepartmentById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for listing customer department by customer_id
    * @author  MangoIt Solutions
    * @param   customer id
    * @return  {object} department deatils.
    */
    getCustomerDepartment(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getDepartmentByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for listing member details by customer_id
    * @author  MangoIt Solutions
    * @param   customer id
    * @return  {object} member deatils for given customer id.
    */
    MemberByCustomerId(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for customer member graph filter
    * @author  MangoIt Solutions
    * @param   {customer_id} 
    * @return  {object} customer member details
    */
    customerMembersByGraphFilter(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let allData = [];
        let finalData = [];
        let month = {};
        let entriesExit = []
        let jan = {}
        let feb = {}
        let mar = {}
        let apr = {}
        let may = {}
        let jun = {}
        let jul = {}
        let aug = {}
        let sep = {}
        let oct = {}
        let nov = {}
        let dec = {}
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberByCustomerId(req.params.customer_id).then((results) => {
                    if (results) {
                        allData = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                month.jan = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '01').length;
                month.feb = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '02').length;
                month.mar = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '03').length;
                month.apr = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '04').length;
                month.may = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '05').length;
                month.jun = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '06').length;
                month.jul = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '07').length;
                month.aug = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '08').length;
                month.sep = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '09').length;
                month.oct = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '10').length;
                month.nov = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '11').length;
                month.dec = allData.filter(o => JSON.stringify(o.created_at).split('-')[1] == '12').length;
                done();
            },

            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'month': month }, { 'entriesExit': entriesExit }] } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function for filter customer member data
    * @author  MangoIt Solutions
    * @param   {customer_id} 
    * @return  {object} customer member details
    */
    customerMembersByFilter(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let allData = [];
        let filteredData = [];
        let statusA = {}
        let gender = {}
        let age = {}
        let appinuse = {}
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberByCustomerId(req.params.customer_id).then((results) => {
                    if (results) {
                        allData = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                statusA.active = allData.filter(o => o.status == 1);
                statusA.inactive = allData.filter(o => o.status == 0);
                gender.male = allData.filter(o => o.gender == 'male');
                gender.female = allData.filter(o => o.gender == 'female');
                gender.other = allData.filter(o => o.gender == 'other');
                age.below = allData.filter(o => o.age < 18);
                age.above = allData.filter(o => o.age > 60);
                age.between = allData.filter(o => o.age >= 18 && o.age <= 60);
                appinuse.yes = allData.filter(o => o.app_in_use == 'yes');
                appinuse.no = allData.filter(o => o.app_in_use == 'no');
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'status': statusA }, { 'gender': gender }, { 'age': age }, { 'appinuse': appinuse }] } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get all customer member details
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} customer member details
    */
    customerMembers(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getAllMembers().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get all customer member details by id
    * @author  MangoIt Solutions
    * @param   {member_id} 
    * @return  {object} customer member details
    */
    MemberById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberById(req.params.member_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);

                    } else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get  customer member details by email
    * @author  MangoIt Solutions
    * @param   {email} 
    * @return  {object} customer member details
    */
    MemberByEmail(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        customerMembersModel.getMemberByEmail(req.params.email).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to add support access member
    * @author  MangoIt Solutions
    * @param   {object} 
    * @return  {message} success message
    */
    supportAcessMembers(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberAcess(req).then((data) => {
                    if (data.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['member:toogleSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
    * Function to update customer department
    * @author  MangoIt Solutions
    * @param   {id} with department_name
    * @return  {message} success message
    */
    updateCustomerDepartment(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getDepartmentById(req.params.id).then((data) => {
                    if (!data) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.checkDepartmentExists(req).then((data) => {
                    if (data.length > 0 && req.params.id != data[0].id) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.updateDepartment(req).then((data) => {
                    if (data.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentUpdationSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentUpdationFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
    * Function to update customer member details
    * @author  MangoIt Solutions
    * @param   {id} with member details
    * @return  {message} success message
    */
    updateCustomerMember(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let s3URLofImage;
        let file;
        let tagsNum = [];
        let tagsString = [];
        let insertTag = [];
        let tags1 = []
        let tag_id;
        let msgCode = membersValidation.memberAddValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        }
        else {
            async.waterfall([
                function (done) {
                    customerMembersModel.getMemberById(req.params.id).then((data) => {
                        if (!data) {
                            response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if ((req.body.tags)) {
                        let arr_tag = req.body.tags;
                        JSON.parse(arr_tag).forEach(element => {
                            if (typeof (element) == 'number') {
                                tagsNum.push(element)
                            }
                            else if (typeof (element) == 'string') {
                                tagsString.push(element)
                            }
                        });
                        done();
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    if (tagsString.length > 0) {
                        for (let i = 0; i < tagsString.length; i++) {
                            customerMembersModel.addMemberTag(tagsString[i], req.body.customerId).then((resData) => {
                                if (resData.affectedRows > 0) {
                                    tag_id = resData.insertId;
                                    insertTag.push(tag_id)
                                    if (i + 1 == tagsString.length) {
                                        done();
                                    }
                                }
                                else {
                                    response = responseFormat.getResponseMessageByCodes(['member:memberAddFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    let age;
                    tags1 = tagsNum.concat(insertTag);
                    if (req.body.DOB) {
                        let x = new Date() - new Date(req.body.DOB);
                        age = Math.floor(x / 1000 / 60 / 60 / 24 / 365);
                    }
                    customerMembersModel.updateMember(req, s3URLofImage, age, tags1).then((data) => {
                        if (data.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['member:memberUpdationSuccess']);
                            res.status(200).json(response);
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['member:memberUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ]);
        }
    }

    /**
    * Function to delete member by customer_id
    * @author  MangoIt Solutions
    * @param   {customer_id} 
    * @return  {message} success message
    */
    deleteMemberByCustomer(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        customerMembersModel.getDeleteMemberByCustomer(req.params.customer_id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['member:memberDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete customer department
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteCustomerDepartment(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let customerId;
        let DepartmentName;
        let deparmentNameExist = [];
        async.waterfall([
            function (done) {
                customerMembersModel.getDepartmentById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        customerId = results[0].customer_id;
                        DepartmentName = results[0].department_name;
                        done();
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.getDeparmentAssignedtoMemebr(customerId, req).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentassignedtomember'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },

            function (done) {
                // if (deparmentNameExist.length == 0) {
                customerMembersModel.getDeleteDepartment(req.params.id).then((results) => {
                    if (results.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentDeleted']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['department:departmentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete customer member
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteCustomerMember(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        customerMembersModel.getDeleteMember(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['member:memberDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['member:memberNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete multiple customer member
    * @author  MangoIt Solutions
    * @param   {array} 
    * @return  {message} success message
    */
    deleteMultiCustomerMember(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        if (req.body.arr) {
            let arrtemp = [];
            let arrTmp = [];
            async.waterfall([
                function (done) {
                    for (let index = 0; index < req.body.arr.length; index++) {
                        const element = req.body.arr[index];
                        customerMembersModel.getDeleteMember(element).then((results) => {
                            if (results.affectedRows > 0) {
                                arrtemp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }

                            } else {
                                arrTmp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            }

                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['member:memberDeleted']);
                    res.status(200).json(response);
                }
            ])
        }
    }

    /**
    * Function to add customer member search
    * @author  MangoIt Solutions
    * @param   {object} with member search details
    * @return  {message} success message
    */
    addCustomerMemberSearch(req, res) {

        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = membersValidation.memberAddValidation(req.body);

        async.waterfall([
            function (done) {
                customerMembersModel.checkFilterName(req).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['search:memberSearchNameExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.addMemberSearch(req).then((resData) => {
                    if (resData.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['search:memberSearchAddSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['search:memberSearchAddFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
    * Function to get member search record by customer_id
    * @author  MangoIt Solutions
    * @param   {customer_id}
    * @return  {object} member search data
    */
    getCustomerMemberSearchRecordByCutomerId(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getMemberSearchByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);

                    } else {
                        response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete member search record by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {messgae} success message
    */
    deleteCustomerMemberSearch(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        customerMembersModel.getDeleteMemberSearch(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['search:memberSearchDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get customer member search record by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} member search record
    */
    getCustomerMemberSearchById(req, res) {
        let data = [];
        let status = [];
        let type = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getMemeberSearchById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data = results;
                        let search_in = data[0].search_in;
                        for (let sta of JSON.parse(data[0].search_in)) {
                            if (sta) {
                                if (sta == 'Active') {
                                    status.push(1)
                                }
                                else if (sta == 'Inactive') {
                                    status.push(0)
                                }
                                else if (sta == 'Honorary member') {
                                    status.push(2)
                                }
                                else if (sta == 'All Members') {
                                    status.push(3)
                                }
                            }
                        }
                        for (let typ of JSON.parse(data[0].type)) {
                            if (typ) {
                                let val;
                                if (typ == 'Member') {
                                    type.push(0);
                                }
                                else if (typ == 'Company') {
                                    type.push(1);
                                }
                                else if (typ == 'Sponsor') {
                                    type.push(2);
                                }
                            }
                        }
                        done()

                    } else {
                        response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {

                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (JSON.parse(data[0].search_in).length > 0 && JSON.parse(data[0].type).length > 0 && JSON.parse(data[0].department).length > 0) {
                    customerMembersModel.getSearchResultForStatusAndtypeAndDepartment(status, type, data).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else if (JSON.parse(data[0].search_in).length > 0 && JSON.parse(data[0].department).length > 0 && JSON.parse(data[0].type).length == 0) {
                    customerMembersModel.getSearchResultForStatusAndDepartment(status, data).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else if (JSON.parse(data[0].search_in).length > 0 && JSON.parse(data[0].type).length > 0 && JSON.parse(data[0].department).length == 0) {
                    customerMembersModel.getSearchResultForStatusAndtype(status, type, data).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else if (JSON.parse(data[0].search_in).length > 0 && JSON.parse(data[0].type).length == 0 && JSON.parse(data[0].department).length == 0) {
                    customerMembersModel.getSearchResultForSearchIn(status, data).then((results) => {
                        if (results.length > 0) {
                            // data = results;
                            // done()
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else if (JSON.parse(data[0].type).length > 0 && JSON.parse(data[0].search_in).length == 0 && JSON.parse(data[0].department).length == 0) {
                    customerMembersModel.getSearchResultForType(type, data).then((results) => {
                        if (results.length > 0) {
                            // data = results;
                            // done()
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else if (JSON.parse(data[0].type).length == 0 && JSON.parse(data[0].search_in).length == 0 && JSON.parse(data[0].department).length > 0) {
                    let department = JSON.parse(data[0].department);
                    customerMembersModel.getSearchResultForDepartment(department, data).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['search:memberSearchNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                }
            }
        ])
    }

    /**
     * Function to store tag name for loggedin customer
     * @author  MangoIt Solutions
     * @param   {object} customerId and tag name
     * @return  Succes message
     */
    addCustomerTag(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.checkTagExists(req).then((data) => {
                    if (data.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.addTag(req).then((resData) => {
                    if (resData.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagAddSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagAddFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
     * Function to update tag details 
     * @author  MangoIt Solutions
     * @param   {id} customerId and tag name
     * @return  Succes message
     */
    updateCustomerTag(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getTagById(req.params.id).then((data) => {
                    if (data.length > 0) {
                        done();

                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.checkTagExists(req).then((data) => {
                    if (data.length > 0 && req.params.id != data[0].id) {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.updateTag(req).then((data) => {
                    if (data.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagUpdationSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagUpdationFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
    * Function for listing customer tag by customer_id
    * @author  MangoIt Solutions
    * @param   {customer id}
    * @return  {object} Tag deatils.
    */
    getCustomerTag(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getTagByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for listing customer Tag by tag id
    * @author  MangoIt Solutions
    * @param   {tag_id}
    * @return  {object} tag deatils.
    */
    getCustomerTagById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                customerMembersModel.getTagById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete customer Tag by tag id
    * @author  MangoIt Solutions
    * @param   {tag_id}
    * @return  {messgae} success message.
    */
    deleteCustomerTag(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let member_id;
        async.waterfall([
            function (done) {
                customerMembersModel.checkTagUsed(req.params.id).then((results) => {
                    if (results.length > 0) {
                        member_id = results;
                        member_id.forEach(e => {
                            let tags = e.tags
                            const newArr = JSON.parse(tags).filter(e => e != req.params.id)
                            customerMembersModel.updatemembertags(e.id, newArr).then((data) => {
                                if (data.affectedRows > 0) {
                                    console.log("tag list updated in customer-member table for member id==", e.id)
                                }
                                else {
                                    response = responseFormat.getResponseMessageByCodes(['tag:memberUpdationFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        })
                        done();
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                customerMembersModel.getDeleteTag(req.params.id).then((results) => {
                    if (results.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagDeleted']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['tag:tagNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

}

