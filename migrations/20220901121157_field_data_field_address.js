exports.up = async function(knex) {
    return knex.schema.createTable('field_data_field_address', function(t) {
        t.increments('entity_id').comment('The entity id this data is attached to	');
        t.string('field_address_country',[32]).defaultTo('NULL').comment('Two letter ISO country code of this address.');
        t.string('field_address_name_line',[255]).defaultTo('NULL').comment('Contents of a primary NameLine element in the xNL XML.');
        t.string('field_address_first_name',[255]).defaultTo('NULL').comment('Contents of the FirstName element of a primary PersonName element in the xNL XML.');
        t.string('field_address_last_name',[255]).defaultTo('NULL').comment('Contents of the LastName element of a primary PersonName element in the xNL XML.');
        t.string('field_address_organisation_name',[255]).defaultTo('NULL').comment('Contents of a primary OrganisationName element in the xNL XML.');
        t.string('field_address_locality',[255]).defaultTo('NULL').comment('The locality of this address.');
        t.string('field_address_dependent_locality',[255]).defaultTo('NULL').comment('The dependent locality of this address.');
        t.string('field_address_postal_code',[255]).defaultTo('NULL').comment('The postal code of this address.');
        t.string('field_address_thoroughfare',[255]).defaultTo('NULL').comment('The thoroughfare of this address.');
        t.string('field_address_second',[255]).defaultTo('NULL');
        t.string('entity_type',[128]).defaultTo('None').comment('The entity type this data is attached to.').notNull();
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('field_data_field_address');
};