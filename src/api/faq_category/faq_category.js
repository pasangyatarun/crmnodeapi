import express from 'express';
import FaqCategoryController from '../../controllers/faq_category/faq_category';

let router = express.Router();
let faqCategoryController = new FaqCategoryController;

router.post('/faqcreate', faqCategoryController.createFaqCategory.bind(faqCategoryController));
router.get('/faq', faqCategoryController.getAllFaq.bind(faqCategoryController));
router.get('/faq/:id', faqCategoryController.getFaqbyId.bind(faqCategoryController));
router.put('/faqupdate/:id', faqCategoryController.updateFaq.bind(faqCategoryController));
router.delete('/faqCategorydelete/:cat_id', faqCategoryController.deleteFaqCategory.bind(faqCategoryController));

export default router;