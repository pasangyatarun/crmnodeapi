import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class OrderModel {

    async getIncommingOrderCustomers(user_id) {
        const rows = await db.query(`SELECT customer_id FROM billwerk_user_customer WHERE user_id = ?`, [user_id]);
        return rows[0];
    }

    async getIncommingOrder() {
        const rows = await db.query(`SELECT * FROM upcomming_user_order`);
        return rows[0];
    }

    async upcommingOrderCreate(data, userId) {
        const rows = await db.query("INSERT INTO `upcomming_user_order`(`planVariantId`,`holder`,`iban`,`mandate_text`,`chairman_name`,`coupon`,`user_id`)VALUES(?,?,?,?,?,?,?)", [
            data.PlanVariantId,
            data.holder,
            data.iban,
            data.mandateText,
            data.chairManName,
            data.coupon,
            userId
        ]);
        return rows[0];
    }

    async getOrdersCompanyname(orderId) {
        const rows = await db.query(`SELECT data FROM order_billwerk WHERE order_id = ?`, [orderId]);
        return rows[0];
    }

    async orderCreate(data) {
        const rows = await db.query("INSERT INTO `order_billwerk`(`order_id`,`quantity`,`planId`,`planVariantId`,`data`)VALUES(?,?,?,?,?)", [
            data.Id,
            data.Quantity,
            data.PlanId,
            data.PlanVariantId,
            JSON.stringify(data)
        ]);
        return rows[0];
    }

    async commitStore(data, id) {
        const rows = await db.query("UPDATE billwerk_user_customer SET commit_response= ? WHERE order_id = ?", [
            JSON.stringify(data),
            id
        ])
        return rows[0];
    }

    async cancelStore(data, id) {
        const rows = await db.query("UPDATE billwerk_user_customer SET cancel_response= ? WHERE contract_id = ?", [
            JSON.stringify(data),
            id
        ])
        return rows[0];
    }

    async orderUserUpdate(logininfo, oid) {
        const rows = await db.query("UPDATE billwerk_user_customer SET login_info= ? WHERE order_id = ?", [
            logininfo,
            oid
        ])
        return rows[0];
    }

    async checkUserDocument(userId) {
        const rows = await db.query(`SELECT * FROM billwerk_user_customer WHERE user_id = ?`, [userId]);
        return rows[0];
    }

    async deleteExistingUserDocument(userId) {
        const rows = await db.query("UPDATE billwerk_user_customer SET user_personalized_pdf_url= ? WHERE `user_id`= ?", ['', userId]);
        return rows[0];
    }

    async orderUserCreate(data, userId, isClub, pdfURL, loginInfo) {
        const rows = await db.query("INSERT INTO `billwerk_user_customer`(`user_id`,`customer_id`,`contract_id`,`order_id`,`plan_id`,`plan_variant_id`,`plan_name`,`plan_variant_name`,`type`,`user_personalized_pdf_url`,`login_info`)VALUES(?,?,?,?,?,?,?,?,?,?,?)", [
            userId,
            data.Contract.CustomerId,
            data.Contract.Id,
            data.Id,
            data.PlanId,
            data.PlanVariantId,
            data.PlanName,
            data.PlanVariantName,
            isClub,
            pdfURL,
            loginInfo
        ]);
        return rows[0];
    }

    async getCustomerId(id) {
        const rows = await db.query(`SELECT customer_id,order_id,type FROM billwerk_user_customer WHERE user_id = ?`, [id]);
        return rows[0];
    }

    async orderCheck(body, id) {
        const rows = await db.query(`SELECT * FROM billwerk_user_customer WHERE plan_variant_id = ? AND user_id = ?`, [body.cart.planVariantId, id]);
        return rows[0];
    }

    async ProductVersionCheck(user_id, catId) {
        const rows = await db.query(`SELECT * FROM billwerk_user_customer WHERE type = ? AND user_id = ?`, [catId, user_id]);
        return rows[0];
    }

    async getLastOrder(oid) {
        const rows = await db.query(`SELECT * FROM order_billwerk WHERE order_id = ?`, [oid]);
        return rows[0];
    }

    async CompulsaryProductCheck(id) {
        const rows = await db.query(`SELECT * FROM billwerk_user_customer WHERE type IN (1,3,4) AND user_id = ?`, [id]);
        return rows[0];
    }

    async findProduct(id) {
        const rows = await db.query(`SELECT * FROM order_billwerk WHERE order_id = ?`, [id]);
        return rows[0];
    }

    async getUserMandate(id) {
        const rows = await db.query(`SELECT commit_response,customer_id FROM billwerk_user_customer WHERE user_id = ? ORDER BY id DESC LIMIT 1`, [id]);
        return rows[0];
    }

    async deleteOrder(id) {
        const rows = await db.query("DELETE FROM `billwerk_user_customer` WHERE `order_id`= ?", [id]);
        return rows[0];
    }

    async deleteOrderObj(id) {
        const rows = await db.query("DELETE FROM `order_billwerk` WHERE `order_id`= ?", [id]);
        return rows[0];
    }

    async componentProductOrderCheck(id, type) {
        const rows = await db.query(`SELECT * FROM billwerk_user_customer WHERE user_id = ? and type = ? `, [id, type]);
        return rows[0];
    }

    async componentProductOrderUserCreate(data, userId, isComp, req) {
        const rows = await db.query("INSERT INTO `billwerk_user_customer`(`user_id`,`customer_id`,`contract_id`,`order_id`,`plan_name`,`plan_variant_name`,`type`)VALUES(?,?,?,?,?,?,?)", [
            userId,
            data.Contract.CustomerId,
            data.Contract.Id,
            data.Id,
            data.LineItems[0].Description,
            data.LineItems[0].Description,
            isComp,
        ]);
        return rows[0];
    }
    async componentProductOrderCreate(data) {
        const rows = await db.query("INSERT INTO `order_billwerk`(`order_id`,`quantity`,`data`)VALUES(?,?,?)", [
            data.Id,
            data.ComponentSubscriptions[0].Quantity,
            JSON.stringify(data)
        ]);
        return rows[0];
    }
    async commitStoreComponentProduct(data, id) {
        const rows = await db.query("UPDATE billwerk_user_customer SET commit_response= ? WHERE order_id = ?", [
            JSON.stringify(data),
            id
        ])
        return rows[0];
    }
}