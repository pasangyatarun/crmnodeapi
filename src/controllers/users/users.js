import responseFormat from '../../core/response-format';
import bcrypt from 'bcryptjs';
import UserModel from '../../models/user/user.js';
import UsersValidation from '../../validators/users/user';
import async from 'async';
import AWSHandler from '../../core/AWSHander';
import configContainer from '../../config/localhost';
import CommonMethods from '../../core/common-methods';
import sendEmail from '../../core/nodemailer.js';
import EmailTemplateModel from '../../models/email_template/email-template'
import fetch from 'node-fetch';
import qs from 'qs';
import jwt from 'jsonwebtoken';

let userModel = new UserModel();
let userValidation = new UsersValidation();
let commomMethods = new CommonMethods();
let emailTemplateModel = new EmailTemplateModel();

export default class UserController {

    /**
    * Function to delete sales customer 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteSalesCustomer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getDeleteSalesCustomer(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['user:userDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete distributor customer 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteDistributorCustomer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getDeleteDistributorCustomer(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['userList:userDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get distributor customer by pagination
    * @author  MangoIt Solutions
    * @param   {id,pagesize,page}
    * @return  {object} customer details
    */
    customerByDistributorByPaginate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userDetails;
        async.waterfall([
            function (done) {
                userModel.getAllCustomerByIdPaginate(req).then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [userDetails, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': userDetails[0] ? userDetails[0].totalCount : 0 } }] } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])
    }

    /**
    * Function to get distributor customer by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} customer details
    */
    customerByDistributor(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userDetails;
        async.waterfall([
            function (done) {
                userModel.getAllCustomerById(req).then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }

            }
        ])
    }

    /**
    * Function to get sales partner by customer
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} customer details
    */
    salesPartnerByCustomer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getSalesPartnerByCustomer(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [] } });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get customer by sales 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} customer details
    */
    customerBySales(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userDetails;
        async.waterfall([
            function (done) {
                userModel.getAllCustomersBySales(req).then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }

            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }

            }
        ])
    }

    /**
    * Function to assign customer to distributor
    * @author  MangoIt Solutions
    * @param   {id} with distributoe details
    * @return  {message} success message
    */
    assignToDistributor(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let idVariable;
        async.waterfall([
            function (done) {
                for (let index = 0; index < req.body.assignedCustomers.length; index++) {
                    const element = req.body.assignedCustomers[index];
                    userModel.selectCustomerToDistributor(req, element.id).then((resData) => {
                        if (resData.length > 0) {
                            userModel.getDeleteDistributorCustomer(resData[0].id).then((deletedRes) => {
                                userModel.assignCustomerToDistributor(req, element.id).then((results) => {
                                    if (results.affectedRows > 0) {
                                        if (index + 1 == req.body.assignedCustomers.length) {
                                            response = responseFormat.getResponseMessageByCodes(['user:userAssigned']);
                                            res.status(200).json(response);
                                        }
                                    } else {
                                        response = responseFormat.getResponseMessageByCodes(['user:userAssignedFail'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                }).catch((error) => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                })
                            })
                        } else {
                            userModel.assignCustomerToDistributor(req, element.id).then((results) => {
                                if (results.affectedRows > 0) {
                                    if (index + 1 == req.body.assignedCustomers.length) {
                                        response = responseFormat.getResponseMessageByCodes(['user:userAssigned']);
                                        res.status(200).json(response);
                                    }
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['user:userAssignedFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    })
                }
            }
        ])
    }


    /**
    * Function to assign customer to sales
    * @author  MangoIt Solutions
    * @param   {id} with sales details
    * @return  {message} success message
    */
    assignToSales(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        for (let index = 0; index < req.body.assignedCustomers.length; index++) {
            const element = req.body.assignedCustomers[index];
            userModel.selectCustomerToSales(req, element.id).then((resData) => {
                if (resData.length > 0) {
                    userModel.getDeleteSalesCustomer(resData[0].id).then((deletedRes) => {
                        userModel.assignCustomerToSales(req, element.id).then((results) => {
                            if (results.affectedRows > 0) {
                                if (index + 1 == req.body.assignedCustomers.length) {
                                    response = responseFormat.getResponseMessageByCodes(['user:userAssigned']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['user:userAssignedFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    })
                } else {
                    userModel.assignCustomerToSales(req, element.id).then((results) => {
                        if (results.affectedRows > 0) {
                            if (index + 1 == req.body.assignedCustomers.length) {
                                response = responseFormat.getResponseMessageByCodes(['user:userAssigned']);
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userAssignedFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            })
        }
    }

    /**
    * Function to get all assigned customer
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} customer details
    */
    customerList(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let assignedCustomers = [];
        async.waterfall([
            function (done) {
                userModel.getAllAssignedCustomers().then((results) => {
                    assignedCustomers = results
                    done();
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getAllcustomer().then((results) => {
                    if (results) {
                        let unassignedCustomers = results.filter(entry1 => !assignedCustomers.some(entry2 => entry1.uid == entry2.cuid));
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: unassignedCustomers } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get sales partner users
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} sales partner customer details
    */
    salesPartnerUsersByPage(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getSalesPartnerByPage(req).then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to get all distributors
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} distributor details
    */
    distributorUsers(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getAllDistributors().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all sales user
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} sales user details
    */
    salesUsers(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getAllSalesUsers().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to add user
    * @author  MangoIt Solutions
    * @param   {object}with user details
    * @return  {message} success message
    */
    addUser(req, res, next) {
        let inserId;
        let gId;
        let file;
        let s3URLofImage;
        let allvariant;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = userValidation.userAddValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        }
        else {
            let templateData;
            let mailOptions;
            async.waterfall([
                function (done) {
                    userModel.checkEmailExists(req.body.email).then((data) => {
                        if (data) {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:emailExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })

                },
                function (done) {
                    userModel.checkUserNameExists(req.body.userName).then((data) => {
                        if (data.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:userNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    let hashPass;
                    bcrypt.hash('Any!23456', 10, function (err, hash) {
                        hashPass = hash;
                        userModel.register(req, hashPass, s3URLofImage).then((resData) => {
                            if (resData.affectedRows > 0) {
                                inserId = resData.insertId;
                                done();
                            }
                            else {
                                response = responseFormat.getResponseMessageByCodes(['userAdd:registrationFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    })
                },
                function (done) {
                    for (let index = 0; index < JSON.parse(req.body.userRole).length; index++) {
                        const element = JSON.parse(req.body.userRole)[index];
                        userModel.registerUserRole(inserId, element).then((resData) => {
                            if (resData.affectedRows > 0) {
                                if (index + 1 == JSON.parse(req.body.userRole).length) {
                                    done();
                                }
                            }
                            else {
                                response = responseFormat.getResponseMessageByCodes(['userAdd:registeredRoleFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    userModel.getAddressAdd(req, inserId).then((addressUpdated) => {
                        if (addressUpdated.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:userAddressUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    userModel.getAccountAdd(req, inserId).then((accountUpdated) => {
                        if (accountUpdated.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:userAccountUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    emailTemplateModel.getemailTemplateByType('Add User').then((data) => {
                        if (data.length > 0) {
                            templateData = data;
                            done();
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })

                },
                function (done) {
                    let roleArrraySPG = JSON.parse(req.body.userRole);
                    let link = configContainer.addUserVerificationLink;
                    let encrypted;
                    if (roleArrraySPG.includes(16)) {
                        encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime()) + '?spg=spg';
                    }
                    else if (roleArrraySPG.includes(8)) {
                        encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime()) + '?customer=customer';
                    }
                    else {
                        encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime());
                    }
                    // if (templateData && templateData.length > 0) {
                    //     mailOptions = {
                    //         to: req.body.email,
                    //         subject: templateData[0].subject,
                    //         html: `<!doctype html>
                    //         <html lang="en-US">
                            
                    //         <head>
                    //         <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    //         <title>Reset Password Email</title>
                    //         <meta name="description" content="Reset Password Email Template.">
                    //         <style type="text/css">
                    //         a:hover {text-decoration: underline !important;}
                    //         </style>
                    //         </head>
                            
                    //         <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    //         <!--100% body table-->
                    //         <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                    //         style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                    //         <tr>
                    //         <td>
                    //         <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    //         align="center" cellpadding="0" cellspacing="0">
                    //         <tr>
                    //         <td style="height:80px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td style="text-align:center;">
                    //         <a href="#" title="logo" target="_blank">
                    //         <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                    //         </a>
                    //         </td>
                    //         </tr>
                    //         <tr>
                    //         <td style="height:20px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td>
                    //         <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                    //         style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                    //         <tr>
                    //         <td style="height:40px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td style="padding:0 35px;">
                    //         <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->
                            
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>
                                                                   
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                                 `+ templateData[0].template_body + `:<br>
                    //                                                 <a href="`+ encrypted + `">` + encrypted + `</a>
                    //                                                 </p><br><br>
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                    //                                                 <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                    //                                                 <!-- <a href="javascript:void(0);"
                    //                                                 style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                    //                                                     Password</a> -->
                    //                                                     </td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="height:40px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     </table>
                    //                                                     </td>
                    //                                                     <tr>
                    //                                                     <td style="height:20px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="text-align:center;">
                    //                                                     <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                    //                                                     </td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="height:80px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     </table>
                    //                     </td>
                    //                 </tr>
                    //             </table>
                    //             <!--/100% body table-->
                    //             </body>
                                
                    //             </html>`,
                    //     }
                    // } else {
                    //     mailOptions = {
                    //         to: req.body.email,
                    //         subject: 'Bitte bestätigen Sie Ihre Email-Adresse (Please confirm your email address)',
                    //         html: `<h3>Dear association member,</h3>

                    //             <h4>Thank you for your interest in our S-Club |
                    //             Online club management.</h4>
                                
                    //            <h5> Please confirm your email address via the following link: </h5>
                    //          <a href="`+ encrypted + `">` + encrypted + `</a>
                    //          <h3>If the forwarding does not work, please copy the link in
                    //             the address bar of your browser.
                                
                    //             After you have successfully confirmed your e-mail address, the access data will be sent
                    //             sent to the specified email address.</h3>`,
                    //     }
                    // }
                    mailOptions = {
                                to: req.body.email,
                                subject: 'Bitte bestätigen Sie Ihre Email-Adresse',
                                html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                <head>
                                <!--[if (gte mso 9)|(IE)]>
                                  <xml>
                                    <o:OfficeDocumentSettings>
                                    <o:AllowPNG/>
                                    <o:PixelsPerInch>96</o:PixelsPerInch>
                                  </o:OfficeDocumentSettings>
                                </xml>
                                <![endif]-->
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                                <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                                <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                                <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                                <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                                <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                                <meta name="color-scheme" content="only">
                                <title></title>
                                
                                <link rel="preconnect" href="https://fonts.gstatic.com">
                                <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                                <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                                
                                <style type="text/css">
                                /*Basics*/
                                body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                                table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                                td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                                td p {margin:0; padding:0;}
                                td div {margin:0; padding:0;}
                                td a {text-decoration:none; color: inherit;} 
                                /*Outlook*/
                                .ExternalClass {width: 100%;}
                                .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                                .ReadMsgBody {width:100%; background-color: #ffffff;}
                                /* iOS BLUE LINKS */
                                a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                                /*Gmail blue links*/
                                u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                                /*Buttons fix*/
                                .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                                .yshortcuts a {border-bottom:none !important;
                                
                                .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                                /*Responsive*/
                                @media screen and (max-width: 639px) {
                                  table.row {width: 100%!important;max-width: 100%!important;}
                                  td.row {width: 100%!important;max-width: 100%!important;}
                                  .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                                  .center-float {float: none!important;margin:auto!important;}
                                  .center-text{text-align: center!important;}
                                  .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                                  .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                                  .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                                  .hide-mobile {display: none!important;}
                                  .menu-container {text-align: center !important;}
                                  .autoheight {height: auto!important;}
                                  .m-padding-10 {margin: 10px 0!important;}
                                  .m-padding-15 {margin: 15px 0!important;}
                                  .m-padding-20 {margin: 20px 0!important;}
                                  .m-padding-30 {margin: 30px 0!important;}
                                  .m-padding-40 {margin: 40px 0!important;}
                                  .m-padding-50 {margin: 50px 0!important;}
                                  .m-padding-60 {margin: 60px 0!important;}
                                  .m-padding-top10 {margin: 30px 0 0 0!important;}
                                  .m-padding-top15 {margin: 15px 0 0 0!important;}
                                  .m-padding-top20 {margin: 20px 0 0 0!important;}
                                  .m-padding-top30 {margin: 30px 0 0 0!important;}
                                  .m-padding-top40 {margin: 40px 0 0 0!important;}
                                  .m-padding-top50 {margin: 50px 0 0 0!important;}
                                  .m-padding-top60 {margin: 60px 0 0 0!important;}
                                  .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                                  .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                                  .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                                  .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                                  .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                                  .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                                  .center-on-mobile {text-align: center!important;}
                                }
                                </style>
                                </head>
                                
                                <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                                
                                <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                                
                                <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                                  <tr><!-- Outer Table -->
                                    <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- Preheader -->
                                  <tr>
                                    <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <!-- Preheader -->
                                </table>
                                
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- simpli-header-1 -->
                                  <tr>
                                    <td align="center">
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                                  <!-- bg-image -->
                                  <tr>
                                    <td align="center" style="border-radius: 36px;">
                                
                                <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                                <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                                <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                                
                                <div>
                                <!-- simpli-header-bg-image -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                                
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                                  <tr>
                                    <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                                
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                      <tr>
                                        <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                      </tr>
                                    </table>
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- simpli-header-bg-image -->
                                </div>
                                
                                <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                
                                    </td>
                                  </tr>
                                  <!-- bg-image -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                                  <!-- basic-info -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    HERZLICH WILLKOMMEN!
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Bestätigung E-Mailadresse
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    wir freuen uns, dass Sie Interesse an der cloudbasierten S-Verein Vereinsverwaltung haben.
                                                    Bestätigen Sie zunächst über den nachfolgenden Link Ihre E-Mail Adresse. 
                                                    Danach können Sie ein persönliches Passwort festlegen und erhalten einen sicheren Zugang zu unserem Bestellsystem.
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                            <!-- Button -->
                                            <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                              <tr>
                                                <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                            <!--[if (gte mso 9)|(IE)]>
                                              <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                  <td align="center" width="35"></td>
                                                  <td align="center" height="50" style="height:50px;">
                                                  <![endif]-->
                                                    <singleline>
                                                      <a href="`+ encrypted +`" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>BESTÄTIGEN</span></a>
                                                    </singleline>
                                                  <!--[if (gte mso 9)|(IE)]>
                                                  </td>
                                                  <td align="center" width="35"></td>
                                                </tr>
                                              </table>
                                            <![endif]-->
                                                </td>
                                              </tr>
                                            </table>
                                            <!-- Buttons -->
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- basic-info -->
                                </table>
                                
                                    </td>
                                  </tr>
                                  <!-- simpli-header-1 -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1A -->
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Weitere Informationen!
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Wie geht es weiter?
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/mouseclick.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Ausführlich Testen
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Die S-Verein Vereinsverwaltung können Sie mit Echtdaten testen.<br />
                                                            Nach der Testphase werden diese Daten entweder rückstandslos gelöscht oder, sofern Sie die Lizenz bis dahin verbindlich bestellen, bleiben sie erhalten.
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- content-1A -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1B -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/usericon.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Datenimport
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                        Sie können die Daten über eine CSV-Schnittstelle unter Verwendung der Online-Hilfe (<a href="https://hilfe.s-verein.de/help/205">https://hilfe.s-verein.de/help/205</a>) selbst importieren oder durch uns als Dienstleistung kostenpflichtig importieren lassen. Wenn wir die Daten importieren sollen, erwarten wir, dass Sie den Einrichtungsassistent abgeschlossen haben. Das beinhaltet die Anlage von Abteilungen und Beiträgen. Hängen Sie in der Bestellung bitte den Export im CSV/TXT Format an.
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- content-1B -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1C -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/fragezeichen.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Noch Fragen?
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;padding-bottom: 25px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Wenn Sie Hilfestellung benötigen, unterstützen wir Sie gerne mit Tutorials zu den einzelnen Funktionen oder gerne auch mit unserem E-Mail Support unter support@s-verein.de
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                              <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                                <tr>
                                                  <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                              <!--[if (gte mso 9)|(IE)]>
                                                <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                  <tr>
                                                    <td align="center" width="35"></td>
                                                    <td align="center" height="50" style="height:50px;">
                                                    <![endif]-->
                                                      <singleline>
                                                        <a href="https://hilfe.s-verein.de/" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Hilfe-Seite</span></a>
                                                      </singleline>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    <td align="center" width="35"></td>
                                                  </tr>
                                                </table>
                                              <![endif]-->
                                                  </td>
                                                  <td>
                                                    <pre>           </pre>
                                                  </td>
                                                  <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                        <tr>
                                                          <td align="center" width="35"></td>
                                                          <td align="center" height="50" style="height:50px;">
                                                          <![endif]-->
                                                            <singleline>
                                                              <a href="https://www.youtube.com/@supports-verein6588" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>TUTORIALS</span></a>
                                                            </singleline>
                                                          <!--[if (gte mso 9)|(IE)]>
                                                          </td>
                                                          <td align="center" width="35"></td>
                                                        </tr>
                                                      </table>
                                                    <![endif]-->
                                                        </td>
                                                </tr>
                                              </table>
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <!-- content-1C -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                  <!-- simpli-footer -->
                                  <tr>
                                    <td align="center">
                                      
                                <!-- Content -->
                                
                                  <tr>
                                    <td align="center">
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                            <multiline>
                                              <div mc:edit Simpli>
                                                Tineon Aktiengesellschaft <br />
                                                Uferpromenade 5, 88709 Meersburg <br />
                                                Registergericht Freiburg HRB 710927 <br />
                                                Vorstand: Jean-Claude Parent <br />
                                                Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                                USt.-IdNr. DE203912818 <br />
                                              </div>
                                            </multiline>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                  <!-- simpli-footer -->
                                </table>
                                
                                    </td>
                                  </tr><!-- Outer-Table -->
                                </table>
                                
                                </body>
                                </html>
                                `,
                            }

                    sendEmail(mailOptions).then((sentdata) => {
                        if (sentdata == 'error') {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:registeredSuccessMailNotSent']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:registeredSuccess']);
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
            ]);
        }
    }

    /**
    * Function to search user details 
    * @author  MangoIt Solutions
    * @param   {object}search filter data
    * @return  {object} user details
    */
    usersBySearch(req, res, next) {
        let userDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUsersbySearch(req.params).then((results) => {
                    if (results) {
                        userDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'userDetails': userDetails }, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': userDetails[0] ? userDetails[0].totalCount : 0 } }] } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])

    }

    /**
    * Function to get users by page size 
    * @author  MangoIt Solutions
    * @param   {page,pagesize}
    * @return  {object} user details
    */
    usersByPageSize(req, res, next) {
        let userDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUsersbyPageSize(req.params).then((results) => {
                    if (results) {
                        userDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'userDetails': userDetails }, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': userDetails[0] ? userDetails[0].totalCount : 0 } }] } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])

    }

    /**
    * Function to get users
    * @author  MangoIt Solutions
    * @param   {uid}
    * @return  {object} user details
    */
    users(req, res, next) {
        let userDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getAllUsers().then((results) => {
                    if (results) {
                        userDetails = results;
                        //done();
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            // function (done) {
            //     for (let i = 0; i < userDetails.length; i++) {
            //         userModel.getUserRolesData(userDetails[i].uid).then((results) => {
            //             if (results.length > 0) {
            //                 userDetails[i]['role'] = results;
            //                 if (i + 1 == userDetails.length) {
            //                    // done();
            //                    console.log("userDetails===",userDetails)
            //                    response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
            //                    res.status(200).json(response);
            //                 }
            //             } else {
            //                 response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
            //                 res.status(200).json(response);
            //             }
            //         }).catch((error) => {
            //             response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            //             res.status(500).json(response);
            //             return;
            //         })
            //     }
            // },
            // function (done) {
            //     for (let i = 0; i < userDetails.length; i++) {
            //         userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
            //             if (results.length > 0) {
            //                 userDetails[i]['domain'] = results;
            //                 if (i + 1 == userDetails.length) {
            //                     console.log("userDetails===",userDetails)
            //                     response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
            //                     res.status(200).json(response);
            //                 }
            //             } else {
            //                 response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
            //                 res.status(200).json(response);
            //             }
            //         }).catch((error) => {
            //             response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            //             res.status(500).json(response);
            //             return;
            //         })
            //     }
            // }
        ])
    }

    /**
    * Function to get user login info
    * @author  MangoIt Solutions
    * @param   {uid}
    * @return  {object} user details
    */
    userLoginInfo(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userData = [];
        async.waterfall([
            function (done) {
                userModel.getUserLoginInfo(req.params.user_id).then((results) => {
                    if (results.length > 0) {
                        let dbId = JSON.parse(results[0].login_info).databaseId;
                        let clubId = JSON.parse(results[0].login_info).clubId;
                        var data = qs.stringify({
                            'grant_type': 'client_credentials',
                            'client_id': 'client-credentials-crm',
                            'client_secret': configContainer.client_secret
                        })
                        fetch(configContainer.keyclock, {
                            'method': 'POST',
                            'headers': {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            'body': data
                        }).then((res) => {
                            res.text().then((re) => {
                                let keycloakToken = JSON.parse(re).access_token;
                                fetch(configContainer.crmClubUrl + 'login-info?database_id=' + dbId + '&club_id=' + clubId + '', {
                                    'method': 'GET',
                                    'headers': {
                                        'Authorization': 'Bearer ' + keycloakToken + '',
                                        'Cookie': 'authenticated=false; liveChatShared=null; publicShared=null'
                                    }
                                }).then((res) => {
                                    res.text().then((re) => {
                                        userData[0] = JSON.parse(re);
                                        done();
                                    })
                                }).catch((err) => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                })
                            })
                        }).catch((err) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                        })
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userData } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get user details by customer id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} user details
    */
    userByCustomerId(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userId;
        let userData;
        let userAcc;
        let userImg;
        let userAdd;
        let userRoleDetails;
        let userDomainDetails;
        async.waterfall([
            function (done) {
                userModel.getUserByCustomerId(req.params.id).then((user) => {
                    if (user.length > 0) {
                        userId = user[0].user_id;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getUserRolesData(userId).then((results) => {
                    if (results.length > 0) {
                        userRoleDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getUserDomainsData(userId).then((results) => {
                    if (results.length > 0) {
                        userDomainDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getUserById(userId).then((results) => {
                    if (results.length > 0) {
                        userData = results;
                        userData[0]['role'] = userRoleDetails;
                        userData[0]['domain'] = userDomainDetails;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }

        ])
    }

    /**
    * Function to get user details by  id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} user details
    */
    userById(req, res) {
        let userData;
        let userAcc;
        let userImg;
        let userAdd;
        let userRoleDetails;
        let userDomainDetails;
        let OldUserStatus;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUserRolesData(req.params.id).then((results) => {
                    if (results.length > 0) {
                        userRoleDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getUserDomainsData(req.params.id).then((results) => {
                    if (results.length > 0) {
                        userDomainDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getOldUserStatus(req.params.id).then((results) => {
                    if (results.length > 0) {
                        OldUserStatus = 1;
                        done();
                    } else {
                        OldUserStatus = 0;
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getUserById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        userData = results;
                        userData[0]['role'] = userRoleDetails;
                        userData[0]['domain'] = userDomainDetails;
                        userData[0]['OldUserStatus'] = OldUserStatus;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get user details by email
    * @author  MangoIt Solutions
    * @param   {email}
    * @return  {object} user details
    */
    userByEmail(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getUserByEmail(req.params.email).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update user details by id
    * @author  MangoIt Solutions
    * @param   {id} with updated user details
    * @return  {message} success message
    */
    updateUser(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        let msgCode = userValidation.userAddValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        }
        else {
            async.waterfall([
                function (done) {
                    userModel.getUserById(req.params.id).then((data) => {
                        if (!data) {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })

                },
                function (done) {
                    userModel.checkEmailExists(req.body.email).then((data) => {
                        if (data) {
                            response = responseFormat.getResponseMessageByCodes(['user:emailExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })

                },
                function (done) {
                    userModel.checkUserNameExists(req.body.userName).then((data) => {
                        if (data.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['user:userNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    let hashPass;
                    bcrypt.hash(req.body.password, 10, function (err, hash) {
                        hashPass = hash;
                        userModel.updateUser(req, hashPass).then((data) => {
                            if (data.affectedRows > 0) {
                                response = responseFormat.getResponseMessageByCodes(['user:userUpdationSuccess']);
                                res.status(200).json(response);
                            }
                            else {
                                response = responseFormat.getResponseMessageByCodes(['user:userUpdationFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    });

                }
            ]);
        }
    }

    /**
    * Function to delete user details by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteUser(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUserOrders(req.params.id).then((data) => {
                    if (data.length == 0) {
                        userModel.getUserDelete(req.params.id).then((results) => {
                            if (results.affectedRows > 0) {
                                response = responseFormat.getResponseMessageByCodes(['userList:userDeleted']);
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                userModel.getDeleteUser(req.params.id).then((results) => {
                    if (results.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['userList:userSoftDeleted']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete multiple user details by id
    * @author  MangoIt Solutions
    * @param   {ids} 
    * @return  {message} success message
    */
    deleteMultiUser(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        if (req.body.arr) {
            let arrtemp = [];
            let arrTmp = [];
            async.waterfall([
                function (done) {
                    for (let index = 0; index < req.body.arr.length; index++) {
                        const element = req.body.arr[index];
                        userModel.getDeleteUser(element).then((results) => {
                            if (results.affectedRows > 0) {
                                arrtemp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            } else {
                                arrTmp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            }

                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['userList:userDeleted']);
                    res.status(200).json(response);
                }
            ])
        }
    }

    /**
    * Function to check user is loggin 
    * @author  MangoIt Solutions
    * @param   {userId} 
    * @return  {values} value
    */
    isUserLoggedIn(userId) {
        let response = responseFormat.createResponseTemplate();
        return userModel.getUserById(userId).then((results) => {
            if (results) {
                return 1;
            } else {
                return -1;
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete cron
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteCron(req, res) {
        let userOrderDetails;
        let userArr = [];
        let uniqueAges;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getbillworkUser().then((results) => {
                    if (results.length > 0) {
                        userOrderDetails = results;

                        for (let i = 0; i < userOrderDetails.length; i++) {
                            let cudate = new Date()
                            let cuday = cudate.getDate().toString().padStart(2, "0");
                            let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
                            let cuyear = cudate.getFullYear();
                            var date = cuyear + "-" + cumonth + "-" + cuday;
                            let cdate = new Date(JSON.parse(userOrderDetails[i].commit_response).EndDate)
                            let cday = cdate.getDate().toString().padStart(2, "0");
                            let cmonth = (cdate.getMonth() + 1).toString().padStart(2, "0");
                            let cyear = cdate.getFullYear();
                            var enddate = cyear + "-" + cmonth + "-" + cday
                            if (JSON.parse(userOrderDetails[i].commit_response).LifecycleStatus == "InTrial" && JSON.parse(userOrderDetails[i].commit_response).EndDate && new Date(date) > new Date(enddate)) {
                                userArr.push(JSON.parse(userOrderDetails[i].user_id))
                            }
                            if (i + 1 == userOrderDetails.length) {
                                const unique = (value, index, self) => {
                                    return self.indexOf(value) === index
                                }
                                uniqueAges = userArr.filter(unique)
                                done();
                            }
                        }
                    } else {

                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    return;
                })

            },
            function (done) {
                uniqueAges.forEach(element => {
                    userModel.getUserOrders(element).then((results) => {
                        if (results.length == 1) {
                            userModel.getDeleteUser(element).then(() => {

                            })
                        }

                    })
                });
            }
        ])
    }

    /**
    * Function to get user details
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} user data
    */
    getUserDetils(req, res) {
        let userDetails;
        let keycloakToken;
        let dbList
        let userArr = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUser().then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        for (let i = 0; i < userDetails.length; i++) {
                            if (JSON.parse(userDetails[i].commit_response).EndDate) {
                                let cudate = new Date()
                                let cuday = cudate.getDate().toString().padStart(2, "0");
                                let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
                                let cuyear = cudate.getFullYear();
                                var date = cuyear + "-" + cumonth + "-" + cuday;
                                let cdate = new Date(JSON.parse(userDetails[i].commit_response).EndDate)
                                let cday = cdate.getDate().toString().padStart(2, "0");
                                let cmonth = (cdate.getMonth() + 1).toString().padStart(2, "0");
                                let cyear = cdate.getFullYear();
                                var enddate = cyear + "-" + cmonth + "-" + cday
                                if (new Date(date) > new Date(enddate)) {
                                    userArr.push(JSON.parse(userDetails[i].login_info));
                                }
                                else {
                                }
                            }
                            if (userDetails.length == i + 1) {
                                done();
                            }
                        }
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    return;
                })
            },
            function (done) {
                var data = qs.stringify({
                    'grant_type': 'client_credentials',
                    'client_id': 'client-credentials-crm',
                    'client_secret': '65055745-34e0-420b-a31f-4519714f20d8'
                })
                fetch(configContainer.keyclock, {
                    'method': 'POST',
                    'headers': {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    'body': data
                }).then((res) => {
                    res.text().then((re) => {
                        keycloakToken = JSON.parse(re).access_token;
                        done()

                    });
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    return;
                })
            },
            function (done) {
                var dataUpdate = JSON.stringify({
                    "active": false,
                })
                for (let i = 0; i < userArr.length; i++) {
                    fetch(configContainer.crmClubUrl + 'update-club?database_id=' + userArr[i].databaseId + '&club_id=' + userArr[i].clubId, {
                        'method': 'POST',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                        'body': dataUpdate
                    }).then((res) => {
                        res.text().then((re) => {
                        });
                    }).catch(() => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])
    }

    /**
    * Function to update iban by user
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    updateIbanByUser(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let checkIban;
        async.waterfall([
            function (done) {
                userModel.getUserById(req.params.id).then((data) => {
                    checkIban = data;
                    if (!data) {
                        done();
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                if (checkIban.length == 0) {
                    userModel.getAccountAdd(req, req.params.user_id).then((accountUpdated) => {
                        if (accountUpdated.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:userAccountUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    userModel.AccountUpdate(req).then((accountUpdated) => {
                        if (accountUpdated && accountUpdated.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['userAdd:userAccountUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
        ]);
    }

    /**
    * Function to get club customer by pagination
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} customer details
    */
    clubsCustomerBypagination(req, res) {
        let authUserId;
        let x;
        let userDataId = [];
        let clubUserId = [];
        let dataCount = 0;
        let userDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        done();
                    }
                })
            },
            function (done) {
                userModel.getUserRoleById(authUserId).then((data) => {

                    if (!data) {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        //Sales Partner 
                        x = data.filter(o => o.rid == 6)
                        if (x.length > 0) {
                            done();
                        }
                        else {
                            done()
                        }

                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                if (x.length > 0) {
                    userModel.getAssignedCustomersByPageId(authUserId, req.params).then((data) => {
                        if (data.length > 0) {
                            dataCount = data[0].totalCount;
                            for (let i = 0; i < data.length; i++) {
                                userDataId.push(data[i].cuid);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (x.length > 0) {
                    userModel.getClubCustomersById(userDataId).then((data) => {
                        if (data.length > 0) {
                            for (let i = 0; i < data.length; i++) {
                                clubUserId.push(data[i].user_id);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user :' + 'User do not have club product'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    userModel.getAllClubCustomersForAdminPaginate(req.params).then((data) => {
                        if (data.length > 0) {
                            dataCount = data[0].totalCount
                            for (let i = 0; i < data.length; i++) {
                                clubUserId.push(data[i].user_id);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user :' + 'User do not have club product'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                userModel.getClubUserDetails(clubUserId, req).then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [userDetails, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': dataCount } }] } });
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }

                })
                    .catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [clubUserId, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': dataCount } }] } });
                res.status(200).json(response);

            }
        ]);

    }

    /**
    * Function to get club customer 
    * @author  MangoIt Solutions
    * @param   {userDetails} 
    * @return  {object} user details
    */
    clubsCustomer(req, res) {
        let authUserId;
        let x;
        let userDataId = [];
        let clubUserId = [];
        let dataId;
        let userDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        done();
                    }
                })
            },
            function (done) {
                userModel.getUserRoleById(authUserId).then((data) => {

                    if (!data) {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        //Sales Partner 
                        x = data.filter(o => o.rid == 6)
                        if (x.length > 0) {
                            done();
                        }
                        else {
                            done()
                        }

                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                if (x.length > 0) {
                    userModel.getAssignedCustomersById(authUserId).then((data) => {
                        if (data.length > 0) {
                            for (let i = 0; i < data.length; i++) {
                                userDataId.push(data[i].cuid);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (x.length > 0) {
                    userModel.getClubCustomersById(userDataId).then((data) => {
                        if (data.length > 0) {
                            for (let i = 0; i < data.length; i++) {
                                clubUserId.push(data[i].user_id);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user :' + 'User do not have club product'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    userModel.getAllClubCustomersForAdmin().then((data) => {
                        if (data.length > 0) {
                            for (let i = 0; i < data.length; i++) {
                                clubUserId.push(data[i].user_id);
                                if (data.length == i + 1) {
                                    done();
                                }
                            }
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['user :' + 'User do not have club product'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                userModel.getClubUserDetails(clubUserId, req).then((results) => {
                    if (results.length > 0) {
                        userDetails = results;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }

                })
                    .catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: clubUserId } });
                res.status(200).json(response);

            }
        ]);

    }

    /**
    * Function to get user details by Author
    * @author  MangoIt Solutions
    * @param   {userDetails} 
    * @return  {object} user details
    */
    userByAuthor(req, res) {
        let authUserId;
        let authRoleId;
        let userDetails;
        let authorRole;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;

                        done();
                    }
                })
            },
            function (done) {
                userModel.getAuthorIdRole(authUserId).then((results) => {
                    if (results) {
                        authorRole = results;
                        authRoleId = authorRole.filter(o => o.rid == 6);
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (authRoleId.length > 0) {
                    userModel.getSalesCustomersById(authUserId).then((results) => {
                        if (results.length > 0) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    userModel.getUserByAuthorId(authUserId).then((results) => {
                        if (results.length > 0) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                if (authRoleId.length > 0) {
                    for (let i = 0; i < userDetails.length; i++) {
                        userModel.getCustomerSalesPartnerData(userDetails[i].cuid).then((results) => {
                            userDetails[i]['salesUserData'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }
                else {
                    for (let i = 0; i < userDetails.length; i++) {
                        userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                            if (results.length > 0) {
                                userDetails[i]['role'] = results;
                                if (i + 1 == userDetails.length) {
                                    done();
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                res.status(200).json(response);
            }

        ])
    }

    /**
    * Function to set commission on products for sales partner
    * @author  MangoIt Solutions
    * @param   {object}with commision details 
    * @return  {message} success message
    */
    setCommission(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = userValidation.commissionSettingValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        }
        else {
            async.waterfall([
                function (done) {
                    userModel.checkCommissionSeted(req.body).then((data) => {
                        if (data && data.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['user:commExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    userModel.addCommissionSetting(req).then((results) => {
                        if (results) {
                            response = responseFormat.getResponseMessageByCodes(['user:commissionSet']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:commissionSetFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to set up commission on products for sales partner
    * @author  MangoIt Solutions
    * @param   {spid}
    * @return  {message} success message
    */
    setupCommissions(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.deleteCommissionSeted(req.params.spid).then((data) => {
                    done();
                })
            },
            function (done) {
                for (let index = 0; index < req.body.length; index++) {
                    const element = req.body[index];
                    userModel.checkCommissionSeted(element, req.params.spid).then((data) => {
                        if (data && data.length > 0) {
                            userModel.updateCommissionSetting(element, req).then((results) => {
                                if (results) {
                                    if (req.body.length == index + 1) {

                                        response = responseFormat.getResponseMessageByCodes(['user:commissionSet']);
                                        res.status(200).json(response);
                                    }
                                } else {
                                    if (req.body.length == index + 1) {

                                        response = responseFormat.getResponseMessageByCodes(['user:commissionSetFail'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                }
                            }).catch((error) => {
                                if (req.body.length == index + 1) {

                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                }
                            })
                        }
                        else {
                            userModel.addCommissionSetting(element, req).then((results) => {
                                if (results) {
                                    if (req.body.length == index + 1) {
                                        response = responseFormat.getResponseMessageByCodes(['user:commissionSet']);
                                        res.status(200).json(response);
                                    }
                                } else {
                                    if (req.body.length == index + 1) {

                                        response = responseFormat.getResponseMessageByCodes(['user:commissionSetFail'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                }
                            }).catch((error) => {
                                if (req.body.length == index + 1) {

                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                }
                            })
                        }
                    }).catch((error) => {
                        if (req.body.length == index + 1) {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }

                    })
                }

            }
        ])
    }

    /**
    * Function to delete commission setting of products for sales partner
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteSetting(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.deleteCommissionSeted(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['user:comSetDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:comSetNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }


    /**
    * Function to get commission setting of products for sales partner id
    * @author  MangoIt Solutions
    * @param   {spid}
    * @return  {object} sales partner commission data
    */
    getCommSettingBySpId(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        userModel.getCommissionSettingSpId(req.params.id).then((results) => {
            if (results && results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:comSetNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get commission setting of products by date interval
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} filtered commission data
    */
    getCommissionByDateInterval(req, res) {
        var assignedCustomerList = [];
        var assignedCustomerIds = [];
        let ordersCount = [];
        var commData = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getCommissionSettingSpId(req.params.id).then((results) => {
                    if (results && results.length > 0) {
                        commData['setting'] = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:comSetNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getAllCustomerById(req).then((results) => {
                    if (results && results.length > 0) {
                        assignedCustomerList = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (assignedCustomerList.length > 0) {
                    for (let i = 0; i < assignedCustomerList.length; i++) {
                        assignedCustomerIds.push(assignedCustomerList[i].uid)
                        if (assignedCustomerList.length == i + 1) {
                            done();
                        }
                    }
                } else {
                    response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                    res.status(200).json(response);
                }
            },
            function (done) {
                userModel.getOrderedCustomers(assignedCustomerIds, commData['setting'][0].start_date).then((results) => {
                    if (results && results.length > 0) {
                        let st_date = JSON.stringify(commData['setting'][0].start_date).split('T')[0].replace('"', '').trim();
                        let originalStartDate = st_date;
                        ordersCount = results.filter(o => (!JSON.parse(o.commit_response).EndDate))
                        let orderConsidered = [];
                        let date_from = req.body.date_from
                        let date_to = req.body.date_to
                        const x = new Date(date_to);
                        const y = new Date(originalStartDate);
                        if (new Date(originalStartDate) < new Date(date_to)) {
                            ordersCount.forEach(element => {
                                let orderstart = JSON.parse(element.commit_response).StartDate.split('T')[0]
                                if (((JSON.parse(element.cancel_response) == null) || (new Date(originalStartDate) < new Date(JSON.parse(element.cancel_response).EndDate.split('T')[0]))) && new Date(orderstart) < new Date(date_to)) {
                                    var date1 = new Date(orderstart);
                                    var date2 = new Date(date_to);
                                    var date3 = new Date(originalStartDate);

                                    let dateDiffInMonths = (date2.getDate() - date1.getDate()) / 30 +
                                        date2.getMonth() - date1.getMonth() +
                                        (12 * (date2.getFullYear() - date1.getFullYear()));

                                    let diffDateToOSD = ((date2.getDate() - date3.getDate()) / 30 +
                                        date2.getMonth() - date3.getMonth() +
                                        (12 * (date2.getFullYear() - date3.getFullYear())));

                                    if (dateDiffInMonths > diffDateToOSD) {
                                        dateDiffInMonths = diffDateToOSD
                                    }

                                    if (JSON.parse(element.cancel_response) && new Date(JSON.parse(element.cancel_response).EndDate.split('T')[0]) <= new Date(date_to)) {
                                        let CE = new Date(JSON.parse(element.cancel_response).EndDate.split('T')[0]);
                                        let diffOfCETOS = ((CE.getDate() - date1.getDate()) / 30 +
                                            CE.getMonth() - date1.getMonth() +
                                            (12 * (CE.getFullYear() - date1.getFullYear())));

                                        dateDiffInMonths = diffOfCETOS;

                                        let diffOfCETSD = ((CE.getDate() - date3.getDate()) / 30 +
                                            CE.getMonth() - date3.getMonth() +
                                            (12 * (CE.getFullYear() - date3.getFullYear())));

                                        if (dateDiffInMonths > diffOfCETSD) {
                                            dateDiffInMonths = diffOfCETSD;
                                        }
                                    }

                                    element['monthCount'] = Math.floor(dateDiffInMonths);
                                    if (commData['setting'][0].commission_duration_type == 'Yearly') {

                                    } else if (commData['setting'][0].commission_duration_type == 'Monthly') {

                                    } else if (commData['setting'][0].commission_duration_type == 'Quaterly') {

                                    }
                                    orderConsidered.push(element)
                                }
                            })
                        }

                        commData['setting'].forEach(element => {
                            let x = orderConsidered.filter(o => o.plan_variant_id == element.product_id)
                            if (x.length > 0) {
                                element['calData'] = x
                                element['count'] = x.length
                                element['plan_variant_name'] = x[0]['plan_variant_name']
                            }
                        });
                        let filteredSettingCount = commData['setting'].filter(o => o.count)
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: filteredSettingCount } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:orderNotExist'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get commission 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} commission data
    */
    getCommission(req, res) {
        var assignedCustomerList = [];
        var assignedCustomerIds = [];
        let ordersCount = [];
        var commData = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getCommissionSettingSpId(req.params.id).then((results) => {
                    if (results && results.length > 0) {
                        commData['setting'] = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:comSetNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                userModel.getAllCustomerById(req).then((results) => {
                    if (results && results.length > 0) {
                        assignedCustomerList = results
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (assignedCustomerList.length > 0) {
                    for (let i = 0; i < assignedCustomerList.length; i++) {
                        assignedCustomerIds.push(assignedCustomerList[i].uid)
                        if (assignedCustomerList.length == i + 1) {
                            done();
                        }
                    }
                } else {
                    response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                    res.status(200).json(response);
                }
            },
            function (done) {
                userModel.getOrderedCustomers(assignedCustomerIds, commData['setting'][0].start_date).then((results) => {
                    if (results && results.length > 0) {
                        let st_date = JSON.stringify(commData['setting'][0].start_date).split('T')[0].replace('"', '').trim();
                        st_date = new Date(new Date(st_date).setFullYear(new Date().getFullYear())).toISOString().split('T')[0];
                        let end_date = new Date(new Date(st_date).setFullYear(new Date().getFullYear() + 1)).toISOString().split('T')[0];
                        ordersCount = results.filter(o => (!JSON.parse(o.commit_response).EndDate))
                        let orderConsidered = [];
                        ordersCount.forEach(element => {
                            if ((JSON.parse(element.cancel_response) == null) || (new Date(st_date) < new Date(JSON.parse(element.cancel_response).EndDate.split('T')[0]) < new Date(end_date))) {
                                orderConsidered.push(element)
                            }
                        });
                        commData['setting'].forEach(element => {
                            let x = orderConsidered.filter(o => o.plan_variant_id == element.product_id)
                            if (x.length > 0) {
                                element['calData'] = x
                                element['count'] = x.length
                                element['plan_variant_name'] = x[0]['plan_variant_name']
                            }
                        });
                        let filteredSettingCount = commData['setting'].filter(o => o.count)
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: filteredSettingCount } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['user:orderNotExist'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

}

