import jwt from 'jsonwebtoken';
import async from 'async';
import RoleModel from '../../models/role/role.js';
import RoleValidation from '../../validators/roles/role.js';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
let roleModel = new RoleModel();
let roleValidation = new RoleValidation();

export default class RoleController {

    /**
     * Function to create roles
     * @author  MangoIt Solutions
     * @param   {object} with role details
     * @return  {message} success message
     */
    roleCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = roleValidation.roleValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    roleModel.getRoleByRole(req.body.role).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['role:roleNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    roleModel.createRole(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['role:roleCreated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['role:roleCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get all roles
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} role details
    */
    roles(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        roleModel.getAllRole().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get  role details by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} role details
    */
    roleById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        roleModel.getRoleById(req.params.id).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to get role details by role
    * @author  MangoIt Solutions
    * @param   {role}
    * @return  {object} role details
    */
    roleByRole(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        roleModel.getRoleByRole(req.params.role).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update role details by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    updateRole(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        let msgCode = roleValidation.roleValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    roleModel.getRoleById(req.params.id).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    roleModel.getRoleByRole(req.body.role).then((results) => {
                        if (results.length > 0 && results[0].rid != req.params.id) {
                            response = responseFormat.getResponseMessageByCodes(['role:roleNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    roleModel.updateRole(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['role:roleUpdateSuccess']);
                            res.status(200).json(response);

                        } else {
                            response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }


    /**
    * Function to delete role details by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteRole(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        roleModel.deleteRole(req.params.id).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['role:roleDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                res.status(200).json(response);
            }

        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all role category
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} role category details
    */
    roleCategory(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        roleModel.getAllRoleCategory().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get role by its weight
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} role category details
    */
    rolesByroleWeight(req, res, next) {
        let authUserId;
        let dataArr = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        roleModel.roleCheck(authUserId).then((found) => {
                            dataArr[0] = found;
                            if (found.length > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['role:roleNotExists'], { code: 417 });
                                res.status(200).json(response);
                            }
                        })
                    }
                })
            },
            function (done) {
                roleModel.getRoleWeight(dataArr[0]).then((results) => {
                    if (results) {
                        if (dataArr[0][0].name == 'Support admin') {
                            results = results.filter(o => o.name != 'Sales-Administrator');
                        }
                        else if (dataArr[0][0].name == 'Sales-Administrator') {
                            results = results.filter(o => o.name != 'Support admin')
                        }
                        else {
                            results = results
                        }
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

}