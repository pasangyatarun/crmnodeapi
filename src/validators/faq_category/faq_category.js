export default class CrmFaqCategoryValidation {
    crmFaqCategoryCreate(category){
        let err = new Array();
        if(!category.category){
            err.push('category_name: Category Name Required');
        }        
        return err;
    }
}