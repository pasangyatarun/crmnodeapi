import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class UsersRolesModel {

    async getAllUsersRoles() {
        const rows = await db.query("SELECT * FROM users_roles");
        return rows[0];
    }

    async getUsersByRole(rid) {
        const rows = await db.query("SELECT * FROM users_roles WHERE rid= ?", [rid]);
        return rows[0];
    }

    async getRoleByUser(uid) {
        const rows = await db.query("SELECT * FROM users_roles WHERE uid = ?", [uid]);
        return rows[0];
    }

    async getUpdateUserRoles(req) {
        const rows = await db.query("UPDATE users_roles SET rid = ? WHERE uid = ?", [
            req.body.userRole,
            req.params.uid
        ]);
        return rows[0];
    }

    async getDeleteRoleByUid(uid) {
        const rows = await db.query("DELETE FROM users_roles WHERE uid = ?", [uid]);
        return rows[0];
    }

    async getUpdateUserRoleByuid(uid) {
        const rows = await db.query("UPDATE users_roles SET rid = ? WHERE uid = ?", [
            9,
            uid
        ]);
        return rows[0];
    }
}