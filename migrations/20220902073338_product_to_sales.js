exports.up = async function(knex) {
    return knex.schema.createTable('product_to_sales', function(t) {
        t.increments('id').primary();
        t.integer('pid',[11]).notNull().defaultTo(0);
        t.integer('sales_id',[11]).notNull().defaultTo(0);
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('product_to_sales');
};
    