exports.up = async function(knex) {
    return knex.schema.createTable('ticket_request_assignedTo', function(t) {
        t.increments('id').primary();
        t.integer('request_id').notNull();
        t.integer('assigned_user_id').notNull();
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('ticket_request_assignedTo');
};
