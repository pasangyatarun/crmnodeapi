import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class NewsModel {

	async getNewsByTitle(title) {
		const rows = await db.conn.promise().query(`SELECT field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid WHERE field_data_field_s_verein_newsletter.title = ?`, [title]);
		return rows[0];
	}

	async getNewsByUrl(text) {
		const rows = await db.conn.promise().query(`SELECT field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid WHERE field_data_field_s_verein_newsletter.url = ?`, [text]);
		return rows[0];
	}

	async CreateNews(req, text, newsImg) {
		const rows = await db.conn.promise().query('INSERT INTO `field_data_field_s_verein_newsletter`(`uid`,`title`,`content`,`url`,`created`,`status`,`image`,`video`,`youtube_url`)VALUES(?,?,?,?,?,?,?,?,?)', [
			req.body.author,
			req.body.title,
			req.body.content,
			text,
			new Date().getTime(),
			1,
			newsImg,
			req.body.video,
			req.body.youtube_url
		]);
		return rows[0];
	}

	async getNewsById(id) {
		const rows = await db.conn.promise().query(`SELECT field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,users.picture as profile_picture,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid WHERE field_data_field_s_verein_newsletter.news_id = ?`, [id]);
		return rows[0];
	}

	async getNewsPagination(req) {
		let param = req.params
		let pageSize = param.pagesize ? param.pagesize : 10;
		let page = param.page ? param.page : 1
		let offset = (pageSize * page) - pageSize;
		if (req.query && req.query.search) {
			let search = req.query.search
			const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount, field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,users.picture as picture,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid AND (users.mail LIKE '%` + search + `%' OR field_data_field_s_verein_newsletter.title LIKE '%` + search + `%') ORDER BY news_id DESC LIMIT ` + pageSize + ` OFFSET ` + offset + ``);
			return rows[0];
		} else {
			const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount, field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,users.picture as picture,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid ORDER BY news_id DESC LIMIT ` + pageSize + ` OFFSET ` + offset + ``);
			return rows[0];
		}
	}

	async getAllNews() {
		const rows = await db.conn.promise().query(`SELECT field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid`);
		return rows[0];
	}

	async deleteNewsbyId(id) {
		const rows = await db.conn.promise().query("DELETE FROM `field_data_field_s_verein_newsletter` WHERE `news_id`=?", [id]);
		return rows[0];
	}

	async getNewsNotify(req) {

		let newsIds = req.body.join();
		const rows = await db.conn.promise().query(`SELECT field_data_field_s_verein_newsletter.news_id,field_data_field_s_verein_newsletter.uid as author_id,users.mail as author,users.picture as picture,field_data_field_s_verein_newsletter.title,field_data_field_s_verein_newsletter.status,field_data_field_s_verein_newsletter.created,field_data_field_s_verein_newsletter.content,field_data_field_s_verein_newsletter.url,field_data_field_s_verein_newsletter.image,field_data_field_s_verein_newsletter.video, field_data_field_s_verein_newsletter.youtube_url,field_data_field_s_verein_newsletter.modified as updated FROM field_data_field_s_verein_newsletter LEFT JOIN users ON users.uid = field_data_field_s_verein_newsletter.uid WHERE field_data_field_s_verein_newsletter.news_id IN (` + newsIds + `)`);
		return rows[0];
	}

	async getUpdateNews(req, text, newsImg) {
		const rows = await db.conn.promise().query("UPDATE field_data_field_s_verein_newsletter SET uid = ?,title= ?,content= ?,url= ?,status= ?,image= ?,video= ?,youtube_url= ?,modified=? WHERE news_id = ?", [
			req.body.author,
			req.body.title,
			req.body.content,
			text,
			1,
			newsImg,
			req.body.video,
			req.body.youtube_url,
			new Date().getTime(),
			req.params.id
		])
		return rows[0];
	}
}
