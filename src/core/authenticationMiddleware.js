import express from 'express';
import jwt from 'jsonwebtoken';
import responseFormat from './response-format';
import configContainer from '../config/localhost';
import routesConfig from './routes';
import CommonMethods from './common-methods';
import UserController  from '../controllers/users/users.js';
import async from 'async';

let app = express.Router();

let config = configContainer.loadConfig(),
  commonMethods = new CommonMethods(),
  userController = new UserController(),
  appRoutes = routesConfig.getRoutes();


app.use((req, res, next) => {
  let response = responseFormat.createResponseTemplate();
  let at = req.headers.authorization || req.headers.Authorization;
  req.headers.authorizationToken = at;
  // all header info validate 
  let headerParams = configContainer.headerParams;
  let headerArr = Object.keys(headerParams);
  let reqHeader = req.headers;
  let reqHeaderArr = Object.keys(reqHeader).map(it => { return it.toLowerCase() });
  async.series([
    function (done) {
      var check = 0;
      for (let i = 0; i < headerArr.length; i++) {
        if (headerParams[headerArr[i]] == 'required' && reqHeaderArr.indexOf(headerArr[i].toLowerCase()) < 0) {
          response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
          res.status(401).json(response);
          return;
        }else{
          check = 1;
        }
      }
      if(check == 1){
        done()
      }
    },
    function(done){     
      if (at) {
        let accessToken = new Buffer.from(at, 'base64').toString('ascii');
        if (accessToken.indexOf(':') <= -1) {
          if (commonMethods.matchUrl(routesConfig.accessAndAuthUrl, req.originalUrl, req.method) && accessToken == configContainer.apiAccessToken) {
            req.headers.ext = 1;

            next();
            return;
          }
          else {
            if (accessToken !== configContainer.apiAccessToken) {
              response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
              res.status(401).json(response);
              return;
            }
            else {
              let excludArr = [];
              for (let i in routesConfig.excludeJwtPaths) {
                excludArr.push(routesConfig.excludeJwtPaths[i].url);
              }
              if (excludArr.indexOf(req.originalUrl) > -1) {
                next();
                return;
              }
              else {
                response = responseFormat.getResponseMessageByCodes(['common404'], { code: 404 });
                res.status(404).json(response);
                return;
              }
            }
          }

        }
        else {
          let apiAccessToken = accessToken.split(':')[0];
          let authToken = accessToken.split(':')[1];
          if (apiAccessToken !== configContainer.apiAccessToken) {
            response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
            res.status(401).json(response);
            return;
          }

          jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
            if (err) {
              if (commonMethods.matchUrl(routesConfig.accessAndAuthUrl, req.originalUrl, req.method) && apiAccessToken == configContainer.apiAccessToken) {
                req.headers.ext = 1;
                next();
                return;
              }
              response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
              res.status(401).json(response);
              return;
            }
            else {
              req.tokenDecoded = decoded;

              let commonMethods = new CommonMethods();
              userController.isUserLoggedIn(decoded.data.userId)
                .then((isuser) => {
                  if (isuser == 1) {
                    //check user id exists or not and status is active
                    req.tokenDecoded = decoded;
                    req.headers.apiAccessToken = apiAccessToken;
                    req.headers.authorization = "Bearer " + authToken;
                    req.headers.authorizationToken = at;
                    next();
                    return;
                  }
                  else {
                    // if accessAndAuthUrl accessed with expired token, request treated as non-logged in user
                    if (commonMethods.matchUrl(routesConfig.accessAndAuthUrl, req.originalUrl, req.method) && apiAccessToken == config.apiAccessToken) {
                      req.headers.ext = 1;
                      next();
                      return;
                    }
                    response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                    res.status(401).json(response);
                    return;
                  }
                })
                .catch((error) => {                
                  response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                  res.status(500).json(response);
                  return;
                })
            }
          });
          return;

        }
      }
      else {
        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
        res.status(401).json(response);
        return;
      }
    }
  ], function (err, results) {
    if (err) {
        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
        res.status(401).json(response);
        return;
    }    
 })
});
//module.exports = app;
export default app;