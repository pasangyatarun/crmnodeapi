exports.up = async function(knex) {
    return knex.schema.createTable('product_assign_customer', function(t) {
        t.increments('id').primary();
        t.string('pid',[255]).notNull();
        t.integer('user_id',[11]).notNull();
        t.integer('admin_id',[11]).notNull();
        t.string('product_name',[255]).notNull();
        t.string('author',[64]).notNull();
     });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('product_assign_customer');
};