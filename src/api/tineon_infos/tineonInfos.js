import express from 'express';
import TineonInfosController from '../../controllers/tineon_infos/tineonInfos'

let router = express.Router();
let tineonInfosController = new TineonInfosController();

router.get('/tineonInfo/:page/pagesize/:pagesize', tineonInfosController.getTineonInfosByPagination.bind(tineonInfosController));
router.delete('/tineonInfoDelete/:id', tineonInfosController.tineonInfoDelete.bind(tineonInfosController));
router.get('/getTineonInfoById/:id', tineonInfosController.getTineonInfobyId.bind(tineonInfosController));

//Api For Tineon to create Tineon Info on CRM web app
router.post('/tineonInfoCreate', tineonInfosController.tineonInfoCreate.bind(tineonInfosController));

export default router;