import async from 'async';
import RolesPermissionsModel from '../../models/role_permission/role_permission';
import RolesPermissionsValidation from '../../validators/roles_permissions/roles_permissions';
import responseFormat from '../../core/response-format';
let rolespermissionsModel = new RolesPermissionsModel();
let rolespermissionsValidation = new RolesPermissionsValidation();

export default class RolesPermissionsController {

    /**
    * Function to get role details by filter
    * @author  MangoIt Solutions
    * @param   {object}with rid,module
    * @return  {object} role details
    */
    rolePermissionFilter(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userDetails;
        async.waterfall([
            function (done) {
                if (!!req.body.module && !!req.body.rid) {
                    rolespermissionsModel.getPermissionsByIdModule(req.body.rid, req.body.module).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!!req.body.module && !req.body.rid) {
                    rolespermissionsModel.getRolePermissionByModule(req.body.module).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!req.body.module && !!req.body.rid) {
                    rolespermissionsModel.getRolePermissionById(req.body.rid).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!req.body.module && !req.body.rid) {
                    rolespermissionsModel.getAllRolePermissions().then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }

            },
            function (done) {
                if (userDetails && userDetails.length > 0) {
                    response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                    res.status(200).json(response);
                }
                else {
                    response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                    res.status(200).json(response);
                }
            }
        ])
    }

    /**
    * Function to create role permission
    * @author  MangoIt Solutions
    * @param   {object}with permission details
    * @return  {message} success message
    */
    rolePermissionCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = rolespermissionsValidation.rolePermissionValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            let permission = '';
            async.waterfall([
                function (done) {
                    rolespermissionsModel.getPermissionsByIdModule(req).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['RolePermission: ' + results[0].permission + ' Permissions Already exist for selected Module'], { code: 417 })
                            res.status(200).json(response);
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    for (let i = 0; i < req.body.permissions.length; i++) {
                        permission = req.body.permissions[i];
                        rolespermissionsModel.createRolePermission(req, permission).then((results) => {
                            if (results.affectedRows > 0) {
                                if (i + 1 == req.body.permissions.length) {
                                    response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionCreated']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionCreationFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }
            ])
        }
    }

    /**
    * Function to get all role permission
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} permission details
    */
    rolesPermissions(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.getAllRolePermissions().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all role permission by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} permission details
    */
    rolePermissionById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.getRolePermissionById(req.params.rid).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all role permission
    * @author  MangoIt Solutions
    * @param   {object} with rid,module
    * @return  {object} permission details
    */
    rolePermissionByIdModule(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.getPermissionsByIdModule(req.body.rid, req.body.module).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all role permission
    * @author  MangoIt Solutions
    * @param   {module} 
    * @return  {object} permission details
    */
    rolePermissionByModule(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.getRolePermissionByModule(req.params.module).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete role permission by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteRolePermission(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.deleteRolePermission(req.params.id).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete role permission by module
    * @author  MangoIt Solutions
    * @param   {module} 
    * @return  {message} success message
    */
    delRolePermissionByModule(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        rolespermissionsModel.deleteRolePermissionByModule(req.body.module).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete role permission by module
    * @author  MangoIt Solutions
    * @param   {module} 
    * @return  {message} success message
    */
    delRolePermissionByIdModule(req, res, next) {
        rolespermissionsModel.deletePermissionsByIdModule(req.body.rid, req.body.module).then((resData) => {
            if (resData.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['rolepermission:rolePermissionsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}