export default class ProductComponentsValidation {

    productComponentsValidation(details, allowedExt) {
        let signupBody = {
            title: details.title,
            description: details.description,
        };
        let err = new Array();
        if(!signupBody.title){
            err.push('title:titleReq');
            }
        if (!signupBody.description) {
            err.push('description:descriptionReq');
        }
        return err;
    }

}