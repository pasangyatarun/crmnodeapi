export default class EmailTemplateValidation {

    emailTemplateCreate(emailTemplateData) {

        let err = new Array();

        if (!emailTemplateData.templateType) {
            err.push('templateType:templateTypeReq');
        }
        if (!emailTemplateData.subject) {
            err.push('subject:subjectReq');
        }
        if (!emailTemplateData.templateBody) {
            err.push('templateBody:templateBodyReq');
        }
        if (!emailTemplateData.url) {
            err.push('url:urlReq');
        }       
        if (!emailTemplateData.headerContent) {
            err.push('headerContent:headerContentReq');
        }
        if (!emailTemplateData.footerContent) {
            err.push('footerContent:footerContentReq');
        }
        return err;
    }
}