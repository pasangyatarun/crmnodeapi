import express from 'express';
import FaqQuestionController from '../../controllers/faq_question/faq_question';

let router = express.Router();
let faqQuestionController = new FaqQuestionController;

router.post('/faqCreateQuestion', faqQuestionController.createFaqQuestion.bind(faqQuestionController));
router.get('/faqQuestion/:cat_id', faqQuestionController.getAllFaqQuestionbyCategory.bind(faqQuestionController));
router.get('/faqQuestionByid/:id', faqQuestionController.getFaqQuestionbyQuestionId.bind(faqQuestionController));
router.put('/faqQuestionupdate/:id', faqQuestionController.updateFaqQuestion.bind(faqQuestionController));
router.delete('/faqQuestiondelete/:id', faqQuestionController.deleteFaqQuestion.bind(faqQuestionController));
//Faq Question Feedback API's
router.post('/faqCategoryFeedback', faqQuestionController.createFaqCategoryFeedback.bind(faqQuestionController));
router.get('/faqCategoryFeedbackCount/:cat_id', faqQuestionController.getFaqCategoryFeedbackCount.bind(faqQuestionController));
router.get('/faqStatusByUserIdAndQuestion/:user_id/:ques_id', faqQuestionController.faqQuestionStatusByUserIdAndQuestionId.bind(faqQuestionController));

export default router;