import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class ProductDisplayModel {
   
    async SelectedProduct( productId) {
        const rows = await db.query('INSERT INTO `product_display`(`product_id`)VALUES(?)', [
            productId
        ]);
        return rows[0];
    }

    async getAllSelectedProducts() {
        const rows = await db.query("SELECT * from product_display");
        return rows[0];
    }
    
    async getSelectedProducts() {
        const rows = await db.query("SELECT product_id from product_display");
        return rows[0];
    }

    async deleteProductsExists() {
        const rows = await db.query("DELETE from product_display");
        return rows[0];
    }    
}