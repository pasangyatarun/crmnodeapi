exports.up = async function (knex) {
    return knex.schema.createTable('users_roles', function (t) {
        t.increments('uid ').primary().comment('Primary Key: users.uid for user.');
        t.integer('rid').notNull().defaultTo(0).comment('Primary Key: role.rid for role.');

     })
     .then(() =>
     knex("users_roles").insert([
    { uid:1 ,rid: 1},
    { uid: 2,rid:29},

   
     
 ])
 )
};

exports.down = async function (knex) {
    await knex.schema.dropTable('users_roles');
};

