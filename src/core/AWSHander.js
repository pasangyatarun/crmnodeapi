import AWS from 'aws-sdk';
import awsCred from '../config/localhost';   //Update credentials on this file
import path from 'path';
import fs from 'fs';
import CoreUtils from '../core/core-utils';
import https from 'https';
let Utils = new CoreUtils();
let credentials = awsCred.awsCredentials;

let AWSHandler = {};
var ep = new AWS.Endpoint(process.env.MINIO_EXTERNAL);
var s3 = new AWS.S3({
    endpoint: ep,
    apiVersion: '2006-03-01',  // apiversion of aws-sdk
    //region:'eu-central-1',     // region of aws-S3
    s3ForcePathStyle: true,
    credentials: credentials,   // credentials of accesskeys and secretkey from AWS
    httpOptions: {
        agent: new https.Agent({ rejectUnauthorized: false })
    }

});

AWSHandler.listBuckets = function () {
    s3.listBuckets(function (err, data) {
        if (err) {
            console.log("err", err);
        }
        console.log("data", data);
    })
}

AWSHandler.uploadMessageDocument = async function (document_title, path) {
    const fileContent = fs.readFileSync(path);
    var upload = s3.upload({
        Bucket: 'tineon-pm',
        Key: document_title,
        Body: fileContent,
        ACL: "public-read"
    });
    return upload.promise();
}

AWSHandler.uploadDocuments = async (path, savePath) => {
    const fileContent = fs.readFileSync(path);
    var upload = s3.upload({
        Bucket: 'tineon-documents',
        Key: savePath,
        Body: fileContent,
        ACL: "public-read"
    });
    return upload.promise();
}

AWSHandler.getUploadedDocuments = async (name) => {
    var params = { Bucket: 'tineon-documents', Key: name};
    var url = s3.getSignedUrl('getObject', params);
   
    return url;
}

AWSHandler.uploadFile = async function (teamName, docTitle, path) {
    const fileContent = fs.readFileSync(path);
    let date = new Date();
    let year = date.getFullYear();
    let month = Utils.returnMonthName(date.getMonth());
    let extension = docTitle.split('.')[1];
    let folderType = "";
    if (Utils.videoFileExtensions.indexOf(extension.toLowerCase()) > -1) {
        folderType = 'Videos'
    } else if (Utils.imageFileExtensions.indexOf(extension.toLowerCase()) > -1) {
        folderType = 'Images'
    } else {
        folderType = 'Documents';
    }

    let key = `${teamName}/${year}/${month}/${folderType}/${docTitle}`;
    var upload = s3.upload({
        Bucket: 'tineon',
        Key: key,
        Body: fileContent,
        ACL: "public-read"
    });
    return upload.promise();
}


export default AWSHandler;

