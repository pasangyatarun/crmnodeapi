
export default class ProductPriceValidation {

    productPriceValidate(details, allowedExt) {
        let signupBody = {
            suid: details.suid,
            cuid: details.cuid,
            price: details.price,
            validuntil: details.validuntil,
        };
        let err = new Array();
        
        if (!signupBody.suid) {
            err.push('suid:suid');
        } if (!signupBody.cuid) {
            err.push('cuid:cuid');
        } if (!signupBody.price) {
            err.push('price:priceReq');
        } if (!signupBody.validuntil) {
            err.push('validuntil:validReq');
        } 
        return err;
    }
}