import pdf from "pdf-creator-node";
import fs  from "fs";

var sentDataPDF;

async function sendPdf(document, options) {
    await pdf.create(document, options).then((res) => {
        sentDataPDF = res;

    })
        .catch((error) => {
            sentDataPDF = 'error';

        });
        return sentDataPDF;
}
export default sendPdf;
