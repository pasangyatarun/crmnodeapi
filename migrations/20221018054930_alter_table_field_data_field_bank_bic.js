exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('field_data_field_bank_bic', function(t) {
        t.string('entity_type',[128]).notNull().defaultTo('').comment('The entity type this data is attached to').alter();
        t.integer('entity_id').unsigned().comment('	The entity type this data is attached to').alter();
        t.string('field_bank_bic_value',[20]).alter();
        t.string('field_bank_bic_format',[255]).alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('field_data_field_bank_bic', function(t) {
       
    });
 };