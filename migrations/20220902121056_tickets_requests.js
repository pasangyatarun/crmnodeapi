exports.up = async function (knex) {
    return knex.schema.createTable('tickets_requests', function (t) {
        t.increments('id').primary();
        t.longtext('ticket_number').notNull();
        t.integer('user_id',[11]).notNull();
        t.string('name',[264]).notNull();
        t.longtext('description').notNull();
        t.string('attachment',[255]);
        t.string('status',[264]).notNull();
        t.string('assigned_to',[264]).notNull();
        t.timestamp('created_at').notNull().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('tickets_requests');
};

