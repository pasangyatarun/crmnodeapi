import express from 'express';
import BillwerkProductController from '../../../controllers/billwerk_controllers/billwerk_product/product';

let router = express.Router();
let billwerkProductController = new BillwerkProductController();

router.get('/billwerk-product', billwerkProductController.getProduct.bind(billwerkProductController));
router.get('/billwerk-normalclub-product', billwerkProductController.getNormalClubProduct.bind(billwerkProductController));
router.get('/billwerk-productbyid/:id', billwerkProductController.getProductById.bind(billwerkProductController));
router.get('/billwerk-productInfo/showhidden/:bolean', billwerkProductController.getProductInfo.bind(billwerkProductController));
router.get('/billwerk-trialproduct', billwerkProductController.getTrialProduct.bind(billwerkProductController));
router.get('/billwerk-product-components', billwerkProductController.getProductComponents.bind(billwerkProductController));
router.get('/billwerk-product-componentbyid/:component_id', billwerkProductController.getComponentProductById.bind(billwerkProductController));
router.get('/billwerk-threemonth-trialproduct', billwerkProductController.getThreeMonthProduct.bind(billwerkProductController));

export default router;