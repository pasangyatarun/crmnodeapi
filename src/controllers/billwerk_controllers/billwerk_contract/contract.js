import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import axios from 'axios';
import { LocalStorage } from 'node-localstorage';
import OrderModel from '../../../models/billwerk_models/order';

let localStorage = new LocalStorage('./scratch');
let ordermodel = new OrderModel();

export default class BillwerkContractController {
    constructor() {
    }

/**
 * Function to get customer end contracts
 * @author  MangoIt Solutions
 * @param   {}
 * @return  {object} customer contract details
 */
    getEndedCustomerContract(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + req.params.customer_id + '/contracts',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        let endedContract = resPonse.data.filter(obj => obj.LifecycleStatus == 'Ended')
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: endedContract } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

/**
 * Function to get customer contract details from billwerk 
 * @author  MangoIt Solutions
 * @param   {customer_id}
 * @return  {object} customer contract details
 */
    getCustomerContract(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + req.params.customer_id + '/contracts',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: resPonse.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

/**
 * Function to get customer cancel contract details from billwerk 
 * @author  MangoIt Solutions
 * @param   {contract_id}
 * @return  {object} customer contract details
 */
    getCancelContract(req, res, next) {
        let previewData;
        let responData = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'contracts/' + req.params.contract_id + '/cancellationpreview',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        previewData = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                let endDateI;
                if (req.body.instantendDate) {
                    endDateI = req.body.instantendDate
                }
                else {
                    endDateI = previewData.EndDate
                }
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'contracts/' + req.params.contract_id + '/end',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: JSON.stringify({ endDate: endDateI }),
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        responData[0] = resPonse.data;
                        done();
                        // response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        // res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.cancelStore(responData[0], req.params.contract_id).then((stored) => {
                    if (stored.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: responData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: responData } });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }

        ])
    }
}