import async from 'async';
import FilterModel from '../../models/filter/filter';
import UserModel from '../../models/user/user';
import responseFormat from '../../core/response-format';

let filterModel = new FilterModel();
let userModel = new UserModel();

export default class FilterController {

    /**
    * Function to filter users based on status OR role 
    * @author  MangoIt Solutions
    * @param   {object} with status or role or both
    * @return  {object} user details
    */
    filterUser(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let userDetails;

        async.waterfall([
            function (done) {
                if (!!req.body.status && !!req.body.role) {
                    filterModel.getUsersByStatusRole(req).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!!req.body.status && !req.body.role) {
                    filterModel.getUsersByStatus(req).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!req.body.status && !!req.body.role) {
                    filterModel.getUsersByRole(req).then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (!req.body.status && !req.body.role) {
                    userModel.getAllUsers().then((results) => {
                        if (results) {
                            userDetails = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    done();
                }
            },
            function (done) {
                if (userDetails && userDetails.length > 0) {
                    done();
                }
                else {
                    response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                    res.status(200).json(response);
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserRolesData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['role'] = results;
                            if (i + 1 == userDetails.length) {
                                done();
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            },
            function (done) {
                for (let i = 0; i < userDetails.length; i++) {
                    userModel.getUserDomainsData(userDetails[i].uid).then((results) => {
                        if (results.length > 0) {
                            userDetails[i]['domain'] = results;
                            if (i + 1 == userDetails.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDetails } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])
    }
}