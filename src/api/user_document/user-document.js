import express   from 'express';
import UserDocumentController from '../../controllers/user_document/user-document';
import multerMW from '../../core/multer';

let router = express.Router();
let userDocumentController = new UserDocumentController();

router.post('/document-upload/:uid', multerMW.single('file'),userDocumentController.uploadDocument.bind(userDocumentController));
router.get('/get-user-documents', userDocumentController.getUserDocument.bind(userDocumentController));
router.delete('/document-delete/:id', userDocumentController.deleteDocument.bind(userDocumentController));
router.get('/get-documentbyid/:id', userDocumentController.getUserDocumentById.bind(userDocumentController));
router.get('/get-documentbyuserid/:user_id', userDocumentController.getUserDocumentByUserId.bind(userDocumentController));
router.get('/get-pesonalize-alluser-documents', userDocumentController.getAllPersonalizeUserDocuments.bind(userDocumentController));
router.post('/get-documentbyname', userDocumentController.getUserDocumentByName.bind(userDocumentController));
router.post('/document-delete/uploads', userDocumentController.deleteDocumentFromUploadsByName.bind(userDocumentController));

export default router;