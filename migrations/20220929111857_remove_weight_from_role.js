exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('role', function(t) {
        t.integer('weight').defaultTo(0).alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('role', function(t) {
        
    });
 };