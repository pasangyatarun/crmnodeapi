exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('order_billwerk', function(t) {
        t.string('planId',[255]).alter();
        t.string('planVariantId',[255]).alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('order_billwerk', function(t) {
       
    });
 };