import express   from 'express';
import NewsController from '../../controllers/news/news';
import multerMW from '../../core/multer';

let router = express.Router();
let newsController = new NewsController();

router.get('/newsbyurl/:url', newsController.getNewsbyURL.bind(newsController));
router.get('/newsbyid/:id', newsController.getNewsbyId.bind(newsController));
router.get('/news', newsController.getNewsAll.bind(newsController));
router.get('/newsbypagination/:page/pagesize/:pagesize', newsController.getNewsByPagination.bind(newsController));
router.post('/newscreate', multerMW.single('file'), newsController.newsCreate.bind(newsController));
router.delete('/newsdelete/:id', newsController.newsDelete.bind(newsController));
router.put('/newsedit/:id',multerMW.single('file'), newsController.newsUpdate.bind(newsController));
router.post('/newsby/newsids',newsController.newsTineonNotify.bind(newsController));

export default router;