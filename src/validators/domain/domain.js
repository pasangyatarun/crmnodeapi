
export default class DomainValidation {
    domainValidation(details, allowedExt) {
        let signupBody = {
            domain: details.domain,
            sitename: details.sitename,
            machineName:details.machineName
        };
        let err = new Array();
        if(!signupBody.domain){
            err.push('domain:domainReq');
            }
        if (!signupBody.sitename) {
            err.push('sitename:sitenameReq');
        }
        if (!signupBody.machineName) {
            err.push('machineName:machineNameReq');
        }
        return err;
    }
}