import express   from 'express';
import FunctionsInClubController  from '../../controllers/function-in-club/funtion-in-club';

let router = express.Router();
let functionInClubController = new FunctionsInClubController();

router.post('/function-club-name-add', functionInClubController.addFunctionClubName.bind(functionInClubController));
router.put('/function-club-update/:id', functionInClubController.updateFunctionClub.bind(functionInClubController));
router.get('/function-in-club', functionInClubController.functionClubName.bind(functionInClubController));
router.get('/functionClubBy-customer/:customer_id', functionInClubController.functionClubByustomerId.bind(functionInClubController));
router.get('/functionClubBy-id/:id', functionInClubController.ClubById.bind(functionInClubController));
router.delete('/functionClub-delete/:id', functionInClubController.getDeleteFunctionClubById.bind(functionInClubController));
export default router;