import async from 'async';
import ProductComponentsModel from '../../models/product_components/product_component';
import ProductComponentsValidation from '../../validators/product_components/product_components';
import responseFormat from '../../core/response-format';

let productComponentsModel = new ProductComponentsModel();
let productComponentsValidation = new ProductComponentsValidation();

export default class ProductComponentsController {

    /**
    * Function to create product components
    * @author  MangoIt Solutions
    * @param   {object} with component details 
    * @return  {message} success message
    */
    productComponentsCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate();

        let msgCode = productComponentsValidation.productComponentsValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productComponentsModel.getproductComponentsByName(req.body.title).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productComponentsModel.createproductComponents(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsCreated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get all product components
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product component details
    */
    productComponents(req, res) {
        let response = responseFormat.createResponseTemplate();
        productComponentsModel.getAllproductComponents().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get product components by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} product component details
    */
    productComponentsById(req, res) {
        let response = responseFormat.createResponseTemplate();
        productComponentsModel.getproductComponentsById(req.params.id).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to get product components by name
    * @author  MangoIt Solutions
    * @param   {title} 
    * @return  {object} product component details
    */
    productComponentsByName(req, res) {
        let response = responseFormat.createResponseTemplate();
        productComponentsModel.getproductComponentsByName(req.params.title).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }


    /**
    * Function to update product components by id
    * @author  MangoIt Solutions
    * @param   {id} with updated details
    * @return  {message} success message
    */
    updateproductComponents(req, res) {
        let response = responseFormat.createResponseTemplate();

        let msgCode = productComponentsValidation.productComponentsValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productComponentsModel.getproductComponentsById(req.params.id).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productComponentsModel.getproductComponentsByName(req.body.title).then((results) => {
                        if (results.length > 0 && results.rid != req.params.id) {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNameExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productComponentsModel.updateproductComponents(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsUpdateSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete product components by id
    * @author  MangoIt Solutions
    * @param   {id} with updated details
    * @return  {message} success message
    */
    deleteproductComponents(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productComponentsModel.deleteproductComponents(req.params.id).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['productComponents:productComponentsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}