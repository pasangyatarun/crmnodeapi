exports.up = async function (knex) {
    return knex.schema.createTable('user_documents', function (t) {
        t.increments('id').primary();
        t.longtext('path').notNull();
        t.integer('created_by',[11]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.datetime('updated_at').defaultTo(knex.fn.now());
     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('user_documents');
};

