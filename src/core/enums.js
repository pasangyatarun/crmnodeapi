var enums  = {
    // production vars 

    /**
     * immigration filing static page (--changes--)
     */
    immigrationFiling: {
        faqId: 31,
    },


    // ----------------------------- END HERE


    /**
     *  All conditions 
     */
    conditions: {
        euqals: "=",
        doubleEquals: "==",
        tripleEquals: "===",
        greaterThan: ">",
        lessThen: "<",
        lessEquals: "<=",
        greaterEquals: ">="
    },
    employeeProjects: {
        currentProject: 1,
        pastProject: 0
    },
    emailRefer: {
        toMailProd: [
            { mailId: 'new-clients@test.com', displayName: null, configKeyName: 'SUPPORTMAILID' }          
        ],
        toMailDev: [
            { mailId: 'sharads@test.in', displayName: null, configKeyName: 'SUPPORTMAILID' }          
        ]
    },


    // expiration time for activation code used in Account Activation mail (in hours ) 
    activationCodeExpiraionTime: 17520, // 17520 hrs = 730 days


    /**
     *  password policy
     */
    passwordPolicy: {
        minimumLength: 8,
        maximumLength: 60,
        requireCapital: true,
        requireLower: false,
        requireNumber: true,
        requireSpecial: true
    },

    /**
     *  Encryption Decryption aes-256-cbc
     */
    encryption: {
        encryptionKey: "Rl#H77vtJiwKS!W-alkSifYKD-Nj7Lb4",
        encryptionValueLength: 16,
    },
}

export default enums;