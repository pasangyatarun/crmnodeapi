
import responseFormat from '../../core/response-format';
import JsonScriptModel from '../../models/json_script/json_script';
import async, { waterfall } from 'async';
import * as fs from 'fs';

let jsonScriptModel = new JsonScriptModel();

export default class JsonScriptController {

	/**
	* Function to get Json data from .json file and insert that data into respected tables
	* @author  MangoIt Solutions
	* @param   {id} 
	* @return  {message} success message
	*/
	getJson(req, res, next) {
		var d = [];
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		async.waterfall([
			function (done) {
				fs.readFile(req.file.path, 'utf8', async (err, data) => {
					if (err) {
						response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
						res.status(500).json(response);
						return;
					}
					var d = data;
					var responce = JSON.parse(d);
					for (let k = 0; k < responce.length; k++) {
						await jsonScriptModel.insertUser(responce[k].Customer).then(async (ress) => {
							if (ress.affectedRows > 0) {
								await jsonScriptModel.insertUserRole(ress.insertId).then(async (roleInserted) => {
									if (roleInserted.affectedRows > 0) {
										await jsonScriptModel.insertUserAddress(responce[k].Customer, ress.insertId).then(async (addinserted) => {
											if (addinserted.affectedRows > 0) {
												await jsonScriptModel.insertIBAN(responce[k].Customer.Payment, ress.insertId).then(async (ibanInserted) => {
													if (ibanInserted.affectedRows > 0) {
														let licenceArr = responce[k].Customer.Licence;
														for (let i = 0; i < licenceArr.length; i++) {
															await jsonScriptModel.insertLicence(licenceArr[i], ress.insertId).then(async (licenceInserted) => {
																if (licenceInserted.affectedRows > 0) {

																	let invoiceArr = licenceArr[i].Invoice
																	for (let j = 0; j < invoiceArr.length; j++) {
																		await jsonScriptModel.insertInvoice(invoiceArr[j], ress.insertId, licenceInserted.insertId).then((invoiceInserted) => {
																			if (invoiceInserted.affectedRows > 0) {
																				if (responce.length == k + 1 && invoiceArr.length == j + 1) {
																					done();
																				}
																			} else {
																				response = responseFormat.getResponseMessageByCodes(['customer:customerInvoiceFailed'], { code: 417 });
																				res.status(200).json(response);
																			}
																		}).catch(function (err) {
																			response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
																			res.status(500).json(response);
																			return;
																		})
																	}
																} else {
																	response = responseFormat.getResponseMessageByCodes(['customer:customerLicenceFailed'], { code: 417 });
																	res.status(200).json(response);
																}
															}).catch(function (err) {
																response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
																res.status(500).json(response);
																return;
															})
														}
													} else {
														response = responseFormat.getResponseMessageByCodes(['customer:customerIBANFailed'], { code: 417 });
														res.status(200).json(response);
													}
												}).catch(function (err) {
													response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
													res.status(500).json(response);
													return;
												})
											} else {
												response = responseFormat.getResponseMessageByCodes(['customer:customerAddressFailed'], { code: 417 });
												res.status(200).json(response);
											}
										}).catch(function (err) {
											response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
											res.status(500).json(response);
											return;
										})
									} else {
										response = responseFormat.getResponseMessageByCodes(['customer:customerRoleFailed'], { code: 417 });
										res.status(200).json(response);
									}
								}).catch(function (err) {
									response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
									res.status(500).json(response);
									return;
								})
							} else {
								response = responseFormat.getResponseMessageByCodes(['customer:customerInsertionFailed'], { code: 417 });
								res.status(200).json(response);
							}
						}).catch(function (err) {
							response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
							res.status(500).json(response);
							return;
						})
					}
				})
			},
			function (done) {
				response = responseFormat.getResponseMessageByCodes(['customer:customersImported']);
				res.status(200).json(response);
			}
		])
	}

	/**
	* Function to get all old user data.
	* @author  MangoIt Solutions
	* @param   {object} with page, pagesize
	* @return  {object} old user data
	*/
	getAllOldUserDataByPaginate(req, res, next) {
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		jsonScriptModel.allOldUserDataByPage(req).then((results) => {
			if (results) {
				response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
				res.status(200).json(response);
			} else {
				response = responseFormat.getResponseMessageByCodes(['User:UserNotExists'], { code: 417 });
				res.status(200).json(response);
			}
		}).catch((error) => {
			response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
			res.status(500).json(response);
			return;
		})
	}

	/**
	* Function to get all old user data.
	* @author  MangoIt Solutions
	* @param   {} 
	* @return  {object} old user data
	*/
	getAllOldUserData(req, res, next) {
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		jsonScriptModel.allOldUserData().then((results) => {
			if (results) {
				response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
				res.status(200).json(response);
			} else {
				response = responseFormat.getResponseMessageByCodes(['User:UserNotExists'], { code: 417 });
				res.status(200).json(response);
			}
		}).catch((error) => {
			response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
			res.status(500).json(response);
			return;
		})
	}

	/**
	* Function to get old user data by userId
	* @author  MangoIt Solutions
	* @param   {user_id} 
	* @return  {object} old user data
	*/
	getUserDataByUserId(req, res, next) {
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		jsonScriptModel.getUserDataById(req.params.uid).then((results) => {
			if (results) {
				response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
				res.status(200).json(response);
			} else {
				response = responseFormat.getResponseMessageByCodes(['User:UserNotExists'], { code: 417 });
				res.status(200).json(response);
			}
		}).catch((error) => {
			response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
			res.status(500).json(response);
			return;
		})
	}

	/**
	* Function to delete old user data 
	* @author  MangoIt Solutions
	* @param   {} 
	* @return  {message} success message
	*/
	deleteAllOldUserData(req, res, next) {
		let oldUserId = [];
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		async.waterfall([
			function (done) {
				jsonScriptModel.getolduserid().then((results) => {
					if (results.length > 0) {
						let obj = results.map(obj => {
							oldUserId.push(obj.user_id);
						});
						done();
					} else {
						response = responseFormat.getResponseMessageByCodes(['customer:OldUserDataNotExist'], { code: 417 });
						res.status(200).json(response);
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserliceneceHistory(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserInvoice(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteOldUserBankIban(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteOldUserAddress(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteOldUserRoles(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteOldUsersData(oldUserId).then((data) => {
					if (data.affectedRows > 0) {
						response = responseFormat.getResponseMessageByCodes(['customer:OldUserDataDeleted']);
						res.status(200).json(response);
					} else {
						response = responseFormat.getResponseMessageByCodes(['customer:OldUserDataNotExist'], { code: 417 });
						res.status(200).json(response);
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			}
		])
	}

	/**
	* Function to delete user data except admin and super admin
	* @author  MangoIt Solutions
	* @param   {} 
	* @return  {message} success message
	*/
	deleteAllUserData(req, res, next) {
		console.log("----")
		let UserId = [];
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		async.waterfall([
			function (done) {
				jsonScriptModel.getuserid().then((results) => {
					if (results.length > 0) {
						let obj = results.map(obj => {
							UserId.push(obj.uid);
						});
						done();
					} else {
						response = responseFormat.getResponseMessageByCodes(['customer:OldUserDataNotExist'], { code: 417 });
						res.status(200).json(response);
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserRoleData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserliceneceHistoryData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserInvoiceData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteuserOrderData().then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteBillerkCustomerData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerAddressData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerBankIbanData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteProductAssignedData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteTicketAssignedData(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteTicketRequestData().then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteTicketCommentData().then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCrmProductOrder(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerDepartment(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerMember(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerTags(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteCustomerDistributer(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteFaqQuestionFeedback(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			
			function (done) {
				jsonScriptModel.deleteFunctionInClub(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteSurveyUsers(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteSurveyResult(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
				jsonScriptModel.deleteSalesPartnerCommission(UserId).then((data) => {
					if (data.affectedRows > 0) {
						done();
					} else {
						done();
					}
				}).catch((error) => {
					response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
					res.status(500).json(response);
					return;
				})
			},
			function (done) {
                response = responseFormat.getResponseMessageByCodes(['User:UserDataDeleted']);
                res.status(200).json(response);
            }
		])
	}


}



