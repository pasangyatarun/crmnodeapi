import CommonMethods from '../../core/common-methods.js';
let commonMethods = new CommonMethods();

export default class ContactAdminValidation {

    contactValidation(contactData){
        let err = new Array();
        if (!contactData.email || !contactData.email.trim() || (!commonMethods.validateEmailid(contactData.email))) {
            err.push('email:email');
        }
        if(!contactData.uid ){
            err.push('uid:uidReq');
        }        
        if(!contactData.subject ){
            err.push('subject:subject');
        }
        if(!contactData.message ){
            err.push('message:msgReq');
        }
        return err;
    }

}