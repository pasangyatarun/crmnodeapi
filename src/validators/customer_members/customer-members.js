import CommonMethods from '../../core/common-methods.js';
let commonMethods = new CommonMethods();

export default class CustomerMembersValidation {

    memberAddValidation(userData){
        let err = new Array();
       
        if (!userData.email || !userData.email.trim() || (!commonMethods.validateEmailid(userData.email))) {
            err.push('email:email');
        }       
        return err;
    }
}