export default class RolesPermissionsValidation {

    rolePermissionValidation(details, allowedExt) {
        let signupBody = {
            roles: details.roles,
            permissions: details.permissions,
            modules: details.modules
        };
        let err = new Array();
        if (!signupBody.roles) {
            err.push('role:roleReq');
        }
        if (!signupBody.permissions && signupBody.permissions.length > 0) {
            err.push('role:permissionsReq');
        }
         if (!signupBody.modules) {
            err.push('role:modulesReq');
        }
        return err;
    }

}