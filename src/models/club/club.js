import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class ClubModel {

    async deleteSupportUser(ids){
        let x = ids.join();
        const rows = await db.conn.promise().query("DELETE FROM `club_support_user` WHERE user_id IN  (" + x + ") ");
        return rows[0];
    }

    async getAllSupportUser(){
        const rows = await db.conn.promise().query("SELECT * FROM `club_support_user`");
        return rows[0];
    }

    async getSupportUser(uid){
        const rows = await db.conn.promise().query("SELECT * FROM `club_support_user` WHERE user_id =?",[uid]);
        return rows[0];
    }
    
    async SupportUserUpdate(uid,username,pswd){
        const rows = await db.conn.promise().query("UPDATE `club_support_user` SET user = ?, password= ? WHERE user_id = ?", [
            username,
            pswd,
            uid
        ]);
        return rows[0];
    }

    async SupportUserSave(uid,username,pswd){
        const rows = await db.conn.promise().query("INSERT INTO `club_support_user`(`user_id`,`user`,`password`)VALUES(?,?,?)", [
            uid,
            username,
            pswd,
        ]);
        return rows[0];
    }  
}