import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import ProductDisplay from '../../../models/product-display/product-display'
import axios from 'axios';
import { LocalStorage } from 'node-localstorage';

let productDisplay = new ProductDisplay();
let localStorage = new LocalStorage('./scratch');

export default class BillwerkProductController {
    constructor() {
    }

    /**
    * Function to get three month period products
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getThreeMonthProduct(req, res, next) {
        let allvariant;
        let variant;
        let trial;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/?externalId=90',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        allvariant = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: allvariant } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
        ])
    }

    /**
    * Function to get 30 days trial products
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getTrialProduct(req, res, next) {
        let allvariant;
        let variant;
        let trial;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/?externalId=30',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        allvariant = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: allvariant } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }

        ])
    }

    /**
    * Function to get products information
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getProductInfo(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productInfo?showHidden=' + req.params.bolean,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])

    }

    /**
    * Function to get products by Id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object}  product details
    */
    getProductById(req, res, next) {
        let variant;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        variant = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {

                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'plans/' + variant.PlanId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        variant.planDetail = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planGroups/' + variant.PlanGroupId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        variant.planGroup = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var a1 = [];
                a1[0] = variant;
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get products components
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getProductComponents(req, res, next) {
        let allVariant;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productInfo',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        let productsall = response.data;
                        allVariant = productsall.Components;
                        allVariant.sort(function(a, b){return a.PricePerUnit-b.PricePerUnit});
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: allVariant } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get normal club products 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getNormalClubProduct(req, res, next) {
        let allVariant;
        let productsall;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productinfo/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        productsall = response.data;
                        allVariant = productsall.PlanVariants;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                productDisplay.getAllSelectedProducts().then((products) => {
                    if (products && products.length > 0) {
                        const a4 = []
                        allVariant.forEach(element => {
                            if (products.find(o => o.product_id == element.Id) || element.AllowWithoutPaymentData == true) {
                                a4.push(element)
                            }
                        });
                        allVariant = [];
                        allVariant = a4;
                        done();
                    } else {
                        done();
                    }

                })
            },
            function (done) {
                for (let i = 0; i < allVariant.length; i++) {
                    let detail = productsall.Plans.find(o => o.Id == allVariant[i].PlanId);
                    allVariant[i]['planDetail'] = detail;
                    if (allVariant.length == i + 1) {
                        let clubPlan = allVariant.filter(o => (o.planDetail.CustomFields.type == 1 || o.planDetail.CustomFields.type == 4) && o.Id != '627a7d68f6750711f0176297');
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: clubPlan } });
                        res.status(200).json(response);
                    }

                }

            }
        ])
    }

    /**
    * Function to get products 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  product details
    */
    getProduct(req, res, next) {
        let allVariant;
        let productsall;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productInfo',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        productsall = response.data;
                        allVariant = productsall.PlanVariants.filter(o => o.Id != '627a7d68f6750711f0176297');
                        done();
                    })
                    .catch(function (error) {
                        if (error && error.isAxiosError == true && error.response && error.response.data && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                for (let i = 0; i < allVariant.length; i++) {
                    let detail = productsall.Plans.find(o => o.Id == allVariant[i].PlanId);
                    let detailGroup = productsall.PlanGroups.find(o => o.Id == allVariant[i].PlanGroupId);
                    allVariant[i]['planDetail'] = detail;
                    allVariant[i]['planGroup'] = detailGroup;
                    if (allVariant.length == i + 1) {
                        done();
                    }
                }
            },
            function (done) {
                let newArrData = [];
                allVariant.forEach(o => {
                    if (!(o.AllowWithoutPaymentData == true)) {
                        newArrData.push(o);
                    }
                });
                newArrData.sort(function(a, b){return a.RecurringFee-b.RecurringFee});
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: newArrData } });
                res.status(200).json(response);
            }
        ])
    }

     /**
    * Function to get component product by Id
    * @author  MangoIt Solutions
    * @param   {component_id} 
    * @return  {object}  component product details
    */
    
     getComponentProductById(req, res, next) {
        let variant;
        let response = responseFormat.createResponseTemplate();
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'components/' + req.params.component_id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        // variant = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [resPonse.data] } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            // function (done) {

            //     var config = {
            //         method: 'get',
            //         url: configContainer.billwerkAccountUrl + 'plans/' + variant.PlanId,
            //         headers: {
            //             'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
            //         }
            //     };

            //     axios(config)
            //         .then(function (response) {
            //             variant.planDetail = response.data;
            //             done();
            //         })
            //         .catch(function (error) {
            //             if (error.isAxiosError == true && error.response.data.Message) {
            //                 response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
            //                 res.status(200).json(response);
            //             } else {
            //                 response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            //                 res.status(500).json(response);
            //                 return;
            //             }
            //         });
            // },
            // function (done) {
            //     var config = {
            //         method: 'get',
            //         url: configContainer.billwerkAccountUrl + 'planGroups/' + variant.PlanGroupId,
            //         headers: {
            //             'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
            //         }
            //     };

            //     axios(config)
            //         .then(function (response) {
            //             variant.planGroup = response.data;
            //             done();
            //         })
            //         .catch(function (error) {
            //             if (error.isAxiosError == true && error.response.data.Message) {
            //                 response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
            //                 res.status(200).json(response);
            //             } else {
            //                 response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            //                 res.status(500).json(response);
            //                 return;
            //             }
            //         });
            // },
            // function (done) {
            //     var a1 = [];
            //     a1[0] = variant;
            //     response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
            //     res.status(200).json(response);
            // }
        ])
    }

}