exports.up = async function(knex) {
    return knex.schema.createTable('field_data_field_s_verein_newsletter', function(t) {
        t.increments('news_id').comment('The entity id this data is attached to');
        t.integer('uid',[10]).notNull();
        t.text('title').notNull();
        t.longtext('content').notNull();
        t.string('url',[254]).notNull();
        t.string('created',[254]).notNull().defaultTo(0);
        t.string('modified',[254]).defaultTo(0);
        t.tinyint('status',[3]).notNull();
        t.string('image',[254]);
        t.string('video',[254]);
        t.string('youtube_url',[254]);
      });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('field_data_field_s_verein_newsletter');
};