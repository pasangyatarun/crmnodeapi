import crypto from 'crypto';
import enums from '../core/enums';

export default class CommonMethods {
    validateEmailid(email) {
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (pattern.test(email)) {
            return true;
        } else {
            return false
        };
    }

    passwordValidation(password) {
        var pattern = /(?=.*[@#%$~!^&*])(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z]).{8,30}/;

        if (pattern.test(password)) {
            return true;
        } else {
            return false
        };
    }

    getDates(startDate, endDate) {
        const dates = []
        let currentDate = startDate
        const addDays = function (days) {
            const date = new Date(this.valueOf())
            date.setDate(date.getDate() + days)
            return date
        }
        while (currentDate <= endDate) {
            dates.push(currentDate)
            currentDate = addDays.call(currentDate, 1)
        }
        return dates
    }


    /**
     * match giver urls with method and regEd
     * @param {*} arrayOfUrl : Url array to match with
     * @param {*} originalUrl : original Url to match 
     * @param {*} method : method of original url
     */
    matchUrl(arrayOfUrl, originalUrl, method) {
        var rs = arrayOfUrl.filter(item => {
            return (item.url == originalUrl && item.methods.indexOf(method) > -1) || (item.regex && item.pattern.test(originalUrl) && item.methods.indexOf(method) > -1)
        })
        return rs.length ? true : false;
    }

    /**
     * Check date is valid or not
     * @param {*} date :Input request date
     */
    isValidDate(date) {
        let regEx = /^\d{4}-\d{2}-\d{2}$/;

        if (!date) {
            return false;
        }


        let m = moment(date);

        if (m.isValid() && m.isAfter('1900-01-01')) {
            return true;
        }
        return false;
    }

    /**
    * Check data is valid integer or not
    * @param {*} data :Input request value
    */
    isValidInteger(data) {
        let regEx = /^\+?(0|[1-9]\d*)$/;

        if (!data) {
            return false;
        }
        else if (data.toString().match(regEx) == null) {
            return false;
        }

        return true;
    }


    /**
    * Check data is valid isValidNumber or not
    * @param {*} data :Input request value
    */
    isValidNumber(data) {

        let regEx = /^[0-9]+([,.][0-9]+)?$/g;
        if (!data) {
            return false;
        }
        else if (data.toString().match(regEx) == null) {
            return false;
        }

        return true;
    }

    /**
   * validate phone no patterns [XXX-XXX-XXXX, XXX.XXX.XXXX, XXX XXX XXXX ,+91 xx-xx-xxxxxx]
   * @param {*} location :location
   */
    isValidPhone(inputtxt) {
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4,6})$/;

        var phone = /^([0|\+[0-9]{1,6})[ ]?([0-9]{2})[-]?([0-9]{2})[-]?([0-9]{6})$/; // +91 xx-xx-xxxxxx

        if (inputtxt.toString().match(phoneno) || inputtxt.toString().match(phone)) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Validate boolean field
     * @param {*} input : input value
     */
    isBoolean(input) {

        return [0, 1, "0", "1", true, false, "true", "false"].indexOf(input) > -1 ? true : false;

    }
    /**
    * Validate Alpha Numeric Value
    * @param {*} input : input value
    */
    isAlphaNumeric(input) {
        if (input === undefined || input === null || input === "")
            return true;
        else
            return input.match(/^[a-z\d\.-_\s]+$/i) ? true : false;

    }

    /**
     * method to handle execption
     * @param {*} fromText : Text contains source method, to log in file
     * @param {*} error : original error     
     */
    catchError(fromText, error) {

        let toEmail = config.errorReportEmail;
        let subject = config.node_env.toUpperCase() + ' : Error occurred in : ' + fromText;
        let body = 'An Error has occurred in : ' + fromText + ' \n Original Error is : ' + error;

        logger.error('Error has occured in : ' + fromText + ' || ERROR : ', error);


        mailer.sendEmail({
            to: toEmail,
            subject: subject,
            html: body
        });

        return { message: ['common500'], code: 500 };
    }
    /**
        * To encrypt any string 
        * @param {*} text : plain text
        */
    encrypt(text) {
        let iv = crypto.randomBytes(enums.encryption.encryptionValueLength);
        let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer.from(enums.encryption.encryptionKey), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return iv.toString('hex') + ':' + encrypted.toString('hex');
    }

    /**
     *  To decrypt any string 
     * @param {*} text :encrypted text
     */
    decrypt(text) {
        return new Promise(function (resolve, reject) {
            try {
                let textParts = text.split(':');
                let iv = new Buffer.from(textParts.shift(), 'hex');
                let encryptedText = new Buffer.from(textParts.join(':'), 'hex');
                let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer.from(enums.encryption.encryptionKey), iv);
                let decrypted = decipher.update(encryptedText);
                decrypted = Buffer.concat([decrypted, decipher.final()]);
                return resolve(decrypted.toString());
            }
            catch (e) {

                return resolve(false)
            }

        })
    }

}