exports.up = async function(knex) {
    return knex.schema.createTable('customer_member_search', function(t) {
        t.increments('id').primary();
        t.integer('customer_id',[11]).notNull();
        t.longtext('filter_name').notNull();
        t.longtext('search_in');
        t.text('type');
        t.text('department');
        t.string('link_the_search',[255]);
        t.datetime('created_at').notNull().defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('customer_member_search');
};

