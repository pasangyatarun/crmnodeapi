exports.up = async function(knex) {
    return knex.schema.createTable('file_managed', function(t) {
        t.increments('fid ').comment('File ID.');
        t.integer('uid').notNull().defaultTo(0).comment('The users.uid of the user who is associated with the file.');
        t.string('filename',[255]).notNull().comment('	Name of the file with no path components. This may differ from the basename of the URI if the file is renamed to avoid overwriting an existing file.');
        t.string('uri',[255]).notNull().comment('The URI to access the file (either local or remote)');
        t.string('filemime',[255]).notNull().comment('The fileâ€™s MIME type.');
        t.bigint('filesize',[20]).notNull().defaultTo(0).comment('The size of the file in bytes');
        t.tinyint('status',[4]).notNull().defaultTo(0).comment('A field indicating the status of the file. Two status are defined in core: temporary (0) and permanent (1). Temporary files older than DRUPAL_MAXIMUM_TEMP_FILE_AGE will be removed during a cron run.');
        t.integer('timestamp ',[10]).notNull().defaultTo(0).comment('UNIX timestamp for when the file was added.');
        t.string('type',[20]).notNull().defaultTo('undefined').comment('The URI to access the file (either local or remote)');
        });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('file_managed');
};