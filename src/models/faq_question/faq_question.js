import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class FaqModel {

    async CreateFaqQuestion(req, faqList) {
        const rows = await db.query('INSERT INTO `faq_question_answer`(`category_id`,`author_id`,`question`,`answer`)VALUES(?,?,?,?)', [
            req.body.category_id,
            req.body.author_id,
            faqList.question,
            faqList.answer
        ]);
        return rows[0];
    }

    async checkFaqQuestion(category_id, arr) {
        const ques = JSON.stringify(arr).replace('[', '').replace(']', '').trim();
        const rows = await db.query("SELECT * from faq_question_answer WHERE category_id = ? AND question IN (" + ques + ") ", [category_id]);
        return rows[0];
    }

    async checkFaqQuestionExistByQId(req){
        const rows = await db.query("SELECT * from faq_question_answer WHERE category_id = ? AND question = ? AND id != ?", [req.body.category_id,req.body.question,req.params.id]);
        return rows[0];
    }

    async AllFaqsByCategoery(cat_id) {
      const rows = await db.query("SELECT faq_question_answer.*,users.uid as uid, users.name as author , users.mail as authorEmail from faq_question_answer LEFT JOIN users ON faq_question_answer.author_id = users.uid WHERE category_id = ?", [cat_id]);
      return rows[0];
    }

    async FaqQuestionByQId(id) {
        const rows = await db.query("SELECT * FROM `faq_question_answer` WHERE id = ?", [id]);
        return rows[0];
    }

    async FaqByVId(vid) {
        const rows = await db.query("SELECT * FROM `faq_questions` WHERE vid = ?", [vid]);
        return rows[0];
    }

    async deleteFAQById(id) {
        const rows = await db.query("DELETE FROM `faq_question_answer` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getUpdateFAQ(req) {     
        const rows = await db.query("UPDATE faq_question_answer SET category_id = ?,author_id = ?,question = ?, answer = ? WHERE id = ?", [
            req.body.category_id,
            req.body.author_id,
            req.body.question,
            req.body.answer,
            req.params.id
        ]);
        return rows[0];
    }

    //Faq Question feedback queries 
    async CreateFaqQuestionFeedback(req) {
        const rows = await db.query('INSERT INTO `faq_question_feedback`(`user_id`,`category_id`,`status`)VALUES(?,?,?)', [
            req.body.user_id,
            req.body.category_id,
            req.body.status
        ]);
        return rows[0];
    }
    
    async UpdateFaqQuestionFeedback(req) {       
        const rows = await db.query("UPDATE faq_question_feedback SET status = ? WHERE user_id = ? AND  question_id = ?", [
            req.body.status,
            req.body.user_id,
            req.body.question_id
        ])
        return rows[0];
    }

    async checkFaqQuestionFeedback(req){
        const rows = await db.query("SELECT *  FROM `faq_question_feedback` WHERE user_id = ? AND question_id = ?",[req.body.user_id, req.body.question_id]);
        return rows[0];
    }

    async FaqQuestionfeedbackCount(req){
        const rows = await db.query("SELECT SUM(CASE WHEN status = 1 THEN 1 END) AS Likes , SUM(CASE WHEN status = 0 THEN 1  END) AS DisLike FROM `faq_question_feedback` WHERE category_id = ?",[req.params.cat_id]);
        return rows[0];
    }

    async faqQuestionStatusByUserIdAndQuestionId(req){
        const rows = await db.query("SELECT *  FROM `faq_question_feedback` WHERE user_id = ? AND question_id = ?",[req.params.user_id, req.params.ques_id]);
        return rows[0];
    }
}