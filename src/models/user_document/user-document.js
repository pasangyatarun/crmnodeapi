import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class UserDocumentModel {

	async documentUpload(req, filename) {
		const rows = await db.query('INSERT INTO `user_documents`(`doc_name`,`created_by`)VALUES(?,?)', [
			filename,
			req.params.uid,
		]);
		return rows[0];
	}

	async userDocumentsByUserId(uid) {
		const rows = await db.query('SELECT customer_id,order_id,contract_id,plan_id,plan_variant_id,plan_name,plan_variant_name,user_personalized_pdf_url from `billwerk_user_customer` WHERE user_id =?', [uid]);
		return rows[0];
	}

	async userDocumentById(id) {
		const rows = await db.query('SELECT * from `user_documents` WHERE id =?', [id]);
		return rows[0];
	}

	async userDocumentByName(name) {
		const rows = await db.query('SELECT * from `user_documents` WHERE doc_name = ?', [name]);
		return rows[0];
	}

	async userspersonalizeDocuments() {
		const rows = await db.query('SELECT * from `billwerk_user_customer` WHERE  user_personalized_pdf_url IS NOT NULL');
		return rows[0];
	}

	async userDocuments() {
		const rows = await db.query('SELECT id ,doc_name,created_by,created_at,updated_at from `user_documents` ORDER BY id DESC');
		return rows[0];
	}

	async userDocumentsDelete(id) {
		const rows = await db.query("DELETE FROM `user_documents` WHERE `id`=?", [id]);
		return rows[0];
	}
}