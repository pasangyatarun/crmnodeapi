import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class UserModel {

    async getAllAssignedCustomers() {
        const rows = await db.conn.promise().query(`SELECT * FROM customer_to_distributor`);
        return rows[0];
    }

    async getCommissionSettingSpId(id) {
        const rows = await db.conn.promise().query(`SELECT * FROM sales_partner_commission WHERE sales_partner_id = ?`, [id]);
        return rows[0];
    }

    async getOrderedCustomers(ids, startDate) {
        let customerIds = ids.join();
        const rows = await db.conn.promise().query(`SELECT billwerk_user_customer.*,users.mail as email ,field_data_field_address.field_address_first_name as firstName,field_data_field_address.field_address_last_name as lastName, field_data_field_address.field_address_organisation_name as society FROM billwerk_user_customer LEFT JOIN users ON billwerk_user_customer.user_id= users.uid LEFT JOIN field_data_field_address ON billwerk_user_customer.user_id= field_data_field_address.entity_id WHERE billwerk_user_customer.user_id IN (` + customerIds + `)`);
        return rows[0];
    }

    async checkCommissionSeted(data, spid) {
        const rows = await db.conn.promise().query(`SELECT * FROM sales_partner_commission WHERE sales_partner_id = ? AND product_id = ?`, [JSON.parse(spid), data.productId]);
        return rows[0];
    }

    async deleteCommissionSeted(spid) {
        const rows = await db.conn.promise().query(`DELETE FROM sales_partner_commission WHERE sales_partner_id = ?`, [JSON.parse(spid)]);
        return rows[0];
    }

    async updateCommissionSetting(data, req) {
        const rows = await db.conn.promise().query("UPDATE sales_partner_commission SET commission_amount_type=?,commission_amount=?,commission_duration_type=?,start_date=? WHERE sales_partner_id = ? AND product_id=?", [
            data.commission_type,
            data.commission_amount,
            data.commission_tenure,
            data.start_date,
            req.params.spid,
            data.productId,
        ]);
        return rows[0];
    }

    async addCommissionSetting(data, req) {
        const rows = await db.conn.promise().query("INSERT INTO `sales_partner_commission`(`sales_partner_id`,`product_id`,`commission_amount_type`,`commission_amount`,`commission_duration_type`,`start_date`)VALUES(?,?,?,?,?,?)", [
            req.params.spid,
            data.productId,
            data.commission_type,
            data.commission_amount,
            data.commission_tenure,
            data.start_date,
        ]);
        return rows[0];
    }

    async userLicenceStatus(req) {
        const rows = await db.conn.promise().query("UPDATE users SET licence_block_status = ? WHERE `uid`=?", [req.body.action, req.params.userid]);
        return rows[0];
    }

    async signupUserSociety(req, userId) {
        const rows = await db.conn.promise().query("INSERT INTO `field_data_field_address`(`entity_id`,`field_address_organisation_name`,`entity_type`)VALUES(?,?,?)", [
            userId,
            req.body.society,
            'user'
        ]);
        return rows[0];
    }

    async selectCustomerToSales(req, id) {
        const rows = await db.conn.promise().query(`SELECT * FROM customer_to_sales WHERE cuid = ? AND sales_id = ?`, [id, req.body.sales_id]);
        return rows[0];
    }

    async selectCustomerToDistributor(req, id) {
        const rows = await db.conn.promise().query(`SELECT * FROM customer_to_distributor WHERE cuid = ? AND distributor_id = ?`, [id, req.body.distributor_id]);
        return rows[0];
    }

    async getAllCustomerByIdPaginate(req) {
        let pageSize = req.params.pagesize ? req.params.pagesize : 10;
        let page = req.params.page ? req.params.page : 1
        let offset = (pageSize * page) - pageSize;

        const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount, customer_to_distributor.id,users.uid,users.name as username,users.mail,users.picture as image,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN customer_to_distributor ON users.uid = customer_to_distributor.cuid LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE customer_to_distributor.distributor_id = ? ORDER BY users.uid DESC LIMIT ` + pageSize + ` OFFSET ` + offset + ``, [req.params.id]);
        return rows[0];
    }

    async getAllCustomerById(req) {
        const rows = await db.conn.promise().query(`SELECT customer_to_distributor.id,users.uid,users.name as username,users.mail,users.picture as image,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN customer_to_distributor ON users.uid = customer_to_distributor.cuid LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE customer_to_distributor.distributor_id = ?`, [req.params.id]);
        return rows[0];
    }
    async getAllCustomersBySales(req) {
        const rows = await db.conn.promise().query(`SELECT customer_to_sales.id,users.uid,users.name as username,users.mail,users.picture as image,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN customer_to_sales ON users.uid = customer_to_sales.cuid LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE customer_to_sales.sales_id = ?`, [req.params.id]);
        return rows[0];
    }

    async getSalesPartnerByCustomer(id) {
        const rows = await db.conn.promise().query(`SELECT customer_to_sales.id,users.uid,users.name as username,users.mail,users.picture as image,users.status,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN customer_to_sales ON users.uid = customer_to_sales.sales_id LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN file_managed ON file_managed.uid = users.uid WHERE customer_to_sales.cuid = ?`, [id]);
        return rows[0];
    }

    async assignCustomerToDistributor(req, userId) {
        const rows = await db.conn.promise().query("INSERT INTO `customer_to_distributor`(`cuid`,`distributor_id`)VALUES(?,?)", [
            userId,
            req.body.distributor_id
        ]);
        return rows[0];
    }
    async assignCustomerToSales(req, userId) {
        const rows = await db.conn.promise().query("INSERT INTO `customer_to_sales`(`cuid`,`sales_id`)VALUES(?,?)", [
            userId,
            req.body.sales_id
        ]);
        return rows[0];
    }

    async getAllcustomer() {
        const rows = await db.conn.promise().query(`SELECT role.rid,role.name as role,users.uid,users.picture as image,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = ?`, ['Customer']);
        return rows[0];
    }

    async getSalesPartnerByPage(req) {
        let param = req.params;
        let search;
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        if (req.query && req.query.search) {
            search = req.query.search
            const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount, role.rid,role.name as role,users.uid,users.picture as image,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = ? AND users.mail LIKE '%` + search + `%' LIMIT ` + pageSize + ` OFFSET ` + offset + ``, ['Sales Partner']);
            return rows[0];
        } else {
            const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount, role.rid,role.name as role,users.uid,users.picture as image,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = ? ORDER BY users.uid DESC LIMIT ` + pageSize + ` OFFSET ` + offset + ``, ['Sales Partner']);
            return rows[0];
        }
    }

    async getAllDistributors() {
        const rows = await db.conn.promise().query(`SELECT role.rid,role.name as role,users.uid,users.picture as image,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = ?`, ['Sales Partner']);
        return rows[0];
    }
    async getAllSalesUsers() {
        const rows = await db.conn.promise().query(`SELECT role.rid,role.name as role,users.uid,users.picture as image,users.name as username,users.mail,users.status,users.created as membersince,users.access,users.contact_number as contactNumber FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = ?`, ['Sales Representative']);
        return rows[0];
    }

    async getUserLoginInfo(uid) {
        const rows = await db.conn.promise().query("SELECT login_info FROM billwerk_user_customer WHERE user_id =  ? AND login_info IS NOT NULL", [uid]);
        return rows[0];
    }

    async getUserRolesData(uid) {
        const rows = await db.conn.promise().query("SELECT users.uid,role.rid,role.cat_id,role.name as role FROM users LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE users.uid =  ?", [uid]);
        return rows[0];
    }

    async getUserRolesDataByAdmin(uid) {
        const rows = await db.conn.promise().query("SELECT * FROM users_roles  WHERE uid =  ?", [uid]);
        return rows[0];
    }

    async getUserDomainsData(uid) {
        const rows = await db.conn.promise().query("SELECT domain.domain_id,domain.sitename FROM users LEFT JOIN users_domain ON users.uid = users_domain.uid LEFT JOIN domain ON domain.domain_id = users_domain.domain_id WHERE users.uid =  ?", [uid]);
        return rows[0];
    }

    async getOldUserStatus(uid) {
        const rows = await db.conn.promise().query("SELECT * FROM licence_history WHERE user_id = ?", [uid]);
        return rows[0];
    }

    async getUserDetail(uid) {
        const rows = await db.conn.promise().query("SELECT users.name,role_category.id AS cat_id,role_category.category_name,users.process_status,users.mail,users.picture,role.rid, role.name as role,field_data_field_address.field_address_organisation_name as society from `users` LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid LEFT JOIN role_category ON role.cat_id = role_category.id WHERE users.uid = ?", [uid]);
        return rows[0];
    }

    async storeResetPasswordToken(email) {
        let dt = new Date().getTime();
        const rows = await db.conn.promise().query("UPDATE users SET reset_password_token = ? WHERE mail = ?", [
            dt,
            email
        ]);
        return rows[0];
    }

    async getUserAddressById(entityId) {
        const rows = await db.conn.promise().query("SELECT field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1 from `field_data_field_address` WHERE entity_id = ?", [entityId]);
        return rows[0];
    }

    async getUserAccountById(entityId) {
        const rows = await db.conn.promise().query("SELECT field_bank_account_value as accountNumber from `field_data_field_bank_account` WHERE entity_id = ?", [entityId]);
        return rows[0];
    }

    async getUserImageById(uId) {
        const rows = await db.conn.promise().query("SELECT filename,uri,status from `file_managed` WHERE uid = ?", [uId]);
        return rows[0];
    }

    async getGuestRole() {
        const rows = await db.conn.promise().query("SELECT * from `role` WHERE name = ?", ['Customer']);
        return rows[0][0];
    }

    async userRegister(req) {
        let dt = new Date().getTime();
        const rows = await db.conn.promise().query("INSERT INTO `users`(`name`,`pass`,`mail`,`signature_format`,`created`,`status`,`init`,`contact_number`)VALUES(?,?,?,?,?,?,?,?)", [
            req.body.email,
            '$S$D8Y50CKl7ABGGwzjSf7kuzyx8sbEYLAEcjs2/mP3sbqQzL6T2Irm',
            req.body.email,
            'plain_text',
            dt,
            0,
            req.body.email,
            req.body.contactNumber
        ]);
        return rows[0];
    }

    async signupUser(req) {
        let domy = 'notnull';
        let domyNum = 0;
        let dt = new Date().getTime();
        const rows = await db.conn.promise().query("INSERT INTO `users`(`name`,`pass`,`mail`,`signature_format`,`created`,`status`,`init`)VALUES(?,?,?,?,?,?,?)", [
            req.body.email,
            '$S$D8Y50CKl7ABGGwzjSf7kuzyx8sbEYLAEcjs2/mP3sbqQzL6T2Irm',
            req.body.email,
            'plain_text',
            dt,
            1,
            req.body.email
        ]);
        return rows[0];
    }

    async signupTrialUser(req) {
        let domy = 'notnull';
        let domyNum = 0;
        let dt = new Date().getTime();
        console.log(" req.body.additional_key--", req.body.additional_key)
        const rows = await db.conn.promise().query("INSERT INTO `users`(`name`,`pass`,`mail`,`signature_format`,`created`,`status`,`init`,`additional_key`)VALUES(?,?,?,?,?,?,?,?)", [
            req.body.email,
            '$S$D8Y50CKl7ABGGwzjSf7kuzyx8sbEYLAEcjs2/mP3sbqQzL6T2Irm',
            req.body.email,
            'plain_text',
            dt,
            1,
            req.body.email,
            req.body.additional_key
        ]);
        return rows[0];
    }

    async checkEmailExists(email) {
        const rows = await db.conn.promise().query("SELECT mail from `users` WHERE mail=?", [email]);
        return rows[0][0];
    }

    async checkUserNameExists(userName) {
        const rows = await db.conn.promise().query("SELECT name,uid from `users` WHERE name = ?", [userName]);
        return rows[0];
    }
    async register(req, hashPass, userImg) {
        let dt = new Date().getTime();
        const rows = await db.conn.promise().query("INSERT INTO `users`(`auther_id`,`name`,`pass`,`mail`,`signature_format`,`created`,`status`,`init`,`contact_number`,`picture`)VALUES(?,?,?,?,?,?,?,?,?,?)", [
            req.body.auther_id,
            req.body.userName,
            hashPass,
            req.body.email,
            'plain_text',
            dt,
            req.body.status,
            req.body.email,
            req.body.contactNumber,
            userImg
        ]);
        return rows[0];
    }

    async registerUserRole(userId, rid) {
        const rows = await db.conn.promise().query("INSERT INTO `users_roles`(`uid`,`rid`)VALUES(?,?)", [
            userId,
            rid
        ]);
        return rows[0];
    }

    async registerUserDomain(userId, domain_id) {
        const rows = await db.conn.promise().query("INSERT INTO `users_domain`(`uid`,`domain_id`)VALUES(?,?)", [
            userId,
            domain_id
        ]);
        return rows[0];
    }

    async getUserPassword(uid) {
        const rows = await db.conn.promise().query("SELECT * from users WHERE uid = ?", [
            uid
        ]);
        return rows[0];
    }

    async login(req) {
        const rows = await db.conn.promise().query("SELECT * from users WHERE mail = ? OR name = ?", [
            req.body.email,
            req.body.email
        ]);
        return rows[0];
    }
    async getRoleDetails(uid) {
        const rows = await db.conn.promise().query(`SELECT role.rid,role_category.id AS cat_id,role_category.category_name, role.name,field_data_field_address.field_address_organisation_name as society FROM role LEFT JOIN users_roles ON role.rid = users_roles.rid LEFT JOIN users ON users.uid = users_roles.uid LEFT JOIN field_data_field_address ON users.uid = field_data_field_address.entity_id LEFT JOIN role_category ON role.cat_id = role_category.id WHERE users.uid = ?`, [uid]);
        return rows[0];
    }

    async getOlduserStatus(uid) {
        const rows = await db.conn.promise().query(`SELECT * FROM licence_history WHERE user_id = ?`, [uid]);
        return rows[0];
    }

    async getRolePermission(rid) {
        const rows = await db.conn.promise().query(`SELECT id,permission,module FROM role_permission WHERE rid = ?`, [rid]);
        return rows[0];
    }

    async getOneUser() {
        const rows = await db.conn.promise().query("SELECT * from users limit 0,1");
        return rows[0];
    }

    async getUsersbySearch(param) {
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        let search = param.search;
        const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount,users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
        field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
        file_managed ON file_managed.uid = users.uid WHERE users.mail LIKE '%`+ search + `%' OR field_data_field_address.field_address_organisation_name LIKE '%` + search + `%' limit ` + pageSize + `  OFFSET ` + offset + ``);
        return rows[0];
    }

    async getUsersbyPageSize(param) {
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        const rows = await db.conn.promise().query(`SELECT COUNT(*) OVER() as totalCount,users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
        field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
        file_managed ON file_managed.uid = users.uid ORDER BY uid DESC limit `+ pageSize + `  OFFSET ` + offset + ``);
        return rows[0];
    }

    async getAllUsers() {
        // const rows = await db.conn.promise().query(`SELECT users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
        // field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
        // file_managed ON file_managed.uid = users.uid`);
        const rows = await db.conn.promise().query(`SELECT users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus,users.uid,role.rid,role.cat_id,role.name as role FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
        field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
        file_managed ON file_managed.uid = users.uid
        LEFT JOIN users_roles ON users.uid = users_roles.uid LEFT JOIN role ON role.rid = users_roles.rid WHERE role.name = 'Customer'`);
        return rows[0];
    }

    async getUserByCustomerId(customer_id) {
        const rows = await db.conn.promise().query(`SELECT user_id FROM billwerk_user_customer WHERE customer_id = ?`, [customer_id]);
        return rows[0];
    }

    async getUserById(id) {
        const rows = await db.conn.promise().query(`SELECT users.uid,users.auther_id,users.name as username,users.mail,users.status,users.licence_block_status,users.support_access as toogleAccess,users.created as membersince,users.access,users.contact_number as contactNumber,users.picture as image, field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
        field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
        file_managed ON file_managed.uid = users.uid WHERE users.uid = ?`, [id]);
        return rows[0];
    }

    async getUserByEmail(email) {
        const rows = await db.conn.promise().query("SELECT * FROM `users` WHERE mail = ?", [email]);
        return rows[0];
    }

    async updateUser(req, hashPass) {
        const rows = await db.conn.promise().query("UPDATE users SET name = ?,mail= ?,pass= ?,theme=?,signature= ?,signature_format=?,status=?,language=?,picture= ? WHERE uid = ?", [
            req.body.userName,
            req.body.email,
            hashPass,
            req.body.theme,
            req.body.signature,
            req.body.signature_format,
            0,
            req.body.language,
            req.body.picture,
            req.params.id
        ]);
        return rows[0];
    }

    async getUserDelete(id) {
        const rows = await db.conn.promise().query("DELETE FROM `users` WHERE `uid`=?", [id]);
        return rows[0];
    }

    async getDeleteUser(id) {
        const rows = await db.conn.promise().query("UPDATE users SET status = ? WHERE `uid`=?", [0, id]);
        return rows[0];
    }

    async getDeleteSalesCustomer(id) {
        const rows = await db.conn.promise().query("DELETE FROM `customer_to_sales` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getDeleteDistributorCustomer(id) {
        const rows = await db.conn.promise().query("DELETE FROM `customer_to_distributor` WHERE `id`=?", [id]);
        return rows[0];
    }

    async updateUserStatus(email, pswd, dt, processStatus) {
        const rows = await db.conn.promise().query("UPDATE users SET pass = ?,status = ?,access = ?,login = ?,process_status = ? WHERE mail = ?", [
            pswd,
            1,
            dt,
            dt,
            processStatus,
            email
        ]);
        return rows[0];
    }

    async updateTrialUserStatus(email, dt) {
        const rows = await db.conn.promise().query("UPDATE users SET status = ?,access = ? WHERE mail = ?", [
            1,
            dt,
            email
        ]);
        return rows[0];
    }

    async updateUserPassword(email, pswd) {
        const rows = await db.conn.promise().query("UPDATE users SET pass = ? , status = 1,reset_password_token = null WHERE mail = ?", [
            pswd,
            email
        ]);
        return rows[0];
    }

    async changeUserPassword(uid, pswd) {
        const rows = await db.conn.promise().query("UPDATE users SET pass = ? WHERE uid = ?", [
            pswd,
            uid
        ]);
        return rows[0];
    }

    async getAddressCreate(req, uid) {
        const rows = await db.conn.promise().query('INSERT INTO `field_data_field_address`(`entity_id`,`field_address_country`,`field_address_name_line`,`field_address_first_name`,`field_address_last_name`,`field_address_organisation_name`,`field_address_locality`,`field_address_postal_code`,`field_address_thoroughfare`,`entity_type`)VALUES(?,?,?,?,?,?,?,?,?,?)', [
            uid,
            req.body.country,
            req.body.firstName + req.body.lastName,
            req.body.firstName,
            req.body.lastName,
            req.body.society,
            req.body.place,
            req.body.zipcode,
            req.body.address,
            'user'
        ]);
        return rows[0];
    }

    async getAddressAdd(req, uid) {
        const rows = await db.conn.promise().query('INSERT INTO `field_data_field_address`(`entity_id`,`field_address_country`,`field_address_name_line`,`field_address_first_name`,`field_address_last_name`,`field_address_organisation_name`,`field_address_locality`,`field_address_postal_code`,`field_address_thoroughfare`,`field_address_second`,`entity_type`)VALUES(?,?,?,?,?,?,?,?,?,?,?)', [
            uid,
            req.body.country,
            req.body.firstName + req.body.lastName,
            req.body.firstName,
            req.body.lastName,
            req.body.society,
            req.body.place,
            req.body.zipcode,
            req.body.addressLine1,
            req.body.addressLine2,
            'user'
        ]);
        return rows[0];
    }

    async getAccountAdd(req, uid) {
        const rows = await db.conn.promise().query('INSERT INTO `field_data_field_bank_iban`(`entity_id`,`field_bank_iban_value`,`entity_type`)VALUES(?,?,?)', [
            uid,
            req.body.iban,
            'user'
        ]);
        return rows[0];
    }

    async AccountUpdate(req) {
        const rows = await db.conn.promise().query("UPDATE field_data_field_bank_iban SET field_bank_iban_value = ?,entity_type = ? WHERE entity_id = ?", [
            req.body.iban,
            'user',
            req.params.uid
        ]);
        return rows[0];
    }

    async getImageAdd(req, uid) {
        const rows = await db.conn.promise().query('INSERT INTO `file_managed`(`uid`,`filename`,`uri`,`filemime`,`filesize`,`status`,`timestamp`,`type`)VALUES(?,?,?,?,?,?,?,?)', [
            uid,
            req.body.account,
            'user'
        ]);
        return rows[0];
    }

    async getAllEXusers() {
        const rows = await db.conn.promise().query(`SELECT uid,name,created from users`);
        return rows[0];

    }

    async getbillworkUser() {
        const rows = await db.conn.promise().query(`SELECT 	user_id,commit_response from billwerk_user_customer where commit_response IS NOT NULL `);
        return rows[0];
    }

    async getUserOrders(id) {
        const rows = await db.conn.promise().query(`SELECT * from billwerk_user_customer where user_id = ?`, [id]);
        return rows[0];
    }

    async getworkUser(id) {
        const rows = await db.conn.promise().query("SELECT uid,name,created from users WHERE `user_id` != ?", [id]);
        return rows[0];
    }

    async getUser() {
        const rows = await db.conn.promise().query("SELECT DISTINCT user_id ,type,login_info,commit_response FROM `billwerk_user_customer` WHERE type = 1 AND login_info IS NOT NULL ");
        return rows[0];
    }

    async getUserRoleById(id) {
        const rows = await db.conn.promise().query("SELECT * FROM `users_roles` WHERE uid =? ", [id]);
        return rows[0];
    }

    async getAllClubCustomersForAdminPaginate(param) {
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        const rows = await db.conn.promise().query("SELECT DISTINCT user_id,COUNT(*) OVER() as totalCount FROM `billwerk_user_customer` WHERE type IN (1,3) AND login_info IS NOT NULL AND login_info != ? ORDER BY user_id DESC LIMIT " + pageSize + " OFFSET " + offset + "", ['OK']);
        return rows[0];
    }

    async getAssignedCustomersByPageId(id, param) {
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        const rows = await db.conn.promise().query("SELECT *,COUNT(*) OVER() as totalCount FROM `customer_to_distributor` WHERE 	distributor_id	 =? LIMIT " + pageSize + " OFFSET " + offset + "", [id]);
        return rows[0];
    }

    async getAssignedCustomersById(id) {
        const rows = await db.conn.promise().query("SELECT * FROM `customer_to_distributor` WHERE 	distributor_id	 =? ", [id]);
        return rows[0];
    }

    async getClubCustomersById(id) {
        let x = id.join();
        const rows = await db.conn.promise().query("SELECT DISTINCT user_id FROM `billwerk_user_customer` WHERE user_id	IN (" + x + ") AND type IN (1,3) AND login_info IS NOT NULL AND login_info != ? ORDER BY user_id ASC", ['OK']);
        return rows[0];
    }

    async getAllClubCustomersForAdmin() {
        const rows = await db.conn.promise().query("SELECT DISTINCT user_id FROM `billwerk_user_customer` WHERE type IN (1,3) AND login_info IS NOT NULL AND login_info != ? ORDER BY user_id ASC", ['OK']);
        return rows[0];
    }
    async getUserByAuthorId(id) {
        const rows = await db.conn.promise().query(`SELECT users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
       field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
       file_managed ON file_managed.uid = users.uid  WHERE users.auther_id=?`, [id]);
        return rows[0];
    }

    async getSalesCustomersById(id) {
        const rows = await db.conn.promise().query(`SELECT users.uid,users.name as username,users.mail,users.status,users.picture as image,users.created as membersince,users.access,users.contact_number as contactNumber,field_address_country as country,field_address_first_name as firstName,field_address_last_name as lastName,field_address_organisation_name as society,field_address_locality as place,field_address_postal_code as zipcode,field_address_thoroughfare as addressLine1,field_address_second as addressLine2,field_bank_account_value as accountNumber,field_bank_code as bankCode,field_bank_iban_value as ibanValue,filename,uri, file_managed.status as imageStatus FROM users LEFT JOIN field_data_field_address ON field_data_field_address.entity_id = users.uid LEFT JOIN field_data_field_bank_account ON field_data_field_bank_account.entity_id = users.uid LEFT JOIN 
       field_data_field_bank_iban ON field_data_field_bank_iban.entity_id = users.uid LEFT JOIN 
       file_managed ON file_managed.uid = users.uid LEFT JOIN customer_to_distributor ON users.uid = customer_to_distributor.cuid  WHERE 	customer_to_distributor.distributor_id	 =?`, [id]);
        return rows[0];
    }
    async getAuthorIdRole(id) {
        const rows = await db.conn.promise().query(`SELECT rid from users_roles  WHERE uid =?`, [id]);
        return rows[0];
    }

    async getCustomerSalesPartnerData(cuid) {
        const rows = await db.conn.promise().query(`SELECT * from users_roles LEFT JOIN users
         ON users_roles.uid = users.uid WHERE users_roles.uid =? AND users_roles.rid = ?`, [cuid, 6]);
        return rows[0];
    }

    async getClubUserDetails(id, req) {
        if (req.query && req.query.search) {
            let search = req.query.search
            let x = id.join();
            const rows = await db.conn.promise().query("SELECT users.uid as uid,users.auther_id as author,users.mail as email,users.created as memberSince,users.status,users.picture as profile, field_data_field_address.field_address_first_name as firstName,field_data_field_address.field_address_last_name as lastName,field_data_field_address.field_address_organisation_name as clubName, field_data_field_address.entity_type as type  from users LEFT JOIN field_data_field_address ON users.uid = field_data_field_address.entity_id WHERE uid IN  (" + x + ") AND (users.mail LIKE '%" + search + "%')");
            return rows[0];
        } else {

            let x = id.join();
            const rows = await db.conn.promise().query("SELECT users.uid as uid,users.auther_id as author,users.mail as email,users.created as memberSince,users.status,users.picture as profile, field_data_field_address.field_address_first_name as firstName,field_data_field_address.field_address_last_name as lastName,field_data_field_address.field_address_organisation_name as clubName, field_data_field_address.entity_type as type  from users LEFT JOIN field_data_field_address ON users.uid = field_data_field_address.entity_id WHERE uid IN  (" + x + ") ");
            return rows[0];
        }
    }
}