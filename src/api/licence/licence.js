import express from 'express';
import LicenceController from '../../controllers/licence/licence';

let router = express.Router();
let licenceController = new LicenceController();

router.put('/club-licence-block/:userid', licenceController.licenceClubBlock.bind(licenceController));

router.get('/licences', licenceController.licences.bind(licenceController));
router.get('/licencebyid/:id', licenceController.licenceById.bind(licenceController));
router.get('/licencebylicence/:title', licenceController.licenceByLicence.bind(licenceController));
router.post('/licencecreate', licenceController.licenceCreate.bind(licenceController));
router.put('/licenceupdate/:id', licenceController.updateLicence.bind(licenceController));
router.delete('/licencedelete/:id', licenceController.deleteLicence.bind(licenceController));

export default router;