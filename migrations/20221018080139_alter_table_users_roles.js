exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('users_roles', function(t) {
        t.integer('uid').unsigned().notNull().defaultTo(0).comment('Primary Key: users.uid for user.').alter();
        t.integer('rid').unsigned().notNull().defaultTo(0).comment('Primary Key: role.rid for role.').alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('users_roles', function(t) {
        
    });
 };