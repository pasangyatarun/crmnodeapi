import async from 'async';
import crypto from 'crypto';
import dotenv from 'dotenv'
dotenv.config();
import TicketRequestModel from '../../models/service_support/ticket_request';
import responseFormat from '../../core/response-format';
import AWSHandler from '../../core/AWSHander';
import TicketRequestValidation from '../../validators/service_support/ticket_request';
import TicketRequestCommentValidation from '../../validators/service_support/ticket_comment';
import sendEmail from '../../core/nodemailer';
import EmailTemplateModel from '../../models/email_template/email-template';
let ticketRequestModel = new TicketRequestModel();
let ticketRequestValidation = new TicketRequestValidation();
let ticketRequestCommentValidation = new TicketRequestCommentValidation();
let emailTemplateModel = new EmailTemplateModel();

export default class TicketRequestController {

  /**
  * Function to create new ticket 
  * @author  MangoIt Solutions
  * @param   {object} with ticket details
  * @return  {message} success message
  */
  createTicket(req, res, next) {
    let file;
    let s3URLofImage;
    let DataId;
    let userDetails;
    let CatDeatils;
    let ticket_number;
    let templateData;
    let mailOptions;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = ticketRequestValidation.requestValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          ticketRequestModel.checkRequestExist(req).then((results) => {
            if (results.length > 0) {
              response = responseFormat.getResponseMessageByCodes(['request:requestNameExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          if (req.file) {
            file = req.file;
            AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
              s3URLofImage = result.Location;
              done();
            }).catch(function (err) {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          } else {
            done();
          }
        },
        function (done) {
          ticket_number = crypto.randomBytes(15).toString('hex');
          ticketRequestModel.checkticketNumberExist(ticket_number).then((results) => {
            if (results.length > 0) {
              ticket_number = crypto.randomBytes(15).toString('hex');
              done();
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.createRequest(req, s3URLofImage, ticket_number).then((results) => {
            if (results.affectedRows > 0) {
              DataId = results.insertId;
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.getCategoryDetails(req).then((catDetails) => {
            CatDeatils = catDetails;
            if (catDetails.length > 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.getRequestDetails(req.params.req_user_id).then((results) => {
            userDetails = results;
            if (results.length > 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Customer ticket').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let date = new Date().toLocaleString();
          let link = process.env.ticketDetailsLink + DataId;
          //         mailOptions = {
          //             to: ["user123@mailinator.com", process.env.support],
          //             subject: templateData[0].subject,
          //             html: `<!doctype html>
          // <html lang="en-US">

          // <head>
          // <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
          // <title>Reset Password Email</title>
          // <meta name="description" content="Reset Password Email Template.">
          // <style type="text/css">
          // a:hover {text-decoration: underline !important;}
          // </style>
          // </head>

          // <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
          // <!--100% body table-->
          // <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
          // style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
          // <tr>
          // <td>
          // <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
          // align="center" cellpadding="0" cellspacing="0">
          // <tr>
          // <td style="height:80px;">&nbsp;</td>
          // </tr>
          // <tr>
          // <td style="text-align:center;">
          // <a href="#" title="logo" target="_blank">
          // <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
          // </a>
          // </td>
          // </tr>
          // <tr>
          // <td style="height:20px;">&nbsp;</td>
          // </tr>
          // <tr>
          // <td>
          // <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
          // style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
          // <tr>
          // <td style="height:40px;">&nbsp;</td>
          // </tr>
          // <tr>
          // <td style="padding:0 35px;">
          // <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         `+ templateData[0].template_body + `:<br>
          //                                         </p><br><br>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         Ticket Number : `+ ticket_number + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                          Requested User EMail: `+ userDetails[0].email + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         Club Name : `+ userDetails[0].ClubName + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                        Category : `+ CatDeatils[0].Category + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         Request Title: `+ req.body.name + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         Request Discription: `+ req.body.description + `<br>
          //                                         </p>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                         Request Creation Date: `+ date + `<br><br>
          //                                         </p>
          //                                         <a href="`+ link + `">` + link + `</a>
          //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
          //                                         <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
          //                                         <!-- <a href="javascript:void(0);"
          //                                         style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
          //                                             Password</a> -->
          //                                             </td>
          //                                             </tr>
          //                                             <tr>
          //                                             <td style="height:40px;">&nbsp;</td>
          //                                             </tr>
          //                                             </table>
          //                                             </td>
          //                                             <tr>
          //                                             <td style="height:20px;">&nbsp;</td>
          //                                             </tr>
          //                                             <tr>
          //                                             <td style="text-align:center;">
          //                                             <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
          //                                             </td>
          //                                             </tr>
          //                                             <tr>
          //                                             <td style="height:80px;">&nbsp;</td>
          //                                             </tr>
          //                                             </table>
          //             </td>
          //         </tr>
          //     </table>
          //     <!--/100% body table-->
          //     </body>

          //     </html>`,
          // };

          mailOptions = {
            to: ["user123@mailinator.com", process.env.support],
            subject: 'Neues Ticket erstellt',
            html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                    <head>
                                    <!--[if (gte mso 9)|(IE)]>
                                      <xml>
                                        <o:OfficeDocumentSettings>
                                        <o:AllowPNG/>
                                        <o:PixelsPerInch>96</o:PixelsPerInch>
                                      </o:OfficeDocumentSettings>
                                    </xml>
                                    <![endif]-->
                                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                                    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                                    <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                                    <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                                    <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                                    <meta name="color-scheme" content="only">
                                    <title></title>
                                    
                                    <link rel="preconnect" href="https://fonts.gstatic.com">
                                    <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                                    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                                    
                                    <style type="text/css">
                                    /*Basics*/
                                    body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                                    table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                    table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                                    td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                                    td p {margin:0; padding:0;}
                                    td div {margin:0; padding:0;}
                                    td a {text-decoration:none; color: inherit;} 
                                    /*Outlook*/
                                    .ExternalClass {width: 100%;}
                                    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                                    .ReadMsgBody {width:100%; background-color: #ffffff;}
                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                                    /*Gmail blue links*/
                                    u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                                    /*Buttons fix*/
                                    .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                                    .yshortcuts a {border-bottom:none !important;
                                    
                                    .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                                    /*Responsive*/
                                    @media screen and (max-width: 639px) {
                                      table.row {width: 100%!important;max-width: 100%!important;}
                                      td.row {width: 100%!important;max-width: 100%!important;}
                                      .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                                      .center-float {float: none!important;margin:auto!important;}
                                      .center-text{text-align: center!important;}
                                      .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                                      .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                                      .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                                      .hide-mobile {display: none!important;}
                                      .menu-container {text-align: center !important;}
                                      .autoheight {height: auto!important;}
                                      .m-padding-10 {margin: 10px 0!important;}
                                      .m-padding-15 {margin: 15px 0!important;}
                                      .m-padding-20 {margin: 20px 0!important;}
                                      .m-padding-30 {margin: 30px 0!important;}
                                      .m-padding-40 {margin: 40px 0!important;}
                                      .m-padding-50 {margin: 50px 0!important;}
                                      .m-padding-60 {margin: 60px 0!important;}
                                      .m-padding-top10 {margin: 30px 0 0 0!important;}
                                      .m-padding-top15 {margin: 15px 0 0 0!important;}
                                      .m-padding-top20 {margin: 20px 0 0 0!important;}
                                      .m-padding-top30 {margin: 30px 0 0 0!important;}
                                      .m-padding-top40 {margin: 40px 0 0 0!important;}
                                      .m-padding-top50 {margin: 50px 0 0 0!important;}
                                      .m-padding-top60 {margin: 60px 0 0 0!important;}
                                      .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                                      .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                                      .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                                      .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                                      .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                                      .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                                      .center-on-mobile {text-align: center!important;}
                                    }
                                    </style>
                                    </head>
                                    
                                    <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                                    
                                    <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                                    
                                    <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                                      <tr><!-- Outer Table -->
                                        <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                                      <!-- Preheader -->
                                      <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="center" class="center-text">
                                          <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <!-- Preheader -->
                                    </table>
                                    
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                                      <!-- simpli-header-1 -->
                                      <tr>
                                        <td align="center">
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                                      <!-- bg-image -->
                                      <tr>
                                        <td align="center" style="border-radius: 36px;">
                                    
                                    <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                                    <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                                    <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                                    
                                    <div>
                                    <!-- simpli-header-bg-image -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                      <tr>
                                        <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                                    
                                    <!-- Content -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                                      <tr>
                                        <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                                    
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                          <tr>
                                            <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                          </tr>
                                        </table>
                                    
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Content -->
                                    
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- simpli-header-bg-image -->
                                    </div>
                                    
                                    <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                    
                                        </td>
                                      </tr>
                                      <!-- bg-image -->
                                    </table>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                                      <!-- basic-info -->
                                      <tr>
                                        <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                          <!-- content -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                                  <singleline>
                                                    <div mc:edit Simpli>
                                                        NEUE ANFRAGE
                                                    </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:24px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                  <singleline>
                                                    <div mc:edit Simpli>
                                                        Kunde: `+ userDetails[0].email + `
                                                    </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                  <singleline>
                                                    <div mc:edit Simpli>
                                                      TicketNumber:  `+ ticket_number + ` <br>
                                                      Mail:         `+ userDetails[0].email + `  <br>
                                                      Club Name:     `+ userDetails[0].ClubName + ` <br>
                                                      Category:      `+ CatDeatils[0].Category + ` <br>
                                                      Request Title:   `+ req.body.name + ` <br>
                                                      Date:          `+ date + ` <br>           
                                                    </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!-- Button -->
                                                <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                                  <tr>
                                                    <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                                <!--[if (gte mso 9)|(IE)]>
                                                  <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                      <td align="center" width="35"></td>
                                                      <td align="center" height="50" style="height:50px;">
                                                      <![endif]-->
                                                        <singleline>
                                                          <a href=` + link + ` target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                                        </singleline>
                                                      <!--[if (gte mso 9)|(IE)]>
                                                      </td>
                                                      <td align="center" width="35"></td>
                                                    </tr>
                                                  </table>
                                                <![endif]-->
                                                    </td>
                                                  </tr>
                                                </table>
                                                <!-- Buttons -->
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                            </tr>
                                          </table>
                                          <!-- content -->
                                        </td>
                                      </tr>
                                      <!-- basic-info -->
                                    </table>
                                    
                                        </td>
                                      </tr>
                                      <!-- simpli-header-1 -->
                                    </table>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                      <!-- simpli-footer -->
                                      <tr>
                                        <td align="center">
                                          
                                    <!-- Content -->
                                    
                                      <tr>
                                        <td align="center">
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                                <multiline>
                                                  <div mc:edit Simpli>
                                                    <br />
                                                    Tineon Aktiengesellschaft <br />
                                                    Uferpromenade 5, 88709 Meersburg <br />
                                                    Registergericht Freiburg HRB 710927 <br />
                                                    Vorstand: Jean-Claude Parent <br />
                                                    Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                                    USt.-IdNr. DE203912818 <br />
                                                  </div>
                                                </multiline>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="center" class="center-text">
                                          <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                                      </tr>
                                    </table>
                                    <!-- Content -->
                                    
                                        </td>
                                      </tr>
                                      <!-- simpli-footer -->
                                    </table>
                                    
                                        </td>
                                      </tr><!-- Outer-Table -->
                                    </table>
                                    
                                    </body>
                                    </html>
                                    `
          }
          sendEmail(mailOptions).then((sentdata) => {
            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
              res.status(200).json(response);
            } else {
              done();
            }
          })

        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Reply from Support User').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let date = new Date().toLocaleString();
          let link = process.env.ticketDetailsLink + DataId;
          // mailOptions = {
          //     to: [userDetails[0].email, process.env.support],
          //     subject: templateData[0].subject,
          //     html: `<!doctype html>
          //         <html lang="en-US">

          //         <head>
          //         <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
          //         <title>Reset Password Email</title>
          //         <meta name="description" content="Reset Password Email Template.">
          //         <style type="text/css">
          //         a:hover {text-decoration: underline !important;}
          //         </style>
          //         </head>

          //         <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
          //         <!--100% body table-->
          //         <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
          //         style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
          //         <tr>
          //         <td>
          //         <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
          //         align="center" cellpadding="0" cellspacing="0">
          //         <tr>
          //         <td style="height:80px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td style="text-align:center;">
          //         <a href="#" title="logo" target="_blank">
          //         <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
          //         </a>
          //         </td>
          //         </tr>
          //         <tr>
          //         <td style="height:20px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td>
          //         <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
          //         style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
          //         <tr>
          //         <td style="height:40px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td style="padding:0 35px;">
          //         <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

          //                                                 <h3>Dear Customer,</h3>

          //                                                 <h4>Thank you for your Request. We will acknowledge you as soon as possible.</h4>
          //                                                <h4>Below are your request Details:<br><br>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 Ticket Number : `+ ticket_number + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                  Requested User EMail: `+ userDetails[0].email + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 Club Name : `+ userDetails[0].ClubName + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                Category : `+ CatDeatils[0].Category + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 Request Title: `+ req.body.name + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 Request Discription: `+ req.body.description + `<br>
          //                                                 </p>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 Request Creation Date: `+ date + `<br><br>
          //                                                 </p>
          //                                                 <a href="`+ link + `">` + link + `</a>

          //                                                 <h3>Thanks&Regards,</h3>
          //                                                     <h3> Support</h3>
          //                                                 <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
          //                                                 <!-- <a href="javascript:void(0);"
          //                                                 style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
          //                                                     Password</a> -->
          //                                                     </td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="height:40px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     </table>
          //                                                     </td>
          //                                                     <tr>
          //                                                     <td style="height:20px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="text-align:center;">
          //                                                     <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
          //                                                     </td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="height:80px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     </table>
          //                     </td>
          //                 </tr>
          //             </table>
          //             <!--/100% body table-->
          //             </body>

          //             </html>`,


          // };
          mailOptions = {
            to: [userDetails[0].email, process.env.support],
            subject: 'Antwort von Support',
            html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                      <head>
                      <!--[if (gte mso 9)|(IE)]>
                        <xml>
                          <o:OfficeDocumentSettings>
                          <o:AllowPNG/>
                          <o:PixelsPerInch>96</o:PixelsPerInch>
                        </o:OfficeDocumentSettings>
                      </xml>
                      <![endif]-->
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                      <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                      <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                      <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                      <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                      <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                      <meta name="color-scheme" content="only">
                      <title></title>
                      
                      <link rel="preconnect" href="https://fonts.gstatic.com">
                      <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                      
                      <style type="text/css">
                      /*Basics*/
                      body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                      table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                      table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                      td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                      td p {margin:0; padding:0;}
                      td div {margin:0; padding:0;}
                      td a {text-decoration:none; color: inherit;} 
                      /*Outlook*/
                      .ExternalClass {width: 100%;}
                      .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                      .ReadMsgBody {width:100%; background-color: #ffffff;}
                      /* iOS BLUE LINKS */
                      a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                      /*Gmail blue links*/
                      u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                      /*Buttons fix*/
                      .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                      .yshortcuts a {border-bottom:none !important;
                      
                      .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                      /*Responsive*/
                      @media screen and (max-width: 639px) {
                        table.row {width: 100%!important;max-width: 100%!important;}
                        td.row {width: 100%!important;max-width: 100%!important;}
                        .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                        .center-float {float: none!important;margin:auto!important;}
                        .center-text{text-align: center!important;}
                        .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                        .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                        .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                        .hide-mobile {display: none!important;}
                        .menu-container {text-align: center !important;}
                        .autoheight {height: auto!important;}
                        .m-padding-10 {margin: 10px 0!important;}
                        .m-padding-15 {margin: 15px 0!important;}
                        .m-padding-20 {margin: 20px 0!important;}
                        .m-padding-30 {margin: 30px 0!important;}
                        .m-padding-40 {margin: 40px 0!important;}
                        .m-padding-50 {margin: 50px 0!important;}
                        .m-padding-60 {margin: 60px 0!important;}
                        .m-padding-top10 {margin: 30px 0 0 0!important;}
                        .m-padding-top15 {margin: 15px 0 0 0!important;}
                        .m-padding-top20 {margin: 20px 0 0 0!important;}
                        .m-padding-top30 {margin: 30px 0 0 0!important;}
                        .m-padding-top40 {margin: 40px 0 0 0!important;}
                        .m-padding-top50 {margin: 50px 0 0 0!important;}
                        .m-padding-top60 {margin: 60px 0 0 0!important;}
                        .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                        .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                        .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                        .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                        .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                        .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                        .center-on-mobile {text-align: center!important;}
                      }
                      </style>
                      </head>
                      
                      <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                      
                      <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                      
                      <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                        <tr><!-- Outer Table -->
                          <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                        <!-- Preheader -->
                        <tr>
                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="center-text">
                            <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                          </td>
                        </tr>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <!-- Preheader -->
                      </table>
                      
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                        <!-- simpli-header-1 -->
                        <tr>
                          <td align="center">
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                        <!-- bg-image -->
                        <tr>
                          <td align="center" style="border-radius: 36px;">
                      
                      <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                      <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                      <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                      
                      <div>
                      <!-- simpli-header-bg-image -->
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <tr>
                          <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                      
                      <!-- Content -->
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                        <tr>
                          <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                      
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                            <tr>
                              <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                            </tr>
                          </table>
                      
                          </td>
                        </tr>
                      </table>
                      <!-- Content -->
                      
                          </td>
                        </tr>
                      </table>
                      <!-- simpli-header-bg-image -->
                      </div>
                      
                      <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                      
                          </td>
                        </tr>
                        <!-- bg-image -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                        <!-- basic-info -->
                        <tr>
                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                            <!-- content -->
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          NEUE ANFRAGE
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:24px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          Kunde: `+ userDetails[0].email + `
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                        TicketNumber:  `+ ticket_number + ` <br>
                                        Mail:         `+ userDetails[0].email + `  <br>
                                        Club Name:     `+ userDetails[0].ClubName + ` <br>
                                        Category:      `+ CatDeatils[0].Category + ` <br>
                                        Request Title:   `+ req.body.name + ` <br>
                                        Date:          `+ date + ` <br>           
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <!-- Button -->
                                  <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                    <tr>
                                      <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                  <!--[if (gte mso 9)|(IE)]>
                                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                                      <tr>
                                        <td align="center" width="35"></td>
                                        <td align="center" height="50" style="height:50px;">
                                        <![endif]-->
                                          <singleline>
                                            <a href=` + link + ` target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                          </singleline>
                                        <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                        <td align="center" width="35"></td>
                                      </tr>
                                    </table>
                                  <![endif]-->
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- Buttons -->
                                </td>
                              </tr>
                              <tr>
                                <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                              </tr>
                            </table>
                            <!-- content -->
                          </td>
                        </tr>
                        <!-- basic-info -->
                      </table>
                      
                          </td>
                        </tr>
                        <!-- simpli-header-1 -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                        <!-- simpli-footer -->
                        <tr>
                          <td align="center">
                            
                      <!-- Content -->
                      
                        <tr>
                          <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                  <multiline>
                                    <div mc:edit Simpli>
                                      <br />
                                      Tineon Aktiengesellschaft <br />
                                      Uferpromenade 5, 88709 Meersburg <br />
                                      Registergericht Freiburg HRB 710927 <br />
                                      Vorstand: Jean-Claude Parent <br />
                                      Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                      USt.-IdNr. DE203912818 <br />
                                    </div>
                                  </multiline>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="center-text">
                            <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                          </td>
                        </tr>
                        <tr>
                          <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                        </tr>
                      </table>
                      <!-- Content -->
                      
                          </td>
                        </tr>
                        <!-- simpli-footer -->
                      </table>
                      
                          </td>
                        </tr><!-- Outer-Table -->
                      </table>
                      
                      </body>
                      </html>
                      `
          };
          sendEmail(mailOptions).then((sentdata) => {
            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestCreatedAndEmailSend']);
              res.status(200).json(response);
            }
          })

        }
      ])
    }
  }

  /**
  * Function to get tickets raised by user 
  * @author  MangoIt Solutions
  * @param   {userId} with pagesize,page
  * @return  {object} ticket request details
  */
  requestByUserIdPagination(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getRequestByUserIdPage(req.params.uid, req).then((results) => {
      if (results && results.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to get tickets raised by user 
  * @author  MangoIt Solutions
  * @param   {userId} 
  * @return  {object} ticket request details
  */
  requestByUserId(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getRequestByUserId(req.params.uid).then((results) => {
      if (results && results.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to get tickets details by id 
  * @author  MangoIt Solutions
  * @param   {id} 
  * @return  {object} ticket request details
  */
  requestById(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        ticketRequestModel.checksupportUserAssigned(req.params.id).then((results) => {
          if (results.length) {
            ticketRequestModel.getDetailswithSupportuser(req.params.id).then((result) => {
              if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
              } else {
                response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
                res.status(200).json(response);
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          }
          else {
            ticketRequestModel.getRequestById(req.params.id).then((result) => {
              if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
              } else {
                response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
                res.status(200).json(response);
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {

      }
    ])
  }

  /**
  * Function to get request user list details 
  * @author  MangoIt Solutions
  * @param   {id} 
  * @return  {object} ticket request details
  */
  requestUsersListByPage(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getRequestUsersByPagination(req).then((result) => {
      if (result.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [result, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': result[0] ? result[0].totalCount : 0 } }] } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to get request user list details 
  * @author  MangoIt Solutions
  * @param   {} 
  * @return  {object} ticket request details
  */
  requestUsersList(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getRequestUsers().then((result) => {
      if (result.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to update request user list details 
  * @author  MangoIt Solutions
  * @param   {id} with updated details
  * @return  {message} success message
  */
  updateRequest(req, res) {
    let file;
    let s3URLofImage;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = ticketRequestValidation.requestValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          ticketRequestModel.getRequestById(req.params.id).then((results) => {
            if (!results) {
              response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.getRequestByRequest(req.body.name).then((results) => {
            if (results.length > 0 && results[0].id != req.params.id) {
              response = responseFormat.getResponseMessageByCodes(['request:requestNameExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          if (req.file) {
            file = req.file;
            AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
              s3URLofImage = result.Location;
              done();
            }).catch(function (err) {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          } else if (req.body.imageUrl) {
            s3URLofImage = req.body.imageUrl;
            done();
          } else {
            done();
          }
        },
        function (done) {
          ticketRequestModel.updateRequest(req, s3URLofImage).then((results) => {
            if (results.affectedRows > 0) {
              response = responseFormat.getResponseMessageByCodes(['request:requestUpdateSuccess']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }
      ])
    }
  }

  /**
  * Function to update request user list details 
  * @author  MangoIt Solutions
  * @param   {id} with updated request status details
  * @return  {message} success message
  */
  updateStatusRequest(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let status = req.body.status;
    let mailOptions;
    let templateData;
    let ticketDetails;
    if (status != 3) {
      ticketRequestModel.updateRequestStatus(req).then((results) => {
        if (results.affectedRows) {
          response = responseFormat.getResponseMessageByCodes(['request:requestUpdateSuccess']);
          res.status(200).json(response);
        } else {
          response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
          res.status(200).json(response);
        }
      }).catch((error) => {
        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
        res.status(500).json(response);
        return;
      })
    }
    else {
      async.waterfall([
        function (done) {
          ticketRequestModel.updateRequestStatus(req).then((results) => {
            if (results.affectedRows) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Ticket Closed').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.requestTicketDetails(req.params.id).then((result) => {
            ticketDetails = result;
            if (ticketDetails) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestAssignedfail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          mailOptions = {
            to: [ticketDetails[0].request_user_email, process.env.support],
            subject: 'Ticket gelöst',
            html: `<!doctype html>
                <html lang="en-US">
                
                <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                 <meta name="description" content="Reset Password Email Template.">
                <style type="text/css">
                a:hover {text-decoration: underline !important;}
                </style>
                </head>
                
                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                <!--100% body table-->
                <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                <tr>
                <td>
                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                align="center" cellpadding="0" cellspacing="0">
                <tr>
                <td style="height:80px;">&nbsp;</td>
                </tr>
                <tr>
                <td style="text-align:center;">
                <a href="#" title="logo" target="_blank">
                <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                </a>
                </td>
                </tr>
                <tr>
                <td style="height:20px;">&nbsp;</td>
                </tr>
                <tr>
                <td>
                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                <tr>
                <td style="height:40px;">&nbsp;</td>
                </tr>
                <tr>
                <td style="padding:0 35px;">
                <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->
                
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>
                                                       
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                        `+ templateData[0].template_body + `:<br>
                                                        </p><br><br>
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                        Ticket Number : `+ ticketDetails[0].ticket_number + `<br>
                                                        </p>
                                                       
                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                                                        <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                                                        <!-- <a href="javascript:void(0);"
                                                        style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                                            Password</a> -->
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                            </tr>
                                                            </table>
                                                            </td>
                                                            <tr>
                                                            <td style="height:20px;">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                            <td style="text-align:center;">
                                                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td style="height:80px;">&nbsp;</td>
                                                            </tr>
                                                            </table>
                            </td>
                        </tr>
                    </table>
                    <!--/100% body table-->
                    </body>
                    
                    </html>`,
          };
          sendEmail(mailOptions).then((sentdata) => {
            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['request:requestUpdateSuccess']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:requestAssigned']);
              res.status(200).json(response);

            }
          })

        }
      ])
    }
  }

  /**
  * Function to delete request user list details 
  * @author  MangoIt Solutions
  * @param   {id} 
  * @return  {message} success message
  */
  deleteRequest(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.deleteRequest(req.params.id).then((results) => {
      if (results.affectedRows) {
        response = responseFormat.getResponseMessageByCodes(['request:requestDeleted']);
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to add comments on Ticket
  * @author  MangoIt Solutions
  * @param   {user_id} with comment details
  * @return  {message} success message
  */
  createTicketComment(req, res, next) {
    let file;
    let s3URLofImage;
    let supportUserDetails;
    let mailOptions;
    let templateData;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = ticketRequestCommentValidation.requestCommentValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          if (req.file) {
            file = req.file;
            AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
              s3URLofImage = result.Location;
              done();
            }).catch(function (err) {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          } else {
            done();
          }
        }, function (done) {
          ticketRequestModel.getSupportuserasigned(req.params.ticket_id).then((results) => {
            if (results.length > 0) {
              supportUserDetails = results
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:PleaseSelectAssigneduser'], { code: 417 });
              res.status(200).json(response);
            }

          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.addComment(req, s3URLofImage).then((results) => {
            if (results.affectedRows > 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:commentCreationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          ticketRequestModel.getRequestCreatedUser(req.params.ticket_id).then((data) => {

            if (req.params.user_id == data[0].user_id) {
              async.waterfall([
                function (done) {
                  emailTemplateModel.getemailTemplateByType('Reply from customer').then((data) => {
                    if (data.length > 0) {
                      templateData = data;
                      done();
                    }
                    else {
                      response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
                      res.status(200).json(response);
                    }
                  }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                  })
                },
                function (done) {
                  ticketRequestModel.getSupportuserasigned(req.params.ticket_id).then((results) => {
                    if (results) {
                      supportUserDetails = results
                      done();
                    } else {
                      response = responseFormat.getResponseMessageByCodes(['request:requestNotExists'], { code: 417 });
                      res.status(200).json(response);
                    }
                  }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                  })
                },
                function (done) {
                  let link = process.env.ticketDetailsLink + supportUserDetails[0].request_id;
                  mailOptions = {
                    to: [supportUserDetails[0].assigned_user_mail, process.env.support],
                    subject: 'Antwort von Kunden',
                    //         html: `<!doctype html>
                    // <html lang="en-US">

                    // <head>
                    // <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    // <meta name="description" content="Reset Password Email Template.">
                    // <style type="text/css">
                    // a:hover {text-decoration: underline !important;}
                    // </style>
                    // </head>

                    // <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    // <!--100% body table-->
                    // <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                    // style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                    // <tr>
                    // <td>
                    // <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    // align="center" cellpadding="0" cellspacing="0">
                    // <tr>
                    // <td style="height:80px;">&nbsp;</td>
                    // </tr>
                    // <tr>
                    // <td style="text-align:center;">
                    // <a href="#" title="logo" target="_blank">
                    // <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                    // </a>
                    // </td>
                    // </tr>
                    // <tr>
                    // <td style="height:20px;">&nbsp;</td>
                    // </tr>
                    // <tr>
                    // <td>
                    // <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                    // style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                    // <tr>
                    // <td style="height:40px;">&nbsp;</td>
                    // </tr>
                    // <tr>
                    // <td style="padding:0 35px;">
                    // <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                         `+ templateData[0].template_body + `:<br>
                    //                                         </p><br><br>
                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                        Requested Customer :" `+ supportUserDetails[0].ticket_user_mail + ` " added Comment. <br>
                    //                                         </p><br><br>
                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                          For Ticket Number: `+ supportUserDetails[0].ticket_number + `<br>
                    //                                         </p><br>
                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                        Comment : `+ req.body.comment + `<br>
                    //                                         </p><br>

                    //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                    //                                         <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                    //                                         <!-- <a href="javascript:void(0);"
                    //                                         style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                    //                                             Password</a> -->
                    //                                             </td>
                    //                                             </tr>
                    //                                             <tr>
                    //                                             <td style="height:40px;">&nbsp;</td>
                    //                                             </tr>
                    //                                             </table>
                    //                                             </td>
                    //                                             <tr>
                    //                                             <td style="height:20px;">&nbsp;</td>
                    //                                             </tr>
                    //                                             <tr>
                    //                                             <td style="text-align:center;">
                    //                                             <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                    //                                             </td>
                    //                                             </tr>
                    //                                             <tr>
                    //                                             <td style="height:80px;">&nbsp;</td>
                    //                                             </tr>
                    //                                             </table>
                    //             </td>
                    //         </tr>
                    //     </table>
                    //     <!--/100% body table-->
                    //     </body>

                    //     </html>`,
                    html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                <head>
                                <!--[if (gte mso 9)|(IE)]>
                                  <xml>
                                    <o:OfficeDocumentSettings>
                                    <o:AllowPNG/>
                                    <o:PixelsPerInch>96</o:PixelsPerInch>
                                  </o:OfficeDocumentSettings>
                                </xml>
                                <![endif]-->
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                                <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                                <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                                <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                                <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                                <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                                <meta name="color-scheme" content="only">
                                <title></title>
                                
                                <link rel="preconnect" href="https://fonts.gstatic.com">
                                <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                                <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                                
                                <style type="text/css">
                                /*Basics*/
                                body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                                table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                                td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                                td p {margin:0; padding:0;}
                                td div {margin:0; padding:0;}
                                td a {text-decoration:none; color: inherit;} 
                                /*Outlook*/
                                .ExternalClass {width: 100%;}
                                .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                                .ReadMsgBody {width:100%; background-color: #ffffff;}
                                /* iOS BLUE LINKS */
                                a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                                /*Gmail blue links*/
                                u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                                /*Buttons fix*/
                                .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                                .yshortcuts a {border-bottom:none !important;
                                
                                .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                                /*Responsive*/
                                @media screen and (max-width: 639px) {
                                  table.row {width: 100%!important;max-width: 100%!important;}
                                  td.row {width: 100%!important;max-width: 100%!important;}
                                  .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                                  .center-float {float: none!important;margin:auto!important;}
                                  .center-text{text-align: center!important;}
                                  .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                                  .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                                  .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                                  .hide-mobile {display: none!important;}
                                  .menu-container {text-align: center !important;}
                                  .autoheight {height: auto!important;}
                                  .m-padding-10 {margin: 10px 0!important;}
                                  .m-padding-15 {margin: 15px 0!important;}
                                  .m-padding-20 {margin: 20px 0!important;}
                                  .m-padding-30 {margin: 30px 0!important;}
                                  .m-padding-40 {margin: 40px 0!important;}
                                  .m-padding-50 {margin: 50px 0!important;}
                                  .m-padding-60 {margin: 60px 0!important;}
                                  .m-padding-top10 {margin: 30px 0 0 0!important;}
                                  .m-padding-top15 {margin: 15px 0 0 0!important;}
                                  .m-padding-top20 {margin: 20px 0 0 0!important;}
                                  .m-padding-top30 {margin: 30px 0 0 0!important;}
                                  .m-padding-top40 {margin: 40px 0 0 0!important;}
                                  .m-padding-top50 {margin: 50px 0 0 0!important;}
                                  .m-padding-top60 {margin: 60px 0 0 0!important;}
                                  .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                                  .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                                  .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                                  .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                                  .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                                  .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                                  .center-on-mobile {text-align: center!important;}
                                }
                                </style>
                                </head>
                                
                                <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                                
                                <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                                
                                <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                                  <tr><!-- Outer Table -->
                                    <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- Preheader -->
                                  <tr>
                                    <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <!-- Preheader -->
                                </table>
                                
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- simpli-header-1 -->
                                  <tr>
                                    <td align="center">
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                                  <!-- bg-image -->
                                  <tr>
                                    <td align="center" style="border-radius: 36px;">
                                
                                <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                                <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                                <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                                
                                <div>
                                <!-- simpli-header-bg-image -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                                
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                                  <tr>
                                    <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                                
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                      <tr>
                                        <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                      </tr>
                                    </table>
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- simpli-header-bg-image -->
                                </div>
                                
                                <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                
                                    </td>
                                  </tr>
                                  <!-- bg-image -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                                  <!-- basic-info -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    NEUE ANFRAGE
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Kunde: `+ supportUserDetails[0].assigned_user_mail + `
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  TicketNumber:  `+ supportUserDetails[0].ticket_number + ` <br>
                                                  Mail:         `+ supportUserDetails[0].ticket_user_mail + `  <br>
                                                  Club Name:      `+ supportUserDetails[0].Clubname + `<br>
                                                  Category:       `+ supportUserDetails[0].category + `<br>
                                                  Request Title: `+ supportUserDetails[0].name + ` <br>
                                                  Date:        `+ supportUserDetails[0].created_at + `   <br>           
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                            <!-- Button -->
                                            <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                              <tr>
                                                <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                            <!--[if (gte mso 9)|(IE)]>
                                              <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                  <td align="center" width="35"></td>
                                                  <td align="center" height="50" style="height:50px;">
                                                  <![endif]-->
                                                    <singleline>
                                                      <a href="`+ link + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                                    </singleline>
                                                  <!--[if (gte mso 9)|(IE)]>
                                                  </td>
                                                  <td align="center" width="35"></td>
                                                </tr>
                                              </table>
                                            <![endif]-->
                                                </td>
                                              </tr>
                                            </table>
                                            <!-- Buttons -->
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- basic-info -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                    <!-- content-1A -->
                                    <tr>
                                      <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                                        <!-- content -->
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                          <tr>
                                            <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                          </tr>
                                          <tr>
                                          </tr>
                                          <tr>
                                            <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                <singleline>
                                                  <div mc:edit Simpli>
                                                    Antwort:
                                                  </div>
                                                </singleline>
                                            </td>
                                          </tr>
                                          <tr>
                                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="center">
                                            
                                            <!-- 2-columns -->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                              <tr>
                                                <td align="center">
                                                <!-- gap -->
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                  <tr>
                                                    <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                  </tr>
                                                </table>
                                                <!-- gap -->
                                  
                                                <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                  
                                                <!-- column -->
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                  <tr>
                                                    <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                        <singleline>
                                                          <div mc:edit Simpli>
                                                          `+ req.body.comment + `
                                                          </div>
                                                        </singleline>
                                                    </td>
                                                  </tr>
                                                </table>
                                                <!-- column -->
                                  
                                                <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                  
                                                </td>
                                              </tr>
                                            </table>
                                            <!-- 2-columns -->
                                  
                                            </td>
                                          </tr>
                                          <tr>
                                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                          </tr>
                                        </table>
                                        <!-- content -->
                                      </td>
                                    </tr>
                                    <!-- content-1A -->
                                </table>
                                
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                    <!-- content-1C -->
                                    <tr>
                                      <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                          <tr>
                                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                    <!-- content-1C -->
                                  </table>
                                  
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                    <!-- simpli-footer -->
                                    <tr>
                                        <td align="center">
                                        </td>
                                    </tr>
                                  <!-- simpli-header-1 -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                  <!-- simpli-footer -->
                                  <tr>
                                    <td align="center">
                                      
                                <!-- Content -->
                                
                                  <tr>
                                    <td align="center">
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                            <multiline>
                                              <div mc:edit Simpli>
                                                <br />
                                                Tineon Aktiengesellschaft <br />
                                                Uferpromenade 5, 88709 Meersburg <br />
                                                Registergericht Freiburg HRB 710927 <br />
                                                Vorstand: Jean-Claude Parent <br />
                                                Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                                USt.-IdNr. DE203912818 <br />
                                              </div>
                                            </multiline>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                  <!-- simpli-footer -->
                                </table>
                                
                                    </td>
                                  </tr><!-- Outer-Table -->
                                </table>
                                
                                </body>
                                </html>
                                `
                  };
                  sendEmail(mailOptions).then((sentdata) => {
                    if (sentdata == 'error') {
                      response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
                      res.status(200).json(response);
                    } else {
                      response = responseFormat.getResponseMessageByCodes(['request:commentCreated']);
                      res.status(200).json(response);
                    }
                  })

                }
              ])
            }

            else {
              async.waterfall([
                function (done) {
                  emailTemplateModel.getemailTemplateByType('Reply from Support User').then((data) => {
                    if (data.length > 0) {
                      templateData = data;
                      done();
                    }
                    else {
                      response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
                      res.status(200).json(response);
                    }
                  }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                  })
                },
                function (done) {
                  let link = process.env.ticketDetailsLink + supportUserDetails[0].request_id;
                  mailOptions = {
                    from: supportUserDetails[0].assigned_user_mail,
                    to: [supportUserDetails[0].ticket_user_mail, process.env.support],
                    subject: 'Antwort von Support',
                    html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                    <head>
                                    <!--[if (gte mso 9)|(IE)]>
                                      <xml>
                                        <o:OfficeDocumentSettings>
                                        <o:AllowPNG/>
                                        <o:PixelsPerInch>96</o:PixelsPerInch>
                                      </o:OfficeDocumentSettings>
                                    </xml>
                                    <![endif]-->
                                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                                    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                                    <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                                    <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                                    <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                                    <meta name="color-scheme" content="only">
                                    <title></title>
                                    
                                    <link rel="preconnect" href="https://fonts.gstatic.com">
                                    <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                                    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                                    
                                    <style type="text/css">
                                    /*Basics*/
                                    body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                                    table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                    table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                                    td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                                    td p {margin:0; padding:0;}
                                    td div {margin:0; padding:0;}
                                    td a {text-decoration:none; color: inherit;} 
                                    /*Outlook*/
                                    .ExternalClass {width: 100%;}
                                    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                                    .ReadMsgBody {width:100%; background-color: #ffffff;}
                                    /* iOS BLUE LINKS */
                                    a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                                    /*Gmail blue links*/
                                    u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                                    /*Buttons fix*/
                                    .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                                    .yshortcuts a {border-bottom:none !important;
                                    
                                    .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                                    /*Responsive*/
                                    @media screen and (max-width: 639px) {
                                      table.row {width: 100%!important;max-width: 100%!important;}
                                      td.row {width: 100%!important;max-width: 100%!important;}
                                      .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                                      .center-float {float: none!important;margin:auto!important;}
                                      .center-text{text-align: center!important;}
                                      .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                                      .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                                      .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                                      .hide-mobile {display: none!important;}
                                      .menu-container {text-align: center !important;}
                                      .autoheight {height: auto!important;}
                                      .m-padding-10 {margin: 10px 0!important;}
                                      .m-padding-15 {margin: 15px 0!important;}
                                      .m-padding-20 {margin: 20px 0!important;}
                                      .m-padding-30 {margin: 30px 0!important;}
                                      .m-padding-40 {margin: 40px 0!important;}
                                      .m-padding-50 {margin: 50px 0!important;}
                                      .m-padding-60 {margin: 60px 0!important;}
                                      .m-padding-top10 {margin: 30px 0 0 0!important;}
                                      .m-padding-top15 {margin: 15px 0 0 0!important;}
                                      .m-padding-top20 {margin: 20px 0 0 0!important;}
                                      .m-padding-top30 {margin: 30px 0 0 0!important;}
                                      .m-padding-top40 {margin: 40px 0 0 0!important;}
                                      .m-padding-top50 {margin: 50px 0 0 0!important;}
                                      .m-padding-top60 {margin: 60px 0 0 0!important;}
                                      .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                                      .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                                      .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                                      .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                                      .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                                      .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                                      .center-on-mobile {text-align: center!important;}
                                    }
                                    </style>
                                    </head>
                                    
                                    <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                                    
                                    <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                                    
                                    <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                                      <tr><!-- Outer Table -->
                                        <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                                      <!-- Preheader -->
                                      <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="center" class="center-text">
                                          <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <!-- Preheader -->
                                    </table>
                                    
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                                      <!-- simpli-header-1 -->
                                      <tr>
                                        <td align="center">
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                                      <!-- bg-image -->
                                      <tr>
                                        <td align="center" style="border-radius: 36px;">
                                    
                                    <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                                    <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                                    <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                                    
                                    <div>
                                    <!-- simpli-header-bg-image -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                      <tr>
                                        <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                                    
                                    <!-- Content -->
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                                      <tr>
                                        <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                                    
                                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                          <tr>
                                            <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                          </tr>
                                        </table>
                                    
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Content -->
                                    
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- simpli-header-bg-image -->
                                    </div>
                                    
                                    <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                    
                                        </td>
                                      </tr>
                                      <!-- bg-image -->
                                    </table>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                                      <!-- basic-info -->
                                      <tr>
                                        <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                          <!-- content -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                                  <singleline>
                                                    <div mc:edit Simpli>
                                                        NEUE ANFRAGE
                                                    </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                  <singleline>
                                                    <div mc:edit Simpli>
                                                        Kunde: `+ supportUserDetails[0].assigned_user_mail + `
                                                    </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                  <singleline>
                                                  <div mc:edit Simpli>
                                                  TicketNumber:  `+ supportUserDetails[0].ticket_number + ` <br>
                                                  Mail:         `+ supportUserDetails[0].ticket_user_mail + `  <br>
                                                  Club Name:      `+ supportUserDetails[0].Clubname + `<br>
                                                  Category:       `+ supportUserDetails[0].category + `<br>
                                                  Request Title: `+ supportUserDetails[0].name + ` <br>
                                                  Date:        `+ supportUserDetails[0].created_at + `   <br>           
                                                </div>
                                                  </singleline>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <!-- Button -->
                                                <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                                  <tr>
                                                    <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                                <!--[if (gte mso 9)|(IE)]>
                                                  <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                      <td align="center" width="35"></td>
                                                      <td align="center" height="50" style="height:50px;">
                                                      <![endif]-->
                                                        <singleline>
                                                          <a href="`+ link + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                                        </singleline>
                                                      <!--[if (gte mso 9)|(IE)]>
                                                      </td>
                                                      <td align="center" width="35"></td>
                                                    </tr>
                                                  </table>
                                                <![endif]-->
                                                    </td>
                                                  </tr>
                                                </table>
                                                <!-- Buttons -->
                                              </td>
                                            </tr>
                                            <tr>
                                              <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                            </tr>
                                          </table>
                                          <!-- content -->
                                        </td>
                                      </tr>
                                      <!-- basic-info -->
                                    </table>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                        <!-- content-1A -->
                                        <tr>
                                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                                            <!-- content -->
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                              <tr>
                                                <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                              </tr>
                                              <tr>
                                              </tr>
                                              <tr>
                                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                    <singleline>
                                                      <div mc:edit Simpli>
                                                        Antwort:
                                                      </div>
                                                    </singleline>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                              </tr>
                                              <tr>
                                                <td align="center">
                                                
                                                <!-- 2-columns -->
                                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                                  <tr>
                                                    <td align="center">
                                                    <!-- gap -->
                                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                      <tr>
                                                        <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                      </tr>
                                                    </table>
                                                    <!-- gap -->
                                      
                                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                      
                                                    <!-- column -->
                                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                      <tr>
                                                        <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                            <singleline>
                                                              <div mc:edit Simpli>
                                                              `+ req.body.comment + `
                                                              </div>
                                                            </singleline>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                    <!-- column -->
                                      
                                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                      
                                                    </td>
                                                  </tr>
                                                </table>
                                                <!-- 2-columns -->
                                      
                                                </td>
                                              </tr>
                                              <tr>
                                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                              </tr>
                                            </table>
                                            <!-- content -->
                                          </td>
                                        </tr>
                                        <!-- content-1A -->
                                    </table>
                                    
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                        <!-- content-1C -->
                                        <tr>
                                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                              <tr>
                                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                              </tr>
                                            </table>
                                          </td>
                                        </tr>
                                        <!-- content-1C -->
                                      </table>
                                      
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                        <!-- simpli-footer -->
                                        <tr>
                                            <td align="center">
                                            </td>
                                        </tr>
                                      <!-- simpli-header-1 -->
                                    </table>
                                    
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                      <!-- simpli-footer -->
                                      <tr>
                                        <td align="center">
                                          
                                    <!-- Content -->
                                    
                                      <tr>
                                        <td align="center">
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                            <tr>
                                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                                <multiline>
                                                  <div mc:edit Simpli>
                                                    <br />
                                                    Tineon Aktiengesellschaft <br />
                                                    Uferpromenade 5, 88709 Meersburg <br />
                                                    Registergericht Freiburg HRB 710927 <br />
                                                    Vorstand: Jean-Claude Parent <br />
                                                    Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                                    USt.-IdNr. DE203912818 <br />
                                                  </div>
                                                </multiline>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td align="center" class="center-text">
                                          <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                                      </tr>
                                    </table>
                                    <!-- Content -->
                                    
                                        </td>
                                      </tr>
                                      <!-- simpli-footer -->
                                    </table>
                                    
                                        </td>
                                      </tr><!-- Outer-Table -->
                                    </table>
                                    
                                    </body>
                                    </html>
                                    `,
                  };

                  sendEmail(mailOptions).then((sentdata) => {
                    if (sentdata == 'error') {
                      response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
                      res.status(200).json(response);
                    } else {
                      response = responseFormat.getResponseMessageByCodes(['request:commentCreated']);
                      res.status(200).json(response);
                    }
                  })

                }
              ])
            }
          })
        },

      ])
    }
  }

  /**
  * Function to add comments on Ticket
  * @author  MangoIt Solutions
  * @param   {id} with comment details
  * @return  {object} comment details
  */
  commentbyticketid(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getCommentByTicketId(req.params.ticket_id).then((result) => {
      if (result.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:commentNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })

  }

  /**
  * Function to add comments on Ticket
  * @author  MangoIt Solutions
  * @param   {id} with comment details
  * @return  {message} success message
  */
  updateComment(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = ticketRequestValidation.requestValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          ticketRequestModel.getCommentById(req.params.id).then((results) => {
            if (!results) {
              response = responseFormat.getResponseMessageByCodes(['request:commentNotExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          if (req.file) {
            file = req.file;
            AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
              s3URLofImage = result.Location;
              done();
            }).catch(function (err) {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          } else {
            done();
          }
        },
        function (done) {
          ticketRequestModel.updateComment(req, s3URLofImage).then((results) => {
            if (results.affectedRows > 0) {
              response = responseFormat.getResponseMessageByCodes(['request:commentUpdateSuccess']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['request:commentNotExists'], { code: 417 });
              res.status(200).json(response);
            }

          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }
      ])
    }
  }

  /**
  * Function to delete comments from Ticket
  * @author  MangoIt Solutions
  * @param   {id}
  * @return  {message} success message
  */
  deleteComment(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.deleteComment(req.params.id).then((results) => {
      if (results.affectedRows) {
        response = responseFormat.getResponseMessageByCodes(['request:commenttDeleted']);
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:commentNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to create ticket comments reply
  * @author  MangoIt Solutions
  * @param   {comment_id,ticket_id,user_id} with reply
  * @return  {message} success message
  */
  createTicketCommentReply(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        ticketRequestModel.addCommentReply(req).then((results) => {
          if (results.affectedRows > 0) {
            response = responseFormat.getResponseMessageByCodes(['request:commentCreated']);
            res.status(200).json(response);
          } else {
            response = responseFormat.getResponseMessageByCodes(['request:commentCreationFail'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      }
    ])
  }

  /**
  * Function to get all support users
  * @author  MangoIt Solutions
  * @param   {}
  * @return  {object} support user details
  */
  getAllSupportUsers(req, res) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    ticketRequestModel.getAllSupportUsers().then((result) => {
      if (result.length > 0) {
        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
        res.status(200).json(response);
      } else {
        response = responseFormat.getResponseMessageByCodes(['request:SupportUsersNotExists'], { code: 417 });
        res.status(200).json(response);
      }
    }).catch((error) => {
      response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
      res.status(500).json(response);
      return;
    })
  }

  /**
  * Function to add support user to ticket to solve the query of customer
  * @author  MangoIt Solutions
  * @param   {object} with reques_id,assigned_user_id
  * @return  {message} success message
  */
  addAssignedUser(req, res) {
    let templateData;
    let ticketDetails;
    let mailOptions;
    let response = responseFormat.createResponseTemplate();
    async.waterfall([
      function (done) {
        ticketRequestModel.addAssignedUser(req).then((result) => {
          if (result.affectedRows > 0) {
            done();
          } else {
            response = responseFormat.getResponseMessageByCodes(['request:requestAssignedfail'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        ticketRequestModel.assignedUserTicketDetails(req).then((result) => {
          ticketDetails = result;
          if (ticketDetails) {
            done();
          } else {
            response = responseFormat.getResponseMessageByCodes(['request:requestAssignedfail'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        emailTemplateModel.getemailTemplateByType('Assigned Support User').then((data) => {
          if (data.length > 0) {
            templateData = data;
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['request:Email template not exist'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        let date = new Date().toLocaleString();
        let link = process.env.ticketDetailsLink + ticketDetails[0].request_id;

        // mailOptions = {
        //     to: ticketDetails[0].assigned_user_mail,
        //     subject: templateData[0].subject,
        //     html: `<!doctype html>
        // <html lang="en-US">

        // <head>
        // <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        // <title>Reset Password Email</title>
        // <meta name="description" content="Reset Password Email Template.">
        // <style type="text/css">
        // a:hover {text-decoration: underline !important;}
        // </style>
        // </head>

        // <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        // <!--100% body table-->
        // <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        // style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
        // <tr>
        // <td>
        // <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
        // align="center" cellpadding="0" cellspacing="0">
        // <tr>
        // <td style="height:80px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td style="text-align:center;">
        // <a href="#" title="logo" target="_blank">
        // <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
        // </a>
        // </td>
        // </tr>
        // <tr>
        // <td style="height:20px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td>
        // <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
        // style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
        // <tr>
        // <td style="height:40px;">&nbsp;</td>
        // </tr>
        // <tr>
        // <td style="padding:0 35px;">
        // <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                         `+ templateData[0].template_body + `:<br>
        //                                         </p><br><br>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                         Ticket Number : `+ ticketDetails[0].ticket_number + `<br>
        //                                         </p>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                          Requested User EMail: `+ ticketDetails[0].ticket_user_mail + `<br>
        //                                         </p>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                         Club Name : `+ ticketDetails[0].Clubname + `<br>
        //                                         </p>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                        Category : `+ ticketDetails[0].category + `<br>
        //                                         </p>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                         Request Title: `+ ticketDetails[0].name + `<br>
        //                                         </p>
        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                         Request Creation Date: `+ ticketDetails[0].created_at + `<br><br>
        //                                         </p>

        //                                         <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
        //                                         <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
        //                                         <!-- <a href="javascript:void(0);"
        //                                         style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
        //                                             Password</a> -->
        //                                             </td>
        //                                             </tr>
        //                                             <tr>
        //                                             <td style="height:40px;">&nbsp;</td>
        //                                             </tr>
        //                                             </table>
        //                                             </td>
        //                                             <tr>
        //                                             <td style="height:20px;">&nbsp;</td>
        //                                             </tr>
        //                                             <tr>
        //                                             <td style="text-align:center;">
        //                                             <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
        //                                             </td>
        //                                             </tr>
        //                                             <tr>
        //                                             <td style="height:80px;">&nbsp;</td>
        //                                             </tr>
        //                                             </table>
        //             </td>
        //         </tr>
        //     </table>
        //     <!--/100% body table-->
        //     </body>

        //     </html>`,
        // };
        mailOptions = {
          to: ticketDetails[0].assigned_user_mail,
          subject: 'Ticket an Support übermittelt',
          html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                    <head>
                    <!--[if (gte mso 9)|(IE)]>
                      <xml>
                        <o:OfficeDocumentSettings>
                        <o:AllowPNG/>
                        <o:PixelsPerInch>96</o:PixelsPerInch>
                      </o:OfficeDocumentSettings>
                    </xml>
                    <![endif]-->
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                    <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                    <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                    <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                    <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                    <meta name="color-scheme" content="only">
                    <title></title>
                    
                    <link rel="preconnect" href="https://fonts.gstatic.com">
                    <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                    
                    <style type="text/css">
                    /*Basics*/
                    body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                    table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                    table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                    td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                    td p {margin:0; padding:0;}
                    td div {margin:0; padding:0;}
                    td a {text-decoration:none; color: inherit;} 
                    /*Outlook*/
                    .ExternalClass {width: 100%;}
                    .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                    .ReadMsgBody {width:100%; background-color: #ffffff;}
                    /* iOS BLUE LINKS */
                    a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                    /*Gmail blue links*/
                    u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                    /*Buttons fix*/
                    .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                    .yshortcuts a {border-bottom:none !important;
                    
                    .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                    /*Responsive*/
                    @media screen and (max-width: 639px) {
                      table.row {width: 100%!important;max-width: 100%!important;}
                      td.row {width: 100%!important;max-width: 100%!important;}
                      .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                      .center-float {float: none!important;margin:auto!important;}
                      .center-text{text-align: center!important;}
                      .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                      .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                      .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                      .hide-mobile {display: none!important;}
                      .menu-container {text-align: center !important;}
                      .autoheight {height: auto!important;}
                      .m-padding-10 {margin: 10px 0!important;}
                      .m-padding-15 {margin: 15px 0!important;}
                      .m-padding-20 {margin: 20px 0!important;}
                      .m-padding-30 {margin: 30px 0!important;}
                      .m-padding-40 {margin: 40px 0!important;}
                      .m-padding-50 {margin: 50px 0!important;}
                      .m-padding-60 {margin: 60px 0!important;}
                      .m-padding-top10 {margin: 30px 0 0 0!important;}
                      .m-padding-top15 {margin: 15px 0 0 0!important;}
                      .m-padding-top20 {margin: 20px 0 0 0!important;}
                      .m-padding-top30 {margin: 30px 0 0 0!important;}
                      .m-padding-top40 {margin: 40px 0 0 0!important;}
                      .m-padding-top50 {margin: 50px 0 0 0!important;}
                      .m-padding-top60 {margin: 60px 0 0 0!important;}
                      .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                      .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                      .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                      .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                      .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                      .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                      .center-on-mobile {text-align: center!important;}
                    }
                    </style>
                    </head>
                    
                    <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                    
                    <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                    
                    <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                      <tr><!-- Outer Table -->
                        <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                      <!-- Preheader -->
                      <tr>
                        <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                      <tr>
                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" class="center-text">
                          <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                        </td>
                      </tr>
                      <tr>
                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                      </tr>
                      <!-- Preheader -->
                    </table>
                    
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                      <!-- simpli-header-1 -->
                      <tr>
                        <td align="center">
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                      <!-- bg-image -->
                      <tr>
                        <td align="center" style="border-radius: 36px;">
                    
                    <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                    <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                    <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                    
                    <div>
                    <!-- simpli-header-bg-image -->
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                      <tr>
                        <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                    
                    <!-- Content -->
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                      <tr>
                        <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                    
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                          </tr>
                        </table>
                    
                        </td>
                      </tr>
                    </table>
                    <!-- Content -->
                    
                        </td>
                      </tr>
                    </table>
                    <!-- simpli-header-bg-image -->
                    </div>
                    
                    <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                    
                        </td>
                      </tr>
                      <!-- bg-image -->
                    </table>
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                      <!-- basic-info -->
                      <tr>
                        <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                          <!-- content -->
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                            <tr>
                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                  <singleline>
                                    <div mc:edit Simpli>
                                        NEUE ANFRAGE
                                    </div>
                                  </singleline>
                              </td>
                            </tr>
                            <tr>
                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:24px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                  <singleline>
                                    <div mc:edit Simpli>
                                        Kunde: `+ ticketDetails[0].ticket_user_mail + `
                                    </div>
                                  </singleline>
                              </td>
                            </tr>
                            <tr>
                              <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                  <singleline>
                                    <div mc:edit Simpli>
                                      TicketNumber: `+ ticketDetails[0].ticket_number + `  <br>
                                      Mail:         `+ ticketDetails[0].ticket_user_mail + `  <br>
                                      Club Name:    `+ ticketDetails[0].Clubname + `  <br>
                                      Category:      `+ ticketDetails[0].category + ` <br>
                                      Request Title: `+ ticketDetails[0].name + ` <br>
                                      Date:         `+ ticketDetails[0].created_at + `  <br>           
                                    </div>
                                  </singleline>
                              </td>
                            </tr>
                            <tr>
                              <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                            </tr>
                            <tr>
                              <td align="center">
                                <!-- Button -->
                                <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                  <tr>
                                    <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                <!--[if (gte mso 9)|(IE)]>
                                  <table border="0" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                      <td align="center" width="35"></td>
                                      <td align="center" height="50" style="height:50px;">
                                      <![endif]-->
                                        <singleline>
                                          <a href="`+ link + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                        </singleline>
                                      <!--[if (gte mso 9)|(IE)]>
                                      </td>
                                      <td align="center" width="35"></td>
                                    </tr>
                                  </table>
                                <![endif]-->
                                    </td>
                                  </tr>
                                </table>
                                <!-- Buttons -->
                              </td>
                            </tr>
                            <tr>
                              <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                            </tr>
                          </table>
                          <!-- content -->
                        </td>
                      </tr>
                      <!-- basic-info -->
                    </table>
                    
                        </td>
                      </tr>
                      <!-- simpli-header-1 -->
                    </table>
                    
                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                      <!-- simpli-footer -->
                      <tr>
                        <td align="center">
                          
                    <!-- Content -->
                    
                      <tr>
                        <td align="center">
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                            <tr>
                              <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                <multiline>
                                  <div mc:edit Simpli>
                                    <br />
                                    Tineon Aktiengesellschaft <br />
                                    Uferpromenade 5, 88709 Meersburg <br />
                                    Registergericht Freiburg HRB 710927 <br />
                                    Vorstand: Jean-Claude Parent <br />
                                    Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                    USt.-IdNr. DE203912818 <br />
                                  </div>
                                </multiline>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" class="center-text">
                          <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                        </td>
                      </tr>
                      <tr>
                        <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                      </tr>
                    </table>
                    <!-- Content -->
                    
                        </td>
                      </tr>
                      <!-- simpli-footer -->
                    </table>
                    
                        </td>
                      </tr><!-- Outer-Table -->
                    </table>
                    
                    </body>
                    </html>
                    `,
        };

        sendEmail(mailOptions).then((sentdata) => {
          if (sentdata == 'error') {
            response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
            res.status(200).json(response);
          } else {
            done();
          }
        })

      },
      function (done) {
        let date = new Date().toLocaleString();
        mailOptions = {
          to: (ticketDetails[0].ticket_user_mail),
          subject: 'Ticket an Support übermittelt',
          html: `<!doctype html>
                                <html lang="en-US">
                                
                                <head>
                                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                <title>Reset Password Email</title>
                                <meta name="description" content="Reset Password Email Template.">
                                <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                                </style>
                                </head>
                                
                                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                                <!--100% body table-->
                                <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                                style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                <td>
                                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                                align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                <td style="height:80px;">&nbsp;</td>
                                </tr>
                                <tr>
                                <td style="text-align:center;">
                                <a href="#" title="logo" target="_blank">
                                <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                                </a>
                                </td>
                                </tr>
                                <tr>
                                <td style="height:20px;">&nbsp;</td>
                                </tr>
                                <tr>
                                <td>
                                <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                <tr>
                                <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <tr>
                                <td style="padding:0 35px;">
                              
                                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">Hello User,</p><br>
                                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">Your Ticket Numebr : `+ ticketDetails[0].ticket_number + ` is assigned to support User : ` + ticketDetails[0].assigned_user_mail + `   </p><br>
                                                                       
                                                                      
                                                                     
                                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">Thanks,</p>
                                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">Support</p>
                
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                            <td style="height:40px;">&nbsp;</td>
                                                                            </tr>
                                                                            </table>
                                                                            </td>
                                                                            <tr>
                                                                            <td style="height:20px;">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                            <td style="text-align:center;">
                                                                            <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                                                                            </td>
                                                                            </tr>
                                                                            <tr>
                                                                            <td style="height:80px;">&nbsp;</td>
                                                                            </tr>
                                                                            </table>
                                            </td>
                                        </tr>
                                    </table>
                                    </body>
                                    
                                    </html>`,
        };
        sendEmail(mailOptions).then((sentdata) => {
          if (sentdata == 'error') {
            response = responseFormat.getResponseMessageByCodes(['request:requestCreated']);
            res.status(200).json(response);
          } else {
            response = responseFormat.getResponseMessageByCodes(['request:requestAssigned']);
            res.status(200).json(response);
          }
        })
      }
    ])
  }


}