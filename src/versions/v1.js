import express from 'express';
import responseFormat from '../core/response-format';
import authRoutes from '../api/auth/auth.js';
import rolesRoutes from '../api/roles/role.js';
import usersRoutes from '../api/users/user.js';
import usersRolesRoutes from '../api/users_roles/users_roles.js';
import productRoutes from '../api/products/product.js';
import productPriceRoutes from '../api/product_price/product-price.js';
import profileEditRoutes from '../api/profile_edit/profile_edit.js';
import contactAdminRoutes from '../api/contact_admin/contact.js';
import newsRoutes from '../api/news/news';
import domainRoutes from '../api/domain/domain';
import licencesRoutes from '../api/licence/licence';
import filtersRoutes from '../api/filters/filters';
import productComponentsRoutes from '../api/product_components/product_components';
import rolePermissionsRoutes from '../api/roles_permissions/roles_permissions';
import pagesRoutes from '../api/static_pages/static-pages';
import surveyRoutes from '../api/survey/survey';
import emailTemplateRoutes from '../api/email_template/email-template';
import authBillwerkRoutes from '../api/billwerk_apis/billwerk_auth/auth';
import productBillwerkRoutes from '../api/billwerk_apis/billwerk_product/product';
import orderBillwerkRoutes from '../api/billwerk_apis/billwerk_order/order';
import invoiceBillwerkRoutes from '../api/billwerk_apis/billwerk_invoice/invoice';
import contractBillwerkRoutes from '../api/billwerk_apis/billwerk_contract/contract';
import userDocumentRoutes from '../api/user_document/user-document';
import languageRoutes from '../api/language_switch/language';
import customermembersRoutes from '../api/customer_members/customer-members';
import crmproductsRoutes from '../api/crm_products/crm_products';
import billworkWebhook from '../api/billwerk_apis/billwerk_webhook/webhook';
import clubRoutes from '../api/club/club'
import TicketRequestRoutes from '../api/service_support/ticket_request'
import faqCategoryRoutes from '../api/faq_category/faq_category'
import faqQuestionRoutes from '../api/faq_questions/faq_question'
import faqStatusRoutes from '../api/faq_status/faq_status'
import jsonScriptRoutes from '../api/json_script/json_script';
import functionInClubRoutes from '../api/function-in-club/funtion-in-club';
import productDisplayRoutes from '../api/product-display/product-display';
import advertisementRouter from '../api/advertisement/advertisement';
import tineonInfosRoutes from '../api/tineon_infos/tineonInfos';
let app = express.Router();

app.use(function (req, res, next) {
	next();
})

app.use('/api', authRoutes);
app.use('/api', rolesRoutes);
app.use('/api', usersRoutes);
app.use('/api', usersRolesRoutes);
app.use('/api', productRoutes);
app.use('/api', productPriceRoutes);
app.use('/api', profileEditRoutes);
app.use('/api', contactAdminRoutes);
app.use('/api', newsRoutes);
app.use('/api', domainRoutes);
app.use('/api',licencesRoutes);
app.use('/api',filtersRoutes);
app.use('/api',productComponentsRoutes);
app.use('/api',rolePermissionsRoutes);
app.use('/api',pagesRoutes);
app.use('/api',authBillwerkRoutes);
app.use('/api',productBillwerkRoutes);
app.use('/api',orderBillwerkRoutes);
app.use('/api',invoiceBillwerkRoutes);
app.use('/api',surveyRoutes);
app.use('/api',emailTemplateRoutes);
app.use('/api',contractBillwerkRoutes);
app.use('/api',userDocumentRoutes);
app.use('/api',languageRoutes);
app.use('/api',customermembersRoutes);
app.use('/api',crmproductsRoutes);
app.use('/api',billworkWebhook)
app.use('/api',clubRoutes)
app.use('/api',TicketRequestRoutes)
app.use('/api',faqCategoryRoutes)
app.use('/api',faqQuestionRoutes)
app.use('/api',faqStatusRoutes)
app.use('/api',jsonScriptRoutes)
app.use('/api',functionInClubRoutes)
app.use('/api',productDisplayRoutes)
app.use('/api',advertisementRouter)
app.use('/api',tineonInfosRoutes)

app.use('*', function (req, res) {
	let response = responseFormat.createResponseTemplate();
	response = responseFormat.getResponseMessageByCodes(['common404'], { code: 404 });
	res.status(404).json(response);
});
export default app;