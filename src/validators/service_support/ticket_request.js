export default class TicketRequestValidation {

    requestValidation(reqdata) {
        let err = new Array();
        if(!reqdata.name){
            err.push('name:nameReq');
            }
        if(!reqdata.status){
                err.push('status:statusReq');
            }
        if(!reqdata.assigned_to){
            err.push('assigned_to:assignedToReq');
            }
        return err;
    }

}