import express   from 'express';
import BillwerkContractController from '../../../controllers/billwerk_controllers/billwerk_contract/contract';

let router = express.Router();
let billwerkContractController = new BillwerkContractController();

router.post('/billwerk-contract-end/:contract_id', billwerkContractController.getCancelContract.bind(billwerkContractController));
router.get('/billwerk-contract-bycustomer/:customer_id', billwerkContractController.getCustomerContract.bind(billwerkContractController));
router.get('/billwerk-endedcontract-bycustomer/:customer_id', billwerkContractController.getEndedCustomerContract.bind(billwerkContractController));

export default router;