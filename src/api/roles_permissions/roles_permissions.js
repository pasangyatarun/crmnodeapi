import express from 'express';
import RolesPermissionsController from '../../controllers/roles_permissions/roles_permissions';

let router = express.Router();
let rolespermissionsController = new RolesPermissionsController();

router.post('/rolepermissionfilter', rolespermissionsController.rolePermissionFilter.bind(rolespermissionsController));
router.get('/rolespermissions', rolespermissionsController.rolesPermissions.bind(rolespermissionsController));
router.get('/rolepermissionbyid/:rid', rolespermissionsController.rolePermissionById.bind(rolespermissionsController));
router.get('/rolepermissionbyidmodule', rolespermissionsController.rolePermissionByIdModule.bind(rolespermissionsController));
router.get('/rolepermissionbymodule/:module', rolespermissionsController.rolePermissionByModule.bind(rolespermissionsController));
router.post('/rolepermissioncreate', rolespermissionsController.rolePermissionCreate.bind(rolespermissionsController));
router.delete('/rolepermissiondelete/:id', rolespermissionsController.deleteRolePermission.bind(rolespermissionsController));
router.delete('/rolepermissionsdelete/bymodule', rolespermissionsController.delRolePermissionByModule.bind(rolespermissionsController));
router.delete('/rolespermissionsdelete/byidmodule', rolespermissionsController.delRolePermissionByIdModule.bind(rolespermissionsController));

export default router;