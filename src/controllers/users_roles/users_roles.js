import UsersRolesModel from "../../models/user_role/user_role.js";
import UserModel from "../../models/user/user.js";
import responseFormat from '../../core/response-format';
import CommonMethods from '../../core/common-methods.js';
let usersrolesModel = new UsersRolesModel();
let commonmethod = new CommonMethods();
let usermodel = new UserModel();

export default class UsersRolesController {

    /**
    * Function to update user role 
    * @author  MangoIt Solutions
    * @param   {email}
    * @return  {message} success message
    */
    updateRole(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        if (req.params.dt && req.params.code) {
            var datetime = new Date();
            const dates = commonmethod.getDates(new Date(req.params.dt), new Date(datetime));
            if (dates.length < 2) {
                usermodel.getUserByEmail(req.params.code).then((userfind) => {
                    if (userfind.length > 0) {
                        usermodel.updateUserStatus(req.params.code).then((emailVerify) => {
                            if (emailVerify.affectedRows > 0) {
                                usersrolesModel.getUpdateUserRoleByuid(userfind[0].uid).then((updateSuc) => {
                                    if (updateSuc.affectedRows > 0) {
                                        return res.status(200).json({ message: "user verified and its role is upgraded to authenticated user." });
                                    } else {
                                        return res.status(200).json({ message: "user verified but its role is not upgraded to authenticated user." });
                                    }
                                }).catch((error) => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                })
                            } else {
                                return res.status(200).json({ message: "user not verified." });
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        return res.status(200).json({ message: "user does not exists." });
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            } else {
                return res.status(200).json({ message: "verification link expired, please registere again." });
            }
        } else {
            return res.status(200).json({ message: "verification link is not valid." });
        }
    }

    /**
    * Function to get all users role
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} users data
    */
    allUsersRoles(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        usersrolesModel.getAllUsersRoles().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get  users role by id
    * @author  MangoIt Solutions
    * @param   {uid}
    * @return  {object} users data
    */
    roleByuid(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        usersrolesModel.getRoleByUser(req.params.uid).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:roleNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get users by roleid
    * @author  MangoIt Solutions
    * @param   {rid}
    * @return  {object} users data
    */
    usersByrid(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        usersrolesModel.getUsersByRole(req.params.rid).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update users role by roleid
    * @author  MangoIt Solutions
    * @param   {uid}with updated role
    * @return  {message} success message
    */
    updateUsersRoles(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        usersrolesModel.getUpdateUserRoles(req).then((result) => {
            if (result.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['user:roleUpdationSuccess']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}