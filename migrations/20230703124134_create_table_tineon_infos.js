exports.up = async function(knex) {
    return knex.schema.createTable('tineon_infos', function(t) {
        t.increments('id').primary();
        t.string('name',[255]).notNull();
        t.string('club_name',[255]).notNull();
        t.string('email',[255]).notNull();
        t.string('association_purpose',[255]);
        t.string('number_of_members',[255]);
        t.string('club_Website',[255]);
        t.timestamp ('created_at').notNull().defaultTo(knex.fn.now());
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('tineon_infos');
};
