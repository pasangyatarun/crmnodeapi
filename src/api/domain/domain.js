import express from 'express';
import DomainController from '../../controllers/domain/domain';

let router = express.Router();
let domainController = new DomainController();

router.get('/domains', domainController.domains.bind(domainController));
router.get('/domainbyid/:id', domainController.domainById.bind(domainController));
router.get('/domainbydomain/:domain', domainController.domainByDomain.bind(domainController));
router.post('/domaincreate', domainController.domainCreate.bind(domainController));
router.put('/domainupdate/:id', domainController.updateDomain.bind(domainController));
router.delete('/domaindelete/:id', domainController.deleteDomain.bind(domainController));

export default router;