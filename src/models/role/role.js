import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class RoleModel {

    async getRoleById(rid) {
        const rows = await db.conn.promise().query("SELECT role.*,role_category.id,role_category.category_name FROM `role` LEFT JOIN `role_category` on role.cat_id = role_category.id WHERE role.rid= ?", [rid]);
        return rows[0];
    }

    async getRoleByRole(role) {
        const rows = await db.conn.promise().query("SELECT * FROM `role` WHERE name = ?", [role]);
        return rows[0];
    }

    async createRole(req) {
        let cat_id;
        if ((req.body.cat_id).length == 0) {
            cat_id = 0;
        }
        else {
            cat_id = req.body.cat_id;
        }
        const rows = await db.conn.promise().query('INSERT INTO `role`(`cat_id`,`name`,`description`)VALUES(?,?,?)', [
            cat_id,
            req.body.role,
            req.body.description
        ]);
        return rows[0];
    }

    async getAllRole() {
        const rows = await db.conn.promise().query("SELECT role.*,role_category.id,role_category.category_name FROM `role` LEFT JOIN `role_category` on role.cat_id = role_category.id");

        return rows[0];
    }

    async deleteRole(id) {
        const rows = await db.conn.promise().query("DELETE FROM `role` WHERE `rid`=?", [id]);
        return rows[0];
    }

    async updateRole(req) {
        const rows = await db.conn.promise().query("UPDATE role SET cat_id=?,name= ?,weight=?,description=? WHERE rid = ?", [
            req.body.cat_id,
            req.body.role,
            0,
            req.body.description,
            req.params.id
        ])
        return rows[0];
    }

    //Role_category table query
    async getAllRoleCategory() {
        const rows = await db.conn.promise().query("SELECT * FROM `role_category`");
        return rows[0];
    }

    async roleCheck(id) {
        const rows = await db.conn.promise().query("SELECT * FROM `users_roles`  JOIN `role`  on users_roles.rid = role.rid  WHERE uid =? ", [id]);
        return rows[0];
    }

    async getRoleWeight(data) {
        const rows = await db.conn.promise().query("SELECT * FROM `role`  WHERE cat_id >= ?", [data[0].cat_id]);
        return rows[0];
    }

}