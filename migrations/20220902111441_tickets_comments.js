exports.up = async function (knex) {
    return knex.schema.createTable('tickets_comments', function (t) {
        t.increments('id').primary();
        t.integer('request_id',[11]).notNull();
        t.longtext('comment').notNull();
        t.string('attachment',[255]);
        t.integer('commented_user',[11]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('tickets_comments');
};

