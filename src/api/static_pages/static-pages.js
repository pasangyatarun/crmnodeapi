import express   from 'express';
import PagesController from '../../controllers/static_pages/static-pages'
import multerMW from '../../core/multer';

let router = express.Router();
let pagesController = new PagesController();

router.get('/pagesbyurl/:url', pagesController.getPagesbyURL.bind(pagesController));
router.get('/pagesbyid/:id', pagesController.getPagesbyId.bind(pagesController));
router.get('/pages', pagesController.getPagesAll.bind(pagesController));
router.post('/pagescreate',multerMW.single('file'), pagesController.pagesCreate.bind(pagesController));
router.delete('/pagesdelete/:id', pagesController.pagesDelete.bind(pagesController));
router.put('/pagesedit/:id', multerMW.single('file'), pagesController.pagesUpdate.bind(pagesController));

export default router;