import async from 'async';
import responseFormat from '../../core/response-format';
import CrmProductModel from "../../models/crm_products/crm_products";
import CrmProductValidation from "../../validators/crm_products/crm_product"
import AWSHandler from '../../core/AWSHander';
import sendEmail from '../../core/nodemailer.js';
import jwt from 'jsonwebtoken';
import configContainer from '../../config/localhost';
import UserModel from '../../models/user/user';
import { dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));
let userModel = new UserModel();
let crmproductModel = new CrmProductModel();
let crmProductValidation = new CrmProductValidation();
export default class CrmProductController {


    /**
    * Function to create crm product
    * @author  MangoIt Solutions
    * @param   {object} with product details
    * @return  {message} success message
    */
    crmproductCreate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmProductValidation.crmproductCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    crmproductModel.getCrmProductByName(req.body.product_name).then((productFind) => {
                        if (productFind.length > 0 && req.params.id != productFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['product:productExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        done();
                    }
                },
                function (done) {
                    crmproductModel.CreateCrmProduct(req, s3URLofImage).then((results) => {
                        if (results) {
                            response = responseFormat.getResponseMessageByCodes(['product:ProductSucess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:Not Created'], { code: 417 });
                            res.status(200).json(response);
                        }
                    })
                }
            ])
        }
    }

    /**
    * Function to get all crm product
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} crm product details
    */
    getCrmProductAll(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let authUserId;
        let userRoleDetail = [];
        let userProducts = [];
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        done();
                    }
                })
            },
            function (done) {
                userModel.getUserRolesData(authUserId).then((resUser) => {
                    if (resUser.length > 0) {
                        for (let index = 0; index < resUser.length; index++) {
                            const element = resUser[index];
                            userRoleDetail.push(element.cat_id)
                            if (resUser.length == index + 1) {
                                done();
                            }
                        }
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:roleNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                crmproductModel.getCrmProductAll().then((results) => {
                    if (results.length > 0) {
                        for (let i = 0; i < results.length; i++) {
                            let a2 = JSON.parse(results[i].non_visible_role_category);
                            const a3 = userRoleDetail.filter(entry1 => !a2.some(entry2 => entry1 === entry2));
                            if (a3.length > 0) {
                                userProducts.push(results[i])
                            }
                            if (results.length == i + 1) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userProducts } });
                                res.status(200).json(response);
                            }
                        };
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get crm product by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} crm product details
    */
    getcrmproductsbyid(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getcrmproductsbyid(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to update crm product by id
    * @author  MangoIt Solutions
    * @param   {id} with updated crm product details
    * @return  {success} success message
    */
    crmproductUpdate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmProductValidation.crmproductCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    crmproductModel.getCrmProductByName(req.body.product_name).then((productFind) => {
                        if (productFind.length > 0 && req.params.id != productFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['product:productExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                        done();
                    } else {
                        done();
                    }
                },
                function (done) {
                    crmproductModel.crmproductUpdate(req, s3URLofImage).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['products:productUpdationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete crm product by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {success} success message
    */
    crmproductDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        crmproductModel.deleteCrmProductbyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to create crm product order 
    * @author  MangoIt Solutions
    * @param   {user_id} 
    * @return  {success} success message
    */
    crmproductOrderCreate(req, res, next) {
        let text;
        let EndDate;
        let orderDetails;
        let file;
        let s3URLofImage;
        let clubname = "";
        let userEmail = '';
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getClubOrder(req.params.user_id).then((productFind) => {
                    if (productFind.length > 0) {
                        EndDate = productFind.find(o => !JSON.parse(o.commit_response).EndDate)
                        if (EndDate) {
                            done();
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['Club:clubProduct'], { code: 417 });
                            res.status(200).json(response);
                        }
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['Club:clubProduct'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                crmproductModel.getCrmProductOrderCheck(req.params.user_id, req.params.product_id).then((productFind) => {
                    if (productFind.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['product:OrderProductExist'], { code: 417 });
                        res.status(200).json(response)
                    } else {
                        done();
                    }
                })
            },
            function (done) {
                if (req.file) {
                    file = req.file;
                    AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                        s3URLofImage = result.Location;
                        done();
                    }).catch(function (err) {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                crmproductModel.CreateCrmProductOrder(req, s3URLofImage).then((results) => {
                    if (results.affectedRows > 0) {
                        orderDetails = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:OrdereFailed'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                userModel.getUserDetail(req.params.user_id).then((userData) => {
                    clubname = userData[0].society;
                    userEmail = userData[0].mail;
                    done();
                })
            },
            function (done) {
                let link = configContainer.crmDashboard;
                if (s3URLofImage) {
                    let mailOptions;
                    let documentUrl = s3URLofImage;
                    mailOptions = {
                        to: configContainer.supportMail,
                        subject: 'Datenimportservice über neue Bestellplattform bestellt |' + clubname,

                        // html: `<!doctype html>
                        //     <html lang="en-US">

                        //     <head>
                        //     <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                        //     <title>Reset Password Email</title>
                        //     <meta name="description" content="Reset Password Email Template.">
                        //     <style type="text/css">
                        //     a:hover {text-decoration: underline !important;}
                        //     </style>
                        //     </head>

                        //     <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                        //    <p>Kunde hat Zusatz für Produkt S-Verein | Online-Vereinsverwaltung bestellt.</p>
                        //     <p>Kunde: `+ clubname + `</p>
                        //     <p>Kunden-eMail: `+ userEmail + `</p>

                        //         </body>

                        //         </html>`,
                        html : `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                        <head>
                        <!--[if (gte mso 9)|(IE)]>
                          <xml>
                            <o:OfficeDocumentSettings>
                            <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                          </o:OfficeDocumentSettings>
                        </xml>
                        <![endif]-->
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                        <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                        <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                        <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                        <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                        <meta name="color-scheme" content="only">
                        <title></title>
                        
                        <link rel="preconnect" href="https://fonts.gstatic.com">
                        <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                        
                        <style type="text/css">
                        /*Basics*/
                        body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                        table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                        table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                        td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                        td p {margin:0; padding:0;}
                        td div {margin:0; padding:0;}
                        td a {text-decoration:none; color: inherit;} 
                        /*Outlook*/
                        .ExternalClass {width: 100%;}
                        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                        .ReadMsgBody {width:100%; background-color: #ffffff;}
                        /* iOS BLUE LINKS */
                        a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                        /*Gmail blue links*/
                        u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                        /*Buttons fix*/
                        .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                        .yshortcuts a {border-bottom:none !important;
                        
                        .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                        /*Responsive*/
                        @media screen and (max-width: 639px) {
                          table.row {width: 100%!important;max-width: 100%!important;}
                          td.row {width: 100%!important;max-width: 100%!important;}
                          .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                          .center-float {float: none!important;margin:auto!important;}
                          .center-text{text-align: center!important;}
                          .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                          .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                          .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                          .hide-mobile {display: none!important;}
                          .menu-container {text-align: center !important;}
                          .autoheight {height: auto!important;}
                          .m-padding-10 {margin: 10px 0!important;}
                          .m-padding-15 {margin: 15px 0!important;}
                          .m-padding-20 {margin: 20px 0!important;}
                          .m-padding-30 {margin: 30px 0!important;}
                          .m-padding-40 {margin: 40px 0!important;}
                          .m-padding-50 {margin: 50px 0!important;}
                          .m-padding-60 {margin: 60px 0!important;}
                          .m-padding-top10 {margin: 30px 0 0 0!important;}
                          .m-padding-top15 {margin: 15px 0 0 0!important;}
                          .m-padding-top20 {margin: 20px 0 0 0!important;}
                          .m-padding-top30 {margin: 30px 0 0 0!important;}
                          .m-padding-top40 {margin: 40px 0 0 0!important;}
                          .m-padding-top50 {margin: 50px 0 0 0!important;}
                          .m-padding-top60 {margin: 60px 0 0 0!important;}
                          .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                          .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                          .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                          .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                          .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                          .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                          .center-on-mobile {text-align: center!important;}
                        }
                        </style>
                        </head>
                        
                        <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                        
                        <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                        
                        <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                          <tr><!-- Outer Table -->
                            <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- Preheader -->
                          <tr>
                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- Preheader -->
                        </table>
                        
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- simpli-header-1 -->
                          <tr>
                            <td align="center">
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                          <!-- bg-image -->
                          <tr>
                            <td align="center" style="border-radius: 36px;">
                        
                        <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                        <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                        <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                        
                        <div>
                        <!-- simpli-header-bg-image -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                          <tr>
                            <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                        
                        <!-- Content -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                          <tr>
                            <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                        
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                              <tr>
                                <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                              </tr>
                            </table>
                        
                            </td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                        </table>
                        <!-- simpli-header-bg-image -->
                        </div>
                        
                        <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                        
                            </td>
                          </tr>
                          <!-- bg-image -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                          <!-- basic-info -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            NEUE BESTELLUNG
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Kunde: "`+ clubname +`"
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Kunde hat ein Datenimportservice bestellt<br>
                                            Datum: "`+ new Date().toJSON() +`"
                        
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <!-- Button -->
                                    <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                      <tr>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                          <td align="center" width="35"></td>
                                          <td align="center" height="50" style="height:50px;">
                                          <![endif]-->
                                            <singleline>
                                              <a href="`+link+`" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Antworten</span></a>
                                            </singleline>
                                          <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                          <td align="center" width="35"></td>
                                        </tr>
                                      </table>
                                    <![endif]-->
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Buttons -->
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- basic-info -->
                        </table>
                        
                            </td>
                          </tr>
                          <!-- simpli-header-1 -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                          <!-- simpli-footer -->
                          <tr>
                            <td align="center">
                              
                        <!-- Content -->
                        
                          <tr>
                            <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                    <multiline>
                                      <div mc:edit Simpli>
                                        <br />
                                        Tineon Aktiengesellschaft <br />
                                        Uferpromenade 5, 88709 Meersburg <br />
                                        Registergericht Freiburg HRB 710927 <br />
                                        Vorstand: Jean-Claude Parent <br />
                                        Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                        USt.-IdNr. DE203912818 <br />
                                      </div>
                                    </multiline>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                          <!-- simpli-footer -->
                        </table>
                        
                            </td>
                          </tr><!-- Outer-Table -->
                        </table>
                        
                        </body>
                        </html>
                        `,
                        attachments: [
                            {
                                filename: req.file.originalname,
                                path: s3URLofImage
                            }
                        ]
                    }

                    sendEmail(mailOptions).then((sentdata) => {
                        if (sentdata == 'error') {
                            response = responseFormat.getResponseMessageByCodes(['product:productOrderedButMailNot']);
                            res.status(200).json(response);
                          //  done();
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes(['product:ProductOrderSucess']);
                res.status(200).json(response);
            }
        ])

    }

    /**
    * Function to get crm product order list
    * @author  MangoIt Solutions
    * @param   {user_id} 
    * @return  {object} order list
    */
    getcrmproductOrderList(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getCrmOrderByUser(req.params.user_id).then((orders) => {
                    if (orders.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: orders } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:OrderedNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])

    }

    /**
    * Function to get all order details for crm product
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} order details
    */
    getCrmOrderAll(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getCrmAllOrders().then((orders) => {
                    if (orders.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: orders } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:OrderedNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get order details by order_id
    * @author  MangoIt Solutions
    * @param   {order_id} 
    * @return  {object} order details
    */
    getcrmOrderDetail(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getorderDetail(req.params.order_id).then((orderDetail) => {
                    if (orderDetail.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: orderDetail } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:OrderedNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })

            }

        ])
    }
    

    /**
    * Function to get all CRM products 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    getAllCrmProduct(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getallcrmproducts().then((result) => {
                    if (result.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })

            }

        ])
    }

      /**
    * Function to get crm product by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} crm product details
    */
      getcrmproductsbyproductid(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getcrmproductsbyproductid(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    
 /**
    * Function to update ordered crm product status by admin
    * @author  MangoIt Solutions
    * @param   {order_id,user_id} with status
    * @return  {success} success message
    */
 crmOrderProductStatusUpdate(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                crmproductModel.getCrmOrderProductById(req.params.order_id).then((orderFind) => {
                    if (orderFind.length > 0) {
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:OrderedNotExists'], { code: 417 });
                        res.status(200).json(response)
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                crmproductModel.crmOrderProductStatusUpdate(req).then((result) => {
                    if (result.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['order:orderUpdationSuccess']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderUpdationFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    
}

}
