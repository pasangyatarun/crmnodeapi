import CommonMethods from '../../core/common-methods.js';
let commonMethods = new CommonMethods();

export default class UsersValidation {

    userAddValidation(userData){
        let err = new Array();
      
        if (!userData.email || !userData.email.trim() || (!commonMethods.validateEmailid(userData.email))) {
            err.push('email:email');
        }
        if(!userData.society ){
            err.push('society:societyName');
        }       
        if (!userData.userRole && userData.userRole.length > 0) {
            err.push('userRole:roleReq');
        }       
        return err;
    }

    commissionSettingValidation(commData){
        let err = new Array();
      
        if(!commData.salesPartnerId ){
            err.push('salesPartnerId:salesPartnerIdReq');
        }
        if(!commData.productId ){
            err.push('productId:productIdReq');
        }
        if(!commData.commissionAmtType ){
            err.push('commissionAmtType:commissionAmtTypeReq');
        }
        if(!commData.commissionAmt ){
            err.push('commissionAmt:commissionAmtReq');
        }
        if(!commData.commissionDurationType ){
            err.push('commissionDurationType:commissionDurationTypeReq');
        }
        if(!commData.startDate ){
            err.push('startDate:startDateReq');
        }
        return err;
    }

}