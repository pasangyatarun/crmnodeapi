import express from 'express';
import ProductComponentsController from '../../controllers/product_components/product_components';

let router = express.Router();
let productComponentsController = new ProductComponentsController();

router.get('/productComponents', productComponentsController.productComponents.bind(productComponentsController));
router.get('/productComponentsbyid/:id', productComponentsController.productComponentsById.bind(productComponentsController));
router.get('/productComponentsbyname/:name', productComponentsController.productComponentsByName.bind(productComponentsController));
router.post('/productComponentscreate', productComponentsController.productComponentsCreate.bind(productComponentsController));
router.put('/productComponentsupdate/:id', productComponentsController.updateproductComponents.bind(productComponentsController));
router.delete('/productComponentsdelete/:id', productComponentsController.deleteproductComponents.bind(productComponentsController));

export default router;