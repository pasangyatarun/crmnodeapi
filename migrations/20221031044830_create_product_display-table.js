exports.up = async function(knex) {
    return knex.schema.createTable('product_display', function(t) {
        t.increments('id').primary();
        t.string('product_id',[255]).notNull();
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('product_display');
};
