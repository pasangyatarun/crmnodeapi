exports.up = async function(knex) {
    return knex.schema.createTable('users', function(t) {
        t.increments('uid').primary();
        t.integer('auther_id',[11]);
        t.string('name',[60]).defaultTo('').comment('Unique user name.');
        t.string('pass',[128]).notNull().defaultTo('').comment('Userâ€™s password (hashed).');
        t.string('mail',[254]).defaultTo('').comment('Userâ€™s e-mail address.');
        t.string('theme',[255]).defaultTo('').comment('Userâ€™s default theme.');
        t.string('signature',[255]).defaultTo('').comment('Userâ€™s signature.');
        t.string('signature_format',[255]).comment('The filter_format.format of the signature');
        t.string('created',[32]).notNull().defaultTo(0).comment('Timestamp for when user was created.');
        t.string('access',[32]).notNull().defaultTo(0).comment('Timestamp for previous time user accessed the site.');
        t.string('login',[32]).notNull().defaultTo(0).comment('Timestamp for userâ€™s last login.');
        t.tinyint('status',[4]).notNull().comment('Whether the user is active(1) or blocked(0).');
        t.string('timezone',[32]).comment('Userâ€™s time zone.');
        t.string('language',[12]).notNull().defaultTo('').comment('Userâ€™s default language.');
        t.string('picture ',[500]).defaultTo('').comment('Foreign key: file_managed.fid of userâ€™s picture.');
        t.string('init',[254]).defaultTo('').comment('E-mail address used for initial account creation.');
        t.longtext('data').comment('A serialized array of name value pairs that are related to the user. Any form values posted during user edit are stored and are loaded into the $user object during user_load(). Use of this field is discouraged and it will likely disappear in a future...');
        t.string('contact_number',[20]);
        t.string('process_status',[20]);
        t.integer('support_access',[11]).notNull().defaultTo(0);
        t.string('licence_block_status',[20]);
        
    })
    .then(() =>
    knex("users").insert([
   { pass: "$2a$10$96TBmqHxepdB0vEoAzMwzOKIWOwde2Jzk/h1HbGFMQRMYJfQ7DMtO",mail:"admin@gmail.com",signature_format:"plain_text",created:"1641190900943",access:"1641190900943",login:"1641190900943",status:1,picture:"https://tineon.s3.eu-central-1.amazonaws.com/Leipzig/2022/July/Images/clem-onojeghuo-2.jpg",init:"admin@gmail.com",contact_number:"82562556412",support_access:0},
   { pass: "$2a$10$YyThhzc0nJKAVwPB.F0/Du2UD6aaG7NhM1JXW2PxNkZgZP2MJaaFO",mail:"super-admin@mailinator.com",signature_format:"plain_text",created:"1650020296126",access:"1660024480701",login:"1660024480701",status:1,picture:"https://tineon.s3.eu-central-1.amazonaws.com/Leipzig/2022/July/Images/clem-onojeghuo-2.jpg",init:"admin2@mailinator.com",contact_number:"452544545445",support_access:0},
    
])
)
};

exports.down = async function(knex) {
    await knex.schema.dropTable('users');
};
