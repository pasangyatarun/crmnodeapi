
import responseFormat from '../../core/response-format';
import FaqStatusModel from '../../models/faq_status/faq_status';
import CrmFaqStatusValidation from '../../validators/faq_status/faq_status';
import async from 'async';

let faqStatusModel = new FaqStatusModel();
let crmFaqStatusValidation = new CrmFaqStatusValidation();

export default class FaqStatusController {

    /**
    * Function to create faq status
    * @author  MangoIt Solutions
    * @param   {object} with status
    * @return  {message} success message
    */
    createFaqStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmFaqStatusValidation.crmFaqStatusCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    faqStatusModel.getFaqStatusByName(req.body.status).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Status:StatusExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    faqStatusModel.CreateFaqStatus(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Status:StatusSucess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['Status:StatusCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get all faq status 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} status details
    */
    getAllFaqStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqStatusModel.AllFaqStatus().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Status:StatusNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get all faq status 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} status details
    */
    getFaqStatusbyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqStatusModel.getFaqStatusbyId(req.params.sid).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['faq:faqNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update faq status 
    * @author  MangoIt Solutions
    * @param   {id} with updated status details
    * @return  {object} status details
    */
    updateFaqStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmFaqStatusValidation.crmFaqStatusCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    faqStatusModel.checkFaqStatusByName(req).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Status:StatusExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    faqStatusModel.getUpdateFAQStatus(req).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Status:statusUpdate']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['category:categoryUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete faq status 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                faqStatusModel.checkFaqStatusSelected(req.params.sid).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['Status:StausAssigned'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                faqStatusModel.deleteStatus(req.params.sid).then((result) => {
                    if (result.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['Status:statusDelete']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['Status:StatusDeleteionFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }
}



