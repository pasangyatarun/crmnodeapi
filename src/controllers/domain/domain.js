import async from 'async';
import DomainModel from '../../models/domain/domain';
import DomainValidation from '../../validators/domain/domain.js';
import responseFormat from '../../core/response-format';
let domainModel = new DomainModel();
let domainValidation = new DomainValidation();

export default class DomainController {

    /**
    * Function to create domain
    * @author  MangoIt Solutions
    * @param   {object} with domain details
    * @return  Succes message
    */
    domainCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = domainValidation.domainValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    domainModel.getDomainById(req.body.domain_id).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainIdExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    domainModel.getDomainBydomain(req.body.domain).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainNameExists'], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    domainModel.createDomain(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainCreated']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get all domain
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} domain details
    */
    domains(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        domainModel.getAllDomain().then((results) => {
            if (results) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }


    /**
    * Function to get domain details by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} domain details
    */
    domainById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        domainModel.getDomainById(req.params.id).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get domain details by domain
    * @author  MangoIt Solutions
    * @param   {domain} 
    * @return  {object} domain details
    */
    domainByDomain(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        domainModel.getDomainBydomain(req.params.domain).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            }
            else {
                response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to update domain details by id
    * @author  MangoIt Solutions
    * @param   {id}  with updated domain details
    * @return  {message} success message
    */
    updateDomain(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        let msgCode = domainValidation.domainValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    domainModel.getDomainById(req.params.id).then((results) => {
                        if (results.length < 1) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    domainModel.getDomainBydomain(req.body.domain).then((results) => {
                        if (results.length > 0 && results[0].domain_id != req.params.id) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainNameExists'], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            done();
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    domainModel.updateDomain(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainUpdateSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete domain details by id
    * @author  MangoIt Solutions
    * @param   {id}  
    * @return  {message} success message
    */
    deleteDomain(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        domainModel.deleteDomain(req.params.id).then((results) => {
            if (results.affectedRows) {
                response = responseFormat.getResponseMessageByCodes(['domain:domainDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['domain:domainNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}
