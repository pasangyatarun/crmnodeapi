import async from 'async';
import responseFormat from '../../core/response-format';
import UserModel from '../../models/user/user';
import ProductModel from '../../models/product/product.js';
import ProductValidation from '../../validators/products/product.js';
import EmailTemplateModel from '../../models/email_template/email-template'
import ProductPriceModel from '../../models/product_price/product-price.js';
import configContainer from '../../config/localhost';
import sendEmail from '../../core/nodemailer.js';
let proPriceModel = new ProductPriceModel();
let productModel = new ProductModel();
let userModel = new UserModel();
let proValidation = new ProductValidation();
let emailTemplateModel = new EmailTemplateModel();

export default class ProductController {

    /**
    * Function to get all customer products
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    getProductCustomerByAdmin(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                productModel.getAllProductToCustomer().then((productData) => {
                    if (productData.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: productData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get all product for customer
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    getProductAdmintoCustomer(req, res, next) {
        let userDa;
        let templateData;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userModel.getUserById(req.body.userId).then((userData) => {
                    if (userData.length > 0) {
                        userDa = userData;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                for (let index = 0; index < req.body.assignedProducts.length; index++) {
                    const element = req.body.assignedProducts[index];
                    productModel.selectProductFromAdmin(element.id).then((resData) => {
                        if (resData.length > 0) {
                            productModel.getDeleteAdminProduct(resData[0].id).then((deletedRes) => {
                                productModel.assignProductAdminToCustomer(req, element).then((results) => {
                                    if (results.affectedRows > 0) {
                                        if (index + 1 == req.body.assignedProducts.length) {
                                            done();
                                        }
                                    } else {
                                        response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                }).catch((error) => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                })
                            })
                        } else {
                            productModel.assignProductAdminToCustomer(req, element).then((results) => {
                                if (results.affectedRows > 0) {
                                    if (index + 1 == req.body.assignedProducts.length) {
                                        done();
                                    }
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    })
                }

            },
            function (done) {
                emailTemplateModel.getemailTemplateByType('Admin Product Assign').then((data) => {
                    if (data.length > 0) {
                        templateData = data;
                        done();
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                let mailOptions;
                for (let k = 0; k < req.body.assignedProducts.length; k++) {
                    let link = configContainer.adminToCustomerProductAssignLink;
                    let encrypted = link + req.body.assignedProducts[k].id;
                    // if (templateData && templateData.length > 0) {
                    //     mailOptions = {
                    //         to: userDa[0].mail,
                    //         subject: templateData[0].subject,
                    //         html: `<!doctype html>
                    //         <html lang="en-US">
                            
                    //         <head>
                    //         <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                    //         <title>Reset Password Email</title>
                    //         <meta name="description" content="Reset Password Email Template.">
                    //         <style type="text/css">
                    //         a:hover {text-decoration: underline !important;}
                    //         </style>
                    //         </head>
                            
                    //         <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                    //         <!--100% body table-->
                    //         <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                    //         style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                    //         <tr>
                    //         <td>
                    //         <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                    //         align="center" cellpadding="0" cellspacing="0">
                    //         <tr>
                    //         <td style="height:80px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td style="text-align:center;">
                    //         <a href="#" title="logo" target="_blank">
                    //         <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                    //         </a>
                    //         </td>
                    //         </tr>
                    //         <tr>
                    //         <td style="height:20px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td>
                    //         <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                    //         style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                    //         <tr>
                    //         <td style="height:40px;">&nbsp;</td>
                    //         </tr>
                    //         <tr>
                    //         <td style="padding:0 35px;">
                    //         <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->
                            
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>
                                                                   
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                    //                                                 `+ templateData[0].template_body + `:<br>
                    //                                                 <a href="`+ encrypted + `">` + encrypted + `</a>
                    //                                                 </p><br><br>
                    //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                    //                                                 <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                    //                                                 <!-- <a href="javascript:void(0);"
                    //                                                 style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                    //                                                     Password</a> -->
                    //                                                     </td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="height:40px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     </table>
                    //                                                     </td>
                    //                                                     <tr>
                    //                                                     <td style="height:20px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="text-align:center;">
                    //                                                     <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                    //                                                     </td>
                    //                                                     </tr>
                    //                                                     <tr>
                    //                                                     <td style="height:80px;">&nbsp;</td>
                    //                                                     </tr>
                    //                                                     </table>
                    //                     </td>
                    //                 </tr>
                    //             </table>
                    //             <!--/100% body table-->
                    //             </body>
                                
                    //             </html>`,
                    //     }
                    // } else {
                    //     mailOptions = {
                    //         to: userDa[0].mail,
                    //         subject: 'You have assigned a product from Tineon AG',
                    //         html: `<h3>Dear association member,</h3>

                    //             <h4>Thank you for your interest in our S-Club |
                    //             Online club management.</h4>
                    //             <h5> Admin is assigned you a product.</h5>
                    //            <h5> Please confirm and order your product via the following link: </h5>
                    //          <a href="`+ encrypted + `">` + encrypted + `</a>
                    //          <h3>If the forwarding does not work, please copy the link in
                    //             the address bar of your browser.
                                
                    //             After you have successfully confirmed your e-mail address, the access data will be sent
                    //             sent to the specified email address.</h3>`,
                    //     }
                    // }
                    mailOptions = {
                                to: userDa[0].mail,
                                subject: "Ihnen wurde ein Produkt zugeordnet",
                                html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                                <head>
                                <!--[if (gte mso 9)|(IE)]>
                                  <xml>
                                    <o:OfficeDocumentSettings>
                                    <o:AllowPNG/>
                                    <o:PixelsPerInch>96</o:PixelsPerInch>
                                  </o:OfficeDocumentSettings>
                                </xml>
                                <![endif]-->
                                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                                <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                                <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                                <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                                <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                                <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                                <meta name="color-scheme" content="only">
                                <title></title>
                                
                                <link rel="preconnect" href="https://fonts.gstatic.com">
                                <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                                <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                                
                                <style type="text/css">
                                /*Basics*/
                                body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                                table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                                table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                                td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                                td p {margin:0; padding:0;}
                                td div {margin:0; padding:0;}
                                td a {text-decoration:none; color: inherit;} 
                                /*Outlook*/
                                .ExternalClass {width: 100%;}
                                .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                                .ReadMsgBody {width:100%; background-color: #ffffff;}
                                /* iOS BLUE LINKS */
                                a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                                /*Gmail blue links*/
                                u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                                /*Buttons fix*/
                                .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                                .yshortcuts a {border-bottom:none !important;
                                
                                .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                                /*Responsive*/
                                @media screen and (max-width: 639px) {
                                  table.row {width: 100%!important;max-width: 100%!important;}
                                  td.row {width: 100%!important;max-width: 100%!important;}
                                  .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                                  .center-float {float: none!important;margin:auto!important;}
                                  .center-text{text-align: center!important;}
                                  .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                                  .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                                  .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                                  .hide-mobile {display: none!important;}
                                  .menu-container {text-align: center !important;}
                                  .autoheight {height: auto!important;}
                                  .m-padding-10 {margin: 10px 0!important;}
                                  .m-padding-15 {margin: 15px 0!important;}
                                  .m-padding-20 {margin: 20px 0!important;}
                                  .m-padding-30 {margin: 30px 0!important;}
                                  .m-padding-40 {margin: 40px 0!important;}
                                  .m-padding-50 {margin: 50px 0!important;}
                                  .m-padding-60 {margin: 60px 0!important;}
                                  .m-padding-top10 {margin: 30px 0 0 0!important;}
                                  .m-padding-top15 {margin: 15px 0 0 0!important;}
                                  .m-padding-top20 {margin: 20px 0 0 0!important;}
                                  .m-padding-top30 {margin: 30px 0 0 0!important;}
                                  .m-padding-top40 {margin: 40px 0 0 0!important;}
                                  .m-padding-top50 {margin: 50px 0 0 0!important;}
                                  .m-padding-top60 {margin: 60px 0 0 0!important;}
                                  .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                                  .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                                  .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                                  .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                                  .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                                  .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                                  .center-on-mobile {text-align: center!important;}
                                }
                                </style>
                                </head>
                                
                                <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                                
                                <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                                
                                <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                                  <tr><!-- Outer Table -->
                                    <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- Preheader -->
                                  <tr>
                                    <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <!-- Preheader -->
                                </table>
                                
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                                  <!-- simpli-header-1 -->
                                  <tr>
                                    <td align="center">
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                                  <!-- bg-image -->
                                  <tr>
                                    <td align="center" style="border-radius: 36px;">
                                
                                <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                                <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                                <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                                
                                <div>
                                <!-- simpli-header-bg-image -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                                
                                <!-- Content -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                                  <tr>
                                    <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                                
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                      <tr>
                                        <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                      </tr>
                                    </table>
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                </table>
                                <!-- simpli-header-bg-image -->
                                </div>
                                
                                <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                
                                    </td>
                                  </tr>
                                  <!-- bg-image -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                                  <!-- basic-info -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    HERZLICH WILLKOMMEN!
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Bestätigung E-Mailadresse
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Ihnen wurde ein Produkt der Tineon AG zugewiesen.<br>
                                                    Sie können dieses über unser Bestellportal einsehen. Bestätigen Sie zunächst über den nachfolgenden Link Ihre E-Mail Adresse.<br>
                                                    Danach können Sie ein persönliches Passwort festlegen und erhalten einen sicheren Zugang zu unserem Bestellsystem.
                                
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                            <!-- Button -->
                                            <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                              <tr>
                                                <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                            <!--[if (gte mso 9)|(IE)]>
                                              <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                  <td align="center" width="35"></td>
                                                  <td align="center" height="50" style="height:50px;">
                                                  <![endif]-->
                                                    <singleline>
                                                      <a href="`+encrypted+`" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>BESTÄTIGEN</span></a>
                                                    </singleline>
                                                  <!--[if (gte mso 9)|(IE)]>
                                                  </td>
                                                  <td align="center" width="35"></td>
                                                </tr>
                                              </table>
                                            <![endif]-->
                                                </td>
                                              </tr>
                                            </table>
                                            <!-- Buttons -->
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- basic-info -->
                                </table>
                                
                                    </td>
                                  </tr>
                                  <!-- simpli-header-1 -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1A -->
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Weitere Informationen!
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Wie geht es weiter?
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/mouseclick.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Ausführlich Testen
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Die S-Verein Vereinsverwaltung können Sie mit Echtdaten testen.<br />
                                                            Nach der Testphase werden diese Daten entweder rückstandslos gelöscht oder, sofern Sie die Lizenz bis dahin verbindlich bestellen, bleiben sie erhalten.
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- content-1A -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1B -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/usericon.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Datenimport
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Eine CSV-Schnittstelle unterstützt Sie beim Datenimport. Gerne übernehmen wir den Datenimport als Dienstleistung für Sie. <br />
                                                            Dabei prüfen wir insbesondere die Bankdaten ihrer Mitglieder auf Plausibilität, um Rückläufer beim Beitragseinzug zu vermeiden.
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <!-- content-1B -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                                  <!-- content-1C -->
                                  <tr>
                                    <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                                      <!-- content -->
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td align="center">
                                          
                                          <!-- 2-columns -->
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                            <tr>
                                              <td align="center">
                                
                                              <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                                <tr>
                                                  <td align="center">
                                                    <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/fragezeichen.png" border="0" editable="true" alt="icon">
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- gap -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                                <tr>
                                                  <td height="20" style="font-size:20px;line-height:20px;"></td>
                                                </tr>
                                              </table>
                                              <!-- gap -->
                                
                                              <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                                
                                              <!-- column -->
                                              <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                                <tr>
                                                  <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Noch Fragen?
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;padding-bottom: 25px;">
                                                      <singleline>
                                                        <div mc:edit Simpli>
                                                            Wenn Sie Hilfestellung benötigen, unterstützen wir Sie gerne mit Tutorials zu den einzelnen Funktionen oder gerne auch mit unserem E-Mail Support unter support@s-verein.de
                                                        </div>
                                                      </singleline>
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- column -->
                                              <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                                <tr>
                                                  <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                              <!--[if (gte mso 9)|(IE)]>
                                                <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                  <tr>
                                                    <td align="center" width="35"></td>
                                                    <td align="center" height="50" style="height:50px;">
                                                    <![endif]-->
                                                      <singleline>
                                                        <a href="https://hilfe.s-verein.de/" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Hilfe-Seite</span></a>
                                                      </singleline>
                                                    <!--[if (gte mso 9)|(IE)]>
                                                    </td>
                                                    <td align="center" width="35"></td>
                                                  </tr>
                                                </table>
                                              <![endif]-->
                                                  </td>
                                                  <td>
                                                    <pre>           </pre>
                                                  </td>
                                                  <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                                    <!--[if (gte mso 9)|(IE)]>
                                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                        <tr>
                                                          <td align="center" width="35"></td>
                                                          <td align="center" height="50" style="height:50px;">
                                                          <![endif]-->
                                                            <singleline>
                                                              <a href="https://www.youtube.com/@supports-verein6588" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>TUTORIALS</span></a>
                                                            </singleline>
                                                          <!--[if (gte mso 9)|(IE)]>
                                                          </td>
                                                          <td align="center" width="35"></td>
                                                        </tr>
                                                      </table>
                                                    <![endif]-->
                                                        </td>
                                                </tr>
                                              </table>
                                
                                              <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                
                                              </td>
                                            </tr>
                                          </table>
                                          <!-- 2-columns -->
                                
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                        </tr>
                                      </table>
                                      <!-- content -->
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <!-- content-1C -->
                                </table>
                                
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                                  <!-- simpli-footer -->
                                  <tr>
                                    <td align="center">
                                      
                                <!-- Content -->
                                
                                  <tr>
                                    <td align="center">
                                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                        <tr>
                                          <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                            <multiline>
                                              <div mc:edit Simpli>
                                                Tineon Aktiengesellschaft <br />
                                                Uferpromenade 5, 88709 Meersburg <br />
                                                Registergericht Freiburg HRB 710927 <br />
                                                Vorstand: Jean-Claude Parent <br />
                                                Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                                USt.-IdNr. DE203912818 <br />
                                              </div>
                                            </multiline>
                                          </td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td align="center" class="center-text">
                                      <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                                  </tr>
                                </table>
                                <!-- Content -->
                                
                                    </td>
                                  </tr>
                                  <!-- simpli-footer -->
                                </table>
                                
                                    </td>
                                  </tr><!-- Outer-Table -->
                                </table>
                                
                                </body>
                                </html>
                                `,
                            }

                    sendEmail(mailOptions).then((sentdata) => {
                        if (sentdata == 'error') {
                            response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productAssigned']);
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })



                }
            }
        ])
    }

    /**
    * Function to delete sales product 
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteSalesProduct(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productModel.getDeleteSalesProduct(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }

        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete distributor product 
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteDistributorProduct(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productModel.getDeleteDistributorProduct(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get distributor product 
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} product details
    */
    productByDistributor(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let productDetails;
        async.waterfall([
            function (done) {
                productModel.getAllProductBydId(req).then((results) => {
                    if (results.length > 0) {
                        productDetails = results;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: productDetails } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get sales product 
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} product details
    */
    productBySales(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let productDetails;
        async.waterfall([
            function (done) {
                productModel.getAllProductsBySales(req).then((results) => {
                    if (results.length > 0) {
                        productDetails = results;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: productDetails } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to  product  assigned to Distributor
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    assignToDistributor(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let idVariable;
        async.waterfall([
            function (done) {
                for (let index = 0; index < req.body.assignedProducts.length; index++) {
                    const element = req.body.assignedProducts[index];
                    productModel.selectProductToDistributor(element.id).then((resData) => {
                        if (resData.length > 0) {
                            productModel.getDeleteDistributorProduct(resData[0].id).then((deletedRes) => {
                                productModel.assignProductToDistributor(req, element.id).then((results) => {
                                    if (results.affectedRows > 0) {
                                        if (index + 1 == req.body.assignedProducts.length) {
                                            response = responseFormat.getResponseMessageByCodes(['product:productAssigned']);
                                            res.status(200).json(response);
                                        }
                                    } else {
                                        response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                }).catch((error) => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                })
                            })
                        } else {
                            productModel.assignProductToDistributor(req, element.id).then((results) => {
                                if (results.affectedRows > 0) {
                                    if (index + 1 == req.body.assignedProducts.length) {
                                        response = responseFormat.getResponseMessageByCodes(['product:productAssigned']);
                                        res.status(200).json(response);
                                    }
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    })
                }
            }
        ])
    }


    /**
    * Function to  assigned product to sales
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    assignToSales(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        for (let index = 0; index < req.body.assignedProducts.length; index++) {
            const element = req.body.assignedProducts[index];
            productModel.selectProductToSales(element.id).then((resData) => {
                if (resData.length > 0) {
                    productModel.getDeleteSalesProduct(resData[0].id).then((deletedRes) => {
                        productModel.assignProductToSales(req, element.id).then((results) => {
                            if (results.affectedRows > 0) {
                                if (index + 1 == req.body.assignedProducts.length) {
                                    response = responseFormat.getResponseMessageByCodes(['product:productAssigned']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    })
                } else {
                    productModel.assignProductToSales(req, element.id).then((results) => {
                        if (results.affectedRows > 0) {
                            if (index + 1 == req.body.assignedProducts.length) {
                                response = responseFormat.getResponseMessageByCodes(['product:productAssigned']);
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productAssignedFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            })
        }
    }

    /**
    * Function to  create Product
    * @author  MangoIt Solutions
    * @param   {object} with product details
    * @return  {message} success message
    */
    productCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        let msgCode = proValidation.productValidate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            let insid;
            async.waterfall([
                function (done) {
                    productModel.createProduct(req).then((results) => {
                        if (results.affectedRows > 0) {
                            insid = results.insertId;
                            proPriceModel.createProPrice(req, results.insertId).then((resu) => {
                                if (resu.affectedRows > 0) {
                                    done();
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['product:productPriceFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }

                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    for (let indexx = 0; indexx < req.body.quantities.length; indexx++) {
                        const element = req.body.quantities[indexx];
                        productModel.createProDetail(req, insid, element.productDetails).then((results) => {
                            if (results.affectedRows > 0) {
                                if (indexx + 1 == req.body.quantities.length) {
                                    done();
                                }
                            }
                            else {
                                response = responseFormat.getResponseMessageByCodes(['product:productDetailFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['product:productSuccess']);
                    res.status(200).json(response);
                }
            ])
        }
    }

    /**
    * Function to  get all Product
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    products(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productModel.getAllProducts().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to  get Product by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} product details
    */
    productById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productModel.getProductById(req.params.pid).then((result) => {
            if (result.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: result } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })

    }

    /**
    * Function to  update Product by id
    * @author  MangoIt Solutions
    * @param   {id} with updated product details
    * @return  {message} success message
    */
    updateProduct(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        let msgCode = proValidation.productValidate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    productModel.getProductById(req.params.pid).then((results) => {
                        if (!results) {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productModel.updateProduct(req).then((results) => {
                        if (results.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);

                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    proPriceModel.updateProPrice(req).then((results) => {
                        if (results.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['product:productIDNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    productModel.deleteProDetail(req.body.pid).then((results) => {
                        for (let indexx = 0; indexx < req.body.quantities.length; indexx++) {
                            const element = req.body.quantities[indexx];
                            productModel.createProDetail(req, req.params.pid, element.productDetails).then((results) => {
                                if (results.affectedRows > 0) {
                                    if (indexx + 1 == req.body.quantities.length) {
                                        done();
                                    }
                                }
                                else {
                                    response = responseFormat.getResponseMessageByCodes(['product:productDetailFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    })
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['product:productUpdationSuccess']);
                    res.status(200).json(response);
                }
            ])
        }
    }

    /**
    * Function to delete Product by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteProduct(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productModel.deleteProduct(req.params.pid).then((results) => {
            if (results.affectedRows > 0) {
                proPriceModel.deleteProPrice(req.params.pid).then((resu) => {
                    if (resu.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['product:productPriceDeleted']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            } else {

                response = responseFormat.getResponseMessageByCodes(['product:productNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to delete multiple Products
    * @author  MangoIt Solutions
    * @param   {ids}
    * @return  {message} success message
    */
    deleteMultiProduct(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        if (req.body.arr) {
            let arrtemp = [];
            let arrTmp = [];
            async.waterfall([
                function (done) {
                    for (let index = 0; index < req.body.arr.length; index++) {
                        const element = req.body.arr[index];
                        productModel.deleteProduct(element).then((results) => {
                            if (results.affectedRows > 0) {
                                arrtemp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            } else {
                                arrTmp.push(element);
                                if (index + 1 == req.body.arr.length) {
                                    done();
                                }
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }, function (done) {
                    if (arrTmp.length != null || arrtemp.length != null) {
                        for (let i = 0; i < req.body.arr.length; i++) {
                            const elem = req.body.arr[i];
                            proPriceModel.deleteProPrice(elem).then((resp) => {
                                if (resp.affectedRows > 0) {
                                    if (i + 1 == req.body.arr.length) {
                                        done();
                                    }

                                } else {
                                    if (i + 1 == req.body.arr.length) {
                                        done();
                                    }
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['product:productDeleted']);
                    res.status(200).json(response);
                }
            ])
        }
    }
}