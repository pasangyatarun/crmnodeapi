import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class ProductComponentsModel {

    async getproductComponentsById(id){
        const rows = await db.conn.promise().query("SELECT * FROM `product_components` WHERE id = ?", [id]);
        return rows[0];
    }

    async getproductComponentsByName(name){
        const rows = await db.conn.promise().query("SELECT * FROM `product_components` WHERE name = ?", [name]);
        return rows[0];
    }

    async createproductComponents(req){
        var datetime = new Date().getTime();
        const rows = await db.conn.promise().query('INSERT INTO `product_components`(`name`,`description`,`created_at`)VALUES(?,?,?)', [
            req.body.title,
            req.body.description,
            datetime
        ]);
        return rows[0];
    }
    
    async getAllproductComponents(){
        const rows = await db.conn.promise().query("SELECT * FROM `product_components`");
        return rows[0];
    }

    async deleteproductComponents(id){
        const rows = await db.conn.promise().query("DELETE FROM `product_components` WHERE `id`=?", [id]);
        return rows[0];
    }
    
    async updateproductComponents(req){
        var datetime = new Date().getTime();

        const rows = await db.conn.promise().query("UPDATE product_components SET name= ?,description=?,modified_at =? WHERE id = ?", [
            req.body.title,
            req.body.description,
            datetime,
            req.params.id
        ])
        return rows[0];
    }
}