exports.up = async function(knex) {
    return knex.schema.createTable('faq_status', function(t) {
        t.increments('sid').primary();
        t.string('status',[255]).notNull();
        t.datetime('created_at').notNull().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('faq_status');
};