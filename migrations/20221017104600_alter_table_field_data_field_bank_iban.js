exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('field_data_field_bank_iban', function(t) {
        t.string('entity_type',[128]).notNull().defaultTo('').comment('The entity type this data is attached to').alter();
        t.integer('mandate',[11]).defaultTo(0).alter();
        t.integer('entity_id').unsigned().comment('The entity type this data is attached to').alter();
        t.string('field_bank_iban_value',[38]).alter();
        t.string('field_bank_iban_format',[255]).alter();
      });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('field_data_field_bank_iban', function(t) {
       
    });
 };