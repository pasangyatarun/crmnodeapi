exports.up = async function(knex) {
    return knex.schema.createTable('crm_product', function(t) {
        t.increments('id').primary();
        t.text('product_name').notNull();
        t.string('image',[255]);
        t.text('description');
        t.integer('price').notNull();
        t.string('component_type',[255]);
        t.string('tax_policy',[255]);
        t.string('non_visible_role_category',[255]);
        t.integer('status').notNull();
        t.string('created_at',[32]);
        t.string('modified_at',[32]);
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('crm_product');
};

