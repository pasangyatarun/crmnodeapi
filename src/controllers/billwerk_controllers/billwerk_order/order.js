import jwt from 'jsonwebtoken';
import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import OrderModel from '../../../models/billwerk_models/order';
import fetch from 'node-fetch';
import axios from 'axios';
import qs from 'qs';
import BillwerkAuthController from '../billwerk_auth/auth';
import { LocalStorage } from 'node-localstorage';
import AWSHandler from '../../../core/AWSHander';
import sendPdf from '../../../core/pdfGenerate';
import sendEmail from '../../../core/nodemailer'
import IBAN from 'iban'
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import UserModel from '../../../models/user/user';
import {dashLogger} from '../../../../logs/logger';

const __dirname = dirname(fileURLToPath(import.meta.url));
let userModel = new UserModel();
let billwerkAuthController = new BillwerkAuthController();
let ordermodel = new OrderModel();
let localStorage = new LocalStorage('./scratch');

export default class BillwerkOrderController {
    constructor() {
    }

    /**
   * Function to validate Iban
   * @author  MangoIt Solutions
   * @param   {iban}
   * @return  {message}  success message
   */
    validateIban(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let stateIban = IBAN.isValid(req.body.iban);
        if (stateIban == true) {
            response = responseFormat.getResponseMessageByCodes(['iban:ValidIBAN']);
            res.status(200).json(response);
        } else {
            response = responseFormat.getResponseMessageByCodes(['iban:InValidIBAN'], { code: 417 });
            res.status(200).json(response);
        }

    }

    /**
    * Function to validate Iban
    * @author  MangoIt Solutions
    * @param   {iban}
    * @return  {message}  success message
    */
    orderSendByCronJob(objdata, userId) {
        return new Promise((resolve, reject) => {
            let response = responseFormat.createResponseTemplate(req.headers.lang);
            let arrData = [];
            let isClubAdministration = '3';
            let s3URLofDoc;
            async.waterfall([
                function (done) {
                    ordermodel.orderCheck(objdata, userId).then((found) => {
                        if (found.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['order:orderAlreadyOrdered'], { code: 417 });
                            return resolve(response);
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    var config = {
                        method: 'post',
                        url: configContainer.billwerkAccountUrl + 'orders/',
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        },
                        data: objdata,
                    };
                    axios(config)
                        .then(function (resPonse) {
                            arrData[0] = resPonse.data;
                            done();
                        })
                        .catch(function (error) {
                            if (error.isAxiosError == true && error.response.data.Message) {
                                response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                                return resolve(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                return reject(response);
                            }
                        });
                },
                function (done) {
                    ordermodel.orderUserCreate(arrData[0], userId, isClubAdministration, s3URLofDoc).then((usered) => {
                        if (usered.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                            return resolve(response);
                        }
                    })
                },
                function (done) {
                    ordermodel.orderCreate(arrData[0]).then((ordered) => {
                        if (ordered.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                            return resolve(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                            return resolve(response);
                        }
                    })
                }
            ])
        })
    }

    /**
   * Function to get order after three month trial
   * @author  MangoIt Solutions
   * @param   {}
   * @return  {}  
   */
    getOrderAfterThreeMonthTrial(req, res, next) {
        let cronProducts;
        let customersCron = [];
        let resDataList = [];
        let isType;
        let customerIds = [];
        let allComponents;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let self = this;
        async.waterfall([
            function (done) {
                ordermodel.getIncommingOrder().then((cronOrders) => {
                    cronProducts = cronOrders;
                    done();
                })
            },
            function (done) {
                for (let k = 0; k < cronProducts.length; k++) {
                    ordermodel.getIncommingOrderCustomers(cronProducts[k].user_id).then((cronCutomers) => {
                        if (cronCutomers.length > 0) {
                            customersCron.push({ 'customer_id': cronCutomers[0].customer_id, 'user_id': cronProducts[k].user_id });
                            if (cronProducts.length == k + 1) {
                                done();
                            }
                        }
                    })
                }
            },
            function (done) {
                billwerkAuthController.signinBillwerk().then((reg) => {
                    done();
                });
            },
            function (done) {
                for (let z = 0; z < customersCron.length; z++) {
                    var config = {
                        method: 'get',
                        url: configContainer.billwerkAccountUrl + 'customers/' + customersCron[z].customer_id + '/contracts',
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        }
                    };
                    axios(config)
                        .then(function (resPonse) {
                            if (resPonse.data.length == 1 && (resPonse.data)[0].EndDate) {
                                resDataList.push((resPonse.data)[0]);
                            }
                            if (customersCron.length == z + 1) {
                                done();
                            }
                        })
                        .catch(function (error) {
                            if (error.isAxiosError == true && error.response.data.Message) {
                                response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            }
                        });
                }
            },
            function (done) {
                for (let y = 0; y < resDataList.length; y++) {
                    var config = {
                        method: 'get',
                        url: configContainer.billwerkAccountUrl + 'plans/' + resDataList[y].PlanId,
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        }
                    };

                    axios(config)
                        .then(function (response) {
                            isType = response.data.CustomFields.type;
                            let d1 = new Date();
                            let dt1 = d1.toISOString().split('T');
                            let d2 = resDataList[y].EndDate;
                            let dt2 = d2.split('T');
                            customerIds.push(resDataList[y].CustomerId);
                            if (resDataList.length == y + 1) {
                                done();
                            }

                        })
                        .catch(function (error) {
                            if (error.isAxiosError == true && error.response.data.Message) {
                                response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            }
                        });
                }
            },
            function (done) {

                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productInfo',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        let productsall = response.data;
                        allComponents = productsall.Components;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });

            },
            function (done) {
                for (let n = 0; n < customerIds.length; n++) {
                    let user = customersCron.find(o => o.customer_id == customerIds[n]);
                    let product = cronProducts.find(o => o.user_id == user.user_id)
                    let spgComponent = allComponents.find(o => o.CustomFields.type == '0')
                    let test;
                    test =
                    {
                        "triggerInterimBilling": false,
                        "CustomerId": user.customer_id,
                        "cart": {
                            "planVariantId": product.planVariantId,

                            "inheritStartDate": false,
                            "CouponCode": product.coupon
                        },
                        "previewAfterTrial": false
                    };
                    test.cart["ComponentSubscriptions"] =
                        [{
                            "ComponentId": spgComponent.Id,
                        }]
                    self.orderSendByCronJob(test, user.user_id).then((responseData) => {
                        //in future, if this process will continue then the commit api need implement here

                    })
                }
            }
        ])
    }

    /**
    * Function to delete the order
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message}  success message
    */
    orderDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        var a1 = [];
        async.waterfall([
            function (done) {
                var config = {
                    method: 'delete',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        a1[0] = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.deleteOrderObj(req.params.id).then((deleted) => {
                    done();
                })
            },
            function (done) {
                ordermodel.deleteOrder(req.params.id).then((deleted) => {
                    if (deleted.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['order:deletedOrder']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderNotExist'], { code: 417 });
                        res.status(200).json(response);
                    }

                })
            }
        ])
    }

    /**
    * Function to preview order details
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object}  order details
    */
    orderPreview(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/preview',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: req.body,
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to decline the order
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  order details
    */
    orderDecline(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id + '/decline',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to commit the order
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  order details
    */
    orderCommit(req, res, next) {
        var a1 = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let arrData = [];
        let variant;
        let isClubAdministration;
        let foundDatas;
        let authUserId;
        let keycloakToken;
        let loginInfo;
        let dbList;
        let item;
        let userData;
        let loginUserData = [];
        let date = new Date().toJSON().split('T');
        let agbDate = date[0];
        let lastOrderId;
        let orderDetails;
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        done();
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id + '/commit',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: req.body
                };
                axios(config)
                    .then(function (resPonse) {
                        a1[0] = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.commitStore(a1[0], req.params.id).then((stored) => {
                    if (stored.affectedRows > 0) {
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    }
                })
            },
            // function (done) {
            //     let at = req.headers.authorizationToken;
            //     let accessToken = new Buffer.from(at, 'base64').toString('ascii');
            //     let authToken = accessToken.split(':')[1];

            //     jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
            //         if (err) {
            //             response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
            //             res.status(401).json(response);
            //             return;
            //         } else {
            //             authUserId = decoded.data.userId;
            //             console.log("authUserId----",authUserId)
            //             done();
            //         }
            //     })
            // },
            function (done) {
                ordermodel.getLastOrder(req.params.id).then((found) => {
                    orderDetails = found;
                    done();
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/' + orderDetails[0].planVariantId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        variant = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'plans/' + variant.PlanId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        isClubAdministration = response.data.CustomFields.type;
                        done();
                    })
                    .catch(function (error) {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.CompulsaryProductCheck(authUserId).then((found) => {
                    foundDatas = found;
                    lastOrderId = foundDatas.reverse()[0].order_id
                    done();
                })
            },
            function (done) {
                ordermodel.getLastOrder(lastOrderId).then((found) => {
                    arrData[0] = JSON.parse(found[0].data);
                    done();
                })
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) {
                    var data = qs.stringify({
                        'grant_type': 'client_credentials',
                        'client_id': 'client-credentials-crm',
                        'client_secret': configContainer.client_secret
                    })
                    fetch(configContainer.keyclock, {
                        'method': 'POST',
                        'headers': {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        'body': data
                    }).then((resP) => {
                        resP.text().then((re) => {
                            keycloakToken = JSON.parse(re).access_token;
                            fetch(configContainer.crmClubUrl + 'list-databases', {
                                'method': 'GET',
                                'headers': {
                                    'Authorization': 'Bearer ' + keycloakToken + '',
                                    'Content-Type': ' application/json'
                                },
                            }).then((resP) => {
                                resP.text().then((re) => {
                                    dbList = JSON.parse(re);
                                    console.log("---dbList--",dbList);
                                    done();
                                }).catch((error) => {
                                    dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                })
                            })
                            // .catch((error) => {
                            //     done();
                            // })
                            .catch((error) => {
                                dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }).catch((error) => {
                            dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    })
                    // .catch((error) => {
                    //     done();
                    // })
                    .catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) {
                    fetch(configContainer.crmClubUrl + 'list-designs?database_id=' + dbList[0].id, {
                        'method': 'GET',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                    }).then((resP) => {
                        resP.text().then((re) => {
                            let arr = JSON.parse(re);
                            console.log("--arr--653--",arr)
                            function random(mn, mx) {
                                return Math.random() * (mx - mn) + mn;
                            }
                            item = arr.find(o => o.value == 'S-Verein');
                            done();
                        }).catch((error) => {
                            dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        });
                    })
                    // .catch(() => {
                    //     done();
                    // })
                    .catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4)) {
                    userModel.getUserById(authUserId).then((results) => {
                        if (results.length > 0) {
                            userData = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) && foundDatas.length == 1) {
                    let ymdDate;
                    var data;
                    const countryCode = [
                        { code: 'DE', name: 'Germany' },
                        { code: 'CH', name: 'Switzerland' },
                        { code: 'AT', name: 'Austria' }
                    ]
                    let countryC = countryCode.find(o => o.name == userData[0].country)
                    countryC = countryC ? countryC.code : 'DE'
                    if (variant.AllowWithoutPaymentData == true) {
                        let date = arrData[0].Contract.EndDate;
                        ymdDate = date.split('T')[0];
                        data = JSON.stringify({
                            "clubName": arrData[0].Customer.CompanyName,
                            "email": arrData[0].Customer.EmailAddress,
                            "numberOfUsers": 25,
                            "design": item.id,
                            "testMode": true,
                            "testEndDate": ymdDate,
                            "country": countryC,
                            "agbCheck": agbDate,
                            "attributes": {
                                "small": true,
                                "fibuLight": true,
                                "fibuPremium": true,
                                "app": true
                            }
                        })
                    } else {
                        data = JSON.stringify({
                            "clubName": arrData[0].Customer.CompanyName,
                            "email": arrData[0].Customer.EmailAddress,
                            "numberOfUsers": 25,
                            "design": item.id,
                            "testMode": false,
                            "country": countryC,
                            "agbCheck": agbDate,
                            "attributes": {
                                "small": true,
                                "fibuLight": true,
                                "fibuPremium": true,
                                "app": true
                            }
                        })
                    }
                    fetch(configContainer.crmClubUrl + 'create-club?database_id=' + dbList[0].id, {
                        'method': 'POST',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                        'body': data
                    }).then((resP) => {
                        resP.text().then((re) => {
                            loginInfo = re;
                            console.log("--loginInfo---756--",loginInfo)
                            done();
                        });
                    })
                    // .catch(() => {
                    //     done();
                    // })
                    .catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) && foundDatas.length == 1) {
                    let dbId = JSON.parse(loginInfo).databaseId;
                    let clubId = JSON.parse(loginInfo).clubId;
                    fetch(configContainer.crmClubUrl + 'login-info?database_id=' + dbId + '&club_id=' + clubId + '', {
                        'method': 'GET',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Cookie': 'authenticated=false; liveChatShared=null; publicShared=null'
                        }
                    }).then((res) => {
                        res.text().then((re) => {
                            loginUserData[0] = JSON.parse(re);
                            done();
                        })
                    }).catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                    })
                } else {
                    done()
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) && foundDatas.length == 1) {
                    let days = 30;
                    if (isClubAdministration == 3) {
                        days = 90;
                    }
                    let username = loginUserData[0].users[0].email.split("@")[0];
                    let mailOptions = {
                        to: loginUserData[0].users[0].email,
                        subject: '[S-Verein] Ihr ' + days + '-tägiger Testzugang zur Online-Vereinsverwaltung',
                        html : `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                        <head>
                        <!--[if (gte mso 9)|(IE)]>
                          <xml>
                            <o:OfficeDocumentSettings>
                            <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                          </o:OfficeDocumentSettings>
                        </xml>
                        <![endif]-->
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                        <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                        <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                        <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                        <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                        <meta name="color-scheme" content="only">
                        <title></title>
                        
                        <link rel="preconnect" href="https://fonts.gstatic.com">
                        <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                        
                        <style type="text/css">
                        /*Basics*/
                        body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                        table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                        table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                        td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                        td p {margin:0; padding:0;}
                        td div {margin:0; padding:0;}
                        td a {text-decoration:none; color: inherit;} 
                        /*Outlook*/
                        .ExternalClass {width: 100%;}
                        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                        .ReadMsgBody {width:100%; background-color: #ffffff;}
                        /* iOS BLUE LINKS */
                        a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                        /*Gmail blue links*/
                        u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                        /*Buttons fix*/
                        .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                        .yshortcuts a {border-bottom:none !important;
                        
                        .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                        /*Responsive*/
                        @media screen and (max-width: 639px) {
                          table.row {width: 100%!important;max-width: 100%!important;}
                          td.row {width: 100%!important;max-width: 100%!important;}
                          .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                          .center-float {float: none!important;margin:auto!important;}
                          .center-text{text-align: center!important;}
                          .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                          .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                          .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                          .hide-mobile {display: none!important;}
                          .menu-container {text-align: center !important;}
                          .autoheight {height: auto!important;}
                          .m-padding-10 {margin: 10px 0!important;}
                          .m-padding-15 {margin: 15px 0!important;}
                          .m-padding-20 {margin: 20px 0!important;}
                          .m-padding-30 {margin: 30px 0!important;}
                          .m-padding-40 {margin: 40px 0!important;}
                          .m-padding-50 {margin: 50px 0!important;}
                          .m-padding-60 {margin: 60px 0!important;}
                          .m-padding-top10 {margin: 30px 0 0 0!important;}
                          .m-padding-top15 {margin: 15px 0 0 0!important;}
                          .m-padding-top20 {margin: 20px 0 0 0!important;}
                          .m-padding-top30 {margin: 30px 0 0 0!important;}
                          .m-padding-top40 {margin: 40px 0 0 0!important;}
                          .m-padding-top50 {margin: 50px 0 0 0!important;}
                          .m-padding-top60 {margin: 60px 0 0 0!important;}
                          .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                          .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                          .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                          .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                          .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                          .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                          .center-on-mobile {text-align: center!important;}
                        }
                        </style>
                        </head>
                        
                        <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#F0F0F0">
                        
                        <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                        
                        <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                          <tr><!-- Outer Table -->
                            <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- Preheader -->
                          <tr>
                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- Preheader -->
                        </table>
                        
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- simpli-header-1 -->
                          <tr>
                            <td align="center">
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                          <!-- bg-image -->
                          <tr>
                            <td align="center" style="border-radius: 36px;">
                        
                        <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                        <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                        <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                        
                        <div>
                          <!-- simpli-header-bg-image -->
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                            <tr>
                            <td align="center" data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png') ">
                          
                          <!-- Content -->
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                            <tr>
                              <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                          
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                <tr>
                                  <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                                </tr>
                              </table>
                          
                              </td>
                            </tr>
                          </table>
                          <!-- Content -->
                          
                              </td>
                            </tr>
                          </table>
                          <!-- simpli-header-bg-image -->
                          </div>
                          
                          <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                          
                              </td>
                            </tr>
                            <!-- bg-image -->
                          </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                          <!-- basic-info -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                          HERZLICH WILLKOMMEN!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:48px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            30-tägiger Testzugang
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Ihr 30-tägiger Testzugang zur Online-Vereinsverwaltung ist für Sie freigeschaltet.
                                            Über den folgenden Link erhalten Sie Ihren Zugang sowie Ihre persönliche Daten (Benutzername :  `+ loginUserData[0].users[0].username + ` und Passwort : vv_pass1) zur S-Verein Vereinsverwaltung.
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <!-- Button -->
                                    <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                      <tr>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                          <td align="center" width="35"></td>
                                          <td align="center" height="50" style="height:50px;">
                                          <![endif]-->
                                            <singleline>
                                              <a href="`+ loginUserData[0].link + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Starten Sie Jetzt</span></a>
                                            </singleline>
                                          <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                          <td align="center" width="35"></td>
                                        </tr>
                                      </table>
                                    <![endif]-->
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Buttons -->
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- basic-info -->
                        </table>
                        
                            </td>
                          </tr>
                          <!-- simpli-header-1 -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1A -->
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                          So geht es weiter!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                          Folgen Sie diesen Schritten!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/mouseclick.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:24px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Erster Schritt
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Besuchen Sie Ihre Vereinsverwaltung unter <br>
                                                  [` + loginUserData[0].link + `]
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- content-1A -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1B -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/usericon.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:24px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  So geht es weiter!
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Geben Sie Ihren Benutzername ein.<br>
                                                  Ihr Benutzername lautet: [`+ loginUserData[0].users[0].username + `]
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- content-1B -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1C -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/schloss.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:24px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                  Dritter Schritt
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:24px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Geben Sie Ihr Passwort ein.<br>
                                                    Ihr Passwort lautet: [vv_pass1]
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- content-1C -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                          <!-- simpli-footer -->
                          <tr>
                            <td align="center">
                              
                        <!-- Content -->
                        
                        <tr>
                          <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                  <multiline>
                                    <div mc:edit Simpli>
                                      Tineon Aktiengesellschaft <br />
                                      Uferpromenade 5, 88709 Meersburg <br />
                                      Registergericht Freiburg HRB 710927 <br />
                                      Vorstand: Jean-Claude Parent <br />
                                      Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                      USt.-IdNr. DE203912818 <br />
                                    </div>
                                  </multiline>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                          <!-- simpli-footer -->
                        </table>
                        
                            </td>
                          </tr><!-- Outer-Table -->
                        </table>
                        
                        </body>
                        </html>`
                    }
                    sendEmail(mailOptions).then((sentdata) => {
                        if (sentdata == 'error') {
                            done();
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },

            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) && foundDatas.length == 2) {
                    let dbId = JSON.parse(foundDatas[1].login_info).databaseId;
                    let clubId = JSON.parse(foundDatas[1].login_info).clubId
                    var dataUpdate = JSON.stringify({
                        "testMode": false,
                        "street": userData[0].addressLine1,
                        "postalCode": userData[0].zipcode,
                        "city": userData[0].place,
                        "contactLastname": userData[0].lastName,
                        "contactFirstname": userData[0].firstName,
                        "customerNumber": userData[0].contactNumber,
                        "agbCheck": agbDate
                    })

                    fetch(configContainer.crmClubUrl + 'update-club?database_id=' + dbId + '&club_id=' + clubId, {
                        'method': 'POST',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                        'body': dataUpdate
                    }).then((resP) => {
                        resP.text().then((re) => {
                            loginInfo = re;
                            done();
                        });
                    })
                    // .catch(() => {
                    //     done();
                    // })
                    .catch((error) => {
                        dashLogger.error(`Error : ${error},Request : ${req.originalUrl},UserId : ${authUserId}`);
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4) {
                    ordermodel.orderUserUpdate(loginInfo, lastOrderId).then((usered) => {
                        if (usered.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    })
                } else {
                    response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                    res.status(200).json(response);
                }
            }
        ])
    }

    /**
    * Function to approve order
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  order details
    */
    orderApprove(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id + '/approve',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to create new  incoming order by user
    * @author  MangoIt Solutions
    * @param   {object} with order details
    * @return  {message}  success message
    */
    incomingOrderByUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        ordermodel.upcommingOrderCreate(req.body, decoded.data.userId).then((stored) => {
                            if (stored.affectedRows > 0) {
                                response = responseFormat.getResponseMessageByCodes(['order:orderStoreSuccess']);
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['order:orderStoreFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        })
                    }
                })
            }
        ])
    }

    /**
    * Function to SPG customer order
    * @author  MangoIt Solutions
    * @param   {object} with order details
    * @return  {object}  order details.
    */
    orderSendSPG(req, res, next) {
        let threeMonthProduct;
        let isType;
        let arrData = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/' + req.body.cart.planVariantId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        threeMonthProduct = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'plans/' + threeMonthProduct.PlanId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        isType = response.data.CustomFields.type;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: req.body,
                };
                axios(config)
                    .then(function (resPonse) {
                        arrData[0] = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        ordermodel.orderUserCreate(arrData[0], decoded.data.userId, isType).then((usered) => {
                            if (usered.affectedRows > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        })
                    }
                })
            },
            function (done) {
                ordermodel.orderCreate(arrData[0]).then((ordered) => {
                    if (ordered.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])

    }

    /**
    * Function to order club product
    * @author  MangoIt Solutions
    * @param   {object} with order details
    * @return  {object}  order details.
    */
    orderClub(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let arrData = [];
        let objData;
        let variant;
        let isClubAdministration;
        let customer;
        let s3URLofDoc;
        let companyname;
        let foundDatas;
        let userOrders = [];
        let authUserId;
        let trialOrdered = false;
        let keycloakToken;
        let loginInfo;
        let dbList;
        let item;
        let userData;
        let loginUserData = [];
        let date = new Date().toJSON().split('T');
        let agbDate = date[0];
        let lastOrderId;
        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        done();
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/' + req.params.vid,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        variant = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {

                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'plans/' + variant.PlanId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        isClubAdministration = response.data.CustomFields.type;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.CompulsaryProductCheck(authUserId).then((found) => {
                    foundDatas = found;
                    lastOrderId = foundDatas.reverse()[0].order_id
                    done();
                })
            },
            function (done) {
                ordermodel.getLastOrder(lastOrderId).then((found) => {
                    arrData[0] = JSON.parse(found[0].data);
                    done();
                })
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3) {
                    var data = qs.stringify({
                        'grant_type': 'client_credentials',
                        'client_id': 'client-credentials-crm',
                        'client_secret': configContainer.client_secret
                    })
                    fetch(configContainer.keyclock, {
                        'method': 'POST',
                        'headers': {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        'body': data
                    }).then((res) => {
                        res.text().then((re) => {
                            keycloakToken = JSON.parse(re).access_token;
                            fetch(configContainer.crmClubUrl + 'list-databases', {
                                'method': 'GET',
                                'headers': {
                                    'Authorization': 'Bearer ' + keycloakToken + '',
                                    'Content-Type': ' application/json'
                                },

                            }).then((res) => {
                                res.text().then((re) => {
                                    dbList = JSON.parse(re);
                                    console.log("---dbList---",dbList);
                                    done();
                                });

                            }).catch(() => {
                                done();
                            })

                        });

                    }).catch(() => {
                        done();
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3) {
                    fetch(configContainer.crmClubUrl + 'list-designs?database_id=' + dbList[0].id, {
                        'method': 'GET',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },

                    }).then((res) => {
                        res.text().then((re) => {
                            let arr = JSON.parse(re);
                            function random(mn, mx) {
                                return Math.random() * (mx - mn) + mn;
                            }

                            item = arr[Math.floor(random(1, 5)) - 1];
                            done();
                        });
                    }).catch(() => {
                        done();
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3) && foundDatas.length == 1) {
                    let ymdDate;
                    var data;
                    if (variant.AllowWithoutPaymentData == true) {
                        let date = arrData[0].Contract.EndDate;
                        ymdDate = date.split('T')[0];
                        data = JSON.stringify({
                            "clubName": req.body.customer.companyName,
                            "email": req.body.customer.emailAddress,
                            "numberOfUsers": 25,
                            "design": item.id,
                            "testMode": true,
                            "testEndDate": ymdDate,
                            "country": req.body.customer.address.country,
                            "agbCheck": agbDate,
                            "attributes": {
                                "small": true,
                                "fibuLight": true,
                                "fibuPremium": true,
                                "app": true
                            }
                        })

                    } else {
                        data = JSON.stringify({
                            "clubName": req.body.customer.companyName,
                            "email": req.body.customer.emailAddress,
                            "numberOfUsers": 25,
                            "design": item.id,
                            "testMode": false,
                            "country": req.body.customer.address.country,
                            "agbCheck": agbDate,
                            "attributes": {
                                "small": true,
                                "fibuLight": true,
                                "fibuPremium": true,
                                "app": true
                            }
                        })
                    }
                    fetch(configContainer.crmClubUrl + 'create-club?database_id=' + dbList[0].id, {
                        'method': 'POST',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                        'body': data
                    }).then((res) => {
                        res.text().then((re) => {
                            loginInfo = re;
                            done();
                        });
                    }).catch(() => {
                        done();
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3) && foundDatas.length == 1) {
                    let dbId = JSON.parse(loginInfo).databaseId;
                    let clubId = JSON.parse(loginInfo).clubId;
                    fetch(configContainer.crmClubUrl + 'login-info?database_id=' + dbId + '&club_id=' + clubId + '', {
                        'method': 'GET',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Cookie': 'authenticated=false; liveChatShared=null; publicShared=null'
                        }
                    }).then((res) => {
                        res.text().then((re) => {
                            loginUserData[0] = JSON.parse(re);
                            done();
                        })
                    }).catch(() => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                    })
                } else {

                    done()
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3) && foundDatas.length == 1) {
                    let days = 30;
                    if (isClubAdministration == 3) {
                        days = 90;
                    }
                    let username = loginUserData[0].users[0].email.split("@")[0];
                    let mailOptions = {
                        to: loginUserData[0].users[0].email,
                        subject: '[S-Verein] Ihr ' + days + '-tägiger Testzugang zur Online-Vereinsverwaltung',
                        html: `<!doctype html>
                        <html lang="en-US">
                        
                        <head>
                        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                        <title>Reset Password Email</title>
                        <meta name="description" content="Reset Password Email Template.">
                        <style type="text/css">
                        a:hover {text-decoration: underline !important;}
                        </style>
                        </head>
                        
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                        <!--100% body table-->
                        <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                        style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                        <tr>
                        <td>
                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                        align="center" cellpadding="0" cellspacing="0">
                        <tr>
                        <td style="height:80px;">&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="text-align:center;">
                        <a href="#" title="logo" target="_blank">
                        <img width="60" src=`+ 'https://tineon.s3.eu-central-1.amazonaws.com/Leipzig/2022/June/Images/v-logo.png' + ` title="logo" alt="logo">
                        </a>
                        </td>
                        </tr>
                        <tr>
                        <td style="height:20px;">&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                        style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                        <tr>
                        <td style="height:40px;">&nbsp;</td>
                        </tr>
                        <tr>
                        <td style="padding:0 35px;">
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ 'Dear club member,' + `</p><br>
                                                               
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                `+ `Your ` + days + `-day test access to the online club administration is activated for you.<br>
                                                                <p></p>
                                                                The following link and your personal data (user name and password) give you access to the S-Verein administration.<br>
                                                                <p></p>
                                                                Link: <a href="`+ loginUserData[0].link + `">` + loginUserData[0].link + `</a> <br>
                                                                Benutzer: `+ username + ` <br>
                                                                Passwort: vv_pass1 <br>
                                                                <p></p>
                                                                <p></p>
                                                                If you have any questions within your test room, we are happy to help. Our service center can be reached by email at  <a href="`+ 'support@s-verein.de' + `">` + 'support@s-verein.de' + `</a>  .  In addition, a comprehensive help, service and FAQ area is available to you on <a href="` + 'www.s-verein.de' + `">` + 'www.s-verein.de' + `</a>  .<br>
                                                                <p></p>
                                                                Your trial period ends automatically after `+ days + ` days.<br>
                                                                <p></p>
                                                                We hope you enjoy testing and we look forward to your feedback.` + `:<br>
                                                                </p><br><br>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ `Kind regards<br>
                                                                
                                                                Your ServiceCenter S-Verein<br>
                                                                <a href="`+ 'support@s-verein.de' + `">` + 'support@s-verein.de' + `</a><br>
                                                                
                                                                
                                                                ++++++++++++++++++++++++++++++++++<br>
                                                                S-Verein is a product of Tineon AG in cooperation with the Sparkassen-Finanzportal GmbH.<br>
                                                                The current terms and conditions and further information can be found at:  <a href="`+ 'www.s-verein.de/agb' + `">` + 'www.s-verein.de/agb' + `</a><br>
                                                                                                                                
                                                                Tineon AG<br>
                                                                Uferpromenade 5<br>
                                                                88709 Meersburg, Germany<br>
                                                                
                                                                Registered office: Meersburg on Lake Constance<br>
                                                                Registered in the register court of Freiburg HRB 706986<br>
                                                                Executive Board: Jean-Claude Parent<br>
                                                                Supervisory Board: Hans-Peter Haubold (Chairman), Eugen Schindler<br>
                                                                VAT ID no. DE203912818` + `,</p><br>
                                                                <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                                                                <!-- <a href="javascript:void(0);"
                                                                style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                                                    Password</a> -->
                                                                    </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="height:40px;">&nbsp;</td>
                                                                    </tr>
                                                                    </table>
                                                                    </td>
                                                                    <tr>
                                                                    <td style="height:20px;">&nbsp;</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="text-align:center;">
                                                                    <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ 'http://crmangular.mangoitsol.com/' + `</strong></p>
                                                                    </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td style="height:80px;">&nbsp;</td>
                                                                    </tr>
                                                                    </table>
                                    </td>
                                </tr>
                            </table>
                            <!--/100% body table-->
                            </body>
                            
                            </html>`,
                    }
                    sendEmail(mailOptions).then((sentdata) => {
                        if (sentdata == 'error') {
                            done();
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3) && foundDatas.length == 2) {
                    userModel.getUserById(authUserId).then((results) => {
                        if (results.length > 0) {
                            userData = results;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['user:userNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3) && foundDatas.length == 2) {
                    let dbId = JSON.parse(foundDatas[0].login_info).databaseId;
                    let clubId = JSON.parse(foundDatas[0].login_info).clubId
                    var dataUpdate = JSON.stringify({
                        "testMode": false,
                        "street": userData[0].addressLine1,
                        "postalCode": userData[0].zipcode,
                        "city": userData[0].place,
                        "contactLastname": userData[0].lastName,
                        "contactFirstname": userData[0].firstName,
                        "customerNumber": userData[0].contactNumber,
                        "agbCheck": agbDate
                    })

                    fetch(configContainer.crmClubUrl + 'update-club?database_id=' + dbId + '&club_id=' + clubId, {
                        'method': 'POST',
                        'headers': {
                            'Authorization': 'Bearer ' + keycloakToken + '',
                            'Content-Type': ' application/json'
                        },
                        'body': dataUpdate
                    }).then((res) => {
                        res.text().then((re) => {
                            loginInfo = re;
                            done();
                        });
                    }).catch(() => {
                        done();
                    })
                } else {
                    done();
                }
            },
            function (done) {
                if (isClubAdministration == 1 || isClubAdministration == 3) {
                    ordermodel.orderUserUpdate(loginInfo, lastOrderId).then((usered) => {
                        if (usered.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    })
                } else {
                    response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                    res.status(200).json(response);
                }
            }
        ])

    }

    /**
    * Function to order send
    * @author  MangoIt Solutions
    * @param   {object} with order details
    * @return  {object}  order details.
    */
    orderSend(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let arrData = [];
        let objData;
        let variant;
        let isClubAdministration;
        let customer;
        let s3URLofDoc;
        let companyname;
        let foundDatas;
        let userOrders = [];
        let authUserId;
        let trialOrdered = false;
        let keycloakToken;
        let loginInfo;
        let dbList;
        let item;
        let userData;
        let loginUserData = [];
        let date = new Date().toJSON().split('T');
        let agbDate = date[0];

        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        ordermodel.orderCheck(req.body, decoded.data.userId).then((found) => {
                            if (found.length > 0) {
                                response = responseFormat.getResponseMessageByCodes(['order:orderAlreadyOrdered'], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                done();
                            }
                        })
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'planVariants/' + req.body.cart.planVariantId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        variant = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'plans/' + variant.PlanId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        isClubAdministration = response.data.CustomFields.type;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },

            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        ordermodel.CompulsaryProductCheck(decoded.data.userId).then((found) => {
                            foundDatas = found;
                            if (found.length > 0) {
                                for (let j = 0; j < found.length; j++) {
                                    var config = {
                                        method: 'get',
                                        url: configContainer.billwerkAccountUrl + 'planVariants/' + found[j].plan_variant_id,
                                        headers: {
                                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                                        }
                                    };
                                    axios(config)
                                        .then(function (resPonse) {
                                            userOrders.push(resPonse.data);
                                            if (j + 1 == found.length) {

                                                if (userOrders.find(o => o.AllowWithoutPaymentData == true)) {
                                                    trialOrdered = true;
                                                    done();
                                                } else {
                                                    done();
                                                }
                                            }
                                        })
                                        .catch(function (error) {
                                            if (error.isAxiosError == true && error.response.data.Message) {
                                                response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                                                res.status(200).json(response);
                                            } else {

                                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                                res.status(500).json(response);
                                                return;
                                            }
                                        });
                                }
                            } else {
                                done();
                            }
                        })
                    }
                })
            },
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
               
                    jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                        if (err) {
                            response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                            res.status(401).json(response);
                            return;
                        } else {
                            ordermodel.ProductVersionCheck(decoded.data.userId, isClubAdministration).then((foundVersion) => {
                                if ((trialOrdered == false && isClubAdministration == 1 && foundVersion.length > 0) || (trialOrdered == true && (isClubAdministration == 1 || isClubAdministration == 3) && foundVersion.length > 1) || (isClubAdministration != 1 && isClubAdministration != 3 && foundVersion.length > 0)) {
                                    response = responseFormat.getResponseMessageByCodes(['order:alreadyOrderdVersion'], { code: 417 });
                                    res.status(200).json(response);
                                } else {
                                    done();
                                }
                            })
                        }   
                    })
            },
            function (done) {
                if ((trialOrdered == false && foundDatas.length < 1 && isClubAdministration != 1 && isClubAdministration != 3 && isClubAdministration != 4) || (trialOrdered == true && foundDatas.length < 2 && isClubAdministration != 1 && isClubAdministration != 3 && isClubAdministration != 4)) {
                    response = responseFormat.getResponseMessageByCodes(['messagePurchase:orderFirstPrimary'], { code: 417 });
                    res.status(200).json(response);
                } else if (foundDatas.length > 0) {
                    objData = req.body;
                    done();
                } else {
                    objData = req.body;
                    done();
                }
            },
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: objData,
                };
                axios(config)
                    .then(function (resPonse) {
                        arrData[0] = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },

            function (done) {
                if (isClubAdministration == 1 && foundDatas.length > 0) {
                    foundDatas.forEach(eleObj => {
                        ordermodel.getOrdersCompanyname(eleObj.order_id).then((orderedObj) => {
                            if (orderedObj.length > 0 && JSON.parse(orderedObj[0].data) && JSON.parse(orderedObj[0].data).Customer.CompanyName) {
                                companyname = JSON.parse(orderedObj[0].data).Customer.CompanyName;
                                done();
                            }
                        })
                    });
                } else if (req.body.customer && req.body.customer.companyName) {
                    companyname = req.body.customer.companyName;
                    done();
                } else {
                    done();
                }
            },
            function (done) {
                if ((isClubAdministration == 1 || isClubAdministration == 3 || isClubAdministration == 4)) {
                    let address;
                    if (variant.AllowWithoutPaymentData == true) {
                        address = "";
                    } else {
                        address = "Tineon AG, Uferpromenade 5, 88709 Meersburg ";
                    }
                    var options = {
                        format: "A3",
                        orientation: "portrait",
                        border: "10mm",
                    };
                    var users = [
                        {
                            CustomerId: arrData[0].Contract.CustomerId,
                        }

                    ];
                    var document = {
                        html: `<!DOCTYPE html
                        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="EN" lang="EN">
                    
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>ADV_Tineon_2018 Kopie</title>
                        <meta name="author" content="Jean-Claude Parent" />
                        <style type="text/css">
                            body {
                                margin: 0;
                                padding: 0;
                                font-family: Arial, Helvetica, sans-serif;
                                color: #000;
                            }
                    
                            h1 {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: bold;
                                text-decoration: none;
                                font-size: 14pt;
                            }
                    
                            p {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            .s1 {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 12pt;
                            }
                    
                            h2 {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: bold;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            .s2 {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 7.5pt;
                            }
                    
                            .a {
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 7.5pt;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l1 {
                                padding-left: 0pt;
                                counter-reset: c1 1;
                            }
                    
                            #l1>li>*:first-child:before {
                                counter-increment: c1;
                                content: counter(c1, decimal)". ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: bold;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l1>li:first-child>*:first-child:before {
                                counter-increment: c1 0;
                            }
                    
                            #l2 {
                                padding-left: 0pt;
                                counter-reset: d1 1;
                            }
                    
                            #l2>li>*:first-child:before {
                                counter-increment: d1;
                                content: "(" counter(d1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l2>li:first-child>*:first-child:before {
                                counter-increment: d1 0;
                            }
                    
                            #l3 {
                                padding-left: 0pt;
                                counter-reset: e1 1;
                            }
                    
                            #l3>li>*:first-child:before {
                                counter-increment: e1;
                                content: "(" counter(e1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l3>li:first-child>*:first-child:before {
                                counter-increment: e1 0;
                            }
                    
                            #l4 {
                                padding-left: 0pt;
                            }
                    
                            #l4>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l5 {
                                padding-left: 0pt;
                            }
                    
                            #l5>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l6 {
                                padding-left: 0pt;
                                counter-reset: f1 1;
                            }
                    
                            #l6>li>*:first-child:before {
                                counter-increment: f1;
                                content: "(" counter(f1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l6>li:first-child>*:first-child:before {
                                counter-increment: f1 0;
                            }
                    
                            #l7 {
                                padding-left: 0pt;
                                counter-reset: g1 1;
                            }
                    
                            #l7>li>*:first-child:before {
                                counter-increment: g1;
                                content: "(" counter(g1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l7>li:first-child>*:first-child:before {
                                counter-increment: g1 0;
                            }
                    
                            #l8 {
                                padding-left: 0pt;
                                counter-reset: h1 1;
                            }
                    
                            #l8>li>*:first-child:before {
                                counter-increment: h1;
                                content: counter(h1, lower-latin)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l8>li:first-child>*:first-child:before {
                                counter-increment: h1 0;
                            }
                    
                            #l9 {
                                padding-left: 0pt;
                                counter-reset: i1 1;
                            }
                    
                            #l9>li>*:first-child:before {
                                counter-increment: i1;
                                content: "(" counter(i1, decimal)") ";
                                color: black;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                            }
                    
                            #l9>li:first-child>*:first-child:before {
                                counter-increment: i1 0;
                            }
                    
                            #l10 {
                                padding-left: 0pt;
                                counter-reset: j1 1;
                            }
                    
                            #l10>li>*:first-child:before {
                                counter-increment: j1;
                                content: "(" counter(j1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l10>li:first-child>*:first-child:before {
                                counter-increment: j1 0;
                            }
                    
                            #l11 {
                                padding-left: 0pt;
                            }
                    
                            #l11>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l12 {
                                padding-left: 0pt;
                                counter-reset: k1 1;
                            }
                    
                            #l12>li>*:first-child:before {
                                counter-increment: k1;
                                content: "(" counter(k1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l12>li:first-child>*:first-child:before {
                                counter-increment: k1 0;
                            }
                    
                            #l13 {
                                padding-left: 0pt;
                                counter-reset: l1 1;
                            }
                    
                            #l13>li>*:first-child:before {
                                counter-increment: l1;
                                content: "(" counter(l1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l13>li:first-child>*:first-child:before {
                                counter-increment: l1 0;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l14 {
                                padding-left: 0pt;
                                counter-reset: m1 1;
                            }
                    
                            #l14>li>*:first-child:before {
                                counter-increment: m1;
                                content: "(" counter(m1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l14>li:first-child>*:first-child:before {
                                counter-increment: m1 0;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l15 {
                                padding-left: 0pt;
                                counter-reset: n1 10;
                            }
                    
                            #l15>li>*:first-child:before {
                                counter-increment: n1;
                                content: counter(n1, decimal)". ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: bold;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l15>li:first-child>*:first-child:before {
                                counter-increment: n1 0;
                            }
                    
                            #l16 {
                                padding-left: 0pt;
                                counter-reset: o1 1;
                            }
                    
                            #l16>li>*:first-child:before {
                                counter-increment: o1;
                                content: "(" counter(o1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l16>li:first-child>*:first-child:before {
                                counter-increment: o1 0;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l17 {
                                padding-left: 0pt;
                                counter-reset: p1 1;
                            }
                    
                            #l17>li>*:first-child:before {
                                counter-increment: p1;
                                content: "(" counter(p1, decimal)") ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l17>li:first-child>*:first-child:before {
                                counter-increment: p1 0;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l18 {
                                padding-left: 0pt;
                                counter-reset: q1 1;
                            }
                    
                            #l18>li>*:first-child:before {
                                counter-increment: q1;
                                content: counter(q1, decimal)". ";
                                color: black;
                                font-family: Arial, sans-serif;
                                font-style: normal;
                                font-weight: bold;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l18>li:first-child>*:first-child:before {
                                counter-increment: q1 0;
                            }
                    
                            #l19 {
                                padding-left: 0pt;
                            }
                    
                            #l19>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l20 {
                                padding-left: 0pt;
                            }
                    
                            #l20>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            #l21 {
                                padding-left: 0pt;
                            }
                    
                            #l21>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            li {
                                display: block;
                            }
                    
                            #l22 {
                                padding-left: 0pt;
                            }
                    
                            #l22>li>*:first-child:before {
                                content: " ";
                                color: black;
                                font-family: Symbol, serif;
                                font-style: normal;
                                font-weight: normal;
                                text-decoration: none;
                                font-size: 11pt;
                            }
                    
                            .main-wrapper {
                                max-width: 1300px;
                                margin: 0 auto;
                                padding: 0 15px;
                            }
                        </style>
                    </head>
                    
                    <body>
                        <div class="main-wrapper">
                            <h1 style="text-align: center;">Auftragsverarbeitungs-Vertrag</h1>
                    
                            <p style="text-align: center;">zwischen</p>
                    
                            <p class="s1" style="text-align: center;">`+ companyname + `, ,</p>
                    
                            <p style="text-indent: 0pt;line-height: 300%;text-align: center;">- nachstehend Auftraggeber genannt </p>
                            <p style="text-align: center;">-und der</p>
                            <p style="text-indent: 0pt;line-height: 13pt;text-align: center;"> ` + address + `
                            </p>
                            <p style="text-indent: 0pt;text-align: center;">- Auftragsverarbeiter, nachstehend Auftragnehmer genannt -</p>
                    
                            <ol id="l1">
                                <li data-list-text="1.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Gegenstand und Dauer des Auftrags
                                    </h2>
                                    <ol id="l2">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">
                                                Gegenstand
                                            </p>
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: 0pt;text-align: justify;">Der
                                                Gegenstand
                                                des Auftrags ergibt sich aus der Vereinbarung der Vertragsparteien bezüglich der Nutzung der
                                                S-Verein-Software gemäß Lizenznutzungsvertrag und den Allgemeinen Geschäftsbedingungen, auf
                                                die
                                                hier verwiesen wird (im Folgenden Leistungsvereinbarung).</p>
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: 0pt;text-align: justify;">Im Rahmen
                                                der
                                                Leistungserbringung ist es erforderlich, dass der Auftragnehmer mit personenbezogenen Daten
                                                umgeht, für die der Auftraggeber als verantwortliche Stelle im Sinne der
                                                datenschutzrechtlichen
                                                Vorschriften fungiert (nachfolgend „Auftraggeber-Daten“ genannt). Dieser Vertrag
                                                konkretisiert
                                                die datenschutzrechtlichen Rechte und Pflichten der Parteien im Zusammenhang mit dem Umgang
                                                des
                                                Auftragnehmers mit Auftraggeber-Daten zur Durchführung der Leistungsvereinbarung</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 5pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Dauer</p>
                                        </li>
                                    </ol>
                                    <p style="padding-top: 6pt;padding-left: 25pt;text-indent: 0pt;text-align: justify;">Die Dauer dieses
                                        Auftrags (Laufzeit) entspricht der Laufzeit der Leistungsvereinbarung.</p>
                    
                                </li>
                                <li data-list-text="2.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Konkretisierung des
                                        Auftragsinhalts
                                    </h2>
                                    <ol id="l3">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 25pt;text-indent: -21pt;text-align: justify;">Art und
                                                Zweck
                                                der vorgesehenen Verarbeitung von Daten</p>
                                            <p style="padding-top: 6pt;padding-left: 25pt;text-indent: 0pt;text-align: justify;">Art und
                                                Zweck
                                                der Verarbeitung personenbezogener Daten durch den Auftragnehmer für den Auftraggeber sind
                                                konkret beschrieben in der Leistungsverein-barung der Parteien.</p>
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: 0pt;text-align: justify;">Die
                                                Erbringung
                                                der vertraglich vereinbarten Datenverarbeitung findet ausschließlich in Deutschland statt.
                                                Jede
                                                Verlagerung in ein Drittland bedarf der vorherigen Zustimmung des Auftraggebers und darf nur
                                                erfolgen, wenn die besonderen Voraussetzungen der Artt. 44 ff. DS-GVO erfüllt sind.</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Art der
                                                Daten
                                            </p>
                                            <p style="padding-top: 3pt;padding-left: 25pt;text-indent: 0pt;text-align: left;">Gegenstand der
                                                Verarbeitung personenbezogener Daten sind folgende Datenarten/-kategorien der
                                                (Vereins-)Mitglieder des Auftraggebers:</p>
                                            <ul id="l4">
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Personenstammdaten</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Kommunikationsdaten (z.B. Telefon, E-Mail)</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;text-align: left;">Vertragsstammdaten
                                                        (z.B.
                                                        Vereinsname, Adresse, vertretungsberechtigte Organe)</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Mitgliedshistorie</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;text-align: left;">Abrechnungs- und
                                                        Zahlungsdaten zur (Vereins-) Mitgliedschaft sowie Daten zu Zahlungsmöglichkeiten
                                                        (z.B.
                                                        Kontodaten, SEPA-Lastschriftmandate)</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 12pt;text-align: left;">
                                                        Planungs- und Steuerungsdaten</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Bildaufnahmen / Photos</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-top: 5pt;padding-left: 30pt;text-indent: -21pt;text-align: left;">Kategorien
                                                betroffener Personen</p>
                                            <p
                                                style="padding-top: 6pt;padding-left: 30pt;text-indent: 0pt;line-height: 12pt;text-align: left;">
                                                Die Kategorien der durch die Verarbeitung betroffenen Personen umfassen:</p>
                                            <ul id="l5">
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Vereins-Mitglieder</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Interessenten</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Abonnenten</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Beschäftigte</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Lieferanten</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        Sponsoren, Fans</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li data-list-text="(4)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Ferner
                                                werden
                                                aus den bestehenden Daten neue Daten generiert und gespeichert -dies umfasst insbesondere
                                                die
                                                automatische Erstellung von Rechnungen und Belegen, das Erstellen von
                                                Zuwendungsbescheinigungen
                                                sowie von Protokollen.</p>
                                        </li>
                                        <li data-list-text="(5)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -18pt;text-align: justify;">Die
                                                Laufzeit
                                                und Kündigung dieses Vertrags richtet sich nach den Bestimmungen zur Laufzeit und Kündigung
                                                der
                                                Leistungsvereinbarung. Eine Kündigung der Leistungsvereinbarung bewirkt automatisch auch
                                                eine
                                                Kündigung dieses Vertrags. Eine isolierte Kündigung dieses Vertrags ist ausgeschlossen.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="3.">
                                    <h2 style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">TOM-Technische und
                                        organisatorische
                                        Maßnahmen</h2>
                                    <ol id="l6">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer hat vor Beginn der Verarbeitung der Auftraggeber-Daten die in <b>Anlage 1
                                                </b>dieses Vertrags aufgelisteten TOM-technische und organisatorische Maßnahmen zu
                                                implementieren und während des Vertrags aufrechtzuerhalten.</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer hat die Sicherheit gem. Art. 28 Abs. 3 lit. c, 32 DS-GVO insbesondere in
                                                Verbindung mit Art. 5 Abs. 1, Abs. 2 DS-GVO herzustellen. Insgesamt handelt es sich bei den
                                                zu
                                                treffenden Maßnahmen um Maßnahmen der Datensicherheit und zur Gewährleistung eines dem
                                                Risiko
                                                angemessenen Schutzniveaus hinsichtlich der Vertraulichkeit, der Integrität, der
                                                Verfügbarkeit
                                                sowie der Belastbarkeit der Systeme. Dabei sind der Stand der Technik, die
                                                Implementierungskosten und die Art, der Umfang und die Zwecke der Verarbeitung sowie die
                                                unterschied-liche Eintrittswahrscheinlichkeit und Schwere des Risikos für die Rechte und
                                                Freiheiten natürlicher Personen im Sinne von Art. 32 Abs. 1 DS-GVO zu berück-sichtigen
                                                [Einzelheiten in Anlage 1].</p>
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-top: 5pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Da die
                                                technischen und organisatorischen Maßnahmen dem technischen Fortschritt und der
                                                technologischen
                                                Weiterentwicklung unterliegen, ist es dem Auftragnehmer gestattet, alternative und adäquate
                                                Maßnahmen umzusetzen, sofern dabei das Sicherheitsniveau der in <b>Anlage 1 </b>festgelegten
                                                Maßnahmen nicht unterschritten wird. Der Auftragnehmer wird solche Änderungen dokumentieren.
                                                Wesentliche Änderungen der Maßnahmen bedürfen der vorherigen schriftlichen Zustimmung des
                                                Auftraggebers und sind vom Auftragnehmer zu dokumentieren und dem Auftraggeber auf
                                                Anforderung
                                                zur Verfügung zu stellen.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li data-list-text="4.">
                                    <h2 style="padding-top: 3pt;padding-left: 21pt;text-indent: -12pt;text-align: justify;">Berichtigung,
                                        Einschränkung und Löschung von Daten</h2>
                                    <ol id="l7">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer darf die Daten, die im Auftrag verarbeitet werden, nicht eigen-mächtig sondern
                                                nur
                                                nach dokumentierter Weisung des Auftraggebers berichtigen, löschen oder deren Verarbeitung
                                                einschränken. Soweit eine betroffene Person sich diesbezüglich unmittelbar an den
                                                Auftragnehmer
                                                wendet, wird der Auftragnehmer dieses Ersuchen unverzüglich an den Auftraggeber
                                                weiterleiten.
                                            </p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 5pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Soweit
                                                vom
                                                Leistungsumfang umfasst, sind Löschkonzept, Recht auf Vergessen-werden, Berichtigung,
                                                Datenportabilität und Auskunft nach dokumentierter Weisung des Auftraggebers unmittelbar
                                                durch
                                                den Auftragnehmer sicherzustellen.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="5.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Qualitätssicherung und sonstige
                                        Pflichten des Auftragnehmers</h2>
                                    <p style="padding-top: 6pt;padding-left: 8pt;text-indent: 0pt;text-align: justify;">Der Auftragnehmer
                                        hat
                                        zusätzlich zu der Einhaltung der Regelungen dieses Auftrags gesetzliche Pflichten gemäß Artt. 28 bis
                                        33
                                        DS-GVO; insofern gewährleistet er insbesondere die Einhaltung folgender Vorgaben:</p>
                                    <ol id="l8">
                                        <li data-list-text="a)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">
                                                Schriftliche
                                                Bestellung eines Datenschutzbeauftragten, der seine Tätigkeit gemäß Artt. 38 und 39 DS-GVO
                                                ausübt und dessen jeweils aktuelle Kontaktdaten auf der Homepage des Auftragnehmers leicht
                                                zugänglich hinterlegt sind.</p>
                                        </li>
                                        <li data-list-text="b)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Die Wahrung der
                                                Vertraulichkeit gemäß Artt. 28 Abs. 3 S. 2 lit. b, 29, 32 Abs. 4 DS-GVO. Der Auftragnehmer
                                                setzt
                                                bei der Durchführung der Arbeiten nur Beschäftigte ein, die auf die Vertraulichkeit
                                                verpflichtet
                                                und zuvor mit den für sie relevanten Bestimmungen zum Datenschutz vertraut gemacht wurden.
                                                Der
                                                Auftragnehmer und jede dem Auftragnehmer unterstellte Person, die Zugang zu
                                                personenbezogenen
                                                Daten hat, dürfen diese Daten ausschließlich entsprechend der Weisung des Auftraggebers
                                                verarbeiten einschließlich der in diesem Vertrag eingeräumten Befugnisse, es sei denn, dass
                                                sie
                                                gesetzlich zur Verarbeitung verpflichtet sind.</p>
                                        </li>
                                        <li data-list-text="c)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Die Umsetzung und
                                                Einhaltung
                                                aller für diesen Auftrag erforderlichen technischen und organisatorischen Maßnahmen gemäß
                                                Artt.
                                                28 Abs. 3 S. 2 lit. c, 32 DS-GVO (Einzelheiten in Anlage 1).</p>
                                        </li>
                                        <li data-list-text="d)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftraggeber und der
                                                Auftragnehmer arbeiten auf Anfrage mit der Aufsichtsbehörde bei der Erfüllung ihrer Aufgaben
                                                zusammen.</p>
                                        </li>
                                        <li data-list-text="e)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Die unverzügliche
                                                Information
                                                des Auftraggebers über Kontrollhandlungen und Maßnahmen der Aufsichtsbehörde, soweit sie
                                                sich
                                                auf diesen Auftrag beziehen. Dies gilt auch, soweit eine zuständige Behörde im Rahmen eines
                                                Ordnungswidrig-keits-oder Strafverfahrens in Bezug auf die Verarbeitung personenbezogener
                                                Daten
                                                bei der Auftragsverarbeitung beim Auftragnehmer ermittelt.</p>
                                        </li>
                                        <li data-list-text="f)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Soweit der Auftraggeber
                                                seinerseits einer Kontrolle der Aufsichtsbehörde, einem Ordnungswidrigkeits- oder
                                                Strafverfahren, dem Haftungsanspruch einer betroffenen Person oder eines Dritten oder einem
                                                anderen Anspruch im Zusammenhang mit der Auftragsverarbeitung beim Auftragnehmer ausgesetzt
                                                ist,
                                                hat ihn der Auftragnehmer nach besten Kräften zu unterstützen.</p>
                                        </li>
                                        <li data-list-text="g)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftragnehmer
                                                kontrolliert
                                                regelmäßig die internen Prozesse sowie die technischen und organisatorischen Maßnahmen, um
                                                zu
                                                gewährleisten, dass die Verarbeitung in seinem Verantwortungsbereich im Einklang mit den
                                                Anforderungen des geltenden Datenschutzrechts erfolgt und der Schutz der Rechte der
                                                betroffenen
                                                Person gewährleistet wird.</p>
                                        </li>
                                        <li data-list-text="h)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Nachweisbarkeit der
                                                getroffenen technischen und organisatorischen Maßnahmen gegenüber dem Auftraggeber im Rahmen
                                                seiner Kontrollbefugnisse nach Ziffer 7 dieses Vertrages.</p>
                                        </li>
                                    </ol>
                                </li>
                                <li data-list-text="6.">
                                    <h2 style="padding-top: 3pt;padding-left: 21pt;text-indent: -12pt;text-align: justify;">
                                        Unterauftragsverhältnisse</h2>
                                    <ol id="l9">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Als
                                                Unterauftragsverhältnisse im Sinne dieser Regelung sind solche Dienstleistungen zu
                                                verstehen,
                                                die sich unmittelbar auf die Erbringung der Hauptleistung beziehen. Nicht hierzu gehören
                                                Nebenleistungen, die der Auftragnehmer z.B. als Telekommunikationsleistungen,
                                                Post-/Transportdienstleistungen, Wartung und Benutzerservice oder die Entsorgung von
                                                Datenträgern sowie sonstige Maßnahmen zur Sicherstellung der Vertraulichkeit, Verfügbarkeit,
                                                Integrität und Belastbarkeit der Hard- und Software von Datenverarbeitungsanlagen in
                                                Anspruch
                                                nimmt. Der Auftragnehmer ist jedoch verpflichtet, zur Gewährleistung des Datenschutzes und
                                                der
                                                Datensicherheit der Daten des Auftraggebers auch bei ausgelagerten Nebenleistungen
                                                angemessene
                                                und gesetzeskonforme vertragliche Vereinbarungen sowie Kontrollmaßnahmen zu ergreifen.</p>
                    
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftragnehmer darf
                                                Unterauftragsverhältnisse hinsichtlich der Verarbeitung oder Nutzung von Auftraggeber-Daten
                                                nur
                                                nach vorheriger, schriftlicher Zustimmung des Auftraggebers begründen. Eine solche
                                                vorherige
                                                Zustimmung darf vom Auftrag-geber nur aus wichtigem, dem Auftragnehmer nachzuweisenden Grund
                                                verweigert werden.</p>
                    
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-left: 29pt;text-indent: -21pt;text-align: justify;">Keiner Zustimmung bedarf
                                                die
                                                Einschaltung von Subunternehmern, bei denen der Subunternehmer lediglich eine Nebenleistung
                                                zur
                                                Unterstützung bei der Leistungserbringung nach dem Hauptvertrag in Anspruch nimmt, auch
                                                wenn
                                                dabei ein Zugriff auf die Auftraggeber-Daten nicht ausgeschlossen werden kann. Der
                                                Auftragnehmer
                                            </p>
                                            <p style="padding-left: 29pt;text-indent: 0pt;text-align: left;">wird mit solchen
                                                Subunternehmern
                                                branchenub gen treffen.</p>
                                            <p style="padding-left: 3pt;text-indent: 0pt;line-height: 13pt;text-align: left;">liche
                                                Geheimhaltungsvereinbarun-</p>
                                        </li>
                                        <li data-list-text="(4)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Unterauftrags-Datenverarbeitungsvertrag muss ein adäquates Schutzniveau aufweisen, welches
                                                demjenigen dieses Vertrags vergleichbar ist.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="7.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Kontrollrechte des Auftraggebers
                                    </h2>
                                    <ol id="l10">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftraggeber hat das Recht, im Benehmen mit dem Auftragnehmer Überprüfungen durchzuführen
                                                oder
                                                durch im Einzelfall zu benennende Prüfer durch-führen zu lassen. Er hat das Recht, sich
                                                durch
                                                Stichprobenkontrollen, die in der Regel rechtzeitig anzumelden sind und den Geschäftsbetrieb
                                                nicht stören dürfen, von der Einhaltung dieser Vereinbarung durch den Auftragnehmer in
                                                dessen
                                                Geschäftsbetrieb zu überzeugen. Für Unterauftragnehmer kann dies nicht immer gewährleistet
                                                werden.</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 5pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer stellt sicher, dass sich der Auftraggeber von der Einhaltung der Pflichten des
                                                Auftragnehmers nach Art. 28 DS-GVO überzeugen kann. Der Auftragnehmer verpflichtet sich, dem
                                                Auftraggeber auf Anforderung die erforderlichen Auskünfte zu erteilen und insbesondere die
                                                Umsetzung der technischen und organisatorischen Maßnahmen nachzuweisen.</p>
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-top: 5pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Der
                                                Nachweis
                                                solcher Maßnahmen, die nicht nur den konkreten Auftrag betreffen, kann erfolgen durch</p>
                                            <ul id="l11">
                                                <li data-list-text="">
                                                    <p
                                                        style="padding-top: 5pt;padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: justify;">
                                                        die Einhaltung genehmigter Verhaltensregeln gemäß Art. 40 DS-GVO;</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;text-align: justify;">die Zertifizierung
                                                        nach einem genehmigten Zertifizierungsverfahren gemäß Art. 42 DS-GVO;</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;line-height: 13pt;text-align: left;">
                                                        aktuelle Testate, Berichte oder Berichtsauszüge unabhängiger Instanzen (z.B.</p>
                                                    <p style="padding-left: 44pt;text-indent: 0pt;text-align: left;">Wirtschaftsprüfer,
                                                        Revision, Datenschutzbeauftragter, IT-Sicherheitsabteilung, Datenschutzauditoren,
                                                        Qualitätsauditoren);</p>
                                                </li>
                                                <li data-list-text="">
                                                    <p style="padding-left: 44pt;text-indent: -14pt;text-align: left;">eine geeignete
                                                        Zertifizierung durch IT-Sicherheits- oder Datenschutzaudit (z.B. nach
                                                        BSI-Grundschutz).
                                                    </p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li data-list-text="(4)">
                                            <p style="padding-top: 3pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Für die
                                                Ermöglichung von Kontrollen durch den Auftraggeber kann der Auftragnehmer einen
                                                Vergütungsanspruch geltend machen.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="8.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Mitteilung bei Verstößen des
                                        Auftragnehmers</h2>
                                    <ol id="l12">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer unterstützt den Auftraggeber bei der Einhaltung der in den Artikeln 32 bis 36
                                                der
                                                DS-GVO genannten Pflichten zur Sicherheit personenbezogener Daten, Meldepflichten bei
                                                Datenpannen, Datenschutz-Folgeabschätz-ungen und vorherige Konsultationen. Hierzu gehören
                                                u.a.
                                            </p>
                                            <p style="padding-top: 6pt;padding-left: 51pt;text-indent: -21pt;text-align: justify;">a.) die
                                                Sicherstellung eines angemessenen Schutzniveaus durch technische und organisatorische
                                                Maßnahmen,
                                                die die Umstände und Zwecke der Verarbeitung sowie die prognostizierte Wahrscheinlichkeit
                                                und
                                                Schwere einer möglichen Rechtsverletzung durch Sicherheitslücken berücksichtigen und eine
                                                sofortige Fest-stellung von relevanten Verletzungsereignissen ermöglichen</p>
                                            <p style="padding-left: 51pt;text-indent: -21pt;text-align: justify;">b.) die Verpflichtung,
                                                Verletzungen personenbezogener Daten unverzüglich an den Auftraggeber zu melden</p>
                                            <p style="padding-left: 51pt;text-indent: -21pt;text-align: justify;">c.) die Verpflichtung, dem
                                                Auftraggeber im Rahmen seiner Informationspflicht gegenüber dem Betroffenen zu unterstützen
                                                und
                                                ihm in diesem Zusammenhang sämtliche relevante Informationen unverzüglich zur Verfügung zu
                                                stellen</p>
                                            <p style="padding-left: 51pt;text-indent: -21pt;text-align: justify;">d.) die Unterstützung des
                                                Auftraggebers für dessen Datenschutz-Folgenab-schätzung</p>
                                            <p style="padding-left: 51pt;text-indent: -21pt;text-align: justify;">e.) die Unterstützung des
                                                Auftraggebers im Rahmen vorheriger Konsultationen mit der Aufsichtsbehörde</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Für
                                                Unterstützungsleistungen, die nicht in der Leistungsbeschreibung enthalten oder nicht auf
                                                ein
                                                Fehlverhalten des Auftragnehmers zurückzuführen sind, kann der Auftragnehmer eine Vergütung
                                                beanspruchen.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="9.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Weisungsbefugnis des
                                        Auftraggebers
                                    </h2>
                                    <ol id="l13">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Weisungen
                                                haben grundsätzlich in Textform zu erfolgen. In akuten Notfällen erteilte mündliche
                                                Weisungen
                                                bestätigt der Auftraggeber unverzüglich in Schriftform.</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer hat den Auftraggeber unverzüglich zu informieren, wenn er der Meinung ist,
                                                eine
                                                Weisung verstoße gegen Datenschutzvorschriften. Der Auftragnehmer ist berechtigt, die
                                                Durchführung der entsprechenden Weisung solange auszusetzen, bis sie durch den Auftraggeber
                                                bestätigt oder geändert wird.</p>
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-top: 5pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der
                                                Auftragnehmer verwendet die Auftraggeber-Daten ausschließlich in Übereinstimmung mit den
                                                Weisungen des Auftraggebers, wie sie abschließend in den Bestimmungen dieses Vertrags
                                                Ausdruck
                                                finden. Einzelweisungen, die von den Festlegungen dieses Vertrags abweichen oder zusätzliche
                                                Anforderungen aufstellen, bedürfen einer vorherigen Zustimmung des Auftragnehmers und
                                                erfolgen
                                                nach Maßgabe des im Hauptvertrag festgelegten Änderungsverfahrens, in dem auch die Übernahme
                                                etwa dadurch bedingter Mehrkosten des Auftragnehmers durch den Auftraggeber zu regeln ist.
                                            </p>
                                        </li>
                                    </ol>
                                </li>
                                <li data-list-text="10.">
                                    <h2 style="padding-top: 5pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Rechtsstellung
                                        des Auftraggebers</h2>
                                </li>
                            </ol>
                            <ol id="l14">
                                <li data-list-text="(1)">
                                    <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftraggeber
                                        steht nach außen, also gegenüber Dritten und den Betroffenen, für die Rechtmäßigkeit der
                                        auftragsgemäßen
                                        Erhebung und Verwendung der Auftraggeber-Daten ein. Er ist nach außen auch für die Wahrung der
                                        Rechte
                                        der Betroffenen verantwortlich.</p>
                                </li>
                                <li data-list-text="(2)">
                                    <p style="padding-top: 5pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftraggeber
                                        ist
                                        Eigentümer der Auftraggeber-Daten und im Verhältnis der Parteien zueinander Inhaber aller etwaigen
                                        Rechte an den Auftraggeber-Daten.</p>
                                </li>
                                <li data-list-text="(3)">
                                    <p style="padding-top: 3pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Der Auftraggeber
                                        ist
                                        für die Rechtmäßigkeit der Erhebung, Verarbeitung und Nutzung der Auftraggeber-Daten sowie für die
                                        Wahrung der Rechte der Betroffenen verantwortlich. Sollten Dritte gegen den Auftragnehmer aufgrund
                                        der
                                        Erhebung, Verarbeitung oder Nutzung von Auftraggeber-Daten Ansprüche geltend machen, wird der
                                        Auftraggeber den Auftragnehmer von allen solchen Ansprüchen auf erstes Anfordern freistellen.</p>
                                </li>
                            </ol>
                    
                            <ol id="l15">
                                <li data-list-text="10.">
                                    <h2 style="padding-left: 27pt;text-indent: -18pt;text-align: left;">Löschung und Rückgabe von
                                        personenbezogenen Daten</h2>
                                    <ol id="l16">
                                        <li data-list-text="(1)">
                                            <p style="padding-top: 6pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Kopien
                                                oder
                                                Duplikate der Daten werden ohne Wissen des Auftraggebers nicht erstellt. Hiervon ausgenommen
                                                sind Sicherheitskopien, soweit sie zur Gewährleistung einer ordnungsgemäßen
                                                Datenverarbeitung
                                                erforderlich sind, sowie Daten, die im Hinblick auf die Einhaltung gesetzlicher
                                                Aufbewahrungspflichten erforderlich sind.</p>
                                        </li>
                                        <li data-list-text="(2)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">Nach
                                                Abschluss der vertraglich vereinbarten Arbeiten oder früher nach Aufforderung durch den
                                                Auftraggeber – spätestens mit Beendigung der Leistungsvereinbarung –hat der Auftragnehmer
                                                sämtliche in seinen Besitz gelangten Unterlagen, erstellte Verarbeitungs- und
                                                Nutzungsergebnisse
                                                sowie Datenbestände, die im Zusammenhang mit dem Auftragsverhältnis stehen, dem Auftraggeber
                                                auszuhändigen oder nach vorheriger Zustimmung datenschutzgerecht zu vernichten. Gleiches
                                                gilt
                                                für Test- und Ausschussmaterial. Das Protokoll der Löschung ist auf Anforderung vorzulegen.
                                            </p>
                                        </li>
                                        <li data-list-text="(3)">
                                            <p style="padding-top: 6pt;padding-left: 30pt;text-indent: -21pt;text-align: justify;">
                                                Dokumentationen, die dem Nachweis der auftrags- und ordnungsgemäßen Datenverarbeitung
                                                dienen,
                                                sind durch den Auftragnehmer entsprechend der jeweiligen Aufbewahrungsfristen über das
                                                Vertragsende hinaus aufzubewahren. Er kann sie zu seiner Entlastung bei Vertragsende dem
                                                Auftraggeber übergeben.</p>
                                        </li>
                                    </ol>
                    
                                </li>
                                <li data-list-text="11.">
                                    <h2 style="padding-left: 27pt;text-indent: -18pt;text-align: left;">Schlussbestimmungen</h2>
                                </li>
                            </ol>
                            <ol id="l17">
                                <li data-list-text="(1)">
                                    <p style="padding-top: 8pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Änderungen,
                                        Ergänzungen und die Aufhebung dieses Vertrags bedürfen der Schriftform. Gleiches gilt für eine
                                        Änderung
                                        oder Aufhebung des Schriftformerfordernisses.</p>
                                </li>
                                <li data-list-text="(2)">
                                    <p style="padding-top: 7pt;padding-left: 29pt;text-indent: -21pt;text-align: justify;">Sollten einzelne
                                        Bestimmungen dieses Vertrags unwirksam sein oder werden oder eine Lücke enthalten, so bleiben die
                                        übrigen Bestimmungen hiervon unberührt. Die Parteien verpflichten sich, anstelle der unwirksamen
                                        Regelung eine solche gesetzlich zulässige Regelung zu treffen, die dem Zweck der unwirksamen
                                        Regelung am
                                        nächsten kommt und den Anforderungen der Artt 28 ff. DS-GVO am besten gerecht wird.</p>
                                </li>
                            </ol>
                    
                            <p style="padding-top: 4pt;padding-left: 8pt;text-indent: 0pt;text-align: left;">Meersburg,</p>
                            <p class="s1" style="padding-top: 5pt;padding-left: 6pt;text-indent: 0pt;text-align: left;">13.01.2021</p>
                    
                            <p style="text-indent: 0pt;line-height: 12pt;text-align: left;">.</p>
                            <p style="text-indent: 0pt;text-align: left;" />
                            <p style="text-indent: 0pt;text-align: left;">
                                <span>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td><img width="200" height="60"
                                                    src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAA8AMgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9UqKM0ZoADWN4o8W6d4R077XfysNzbIoYlLyzOeiIo5JNQeMPF0Phazi2wte6jdP5NnYxffnk9PYDqT0Aqj4W8GywXZ1rXpV1DxBKD8+P3Vop/wCWcI7Adz1Pf0rrp0oqPta3w9F1fp2Xd/mcdStJz9lR+Lq+i9e77L77HlHxZ+Onjz4aeH08ZXfhax07wjbXESXUN5c5vTG7Bd2F+VTkjjJ969+sbpL6zguY+Y5kWRfoRkfzr58/bukkvPganh+C2e6uPEWtadpMaxthl8y4TLD6KCePSvoHTbVdP062tUzthiWNcnJwAB/StK8oTpQlGCjvtfbTu2KhSqUpyUpuSdt7b69krLYs0Vw/iX4jyW+pyaJ4c09vEGvRjMsMb7IbYHoZpOi57LyT6VlfCz4oav4q8UeJPDXiHS4dL1rRjFIy20hkjeOQEqQxHXil9Treyda2i13V7bXtvbXcl46h7ZULu702drpXtfa9k9LnptFGaM1wneFFGaM0AFFcz8QfiR4e+F3h+TWvEupR6dYqwRSwLPK56IijlmPoBVP4W/FjQfjB4efWfD0k72aTNAwuYTGwYdRg/WtVSm4OoovlXXoZOrTU1TclzPp1OyoozRmsjUKKM0ZoAKKM0ZoAKKM0ZoAKKM0ZoAKKM0UAFZ+v65aeG9IutRvX2W9uhdsck+gA7knAA960CcV5xa3f/CyvHExTbJ4b8PzGPPVbq+HX/gMQ/Nj7V00Kam3KfwrV/wCXq9jmr1HCKjD4nov8/Rb/AIbml4J0G6v7p/FGuxFNXu0xBbOcixgPIjX0Y8Fj3PHQCu1oHAoNRVqutPmenZdl2RdGkqMOVfN933Z4L8aVbxZ8fvhB4WVi9vaXF14iu4gxA2wR7ImOOv7yQcGut8beOLvXfE3/AAgnhO5C60UEmqagg3LpVuehPbzX6Kp/3jwK8e+JPxUTwH8QvH/jC1t01PxCI7Xwf4YscZa6vmBmZFA5wGdSx9E7Yr2v4HfDSX4beC44dTuzqnibUXN9rOpuoV7m7cZc8dFX7qjsFFejUjGjGMpLVJWXdvW78kmvnY5Yt1uZRdk2/uWmnrY6rwr4T07wdpEWn6bD5cS8u7HdJK3d3Y8sxPJJrzLwChu/2jviZeqoEFtZ6faMwPBfYznPvgivY5pFhid2IVVBJJ7Cvzw+Gfxc8S/tX+LvGHgjwW11oNhqOuXFz4p8UW3DQ6dG3k29vbuOskyxn5h91c49ajDSlUjWcpatbv1Tf5Dq0kpUlFaRd/wa/U+sbn4v6p8QPEWoeH/hxawXyadKbfUPEl2d1jbSgcxx4OZnHcDgHgmszTNf8WeC/jponhjWPEX/AAkFlrdhNc7WgWLyHT+6F6D6mvVfCXhTSPh94e07w/oOnx2GmWcYiighGAoHcnuT1JPJNfPfwnvJfjH+194/8axSGTw14MtE8K6ayMfLmu2Ikun6YO35UyD61dPEU4RlTUFy2au1q30d9ba9F+O5jVwlSpKNR1HzJp6NpW6qy0d1prc+o6pa1q9poGk3epX0y29naxNNNK5wFVRkmrteS/FHxVo9zfXcGs31vY+EvDaLqWu3U7gR7lG+KA/o5Hso71wUKXtZ2eiWr9P8+i8zurVHTj7qu3ovX+tX5HEatHd6zEnjjxLYrc+JdTY2vhDw9cJ5i2Ib7szJj/WkfOzEfKOK9i+Fnw9tfhp4PtdHt282bc091PjBmnc7pH/Ek18xaJ+0Tr+rX6fFy58EXLeBHZbOxurvMcttaseZUTuX4JOMdADX2Bo+rWmu6XaajYzLcWd1Es0MqHIZWGQfyNevmEalGjCnb3Xq7bX/AJf+3b631bbZ5GBnTr15zfxR01VnbrLz5mtLaJJLuXK+a/2hPjJ4t8NftHfBDwB4SukiTX7y6utaieJX32UScjJ5XkkgjH3etfShOAa+QvhDYx/Gj9uH4i/EjyUl0TwXYx+EtLuNuVkucmS5dT6ruK5H94ivFglq30PePrwdKWkZgoJPAHevM9d1/U/iRqMmheFrk2mkROY9S16Pkehhtz3fsW6L9aujRdZvWyW7ey/roupz1q8aKWl29kt3/XV7I5z40/tL2vw432ei6TN4i1CO4itriSM7ba1kkOEV37sf7o59a9qtHeW1heVQkjICyg8A45FeF+K/Bel6h8RPAvw/0uyEOi6Qx8Q6gqHO4xnbAJCeWLSEtk9dte7TzxWlu8szrFFGpZ3cgKoHJJPYV14uNCFOmqUXd3bb3fRemzduz3ZyYR15VKkq0rpWSS2XV+b3Sv3WyJDxVCy13T9Rvbm0tb23uLq2x58MUoZ4s9NwB4rwGf4h61+0rr8+ieAL2fSfAFoxj1LxdAMNeuDgwWhPUdQZPy93fs8eFLHw38YviTb6NG0WkWItbJQ8hdnlALMxY9SSST9aqGCj7CdScrSik7erS1fS99ERPGyVenThG8ZNq9+yb0XXbV/dc+i6K4z4j/Fzw18LbCOfXb9Yppjtt7OIb5529FQcn69Ki+EXxPtviz4WbW7WxmsIRcPAI5yCx2454+tcaw1Z0XiOX3L2v5nY8VRVZYfm99q9vI7iiiiuY6jhfjL4zufB/g2T+zAr67qMqadpsRP3p5TtB9woyx9lra8BeEbfwP4T07RrcmT7PH+8mb70sh5dz7sxJ/GvPvEST+LP2jPDmnSW7HSvDumS6oZGQ7WuZG8pBnpkLuP417DgV3Vf3VGFJfa95/kvw1+Zw0v3tadR/Z91fg3+OnyFrA8feLbTwH4I17xHfPss9KsZr2U/7MaFj/Kt7Aryr9pz4eeIPiv8HNc8IeHJra2vNX8u1lnumIWOAuPMOB1O0HjvXNSipzUW7JnXOXLFytc+OP8Agm34M8SfGfxJrPxe8bz3E9lY6leHQrGYny1urht1zOAe6jbGPTDV+jgGK4/4RfDLSvg78OdC8IaMuLHS7dYRIQA0r9Xkb/aZiWPua7DAqq1T2kr9EEVZHl/7Td3rtt8CvGSeGbO7vddurB7S0jsVzKHl/d7h6bd2c9sVz/7Hf7Ptv+zn8E9H8OvHH/bk6C71adOd9ywGVz3CjCj6e9e3kA0uKXtLU/Zpdd/0Fyvn5m/keN/tdfFO/wDg/wDATxPruixSzeIJYlsNKjgjMjtdzMI4sKAckFs474xR+yL8I5vgr8A/C3h6/RhrbQfbtVkc5d7yY+ZKWPchmx+Fev3NpBeIEniSZAwYLIoYBgcg89wakAAqeZcnKiyvqlxNa6bdTW8JuLiOJ3jhBwXYAkLk+pwK/OD9kb4HeN/2kvEHiDxR8WpbyDwbFr819J4al2iLU9SVgpMhAzJDFtCBSSCV44HP6RXdql5azQOWCSoUYqcHBGDg9qg0fR7LQNNgsLC3S1s4FCRxRjAUf571rCooU2l8Tt/X9f5Gcoyc12/Ura74X03xD4au9Cu7WN9Mubc2z24UBQhGMAdsdvpXgvwe8cp8Eb+5+G/jW7TT4bSVm0jUZzthmgYkqpY8D/8AWK+kMCud8ZfD3w58QLEWniHSLbVYBnaJ0yV+h6j8K6cNiacISoV03CWum6a2a/VdTgxeFqTnDEYdpTjprs090/zT6M8P+Pn7T9tFBD4F+F1xH4t+I2ur5FpFpciypYIwwbiZwcIqjmvRv2evg3Y/AL4VaX4WgnN3cxB7nUL+T711dSHfNKx92J69sVueAfhD4N+F0EsXhXw5p+ied/rXtYQryf7zdT+JqTxh4ZvfGF1b6dNMbXw+uJLtYnKy3RB4iyOif3vXp61F6U7UoO0d23u/l+Svvu+3S3VhHnavJ6WW39d326GFd3958V7qbT9MklsvCkTtFeaih2vfEHBjhI6J2Z+/Qetd7pWk2eh6bBY2FtHaWkCBI4Yl2qoHoKms7OCwtore2iSCCJQiRRrtVQOgA7CpsCsatbnShBWiun6vu/6RVKjyN1Ju8n1/Rdl/TPC/Bnjrw/pPxH+K+ua7rFjp4014IppLqdU8i2jQnccnhd278RXmEPi3xT+3FrzWXh1rzwv8DLWQx3urMDDd+ImB5ig7pB2Ynk9PavT/AB1+xn8MfiP8QJvFuvaXd3d7cFWubRbt0tbgrjHmRjG4cA4zgntXtOlaTZaHp1vYafaw2NlboI4re3QJHGo6BVHAFdmIxFKVRVaW9or0skvnsc2Fw9SnSdKr3k9Ot23+pW8OeGtM8H6BZ6Po1lFp+m2cQigtoF2qijoP/r96+FPAfxX+OkXizxz4J8G/CuWy8RX2vXMr+KteDx6da2+4rHIRgeaxAyApI5H0r7/wKTaM1z0sS6cZxkr81r69nfXq/vR1ToQnOE7axvbyvofK99+z5dfB74W+NvHl1e3PxE+LiaPdzRaxfxmTZLsYiO3i52qM4AHOPrivPv2ef26PhF8OP2fPDdprHiL7R4tiiZb7RdOtXlunudxL7UAxtJ6HOK+6SoIwelcrYfCnwZpWtyazZ+FdGtdWkJZ72GwjWYnudwXOa1+turFwrttXT0022XZWv2IWHhCaqQSuk1frra/321Mv4Q/GjRvjHpc93pcF5YTW5UT2WoReVPFuGRuXt9PaivGPgnr18/7bvx10J2T+zLXT9LuYY0QDa8isWye5PWiufEqiqn7hNRst/TX8SsP7ZU7V2nLXbtfT8LX8z6kCKH3cbsYzinZ96WiuU6RM+9GfelooATPvRn3paKAEz70Z96WigBM+9GfelooATPvRn3paKAEz70Z96WigBM+9GfelooATPvRn3paKAEz70Z96WigBM+9GfelooATPvSE8dadRQB8ifCNp9K/4KJ/Gu1uAyjUfD2l3UG7+JE+UkewJIor6Z/4QPQD47Xxh/ZkP/CSiwOm/2iMiQ25cP5Z7EbgD60VpNp2sJH//2QAA" />
                                            </td>
                                        </tr>
                                    </table>
                                </span>
                            </p>
                            <p style="padding-top: 4pt;padding-left: 8pt;text-indent: 0pt;text-align: left;">
                                ................................................</p>
                            <p style="padding-top: 8pt;padding-left: 8pt;text-indent: 0pt;text-align: left;">Jean-Claude Parent, Vorstand
                            </p>
                    
                            <p class="s2" style="padding-left: 8pt;text-indent: 0pt;line-height: 109%;text-align: left;"><a
                                    href="http://www.tineon.de/" class="a" target="_blank">Tineon AG - Uferpromenade 5 - 88709 Meersburg –
                                </a>www.tineon.de - Sitz der Gesellschaft: Meersburg – Ust.-ID DE 203 912 818 Registergericht Freiburg HRB
                                706
                                086 –Vorstand: Jean-Claude Parent - Vorsitzender des Aufsichtsrats: Hans-Peter Haubold</p>
                            <h2 style="padding-top: 3pt;padding-left: 8pt;text-indent: 0pt;text-align: left;">Anlage 1 – TOM Technisch
                                Organisatorische Maßnahmen</h2>
                    
                            <ol id="l18">
                                <li data-list-text="1.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Vertraulichkeit (Art. 32 Abs. 1
                                        lit.
                                        b DS-GVO)</h2>
                                    <ul id="l19">
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Zutrittskontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Kein unbefugter Zutritt zu
                                                Datenverarbeitungsanlagen, z.B.: Magnet- oder Chipkarten, Schlüssel, elektrische Türöffner,
                                                Werkschutz bzw. Pförtner, Alarmanlagen, Videoanlagen;</p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Zugangskontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Keine unbefugte
                                                Systembenutzung,
                                                z.B.: (sichere) Kennwörter, automatische Sperr-mechanismen, Zwei-Faktor-Authentifizierung,
                                                Verschlüsselung von Datenträgern;</p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Zugriffskontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Kein unbefugtes Lesen,
                                                Kopieren,
                                                Verändern oder Entfernen innerhalb des Systems, z.B.: Berechtigungskonzepte und
                                                bedarfsgerechte
                                                Zugriffsrechte, Protokollierung von Zugriffen;</p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Trennungskontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Getrennte Verarbeitung von
                                                Daten, die zu unterschiedlichen Zwecken erhoben wurden, z.B. Mandantenfähigkeit, Sandboxing;
                                            </p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Pseudonymisierung (Art. 32 Abs. 1 lit. a DS-GVO; Art. 25 Abs. 1 DS-GVO)</p>
                                            <p style="padding-top: 6pt;padding-left: 22pt;text-indent: 0pt;text-align: justify;">Die
                                                Verarbeitung personenbezogener Daten in einer Weise, dass die Daten ohne Hinzuziehung
                                                zusätzlicher Informationen nicht mehr einer spezifischen betroffenen Person zugeordnet
                                                werden
                                                können, sofern diese zusätzlichen Informationen gesondert aufbewahrt werden und
                                                entsprechende
                                                technischen und organisatorischen Maßnahmen unterliegen;</p>
                    
                                        </li>
                                    </ul>
                                </li>
                                <li data-list-text="2.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Integrität (Art. 32 Abs. 1 lit. b
                                        DS-GVO)</h2>
                                    <ul id="l20">
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Weitergabekontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Kein unbefugtes Lesen,
                                                Kopieren,
                                                Verändern oder Entfernen bei elektronischer Übertragung oder Transport, z.B.:
                                                Verschlüsselung,
                                                Virtual Private Networks (VPN), elektronische Signatur;</p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Eingabekontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Feststellung, ob und von wem
                                                personenbezogene Daten in Datenverarbeitungssysteme eingegeben, verändert oder entfernt
                                                worden
                                                sind, z.B.: Protokollierung, Dokumenten-management;</p>
                    
                                        </li>
                                    </ul>
                                </li>
                                <li data-list-text="3.">
                                    <h2 style="padding-left: 21pt;text-indent: -12pt;text-align: justify;">Verfügbarkeit und Belastbarkeit
                                        (Art.
                                        32 Abs. 1 lit. b DS-GVO)</h2>
                                    <ul id="l21">
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                                Verfügbarkeitskontrolle</p>
                                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Schutz gegen zufällige oder
                                                mutwillige Zerstörung bzw. Verlust, z.B.: Backup-Strategie (online/offline;
                                                on-site/off-site),
                                                unterbrechungsfreie Stromversorgung (USV), Viren-schutz, Firewall, Meldewege und
                                                Notfallpläne;
                                            </p>
                                        </li>
                                        <li data-list-text="">
                                            <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">Rasche
                                                Wiederherstellbarkeit (Art. 32 Abs. 1 lit. c DS-GVO);</p>
                    
                                        </li>
                                    </ul>
                                </li>
                                <li data-list-text="4.">
                                    <h2 style="padding-top: 10pt;padding-left: 22pt;text-indent: -14pt;text-align: left;">Verfahren zur
                                        regelmäßigen Überprüfung, Bewertung und Evaluierung (Art. 32 Abs. 1 lit. d DS-GVO; Art. 25 Abs. 1
                                        DS-GVO)</h2>
                                </li>
                            </ol>
                            <ul id="l22">
                                <li data-list-text="">
                                    <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                        Datenschutz-Management;</p>
                                </li>
                                <li data-list-text="">
                                    <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                        Incident-Response-Management;</p>
                                </li>
                                <li data-list-text="">
                                    <p style="padding-top: 3pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">
                                        Datenschutzfreundliche Voreinstellungen (Art. 25 Abs. 2 DS-GVO);</p>
                                </li>
                                <li data-list-text="">
                                    <p style="padding-top: 5pt;padding-left: 22pt;text-indent: -14pt;text-align: justify;">Auftragskontrolle
                                    </p>
                                </li>
                            </ul>
                            <p style="padding-left: 22pt;text-indent: 0pt;text-align: justify;">Keine Auftragsdatenverarbeitung im Sinne von
                                Art. 28 DS-GVO ohne entsprechende Weisung des Auftraggebers, z.B.: Eindeutige Vertragsgestaltung,
                                formalisiertes
                                Auftragsmanagement, strenge Auswahl des Dienstleisters, Vorabüberzeugungs-pflicht, Nachkontrollen.</p>
                        </div>
                    </body>
                    
                    </html>`,
                        data: {
                            users: users,
                        },
                        path: __dirname + '../../../../../uploads/adv_46229.pdf',
                        type: "",
                    };
                    sendPdf(document, options).then((sentdata) => {
                        if (sentdata == 'error') {
                            done();
                        } else {
                            let path = authUserId + '/' + arrData[0].Contract.CustomerId + '/' + arrData[0].Id + `/adv_46229.pdf`;
                            AWSHandler.uploadDocuments(sentdata.filename, path).then(function (result) {
                                s3URLofDoc = result.Location;
                                done();
                            }).catch(function (err) {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    })

                } else {
                    done();
                }
            },
            function (done) {
                ordermodel.checkUserDocument(authUserId).then((results) => {
                    if (results.length > 0) {
                        ordermodel.deleteExistingUserDocument(authUserId).then((deleted) => {
                            done();
                        })
                    }
                    else {
                        done();
                    }
                })
            },
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];
                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        ordermodel.orderUserCreate(arrData[0], decoded.data.userId, isClubAdministration, s3URLofDoc, loginInfo).then((usered) => {
                            if (usered.affectedRows > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        })
                    }
                })
            },
            function (done) {
                ordermodel.orderCreate(arrData[0]).then((ordered) => {
                    if (ordered.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get order details by users status
    * @author  MangoIt Solutions
    * @param   {user_id} with status
    * @return  {object}  order details.
    */
    getOrderByUserStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let customer;
        async.waterfall([
            function (done) {
                ordermodel.getCustomerId(req.params.user_id).then((customered) => {
                    if (customered.length > 0) {
                        customer = customered[0].customer_id
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders?search=' + customer + '&status=' + req.body.status,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
        ])
    }

    /**
    * Function to get order status
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {}  order status.
    */
    getOrdersStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let statusArray = ['InProgress', 'PaymentPending', 'Aborted', 'Completed', 'Failed', 'Expired', 'ApprovalPending', 'Approved', 'Declined', 'Incomplete']
        async.waterfall([
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: statusArray } });
                res.status(200).json(response);
            },
        ])
    }

    /**
    * Function to get order by status
    * @author  MangoIt Solutions
    * @param   {object} with status 
    * @return  {object}  order details.
    */
    getOrdersByStatus(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders?status=' + req.body.status,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
        ])
    }

    /**
    * Function to get Inprogress orders
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  order details.
    */
    getOrdersInProgress(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let allOrders;
        let other = [];
        let completed = [];
        var arr = [];

        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        allOrders = response.data;
                        done();

                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                for (let i = 0; i < allOrders.length; i++) {
                    if (allOrders[i].Status == 'Completed') {
                        completed.push(allOrders[i]);
                        if (allOrders.length == i + 1) {
                            arr['inProgressOrders'] = other;
                            arr['completedOrders'] = completed;
                            done();
                        }
                    } else {
                        other.push(allOrders[i]);
                        if (allOrders.length == i + 1) {
                            arr['inProgressOrders'] = other;
                            arr['completedOrders'] = completed;
                            done();
                        }
                    }
                }
            },
            function (done) {

                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'incompleteOrders': arr.inProgressOrders }, { 'completedOrders': arr.completedOrders }] } });
                res.status(200).json(response);
            }
        ])
    }


    /**
    * Function to get Licence details by customer
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object}  Licence details
    */
    getLicencesByCustomerId(req, res, next) {
        let customer;
        let userHistroy = [];
        let userLicences = [];
        let customerData;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                ordermodel.getCustomerId(req.params.id).then((customered) => {
                    if (customered.length > 0) {
                        customer = customered[0].customer_id
                        customerData = customered.filter(o => o.type == 1 || o.type == 3 || o.type == 4);
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [] } });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders?search=' + customer,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        userHistroy = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                userLicences = userHistroy.filter(entry1 => customerData.some(entry2 => entry1.Id === entry2.order_id));
                for (let i = 0; i < userLicences.length; i++) {
                    var config = {
                        method: 'get',
                        url: configContainer.billwerkAccountUrl + 'contracts/' + userLicences[i].Contract.Id,
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        }
                    };
                    axios(config)
                        .then(function (response) {
                            userLicences[i].Contract['EndDateForStatus'] = response.data.EndDate ? response.data.EndDate : ''
                            if (userLicences.length == i + 1) {
                                done();
                            }
                        })
                        .catch(function (error) {
                            if (userLicences.length == i + 1) {

                                if (error.isAxiosError == true && error.response.data.Message) {
                                    response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                                    res.status(200).json(response);
                                } else {

                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                    return;
                                }
                            }
                        });
                }
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userLicences } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get order details by customer
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object}  order details
    */
    getOrdersByCustomerId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let customer;
        let customer_details;
        async.waterfall([
            function (done) {
                ordermodel.getCustomerId(req.params.id).then((customered) => {
                    if (customered.length > 0) {
                        customer_details = customered;
                        customer = customered[0].customer_id
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [] } });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders?search=' + customer,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        for (let i = 0; i < (response.data).length; i++) {
                            if(customer_details.find(o => o.order_id == response.data[i].Id) === undefined){
                                response.data[i]['type'] = 5;
                            }
                            else{
                                response.data[i]['type'] = customer_details.find(o => o.order_id == response.data[i].Id).type
                            }
                        }
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error && error.isAxiosError == true && error.response && error.response.data && error.response.data.Message) {
                            //  if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }

        ])
    }

    /**
    * Function to get order details by order Id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object}  order details
    */
    getOrdersByOrderId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [response.data] } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get order details by page
    * @author  MangoIt Solutions
    * @param   {pagesize,page} 
    * @return  {object}  order details
    */
    getOrdersByPage(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let take = req.params.pagesize ? req.params.pagesize : 10
        let page = req.params.page ? req.params.page : 1
        let skip = (take * page) - take;
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders?skip=' + skip + '&&take=' + take + '',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get order 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object}  order details
    */
    getOrders(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'orders/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get Billwerk mandate pdf document 
    * @author  MangoIt Solutions
    * @param   {uid} 
    * @return  {file}  pdf file
    */
    getBillwerkMandatePdf(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let mandateData;
        let customerInfo;
        let customerId;
        let contractInfo;
        async.waterfall([
            function (done) {
                ordermodel.getUserMandate(req.params.uid).then((mandate) => {
                    if (mandate.length > 0 && JSON.parse(mandate[0].commit_response).PaymentBearer.MandateReference) {
                        mandateData = JSON.parse(mandate[0].commit_response);
                        customerId = mandate[0].customer_id
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:MandateNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + customerId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        customerInfo = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + customerId + '/contracts',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        contractInfo = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                if (contractInfo.length > 0 && contractInfo[0].PaymentBearer) {
                    var options = {
                        format: "A3",
                        orientation: "portrait",
                        border: "10mm",
                        header: {
                            height: "25mm",
                            contents: '<div style="text-align: center;"> SEPA Mandate </div>'
                        },
                    };
                    var users = [
                        {
                            CustomerId: mandateData.CustomerId,
                            CustomerName: mandateData.CustomerName,
                        }

                    ];
                    var document = {
                        html: `<!DOCTYPE html>
                    <html>
                      <head>
                        <meta charset="utf-8" />
                        <title>SEPA MANDATE!</title>
                      </head>
                      <body>
                      <table>
                       <tr>
                          <td style="color:#000; font-size:15px; font-weight:600;">„Ich/Wir  </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.FirstName + ` ` + customerInfo.LastName + ` </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.CompanyName + `</td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.EmailAddress + ` </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; "> `+ customerInfo.Address.Street + ` </td>
                       </tr>

                       <tr> 
                          <td style="color:#333; font-size:13px; "> `+ customerInfo.Address.PostalCode + ` ` + customerInfo.Address.City + ` </td>
                       </tr>

                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.Address.Country + `  </td>
                       </tr>
                       <tr>
                        <td style="color:#333; font-size:13px; "> `+ contractInfo[0].PaymentBearer.MandateText + `  </td>
                     </tr>
                        <tr>
                        <td style="color:#333; font-size:13px; "> MandateSignatureDate:  `+ contractInfo[0].PaymentBearer.MandateSignatureDate + ` </td>
                     </tr>
                        <tr>
                           <td style="color:#333; font-size:13px; "> MandateReference:  `+ contractInfo[0].PaymentBearer.MandateReference + `“      </td>
                        </tr>
                       </table>
                      
                         </div>
                      </body>
                    </html>`,
                        data: {
                            users: users,
                        },
                        path: __dirname + '../../../../../uploads/output.pdf',
                        type: "",
                    };
                    sendPdf(document, options).then((sentdata) => {
                        if (sentdata == 'error') {
                            response = responseFormat.getResponseMessageByCodes(['order:pdfSentFail'], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            let path = req.params.uid + '/' + mandateData.CustomerId + `/output.pdf`;
                            AWSHandler.uploadDocuments(sentdata.filename, path).then(function (result) {
                                let s3URLofDoc = result.Location;
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'filename': s3URLofDoc }] } });
                                res.status(200).json(response);
                            }).catch(function (err) {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })

                        }
                    })
                } else {

                }
            }
        ])
    }

    /**
    * Function to get  mandate pdf document 
    * @author  MangoIt Solutions
    * @param   {uid} 
    * @return  {file}  pdf file path
    */
    getMandatePdf(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let mandateData;
        let customerInfo;
        let customerId;
        async.waterfall([
            function (done) {
                ordermodel.getUserMandate(req.params.uid).then((mandate) => {
                    if (mandate.length > 0 && JSON.parse(mandate[0].commit_response).PaymentBearer.MandateReference) {
                        mandateData = JSON.parse(mandate[0].commit_response);
                        customerId = mandate[0].customer_id
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:MandateNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + customerId,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        customerInfo = response.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var options = {
                    format: "A3",
                    orientation: "portrait",
                    border: "10mm",
                    header: {
                        height: "25mm",
                        contents: '<div style="text-align: center;"> SEPA Mandate </div>'
                    },

                };
                var users = [
                    {
                        CustomerId: mandateData.CustomerId,
                        CustomerName: mandateData.CustomerName,
                    }

                ];
                var document = {
                    html: `<!DOCTYPE html>
                    <html>
                      <head>
                        <meta charset="utf-8" />
                        <title>SEPA MANDATE!</title>
                      </head>
                      <body>
                      <table>
                       <tr>
                          <td style="color:#000; font-size:15px; font-weight:600;">„Ich/Wir  </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.FirstName + ` ` + customerInfo.LastName + ` </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.CompanyName + `</td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.EmailAddress + ` </td>
                       </tr>
                       <tr> 
                          <td style="color:#333; font-size:13px; "> `+ customerInfo.Address.Street + ` </td>
                       </tr>

                       <tr> 
                          <td style="color:#333; font-size:13px; "> `+ customerInfo.Address.PostalCode + ` ` + customerInfo.Address.City + ` </td>
                       </tr>

                       <tr> 
                          <td style="color:#333; font-size:13px; ">`+ customerInfo.Address.Country + `  </td>
                       </tr>
                       <tr>
                           <td style="color:#000; font-size:13px; ">ermächtige/ ermächtigen <br/>
                           (A) {Merchant.CompanyName} Zahlungen von meinem/ unserem Konto <br/>
                            </td>
                       </tr>
                       <tr> 
                       <td style="color:#333; font-size:13px; ">{Pay.BIC}  </td>
                    </tr>
                       <tr>
                           <td style="color:#333; font-size:13px; ">   `+ mandateData.PaymentBearer.IBAN + `  </td>
                       </tr>

                       <tr>
                           <td style="color:#333; font-size:13px; ">  `+ mandateData.PaymentBearer.Holder + `    </td>
                       </tr>
                       <tr>
                           <td style="color:#333; font-size:13px;">
                           mittels Lastschrift einzuziehen. Zugleich <br/>
                           (B) weise ich mein/ weisen wir unser Kreditinstitut an, die von {Merchant.CompanyName} auf 
                           mein/ unser Konto  
                           </td>
                       </tr>
                       <tr> 
                       <td style="color:#333; font-size:13px; ">{Pay.BIC}  </td>
                    </tr>
                       <tr>
                           <td style="color:#333; font-size:13px; ">   `+ mandateData.PaymentBearer.IBAN + `   </td>
                       </tr>

                        <tr>
                            <td style="color:#333; font-size:13px; ">  `+ mandateData.PaymentBearer.Holder + `      </td>
                        </tr>
                        <tr>
                            <td style="color:#333; font-size:13px; ">
                            gezogenen Lastschriften einzulösen. <br/>
                            Hinweis: Ich kann/ Wir können innerhalb von acht Wochen, beginnend mit dem 
                            Belastungsdatum, die Erstattung des belasteten Betrages verlangen. Es gelten dabei die mit 
                            meinem/ unserem Kreditinstitut vereinbarten Bedingungen. 
                           
                            </td>
                        </tr>
                        <tr>
                        <td style="color:#333; font-size:13px; "> {CurrentDay} </td>
                     </tr>
                        <tr>
                           <td style="color:#333; font-size:13px; ">  `+ mandateData.PaymentBearer.MandateReference + `“      </td>
                        </tr>
                       </table>
                      
                         </div>
                      </body>
                    </html>`,
                    data: {
                        users: users,
                    },
                    path: __dirname + '../../../../../uploads/output.pdf',
                    type: "",
                };
                sendPdf(document, options).then((sentdata) => {
                    if (sentdata == 'error') {
                        response = responseFormat.getResponseMessageByCodes(['order:pdfSentFail'], { code: 417 });
                        res.status(200).json(response);
                    } else {
                        let resPath = sentdata.filename.split('/uploads/output.pdf')[0].split('/').reverse()[0] + '/uploads/output.pdf';
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'filename': resPath }] } });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

     /**
    * Function to order send for component product
    * @author  MangoIt Solutions
    * @param   {object} with component product order details
    * @return  {object}  order details.
    */
     componentOrderSend(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let arrData = [];
        let objData;
        let authUserId;
        let date = new Date().toJSON().split('T');

        async.waterfall([
            function (done) {
                let at = req.headers.authorizationToken;
                let accessToken = new Buffer.from(at, 'base64').toString('ascii');
                let authToken = accessToken.split(':')[1];

                jwt.verify(authToken, configContainer.jwtSecretKey, function (err, decoded) {
                    if (err) {
                        response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                        res.status(401).json(response);
                        return;
                    } else {
                        authUserId = decoded.data.userId;
                        ordermodel.componentProductOrderCheck(decoded.data.userId,req.body.type).then((found) => {
                            if (found.length > 0) {
                                response = responseFormat.getResponseMessageByCodes(['order:alreadyOrderdVersion'], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                done();
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                })
            },
            function (done) {
                objData = 
                {
                    "TriggerInterimBilling": true,
                    "ContractId": req.body.ContractId,
                    "Cart": {
                        "InheritStartDate": false,                                                                                                      
                        "ComponentSubscriptions": [
                            {
                                "ComponentId": req.body.ComponentId,
                                "Quantity": req.body.Quantity
                            }
                        ]
                    },
                    "PreviewAfterTrial": false
                }
                if(objData){
                    done();
                }
                else {
                    response = responseFormat.getResponseMessageByCodes(['order:Object datanot exist'], { code: 417 });
                        res.status(200).json(response);  
                }
            } ,
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data: objData,
                };
                axios(config)
                    .then(function (resPonse) {
                        arrData[0] = resPonse.data;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
              let isComponentprodut ;
              if(arrData[0].CustomFields.type){
                isComponentprodut = arrData[0].CustomFields.type;
              }
              else {
                isComponentprodut = req.body.type;
              }
                ordermodel.componentProductOrderUserCreate(arrData[0],authUserId,isComponentprodut,req).then((ordered) => {
                    if (ordered.affectedRows > 0) {
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
           
            function (done) {
                ordermodel.componentProductOrderCreate(arrData[0],authUserId,req).then((ordered) => {
                    if (ordered.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: arrData } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

     /**
    * Function to commit the component product order
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  order details
    */
     componentOrderCommit(req, res, next) {
        var a1 = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        // let date = new Date().toJSON().split('T');
       
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'orders/' + req.params.id + '/commit',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    },
                    data : req.body
                };
                axios(config)
                    .then(function (resPonse) {
                        a1[0] = resPonse.data;
                        done();
                        // response = responseFormat.getResponseMessageByCodes('', { content: { dataList: resPonse.data } });
                        // res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                ordermodel.commitStoreComponentProduct(a1[0], req.params.id).then((stored) => {
                    if (stored.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['order:orderFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },

        
        ])
    }

}
