exports.up = async function(knex) {
    return knex.schema.createTable('field_data_field_bank_blz', function(t) {
        t.string('entity_type',[128]).notNull().comment('The entity type this data is attached to');
        t.increments('entity_id').comment('	The entity type this data is attached to');
        t.string('field_bank_blz_value',[8]).defaultTo('NULL');
        t.string('field_bank_blz_format',[255]).defaultTo('NULL');
      });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('field_data_field_bank_blz');
};