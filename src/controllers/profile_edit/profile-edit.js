import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import async from 'async';
import ProfileEditModel from '../../models/profile_edit/profile-edit';
import UsersRolesModel from '../../models/user_role/user_role';
import ProfileEditValidation from '../../validators/profile_edit/profile-edit';
import responseFormat from '../../core/response-format';
import UserModel from '../../models/user/user';
import configContainer from '../../config/localhost';
import AWSHandler from '../../core/AWSHander';
let userModel = new UserModel();
let usersrolesmodel = new UsersRolesModel();
let profileEditModel = new ProfileEditModel();
let profileEditvalidate = new ProfileEditValidation();
export default class ProfileEditController {

    /**
    * Function to update profile details
    * @author  MangoIt Solutions
    * @param   {id} with updated profile details
    * @return  {message} success message
    */
    editProfile(req, res, next) {
        let file;
        let s3URLofImage;
        let accDetails;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = profileEditvalidate.profileEditValidation(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                        done();
                    }
                    else {
                        done();
                    }
                },
                function (done) {
                    profileEditModel.getProfileUpdate(req, s3URLofImage).then((updatedProfile) => {
                        if (updatedProfile.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['Signup:userUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.body.password && req.body.password != null && req.body.password != "") {
                        let hashPass;
                        bcrypt.hash(req.body.password, 10, function (err, hash) {
                            hashPass = hash;
                            userModel.changeUserPassword(req.params.uid, hashPass).then((emailVerify) => {
                                if (emailVerify.affectedRows > 0) {
                                    done();
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['Signup:passwordChangedFail'], { code: 417 });
                                    res.status(200).json(response)
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        })
                    } else {
                        done();
                    }
                },
                function (done) {
                    let typeHead = req.headers.authorization || req.headers.Authorization;
                    let authHead = typeHead.split(' ')[1];
                    jwt.verify(authHead, configContainer.jwtSecretKey, function (err, decoded) {
                        if (decoded) {
                            userModel.getRoleDetails(decoded.data.userId).then((resUser) => {
                                let adminRoleType = resUser.filter(o => o.name == 'Administrator');
                                if (adminRoleType.length > 0 || decoded.data.roleId == 1) {
                                    if (JSON.parse(req.body.userRole) && JSON.parse(req.body.userRole).length > 0) {
                                        usersrolesmodel.getDeleteRoleByUid(req.params.uid).then((resDel) => {
                                            for (let index = 0; index < JSON.parse(req.body.userRole).length; index++) {
                                                const element = JSON.parse(req.body.userRole)[index];
                                                userModel.registerUserRole(req.params.uid, element).then((resData) => {
                                                    if (resData.affectedRows > 0) {
                                                        if (index + 1 == JSON.parse(req.body.userRole).length) {
                                                            done();
                                                        }
                                                    }
                                                    else {
                                                        response = responseFormat.getResponseMessageByCodes(['userAdd:registeredRoleFail'], { code: 417 });
                                                        res.status(200).json(response);
                                                    }
                                                }).catch((error) => {
                                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                                    res.status(500).json(response);
                                                    return;
                                                })
                                            }
                                        })
                                    } else {
                                        response = responseFormat.getResponseMessageByCodes(['userAdd:roleReq'], { code: 417 });
                                        res.status(200).json(response);
                                    }
                                } else {
                                    done();
                                }
                            })
                        }
                        else {
                            response = responseFormat.getResponseMessageByCodes(['common401'], { code: 401 });
                            res.status(401).json(response);
                            return;
                        }
                    })
                },
                function (done) {
                    profileEditModel.getAddressById(req.params.uid).then((addressData) => {
                        if (addressData.length > 0) {
                            profileEditModel.AddressUpdate(req).then((addressUpdated) => {
                                if (addressUpdated.affectedRows > 0) {
                                    done();
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['Signup:userAddressUpdationFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        } else {
                            profileEditModel.getAddressUpdate(req).then((addressUpdated) => {
                                if (addressUpdated.affectedRows > 0) {
                                    done();
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['Signup:userAddressUpdationFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    profileEditModel.getAccountById(req.params.uid).then((accountData) => {
                        accDetails = accountData;
                        done();
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (accDetails.length > 0) {
                        profileEditModel.AccountUpdate(req).then((accountUpdated) => {
                            if (accountUpdated && accountUpdated.affectedRows > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['Signup:userAccountUpdationFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        })
                    } else {
                        profileEditModel.getAccountUpdate(req).then((accountUpda) => {
                            if (accountUpda && accountUpda.affectedRows > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['Signup:userAccountUpdationFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['Signup:userUpdationSuccess']);
                    res.status(200).json(response);
                }
            ])
        }
    }
}