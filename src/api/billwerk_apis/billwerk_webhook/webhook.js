import express   from 'express';
import BillwerkWebhookController from '../../../controllers/billwerk_controllers/billwerk_webhook/webhook';

let router = express.Router();
let billwerkWebhookController = new BillwerkWebhookController();
router.use(function (req, res, next) {
	next();
})
router.use('/api/billwerk-payment-escalated',billwerkWebhookController.paymentEscalatedBilling.bind(billwerkWebhookController))

export default router;