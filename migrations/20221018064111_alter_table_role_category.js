exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('role_category', function(t) {
        t.timestamp('created_at').nullable().alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('role_category', function(t) {
        
    });
 };