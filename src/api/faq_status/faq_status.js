import express from 'express';
import FaqStatusController from '../../controllers/faq_status/faq_status';

let router = express.Router();
let faqStatusController  = new FaqStatusController ;

router.post('/faqStatuscreate', faqStatusController.createFaqStatus.bind(faqStatusController));
router.get('/faqStatus', faqStatusController.getAllFaqStatus.bind(faqStatusController));
router.get('/faqStatus/:sid', faqStatusController.getFaqStatusbyId.bind(faqStatusController));
router.put('/faqStatusUpdate/:sid', faqStatusController.updateFaqStatus.bind(faqStatusController));
router.delete('/Statusdelete/:sid', faqStatusController.deleteStatus.bind(faqStatusController));

export default router;