exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('email_template', function(t) {
        t.longtext('logo').alter();
        t.longtext('url').alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('email_template', function(t) {
       
    });
 };