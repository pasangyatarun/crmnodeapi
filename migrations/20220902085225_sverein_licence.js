exports.up = async function(knex) {
    return knex.schema.createTable('sverein_licence', function(t) {
        t.increments('lid ').primary().comment('Primary key; a unique id for the licence.');
        t.integer('cuid ',[11]).notNull().defaultTo(0).comment('User ID (customer).');
        t.integer('pid ',[11]).notNull().defaultTo(0).comment('Product ID.');
        t.text('data').notNull().comment('Serialized field with data provided during registration of this licence.');
        t.string('domain',[100]).comment('Domain name (for homepage and portal).');
        t.text('publish_to').notNull();
        t.string('title',[64]).notNull();
        t.tinyint('state',[3]).notNull().defaultTo(1).comment('	Status of the licence: (1) testmode and active (2) active (3) terminated and still active (11) inactive (12) archived');
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('sverein_licence');
};