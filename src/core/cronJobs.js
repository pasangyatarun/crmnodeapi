import express from 'express';
import cron from 'node-cron';
import sendEmail from '../core/nodemailer';

import UserController from '../controllers/users/users';
import BillwerkCustomerController  from '../controllers/billwerk_controllers/billwerk_customer/customer';
import ClubController from '../controllers/club/club'

let userController = new UserController();
let billwerkCustomerController = new BillwerkCustomerController();
let clubController = new ClubController(); 
let app = express.Router();
app.use(function (req, res, next) { 
  cron.schedule('* * * * *', () => {
    console.log('running every minute');
  });
  next();
});
export default app;



  cron.schedule('0 0 * * *', () => {
    console.log('running for support user delete after 7 days');
    clubController.supportUserDeleteAfterSevenDays();
  });

  cron.schedule('0 0 * * *', function() {
    console.log('running a task every minute!!!!');
    userController.getUserDetils();
    console.log('usercontroller cron job run');
  });



  /// billwerk customrs import into db
  cron.schedule('* * * * *', () => {
    console.log('running cronJob for billwerk customer import in database tables');
    billwerkCustomerController.getBillwerkCustomer();
  });

  //export default cron ;