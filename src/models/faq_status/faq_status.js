import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class FaqStatusModel {

    async AllFaqStatus() {
        const rows = await db.query("SELECT * from faq_status");
        return rows[0];
    }
 
    async CreateFaqStatus(req) {
        const rows = await db.query('INSERT INTO `faq_status`(`status`)VALUES(?)', [
            req.body.status
        ]);
        return rows[0];
    }
   
    async getFaqStatusByName(status) {
        const rows = await db.query("SELECT * from faq_status where `status`= ?",[status]);
        return rows[0];
    }
    
    async checkFaqStatusByName(req){
        const rows = await db.query("SELECT * FROM faq_status WHERE status = ? AND sid != ?",[req.body.status,req.params.sid]);
        return rows[0];
    }
    
    async getFaqStatusbyId(sid) {
        const rows = await db.query("SELECT * FROM `faq_status` WHERE sid = ?", [sid]);
        return rows[0];
    }

    async checkFaqStatusSelected(sid) {
        const rows = await db.query("SELECT * FROM `tickets_requests` WHERE status = ?", [sid]);
        return rows[0];
    }

    async deleteStatus(sid) {
        const rows = await db.query("DELETE FROM `faq_status` WHERE `sid`=?", [sid]);
        return rows[0];
    }

    async getUpdateFAQStatus(req) {
        const rows = await db.query("UPDATE faq_status SET status = ? WHERE sid = ?", [
            req.body.status,
            req.params.sid
        ]);
        return rows[0];
    }
}