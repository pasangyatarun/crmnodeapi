export default class CrmProductValidation {
    crmproductCreate(productData){
        let err = new Array();
        if(!productData.product_name){
            err.push('product_name: Product Name Required');
        }
        if(!productData.price ){
            err.push('price:Product Price Required');
        } 
        if(!productData.description ){
            err.push('description:Product Description Required');
        }        
        return err;
    }
}