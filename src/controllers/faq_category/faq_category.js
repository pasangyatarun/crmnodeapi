import responseFormat from '../../core/response-format';
import FaqCategoryModel from '../../models/faq_category/faq_category';
import CrmFaqCategoryValidation from '../../validators/faq_category/faq_category';
import async from 'async';

let faqCategoryModel = new FaqCategoryModel();
let crmFaqCategoryValidation = new CrmFaqCategoryValidation();

export default class FaqCategoryController {

    /**
    * Function to create faq category
    * @author  MangoIt Solutions
    * @param   {object} with faqCategory details
    * @return  {message} success message
    */
    createFaqCategory(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmFaqCategoryValidation.crmFaqCategoryCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    faqCategoryModel.getFaqCategoryByName(req.body.category).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Category:categoryExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    faqCategoryModel.CreateFaqCategory(req).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['category:categorySucess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['category:categoryCreationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to get faq categories
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} Faq category details
    */
    getAllFaq(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqCategoryModel.AllFaqCategory().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['faq:faqNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get faq categories by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} Faq category details
    */
    getFaqbyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        faqCategoryModel.FaqById(req.params.id).then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['faq:faqNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update faq categories by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    updateFaq(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = crmFaqCategoryValidation.crmFaqCategoryCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    faqCategoryModel.checkFaqCategoryByName(req).then((results) => {
                        if (results.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['Category:categoryExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    faqCategoryModel.getUpdateFAQ(req).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['category:categoryUpdate']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['category:categoryUpdationFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
    * Function to delete faq categories by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    deleteFaqCategory(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                faqCategoryModel.checkFaqQuestion(req.params.cat_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['Category:categoryquestionExist'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                faqCategoryModel.deleteCategory(req.params.cat_id).then((result) => {
                    if (result.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['category:categoryDelete']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['category:categoryDeleteionFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }
}



