exports.up = async function(knex) {
    return knex.schema.createTable('role_permission', function(t) {
        t.increments('id').primary();
        t.integer('rid',[10]).notNull();
        t.string('permission',[128]).notNull().comment('A single permission granted to the role identified by rid.');
        t.string('module',[255]).notNull().comment('The module declaring the permission');
         })
         .then(() =>
         knex("role_permission").insert([
        { rid:2 ,permission: "VIEW",module :"customerAssign"},
        { rid:3 ,permission: "ADD",module :"product"},
        { rid:3,permission: "VIEW",module :"product"},
        { rid:3 ,permission: "ADD",module :"customerAssign"},
        { rid:3 ,permission: "VIEW",module :"customerAssign"},
        { rid:4 ,permission: "ADD",module :"customerAssign"},
        { rid:4 ,permission: "VIEW",module :"customerAssign"},
        { rid:6,permission: "ADD",module :"customerAssign"},
        { rid:6 ,permission: "VIEW",module :"customerAssign"},
        { rid:6 ,permission: "VIEW",module :"user"},
        { rid:6,permission: "ADD",module :"product"},
        { rid:6,permission: "VIEW",module :"product"},
        { rid:6,permission: "EDIT",module :"product"},
        { rid:2,permission: "ADD",module :"productAssign"},
        { rid:2,permission: "VIEW",module :"productAssign"},
        { rid:2 ,permission: "ADD",module :"user"},
        { rid:2 ,permission: "VIEW",module :"user"},
        { rid:2 ,permission: "EDIT",module :"user"},
        { rid:2 ,permission: "DELETE",module :"user"},
        { rid:2 ,permission: "ADD",module :"staticWebPage"},
        { rid:2 ,permission: "VIEW",module :"staticWebPage"},
        { rid:1 ,permission: "ADD",module :"role"},
        { rid:1 ,permission: "VIEW",module :"role"},
        { rid:1 ,permission: "EDIT",module :"role"},
        { rid:1 ,permission: "DELETE",module :"role"},
        { rid:2 ,permission: "VIEW",module :"news"},
        { rid:3 ,permission: "VIEW",module :"news"},
        { rid:1 ,permission: "ADD",module :"staticWebPage"},
        { rid:1 ,permission: "VIEW",module :"staticWebPage"},
        { rid:1,permission: "ADD",module :"customerAssign"},
        { rid:2 ,permission: "ADD",module :"role"},
        { rid:2 ,permission: "VIEW",module :"role"},
        { rid:2 ,permission: "EDIT",module :"role"},
        { rid:2 ,permission: "DELETE",module :"role"},
        { rid:29 ,permission: "ADD",module :"user"},
        { rid:29 ,permission: "VIEW",module :"user"},
        { rid:29,permission: "EDIT",module :"user"},
        { rid:29,permission: "DELETE",module :"user"},
        { rid:3 ,permission: "ADD",module :"user"},
        { rid:3 ,permission: "VIEW",module :"user"},
        { rid:3 ,permission: "EDIT",module :"user"},
        { rid:3 ,permission: "DELETE",module :"user"},
        { rid:4 ,permission: "ADD",module :"user"},
        { rid:4 ,permission: "VIEW",module :"user"},
        { rid:4 ,permission: "EDIT",module :"user"},
        { rid:6 ,permission: "VIEW",module :"clubs"},
        { rid:8 ,permission: "ADD",module :"news"},
        { rid:8 ,permission: "VIEW",module :"news"},
        { rid:1 ,permission: "ADD",module :"user"},

         ])
     )
};

exports.down = async function(knex) {
    await knex.schema.dropTable('role_permission');
};
