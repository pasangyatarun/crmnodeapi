import CommonMethods from '../../core/common-methods.js';
let commonMethods = new CommonMethods();

export default class ProfileEditValidation {

    profileEditValidation(profileData) {
        let err = new Array();

        if (!profileData.email || !profileData.email.trim() || (!commonMethods.validateEmailid(profileData.email))) {
            err.push('email:email');
        }      
        return err;
    }
}