exports.up = async function(knex) {
    return knex.schema.createTable('licence_invoice', function(t) {
        t.increments('id').primary();
        t.string('invoice_number',[264]).notNull();
        t.string('invoice_date',[264]).notNull();
        t.integer('invoice_net_amount').notNull();
        t.integer('invoice_tax_amount').notNull();
        t.integer('invoice_gross_amount').notNull();
        t.string('invoice_period',[264]).notNull();
        t.integer('licence_history_id').notNull();
        t.integer('user_id').notNull();

         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('licence_invoice');
};
    