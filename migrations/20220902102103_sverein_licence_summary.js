exports.up = async function(knex) {
    return knex.schema.createTable('sverein_licence_summary', function(t) {
        t.increments('id').primary().comment('Unique table id');
        t.integer('suid',[11]).notNull().defaultTo(0).comment('Salespartner UID');
        t.integer('cuid',[11]).notNull().defaultTo(0).comment('Customer UID');
        t.string('updated',[254]).notNull().defaultTo(0).comment('The UNIX timestamp of last update of this record');
        t.integer('since',[11]).notNull().defaultTo(0).comment('The UNIX timestamp since when the salespartner signed the SFP agreement');
        t.string('installed',[254]).notNull().defaultTo(0).comment('The UNIX timestamp when the licence was installed');
        t.integer('ordered').notNull().defaultTo(0).comment('The UNIX timestamp when the licence was ordered');
        t.integer('pid',[11]).notNull().defaultTo(0).comment('Product ID');
        t.string('mainlid',[11]).notNull().defaultTo(0).comment('Main licence ID');
        t.string('price',[11]).notNull().defaultTo(0).comment('Price in Cent');
        t.integer('state',[11]).notNull().defaultTo(0).comment('Licence state');
        t.string('customername',[255]).notNull().comment('Name of customer club');
        t.string('salespartnername',[255]).notNull().comment('Name of sales partner');
        t.string('postcode',[255]).notNull().comment('Postal code customer club');
        t.string('city',[255]).notNull().comment('City customer club');
        t.integer('sublid',[11]).defaultTo(0).comment('Licence ID of addon if applicable');
        t.tinyint('commissionable',[3]).notNull().defaultTo(0).comment('1 if this licence entitles the sales partner (Vermittler-Spk) to a commission, 0 otherwise');
        t.integer('invoiceable',[10]).defaultTo(0).comment('The UNIX timestamp when the licence was made invoiceable');
        t.integer('cancelled',[10]).defaultTo(0).comment('The UNIX timestamp when the licence was made cancelled/terminated	');
        t.tinyint('since2019',[3]).notNull().defaultTo(0).comment('	1 if this licence was created within the new 2019 SFP deal, 0 for old licences');
        t.integer('commissionsp',[11]).notNull().defaultTo(0).comment('Commission value for salespartner in Cent');
        t.integer('commissionsfp',[11]).notNull().defaultTo(0).comment('Commission value for SFP in Cent');
        
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('sverein_licence_summary');
};

