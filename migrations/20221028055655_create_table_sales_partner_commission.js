exports.up = async function(knex) {
    return knex.schema.createTable('sales_partner_commission', function(t) {
        t.increments('id').primary();
        t.integer('sales_partner_id',[11]).notNull();
        t.string('product_id',[264]).notNull();
        t.string('commission_amount_type',[255]).notNull();
        t.integer('commission_amount',[11]).notNull();
        t.string('commission_duration_type',[255]).notNull();
        t.datetime('start_date').notNull();
        t.timestamp('created_at').notNull().defaultTo(knex.fn.now());
        t.datetime('modified_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));

    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('sales_partner_commission');
};
