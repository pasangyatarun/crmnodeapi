export default class RoleValidation {

    roleValidation(details, allowedExt) {
        let signupBody = {
            role: details.role,
            weight: details.weight,
        };
        let err = new Array();
        if(!signupBody.role){
            err.push('role:roleReq');
            }
        return err;
    }

}