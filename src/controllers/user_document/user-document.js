import async from 'async';
import UserDocumentModel from '../../models/user_document/user-document';
import responseFormat from '../../core/response-format';
import AWSHandler from '../../core/AWSHander';
import { dirname } from 'path';
import path from 'path';
import mime from 'mime';
import { fileURLToPath } from 'url';
import fs from 'fs';
import https from 'https';
import Path from 'path'

const __dirname = dirname(fileURLToPath(import.meta.url));

let userDocumentModel = new UserDocumentModel();

export default class UserDocumentController {


    /**
    * Function to  get user document by userId
    * @author  MangoIt Solutions
    * @param   {user_id}
    * @return  {object}user document
    */
    getUserDocumentByUserId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userDocumentsByUserId(req.params.user_id).then((userDoc) => {
                    if (userDoc.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDoc } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to  get user document by document name
    * @author  MangoIt Solutions
    * @param   {document_name}
    * @return  {object}user document details
    */
    getUserDocumentByName(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userDocumentByName(req.body.name).then((userDoc) => {
                    if (userDoc.length > 0) {
                        AWSHandler.getUploadedDocuments(userDoc[0].doc_name).then(async function (result) {
                            https.get(result, (resp) => {
                                let nm = userDoc[0].doc_name.split('/')[2]
                                try {
                                    if (!fs.existsSync(__dirname + '/../../../' + 'uploads/documents/')) {
                                        fs.mkdirSync(__dirname + '/../../../' + 'uploads/documents/');
                                    }
                                } catch (err) {
                                    console.log(err);
                                }
                                const path1 = __dirname + '/../../../' + 'uploads/documents/' + nm;
                                const writeStream = fs.createWriteStream(path1);
                                resp.pipe(writeStream);
                                writeStream.on('finish', function() {
                                    console.log("Download Completed!");
                                    var file = fs.createReadStream(path1);
                                    var stat = fs.statSync(path1);
                                    var filename = path.basename(path1);
                                    var mimetype = mime.lookup(path1);
                                    res.setHeader('Content-Length', stat.size);
                                    res.setHeader('Content-Type', mimetype);
                                    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
                                    file.pipe(res);
                                });
                            })
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }
    

    /**
    * Function to  get user document by Id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}user document details
    */
    getUserDocumentById(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userDocumentById(req.params.id).then((userDoc) => {
                    if (userDoc.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDoc } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to  delete user document by Id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    deleteDocument(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userDocumentsDelete(req.params.id).then((docDeleted) => {
                    if (docDeleted.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['upload:uploadDocDeleted']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get users all personalize documents
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} document details
    */
    getAllPersonalizeUserDocuments(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userspersonalizeDocuments().then((userDoc) => {
                    if (userDoc.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDoc } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get users document
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} document details
    */
    getUserDocument(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                userDocumentModel.userDocuments().then((userDoc) => {
                    if (userDoc.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userDoc } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to upload users document
    * @author  MangoIt Solutions
    * @param   {user_id}with document details
    * @return  {message} success message
    */
    uploadDocument(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let file;
        let s3URLofDoc;
        let accDetails;
        let filename;
        async.waterfall([
            function (done) {
                if (req.file) {
                    file = req.file;
                    let mimetype = file.mimetype.replace(/\//g, '-');
                    let path = `${req.params.uid}/${mimetype}/${file.originalname}`
                    filename = `${req.params.uid}/${mimetype}/${file.originalname}`
                    AWSHandler.uploadDocuments(file.path, path).then(function (result) {
                        s3URLofDoc = result.Location;
                        done();
                    }).catch(function (err) {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else if (req.body.docUrl) {
                    s3URLofDoc = req.body.docUrl;
                    filename = req.body.docName
                    done();
                }
            },
            function (done) {
                userDocumentModel.documentUpload(req, filename).then((uploaded) => {
                    if (uploaded.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentUpdated']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['upload:documentUpdationFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to delete users document by name
    * @author  MangoIt Solutions
    * @param   {object}with document name
    * @return  {message} success message
    */
    deleteDocumentFromUploadsByName(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                let nm = req.body.name.split('/')[2];
                const path = 'uploads/documents/' + nm;
                fs.stat(path, function (err, stats) {
                    if (err) {
                        console.error(err);
                        response = responseFormat.getResponseMessageByCodes(['Deleted:documentNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        fs.unlink(path, function (err) {
                            if (err) return console.log(err);
                            response = responseFormat.getResponseMessageByCodes(['Deletd:uploadDocDeleted']);
                            res.status(200).json(response);
                        });
                    }
                })
            }
        ])
    }

}