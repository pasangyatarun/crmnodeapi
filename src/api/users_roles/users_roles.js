import express   from 'express';
import UsersRolesController from '../../controllers/users_roles/users_roles.js'

let router = express.Router();
let usersRolesController = new UsersRolesController();

router.get('/usersroles', usersRolesController.allUsersRoles.bind(usersRolesController));
router.get('/rolebyuserid/:uid', usersRolesController.roleByuid.bind(usersRolesController));
router.get('/usersbyroleid/:rid', usersRolesController.usersByrid.bind(usersRolesController));
router.put('/update/users-roles/:id', usersRolesController.updateUsersRoles.bind(usersRolesController));
router.get('/verifyemail/:code/:dt', usersRolesController.updateRole.bind(usersRolesController));

export default router;