import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class RolesPermissionsModel {

    async deletePermissionsByIdModule(rid, module) {
        const rows = await db.conn.promise().query("DELETE FROM `role_permission` WHERE `rid`=? AND `module`=?", [rid, module]);
        return rows[0];
    }

    async getPermissionsByIdModule(req) {
        const permissions= JSON.stringify(req.body.permissions).replace('[', '').replace(']', '').trim()
        const rows = await db.conn.promise().query("SELECT role_permission.id,role.name as role, role_permission.rid,role_permission.permission,role_permission.module FROM role_permission LEFT JOIN role ON role.rid = role_permission.rid WHERE role_permission.rid = ? AND role_permission.module =? AND role_permission.permission  IN ("+ permissions +")", [req.body.roles, req.body.modules]);
        return rows[0];
    }

    async getRolePermissionById(rid) {
        const rows = await db.conn.promise().query("SELECT role_permission.id,role.name as role, role_permission.rid,role_permission.permission,role_permission.module FROM role_permission LEFT JOIN role ON role.rid = role_permission.rid WHERE role_permission.rid = ?", [rid]);
        return rows[0];
    }

    async getRolePermissionByModule(module) {
        const rows = await db.conn.promise().query("SELECT role_permission.id,role.name as role, role_permission.rid,role_permission.permission,role_permission.module FROM role_permission LEFT JOIN role ON role.rid = role_permission.rid WHERE role_permission.module = ?", [module]);
        return rows[0];
    }

    async createRolePermission(req, permission) {
        const rows = await db.conn.promise().query('INSERT INTO `role_permission`(`rid`,`permission`,`module`)VALUES(?,?,?)', [
            req.body.roles,
            permission,
            req.body.modules
        ]);
        return rows[0];
    }

    async getAllRolePermissions() {
        const rows = await db.conn.promise().query("SELECT role_permission.id,role.name as role, role_permission.rid,role_permission.permission,role_permission.module FROM role_permission LEFT JOIN role ON role.rid = role_permission.rid");
        return rows[0];
    }

    async deleteRolePermission(id) {
        const rows = await db.conn.promise().query("DELETE FROM `role_permission` WHERE `id`=?", [id]);
        return rows[0];
    }

    async deleteRolePermissionByModule(module) {
        const rows = await db.conn.promise().query("DELETE FROM `role_permission` WHERE `module`=?", [module]);
        return rows[0];
    }

}