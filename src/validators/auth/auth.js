import CommonMethods from '../../core/common-methods.js';
let commonMethods = new CommonMethods();

export default class AuthValidation {

    userRegistrationValidation(details) {
        let signupData = {
            email: details.email,
            confirmEmail: details.conEmail,
            society: details.society,
            firstName: details.firstName,
            lastName: details.lastName,
            address: details.address,
            zipcode: details.zipcode,
            place: details.place,
            country: details.country,
            contactNumber: details.contactNumber,
            productId: details.productId
        };

        let err = new Array();

        if (!signupData.email || !signupData.email.trim() || (!commonMethods.validateEmailid(signupData.email))) {
            err.push('email:email');
        } 
        if (!signupData.confirmEmail || !signupData.confirmEmail.trim() || signupData.confirmEmail != signupData.email) {
            err.push('confirmEmail:confirmEmail');
        }        
        if (!signupData.society) {
            err.push('society:societyName');
        }
        if (!signupData.firstName) {
            err.push('firstName:firstName');
        }
        if (!signupData.lastName) {
            err.push('lastName:surName');
        }
        if (!signupData.address) {
            err.push('address:addressLine');
        }
        if (!signupData.zipcode) {
            err.push('zipcode:zipcode');
        }
        if (!signupData.place) {
            err.push('place:placeReq');
        }
        return err;
    }

    passwordValidation(passData) {
        let err = new Array();
        if (!passData.password || (!commonMethods.passwordValidation(passData.password))) {
            err.push('password:password');
        }
        if (!passData.confirmPassword || !passData.confirmPassword.trim() || passData.confirmPassword != passData.password) {
            err.push('confirmPassword:confirmPassword');
        }
        return err;
    }

    signupNewValidation(details, allowedExt) {
        let signupData = {
            email: details.email,
            confirmEmail: details.conEmail,
            society: details.society,
        };
        let err = new Array();

        if (!signupData.email || !signupData.email.trim() || (!commonMethods.validateEmailid(signupData.email))) {
            err.push('email:email');
        }         
        if (!signupData.confirmEmail || !signupData.confirmEmail.trim() || signupData.confirmEmail != signupData.email) {
            err.push('confirmEmail:confirmEmail');
        }
        if (!signupData.society) {
            err.push('society:societyName');
        }
        return err;
    }

    signinValidation(details) {

        let signinBody = {
            email: details.email,
            password: details.password
        }
        let err = [];

        if (!signinBody.email || !signinBody.password) {
            err.push('Signin:invalidUserNamePassword');
        }
        return err;
    }

}