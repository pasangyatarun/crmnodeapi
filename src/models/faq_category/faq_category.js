import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class FaqCategoryModel {

    async AllFaqCategory() {
        const rows = await db.query("SELECT * from faq_category");
        return rows[0];
    }
 
    async CreateFaqCategory(req) {
        const rows = await db.query('INSERT INTO `faq_category`(`category`)VALUES(?)', [
            req.body.category
        ]);
        return rows[0];
    }
   
    async getFaqCategoryByName(category) {
        const rows = await db.query("SELECT * from faq_category where `category`= ?",[category]);
        return rows[0];
    }
    
    async checkFaqCategoryByName(req){
        const rows = await db.query("SELECT * FROM faq_category WHERE category = ? AND id != ?",[req.body.category,req.params.id]);
        return rows[0];
    }

    async FaqById(id) {
        const rows = await db.query("SELECT * FROM `faq_category` WHERE id = ?", [id]);
        return rows[0];
    }

    async checkFaqQuestion(catId) {
        const rows = await db.query("SELECT * FROM `faq_question_answer` WHERE category_id = ?", [catId]);
        return rows[0];
    }

    async deleteCategory(catId) {
        const rows = await db.query("DELETE FROM `faq_category` WHERE `id`=?", [catId]);
        return rows[0];
    }

    async getUpdateFAQ(req) {
        const rows = await db.query("UPDATE faq_category SET category = ? WHERE id = ?", [
            req.body.category,
            req.params.id
        ]);
        return rows[0];
    }
}