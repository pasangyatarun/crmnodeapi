import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class BillwerkCustomerModel {

    async checkEmailExists(email) {
        const rows = await db.query("SELECT mail from `users` WHERE mail=?", [email]);
        return rows[0];
    }

    async insertUser(data) {
        let dt = new Date(data.CreatedAt).getTime()
        const rows = await db.query("INSERT INTO `users`(`name`,`pass`,`mail`,`created`,`access`,`login`,`signature_format`,`status`,`language`,`init`)VALUES(?,?,?,?,?,?,?,?,?,?)", [
            data.EmailAddress,
           '$S$D8Y50CKl7ABGGwzjSf7kuzyx8sbEYLAEcjs2/mP3sbqQzL6T2Irm',
            data.EmailAddress,
            dt,
            dt,
            dt,
            'plain_text',
            1,
            data.Language||'',
            data.EmailAddress
        ]);
       return rows[0];
    }

    async insertUserRole(userid) {
        const roleSelect = await db.query("SELECT * FROM `role` WHERE name=?", ['Customer']);
        const rows = await db.query("INSERT INTO `users_roles`(`uid`,`rid`)VALUES(?,?)", [
            userid,
            roleSelect[0][0].rid,
        ]);
        return rows[0];
    }

    async insertUserAddress(customerData, userId) {
        if(customerData.Address.Country == 'DE'){
            customerData.Address.Country = 'Germany'
        }
        else if(customerData.Address.Country == 'CH'){
            customerData.Address.Country = 'Switzerland'
        }
        else if(customerData.Address.Country == 'AT'){
            customerData.Address.Country = 'Austria'
        }
        else{
            customerData.Address.Country = customerData.Address.Country
        }
        const rows = await db.query('INSERT INTO `field_data_field_address`(`entity_id`,`field_address_name_line`,`field_address_country`,`field_address_first_name`,`field_address_last_name`,`field_address_organisation_name`,`field_address_locality`,`field_address_postal_code`,`field_address_thoroughfare`,`field_address_second`,`entity_type`)VALUES(?,?,?,?,?,?,?,?,?,?,?)', [
            userId,
            customerData.FirstName + customerData.LastName,
            customerData.Address.Country,
            customerData.FirstName,
            customerData.LastName,
            customerData.CompanyName,
            customerData.Address.City,
            customerData.Address.PostalCode,
            customerData.Address.FormattedStreetAddressLine,
            customerData.Address.FormattedCityAddressLine,
            'user'
        ]);
        return rows[0];
    }
}