import express from 'express';
import responseFormat from './response-format';
import UserModel from './../models/user/user';
let userModel = new UserModel();
let app = express.Router();
app.use(function (req, res, next) { 
  let response = responseFormat.createResponseTemplate();
  userModel.getOneUser().then((results) => {
      if (results && results.length == 0) {
        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
        res.status(500).json(response);
        return;
      }
  }).catch((error) => {
  response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
  res.status(500).json(response);
  return;
  })
  next();
});
export default app;