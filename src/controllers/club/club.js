import async from 'async';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import ClubModel from '../../models/club/club';
import UserModel from '../../models/user/user';
import qs from 'qs';
import fetch from 'node-fetch';

let userModel = new UserModel();
let clubModel = new ClubModel();

export default class ClubController {
    constructor() {
    }

    /**
    * Function to get support user
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} details
    */
    getSupportUserPass(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                clubModel.getSupportUser(req.params.uid).then((saved) => {
                    if (saved.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: saved } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['club:supportUserNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            }
        ])
    }

    /**
    * Function to get support user
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} support user details
    */
    getSupportUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let templateData;
        let templateDataUser;
        let mailOptions;
        let mailOptionsUser;
        let userData = [];
        let userExists = [];
        async.waterfall([
            function (done) {
                userModel.getUserLoginInfo(req.params.uid).then((results) => {
                    if (results.length > 0) {
                        let dbId = JSON.parse(results[0].login_info).databaseId;
                        let clubId = JSON.parse(results[0].login_info).clubId;
                        var data = qs.stringify({
                            'grant_type': 'client_credentials',
                            'client_id': 'client-credentials-crm',
                            'client_secret': configContainer.client_secret
                        })
                        fetch(configContainer.keyclock, {
                            'method': 'POST',
                            'headers': {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            'body': data
                        }).then((res) => {
                            res.text().then((re) => {
                                let keycloakToken = JSON.parse(re).access_token;
                                let sub = req.body.subject;
                                fetch(configContainer.crmClubUrl + 'support-user?database_id=' + dbId + '&club_id=' + clubId + '&subject=' + sub, {
                                    'method': 'GET',
                                    'headers': {
                                        'Authorization': 'Bearer ' + keycloakToken + '',
                                        'Cookie': 'authenticated=false; liveChatShared=null; publicShared=null'
                                    }
                                }).then((res) => {
                                    res.text().then((re) => {
                                        userData[0] = JSON.parse(re);
                                        done();
                                    })
                                }).catch(() => {
                                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                    res.status(500).json(response);
                                })
                            })
                        }).catch(() => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                        })
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['club:userNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                })
            },
            function (done) {
                clubModel.getSupportUser(req.params.uid).then((data) => {
                    if (data.length > 0) {
                        userExists = data;
                        done()
                    } else {
                        done()
                    }
                })
            },
            function (done) {
                if (userExists && userExists.length > 0) {
                    clubModel.SupportUserUpdate(req.params.uid, userData[0].user, userData[0].password).then((saved) => {
                        if (saved.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userData } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['club:supportUserFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    })
                } else {
                    clubModel.SupportUserSave(req.params.uid, userData[0].user, userData[0].password).then((saved) => {
                        if (saved.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userData } });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['club:supportUserFail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    })
                }
            },
        ])

    }

    /**
    * Function to delete support user after seven days
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {} 
    */
    supportUserDeleteAfterSevenDays(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let supportUsers = [];
        let supportUserIds = [];
        async.waterfall([
            function (done) {
                clubModel.getAllSupportUser().then((data) => {
                    if (data.length > 0) {
                        supportUsers = data;
                        done();
                    }
                })
            },
            function (done) {
                for (let i = 0; i < supportUsers.length; i++) {
                    let date = supportUsers[i].created_at;
                    let da = date.setDate(date.getDate() + 7)
                    let tdate = new Date();
                    let newDate = new Date(da);
                    if (JSON.stringify(newDate).split('T')[0] <= JSON.stringify(tdate).split('T')[0]) {
                        supportUserIds.push(supportUsers[i].user_id);
                        if ((supportUsers.length == i + 1) && supportUserIds.length > 0) {
                            done();
                        }
                    } else {
                        if ((supportUsers.length == i + 1) && supportUserIds.length > 0) {
                            done();
                        }
                    }
                }
            },
            function (done) {
                clubModel.deleteSupportUser(supportUserIds).then((data) => {
                    if (data.affectedRows > 0) {
                    } else {

                    }
                })
            }
        ])
    }
}