import express from 'express';
import SurveyController from '../../controllers/survey/survey'

let router = express.Router();
let surveyController = new SurveyController();

router.get('/allsurveylist', surveyController.getAllSurvey.bind(surveyController));
router.get('/surveybyid/:id', surveyController.getSurveybyId.bind(surveyController));
router.get('/activesurvey', surveyController.getActiveSurveyList.bind(surveyController));
router.get('/activesurveypagination/:page/pagesize/:pagesize', surveyController.getActiveSurveyListByPage.bind(surveyController));
//router.get('/completedsurvey', surveyController.getCompletedSurvey.bind(surveyController));

router.get('/completedsurveypagination/:page/pagesize/:pagesize', surveyController.getCompletedSurveyByPagination.bind(surveyController));
router.get('/completedsurvey/:user_id', surveyController.getCompletedSurveyByUser.bind(surveyController));
router.get('/completedsurveypagination/:user_id/page/:page/pagesize/:pagesize', surveyController.getCompletedSurveyByUserPagination.bind(surveyController));
router.get('/activesurvey/:user_id', surveyController.getActiveSurvey.bind(surveyController));
router.get('/activesurveypagination/:user_id/page/:page/pagesize/:pagesize', surveyController.getActiveSurveyPaginate.bind(surveyController));
router.get('/myvotedsurvey/:user_id', surveyController.getVotedSurvey.bind(surveyController));
router.get('/myvotedsurveypaginate/:user_id/page/:page/:pagesize/:pagesize', surveyController.getVotedSurveyPagination.bind(surveyController));
router.get('/votedsurveybysurvey/:survey_id', surveyController.getVotedSurveyById.bind(surveyController));
router.get('/votebysurveyanswer/:survey_id', surveyController.getVoteBySurveyAns.bind(surveyController));
router.get('/votedsurveybyanswer/:answer_id', surveyController.getVotedSurveyByAnswer.bind(surveyController));
router.get('/surveyvotebyuser/:survey_id/user/:user_id', surveyController.getVoteBySurveyUser.bind(surveyController));
router.post('/surveycreate', surveyController.surveyCreate.bind(surveyController));
router.put('/surveyupdate/:id', surveyController.surveyUpdate.bind(surveyController));
router.post('/surveyaddvote', surveyController.surveyAddVote.bind(surveyController));
router.put('/surveyupdatevote', surveyController.surveyUpdateVote.bind(surveyController));
router.get('/surveyactiveclose/:id', surveyController.surveyActiveClose.bind(surveyController));
router.post('/surveyread', surveyController.surveyRead.bind(surveyController));
router.post('/surveyNotify',surveyController.surveyNotification.bind(surveyController));
router.delete('/surveydelete/:id', surveyController.surveyDelete.bind(surveyController));
// API for Tineon CRM survey Section
router.post('/surveyaddvoteTineon', surveyController.surveyAddVoteTineon.bind(surveyController));
router.post('/surveyby/surveyids',surveyController.surveyTineonNotify.bind(surveyController));
router.get('/crm-activesurvey/:page/pagesize/:pagesize', surveyController.getActiveSurveyListCrm.bind(surveyController));
// router.get('/crm-myvotedsurvey/:user_id', surveyController.getMyVotedSurvey.bind(surveyController));
router.get('/crm-myvotedsurvey/:user_id/:page/pagesize/:pagesize', surveyController.getMyVotedSurvey.bind(surveyController));

router.get('/crm-surveyvotebyuser/:survey_id/user/:user_id', surveyController.getVoteBySurveyTineonUser.bind(surveyController));
router.get('/crm-surveybyid/:id', surveyController.getSurveybyIdTineon.bind(surveyController));
router.get('/crm-surveyvotebyAnswer/:survey_id/answer/:answer_id', surveyController.getVoteBySurveyAnswer.bind(surveyController));
router.put('/crm-surveyupdatevote', surveyController.surveyUpdateVoteTineonUser.bind(surveyController));
router.get('/completedsurvey/:page/pagesize/:pagesize', surveyController.getCompletedSurvey.bind(surveyController));

export default router;