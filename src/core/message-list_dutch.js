
let germanMessageList = {
    languageSuccess: "language set successfully",
    invalidAuthToken: "Ungültiger Token oder Token ist abgelaufen, bitte melden Sie sich erneut an",
    unauthorized401: "Unbefugter Zugang",
    common401: "Unbefugter Zugang",
    common404: "Nicht gefunden",
    common402: "Falsches Datenformat",
    common500: "Etwas ist schief gelaufen, bitte versuchen Sie es nach einiger Zeit erneut.",
    common400: "Schlechte Anfrage",
    Common204: "Kein Inhalt",
    saved: "Erfolgreich gespeichert",
    deleted: "Daten erfolgreich löschen",
    emailExists: "Die E-Mail ist bereits bei uns registriert",
    emailNotExists: "Die E-Mail ist nicht bei uns registriert",
    userNameExists: "Der Benutzername ist bereits bei uns registriert",
    emailUserNameNotExists: "Die angegebene E-Mail oder der Benutzername ist nicht bei uns registriert",
    blankRequest: "Leere Anfrage kann nicht bearbeitet werden",
    resumeFormat: "Bei der Bearbeitung der Wiederaufnahme ist ein Fehler aufgetreten, bitte kontaktieren Sie den Support",
    fileSizeError: "Die Datei, die Sie hochladen, überschreitet die 20 MB Grenze",


    //Account
    invalidUserNamePassword: "Ungültiger Benutzername und/oder Passwort",
    accountNotActivated: "Verifizierung erforderlich. Sie haben eine E-Mail mit einem Aktivierungslink erhalten.",
    deActivateAccount: "Ihr Konto ist deaktiviert. Bitte kontaktieren Sie den Support unter support@tineon.de",
    inactiveAccount: "Konto ist nicht aktiv",
    accountDisabled: "Ihr Konto ist deaktiviert. Bitte kontaktieren Sie den Support unter support@tineon.de",
    unAuthorizedAccount: "Ihr Konto ist nicht autorisiert",
    accFirstName: "Sind Sie sicher, dass Sie Ihren Vornamen richtig eingegeben haben?",
    accLastName: "Sind Sie sicher, dass Sie Ihren Nachnamen richtig eingegeben haben?",
    accUserDomain: "Bitte wählen Sie den gewünschten Bereich.",


    // password: "Verwenden Sie mindestens 8 Zeichen, darunter 1 Großbuchstabe und 1 Symbol", // "Das Passwort muss alphanumerisch sein und ein Sonderzeichen mit einer Länge von 8-20 Zeichen enthalten.",
    reCaptchaCode: "Ein gültiges reCaptcha ist erforderlich",
    termsAndConditions: "Bitte stimmen Sie unseren Datenschutzrichtlinien und Nutzungsbedingungen zu, um fortzufahren.",
    invalidDocType: "Ungültiger Dateityp",
    resumeFileName: "Eine gültige Datei ist erforderlich",
    registeredSuccess: "Benutzer erfolgreich registriert und der Verifizierungslink wird an das Benutzerkonto gesendet",
    registrationFail: "Benutzerregistrierung fehlgeschlagen",
    countryCode: "Ein gültiges Land ist erforderlich",
    invalidCode: "Ein gültiger Code ist erforderlich",
    code: "Eine gültige Code ist erforderlich",
    expiredCode: "Ihr Code ist abgelaufen",
    codeORusername: "Ein gültiger Code oder Benutzername ist erforderlich",
    invalidUser: "Ungültiger Benutzer",
    validCode: "Code ist gültig",
    name: "Ein gültiger Name ist erforderlich",
    profilePicture: "Profilfoto ist erforderlich",
    jobStatusUpdated: "Erfolgreich aktualisiert",
    notLookingInvalidCode: "Ihre Anfrage konnte nicht validiert werden. Bitte versuchen Sie es später erneut.",
    notLookingExpiredCode: "Ihr Autorisierungscode ist abgelaufen",
    registeredSuccessLink: "Erfolgreiche Registrierung und Bestätigungs-E-Mail an Ihr E-Mail-Konto gesendet",
    mailNotAdmin: "Mailversand an Admin fehlgeschlagen",
    mailNotUser: "Mailversand an Benutzer fehlgeschlagen",
    registeredSuccessMailNotSent: "Benutzer erfolgreich registriert, aber die Verifizierungsmail an das Benutzerkonto ist fehlgeschlagen",
    invalidPassword: "Bitte geben Sie ein gültiges Passwort ein",
    changePassword: "Please change your password",
    alreadyChangedPassword: "Sie haben das Passwort bereits zurückgesetzt.",
    
    //Account Activate
    employeeDetailsId: "Eine gültige employeeDetailsId ist erforderlich",
    userName: "Benutzername ist erforderlich",
    otpCode: "Ein gültiger OTP-Code ist erforderlich",
    otpExpired: "Das OTP ist abgelaufen",
    accountAlreadyActivated: "Ihr Konto ist bereits aktiv",
    accountActivatedSuccess: "Ihr Konto ist nicht aktiv",
    accountActivatelink: "Eine E-Mail mit dem Link zur Kontoaktivierung wurde an Ihre E-Mail-ID gesendet.",
    otpSentSuccess: "OTP wurde erfolgreich an Ihre E-Mail-ID gesendet",
    passwordMismatch: "Passwörter müssen übereinstimmen",


    //change password
    passwordChangedSuccess: "Ihr Passwort wurde erfolgreich geändert",
    passwordCantBeSame: "Ihr neues Passwort muss sich von allen zuvor verwendeten Passwörtern unterscheiden",
    confirmPasswordNotMatched: "Neues Passwort und Bestätigungspasswort stimmen nicht überein",
    invalidOldPassword: "Ein gültiges altes Passwort ist erforderlich",
    resetPasswordMail: "Eine E-Mail mit dem Link zum Zurücksetzen des Passworts wurde an Ihre E-Mail-ID gesendet.",
    confirmPassword: "Kennwort und Bestätigungskennwort stimmen nicht überein",
    mailSentFail: "Mail gesendet fehlgeschlagen, bitte versuchen Sie es später noch einmal",
    mailSentSuccess: "Mail erfolgreich gesendet",
    passwordChangedFail: "Passwortänderung fehlgeschlagen",


    //logout
    logOutSuccess: "Benutzer erfolgreich abgemeldet",


    // email-verification
    verificationFail: "E-Mail-Überprüfung fehlgeschlagen, bitte versuchen Sie es erneut",
    invalidLink: "Link ist ungültig",
    linkExpired: "Ihr Link ist abgelaufen,",
    alreadyVerified: "Sie haben Ihr Kontopasswort bereits verifiziert",
    alreadyVerifiedEmail: "Sie haben Ihre E-Mail bereits verifiziert",


    // licence
    licenceNotExists: "Lizenz nicht vorhanden",
    licenceRegisteredSuccess: "Benutzerlizenz erfolgreich registriert",
    licenceRegistrationFail: "Registrierung der Benutzerlizenz fehlgeschlagen",
    licenceUpdationSuccess: "Benutzerlizenz erfolgreich aktualisiert",
    licenceUpdationFail: "Aktualisierung der Benutzerlizenz fehlgeschlagen",
    licenceIdExists: "Lizenz-ID ist bereits bei uns registriert",
    licenceNameExists: "Der Lizenzname ist bereits bei uns registriert",
    licenceDeleted: "Lizenz erfolgreich gelöscht",
    licenceCreated: "Die Benutzerlizenz wurde erfolgreich erstellt",
    licenceCreationFail: "Ungültige Lizenzerstellung",
    licenceUpdateSuccess: "Die Lizenz wurde erfolgreich aktualisiert",
    userLicenceRegistrationFail: "Lizenzregistrierung fehlgeschlagen",
    userLicenceRegistrationSuccess: "Erfolgreiche Lizenzregistrierung",
    licenceActivated: "Vereins Lizenz aktiviert",
    licenceBlocked: "Vereins Lizenz blockiert",

    //role
    roleNotExists: "Rolle existiert nicht",
    roleRegisteredSuccess: "Benutzerrolle erfolgreich registriert",
    roleRegistrationFail: "Registrierung der Benutzerrolle fehlgeschlagen",
    roleUpdationSuccess: "Benutzerrolle erfolgreich aktualisiert",
    roleUpdationFail: "Aktualisierung der Benutzerrolle fehlgeschlagen",
    roleIdExists: "Role-ID ist bereits bei uns registriert",
    roleNameExists: "Der Rollenname ist bereits bei uns registriert",
    roleDeleted: "Rolle erfolgreich gelöscht",
    roleCreated: "Die Benutzerrolle wurde erfolgreich erstellt",
    roleCreationFail: "Ungültige Rollenerstellung",
    roleUpdateSuccess: "Die Rolle wurde erfolgreich aktualisiert",


    //productComponents
    productComponentsNotExists: "Die Produktkomponente existiert nicht",
    productComponentsRegisteredSuccess: "Produktkomponente erfolgreich registriert",
    productComponentsRegistrationFail: "Registrierung der Produktkomponente fehlgeschlagen",
    productComponentsUpdationSuccess: "Produktkomponente erfolgreich aktualisiert",
    productComponentsUpdationFail: "Aktualisierung der Produktkomponente fehlgeschlagen",
    productComponentsIdExists: "Die Produktkomponenten-ID ist bereits bei uns registriert",
    productComponentsNameExists: "Der Name der Produktkomponente ist bereits bei uns registriert",
    productComponentsDeleted: "Produktkomponente erfolgreich gelöscht",
    productComponentsCreated: "Die Produktkomponente wurde erfolgreich erstellt",
    productComponentsCreationFail: "Ungültige Produktkomponente erstellt",
    productComponentsUpdateSuccess: "Die Komponente Produkt wurde erfolgreich aktualisiert",


    //domain
    domainNotExists: "Domain existiert nicht",
    domainRegisteredSuccess: "Domain erfolgreich registriert",
    domainRegistrationFail: "Domainregistrierung fehlgeschlagen",
    domainUpdationSuccess: "Domain erfolgreich aktualisiert",
    domainUpdationFail: "Domänenaktualisierung fehlgeschlagen",
    domainIdExists: "Domain-ID ist bereits bei uns registriert",
    domainNameExists: "Der Domainname ist bereits bei uns registriert",
    domainDeleted: "Domain erfolgreich gelöscht",
    domainCreated: "Die Domäne wurde erfolgreich erstellt",
    domainCreationFail: "Ungültige Domänenerstellung",
    domainUpdateSuccess: "Die Domäne wurde erfolgreich aktualisiert",


    //user
    userNotExists: "Benutzer existiert nicht",
    registeredSuccessAsGuest: "Erfolgreiche Registrierung als Gastbenutzer.",
    registeredRoleFail: "Benutzer registriert, aber Rolle ist nicht erfolgreich registriert",
    registeredDomainFail: "Benutzer registriert, aber Domain ist nicht erfolgreich registriert",
    userUpdationSuccess: "Benutzer erfolgreich aktualisiert",
    userUpdationFail: "Benutzeraktualisierung fehlgeschlagen",
    userDeleted: "Benutzer erfolgreich gelöscht",
    userSoftDeleted: "Kann Kunde nicht löschen, Rechnung ist vorhanden",
    userDelRoleNotExists: "Der Benutzer wurde gelöscht, aber seine Rolle ist nicht vorhanden",
    userAssigned: "Kunde erfolgreich zugeordnet",
    userAssignedFail: "Kundenzuordnung fehlgeschlagen",
    commissionSet: "Provisionseinstellung gespeichert",
    commissionSetFail: "Provisionseinstellung fehlgeschlagen",
    commExists: "Sie haben bereits eine Provision für dieses Produkt festgelegt",
    comSetNotExists: "Provisionseinstellung existiert nicht",
    comSetDeleted: "Provisionseinstellung gelöscht",


    //club
    supportUserSuccess: "Support User erstellt",
    supportUserFail: "Support User nicht erstellt",
    supportUserNotExists: "Support User existiert nicht",


    // profile-edit
    userAddressUpdationFail: "Aktualisierung der Benutzeradresse fehlgeschlagen",
    userAccountUpdationFail: "Aktualisierung des Benutzerkontos fehlgeschlagen",
    userImageUpdationFail: "Aktualisierung des Benutzerbildes fehlgeschlagen",


    //product-price
    productSuccess: "Das Produkt wurde erfolgreich erstellt",
    productPriceFail: "Ungültige Produktpreiserstellung.",

    //faqCateogry	
    categoryExists: "Kategorie existiert bereits",
    categorySucess: "Kategorie erstellt",
    categoryCreationFail: "Kategorie konnte nicht erstellt werden",
    categoryUpdate: "Kategorie aktualisiert",
    categoryUpdationFail: "Kategorie konnte nicht aktualisiert werden",
    categoryDelete: "Kategorie gelöscht",
    categoryDeleteionFail: "Kategorie konnte nicht gelöscht werden",
    categoryquestionExist: "In dieser Kategorie sind Fragen vorhanden, Kategorie kann nicht gelöscht werden",

    //faqStatus	
    StausAssigned: "Status bereits einer Anfrage zugewiesen, kann nicht gelöscht werden",
    statusDelete: "Status gelöscht",
    StatusDeleteionFail: "Status konnte nicht gelöscht werden",
    StatusExists: "Status existiert bereits",
    StatusNotExists: "Status existiert nicht",
    StatusSucess: "Status erstellt",
    StatusCreationFail: "Status konnte nicht erstellt werden",
    statusUpdate: "Status aktualisiert",

    //faqQuestion	
    FaqQuestionCreated: "Frage erstellt",
    FaqQuestionCreationFail: "Frage konnte nicht erstellt werden",
    FaqQuestionExists: "Frage existiert bereits",
    FaqQuestionNotExists: "Frage existiert nicht",
    FaqQuestionUpdate: "Frage aktualisiert",
    FaqQuestionUpdationFail: "Frage konnte nicht aktualisiert werden",
    FaqQuestionDelete: "Frage gelÃ¶scht",

    //faqQuestion feedback	
    FaqFeedbakCreated: "Feedback gespeichert",
    FaqFeedbackFail: "Feedback nicht gespeichert",
    FaqFeedbackNotExist: "Feedback existiert nicht",
    FaqFeedbackUpdated: "Feedback aktualisiert",

    //faq
    faqNotExists:"FAQ existiert nicht",

    //billwerk
    AuthSuccess: "Auth-Token erfolgreich generiert",
    orderFail: "Bestellung erfolgreich, aber Einfügen des Bestelldatensatzes fehlgeschlagen",
    orderAlreadyOrdered: "Sie haben dieses Produkt bereits bestellt.",
    orderFirstPrimary: "Um die Teilprodukte zu erwerben, bestellen Sie bitte zuerst das Produkt Club-Administration.",
    orderNotExist: "Auftrag bereits gelöscht oder nicht vorhanden",
    deletedOrder: "Auftrag erfolgreich gelöscht",
    alreadyOrderdVersion: "Sie haben bereits eine andere Version dieses Produkts bestellt",
    orderStoreSuccess: "Ihre Bestellung wurde erfolgreich gespeichert, sie wird nach der kostenlosen Testphase fortgesetzt.",
    orderStoreFail: "Ihre bevorstehende Bestellung ist fehlgeschlagen",
    InValidIBAN: "Bitte geben Sie eine gültige IBAN ein",


    //survey
    surveyExists: "Umfrage existiert bereits",
    surveyCreationFail: "Erstellung der Umfrage fehlgeschlagen",
    surveyCreated: "Umfrage erfolgreich erstellt",
    surveyNotExists: "Umfrage gibt es nicht",
    surveyUpdated: "Umfrage erfolgreich aktualisiert",
    voteSuccess: "Umfrage erfolgreich abgeschlossen",
    voteFail: "Abstimmung in der Umfrage gescheitert",


    //contact
    contactFail: "Kontakt gescheitert",

    //advertisement
    advertisementExist: "Werbeanzeige existiert bereits",
    AdvertisementCreatedsucess: "Werbeanzeige erstellt",
    AdvertisementCreatedfail: "Werbeanzeige konnte nicht erstellt werden",
    advertisementUpdationSuccess: "Werbeanzeige aktualisiert",
    AdvertisementNotExists: "Werbeanzeige existiert nicht",
    advertisementDeleted: "Werbeanzeige gelöscht",

    //product
    productCreationFail: "Produkterstellung gescheitert.",
    productNotExists: "Das Produkt existiert nicht",
    productIDNotExists: "Die Produkt-ID existiert nicht",
    productDeleted: "Produkt erfolgreich gelöscht",
    productUpdated: "Produkt erfolgreich aktualisiert",
    productUpdationSuccess: "Produkt erfolgreich aktualisiert",
    productPriceDeleted: "Das Produkt und sein Preis wurden erfolgreich GELÖSCHT",
    productPriceUpdated: "Das Produkt und sein Preis wurden erfolgreich aktualisiert",
    productDetailFail: "Erstellung von Produktdetails fehlgeschlagen",
    productAssigned: "Produkt erfolgreich zugewiesen",
    productAssignedFail: "Produkt zuweisen fehlgeschlagen",
    ProductSucess: "Produkt succesvol aangemaakt",
    productExists: "Produkt existiert bereits",
    productImgRequired: "Produkt Foto hinzufügen",
    ProductOrderSucess: "Produkt erfolgreich bestellt",

    //crmProductOrder
    clubProduct: "Bitte zuerst eine Vereinsverwaltung bestellen",
    OrderProductExist: "Sie haben dieses Produkt bereits bestellt",
    OrderedNotExists: "Bisher kein Produkt bestellt",
    OrdereFailed: "Kauf fehlgeschlagen",
    productOrderedButMailNot: "Ihr Produktbestellvorgang ist unvollständig, bitte wenden Sie sich an das Support-Team unter: support@s-verein.de",
    OrderUpdationSuccess: "Order Status Successfully Updated",
    orderUpdationFail: "Order Status Updation Failed",

    //mandate
    MandateNotExists: "Das Mandat existiert nicht.",
    pdfSentFail: "PDF-Erzeugung fehlgeschlagen",


    //social login
    accessToken: "Ein gültiges accessToken ist erforderlich",
    accessTokenExpired: "Das Zugriffstoken ist abgelaufen, bitte versuchen Sie es erneut",
    emailNotMatchWithSocailEmail: "Eingegebene E-Mail stimmt nicht mit der E-Mail in den sozialen Medien überein",
    socialEmailRequired: "Die Benutzerregistrierung konnte nicht abgeschlossen werden, da die E-Mail-Adresse in den sozialen Medien erforderlich ist. Bitte aktualisieren Sie Ihre sozialen Medien und versuchen Sie es erneut.",
    id: "Eine gültige ID ist erforderlich",
    passwordCreatedSuccess: "Passwort für Social Login wurde erfolgreich erstellt",
    passwordAlreadyCreated: "Das Passwort für Ihren Social Login wurde bereits erstellt",
    passwordAlreadyExists: "Passwort wurde bereits erstellt",
    passwordCreated: "Passwort erfolgreich erstellt",


    // role_permissions
    rolePermissionsNotExists: "Rollenberechtigungen sind nicht vorhanden.",
    rolePermissionCreated: "Rollenberechtigung erfolgreich erstellt.",
    rolePermissionCreationFail: "Erstellung einer Rollenberechtigung fehlgeschlagen.",
    rolePermissionDeleted: "Rollenberechtigung erfolgreich gelöscht.",


    //news
    newsExists: "Der Nachrichtentitel existiert bereits, bitte verwenden Sie einen anderen.",
    newsNotExists: "News gibt es nicht.",
    newsDeleted: "Nachrichten erfolgreich gelöscht",
    newsUpdationSuccess: "Nachrichten erfolgreich aktualisiert",


    //email-template
    emailTemplateNotExists: "E-Mail-Vorlage ist nicht vorhanden",
    emailTemplateExists: "E-Mail-Vorlage existiert bereits",
    emailTemplateImgRequired: "Vorlagenbild ist erforderlich",
    emailTemplateCreationSuccess: "E-Mail-Vorlage erfolgreich erstellt",
    emailTemplateCreationFail: "Erstellung einer E-Mail-Vorlage fehlgeschlagen",
    emailTemplateDeleted: "E-Mail-Vorlage erfolgreich gelöscht",
    emailTemplateUpdationSuccess: "E-Mail-Vorlage erfolgreich aktualisiert",


    //pages
    pagesExists: "Seitentitel existiert bereits, bitte einen anderen verwenden.",
    pagesNotExists: "Die Seite existiert nicht.",
    pagesDeleted: "Seite erfolgreich gelöscht",
    pagesUpdationSuccess: "Seite erfolgreich aktualisiert",
    pagesURLAliasExists: "URL-Alias bereits verwendet, bitte einen anderen verwenden",


    //document upload
    documentUpdated: "Dokument wurde erfolgreich hochgeladen",
    documentUpdationFail: "Dokument-Upload fehlgeschlagen",
    documentNotExists: "Benutzerdokumente sind nicht vorhanden",
    uploadDocDeleted: "Hochgeladenes Dokument wird erfolgreich gelöscht",


    //request ticket	
    requestNameExists: "Diese Anfrage ist bereits vorhanden, bitte wählen Sie eine andere",
    requestCreated: "Anfrage erstellt",
    requestCreatedAndEmailSend: "Anfrage erstellt und E-Mail an Support gesendet",
    requestCreationFail: "Anfrage konnte nicht erstellt werden",
    requestNotExists: "Anfrage existiert nicht",
    requestUpdateSuccess: "Anfrage aktualisiert",
    requestDeleted: "Anfrage gelöscht",

    //reuest ticket assigned to support users	
    requestAssigned: "Ticket einem Support Mitarbeiter zugewiesen",
    requestAssignedfail: "Ticket konnte nicht zugewiesen werden",

    //comment ticket	
    commentCreated: "Kommentar erstellt",
    commentCreationFail: "Kommentar konnte nicht erstellt werden",
    commentNotExists: "Kommentar existiert nicht",
    commentUpdateSuccess: "Kommentar aktualisiert",
    commenttDeleted: "Kommentar gelöscht",

    //customer membership	
    memberNameExists: "Username bereits vergeben, wählen Sie einen anderen",
    memberAddSuccess: "Mitglied hinzugefügt",
    memberAddFail: "Mitglied hinzufügen fehlgeschlagen",
    memberNotExists: "Mitglied existiert nicht",
    memberUpdationSuccess: "Mitglied aktualisiert",
    memberUpdationFail: "Mitglied konnte nicht aktualisiert werden",
    memberDeleted: "Mitglied gelöscht",
    toogleSuccess: "Toggle gespeichert",
    departmentExists: "Abteilung existiert bereits",
    departmentAddSuccess: "Abteilung hinzugefügt",
    departmentAddFail: "Abteilung konnte nicht hinzugefügt werden",
    departmentNotExists: "Abteilung existiert nicht",
    departmentUpdationSuccess: "Abteilung aktualisiert",
    departmentUpdationFail: "Abteilung konnte nicht aktualisiert werden",
    departmentDeleted: "Abteilung gelöscht",
    deparmentNameUsed: "Abteilungsname wird bereits als Mitglied verwendet",
    departmentassignedtomember: "Mitglied ist dieser Abteilung zugeordnet",

    //customer tag	
    tagExists: "Tag existiert bereits",
    tagAddSuccess: "Tag hinzugefügt",
    tagAddFail: "Tag konnte nicht zugewiesen werden",
    tagNotExists: "Tag existiert nicht",
    tagUpdationSuccess: "Tag aktualisiert",
    tagUpdationFail: "Tag konnte nicht aktualisiert werden",
    tagDeleted: "Tag gelöscht",

    // customer member search	
    memberSearchAddSuccess: "Mitgliederliste hinzugefügt",
    memberSearchAddFail: "Mitgliederliste konnte nicht hinzugefügt werden",
    memberSearchNameExists: "Mitgliederliste existiert bereits, bitte ändern Sie den Namen",
    memberSearchDeleted: "Mitgliederliste gelöscht",
    memberSearchNotExists: "Mitgliederliste existiert nicht",

    //FunctionInClub	
    clubExists: "Funktion existiert bereits",
    clubAddSuccess: "Funktion wurde hinzugefügt",
    clubAddFail: "Funktion konnte nicht hinzugefügt werden",
    clubNotExists: "Funktion existiert nicht",
    clubUpdationSuccess: "Funktion aktualisiert",
    clubUpdationFail: "Funktion konnte nicht aktualisiert werden",
    clubDeleted: "Funktion gelöscht",
    FunctionNameUsed: "Funktion bei einem Mitglied vorhanden",

    //ProductDisplay	
    ProductAddSuccess: "Produkt hinzugefügt",
    ProductAddFail: "Produkt konnte nicht hinzugefügt werden",
    DisplayProductNotExists: "Produkt existiert nicht",

    //validation errors
    oldNewPassSame: "Altes Passwort und neues Passwort dürfen nicht gleich sein",
    oldPassword: "Bitte ein gültiges Passwort eingeben",
    password: "Das Passwort muss Großbuchstaben, Kleinbuchstaben, Zahlen und Symbole enthalten und zwischen 8 und 30 Zeichen lang sein.",
    confirmPassword: "Das Bestätigungspasswort muss mit dem Passwort übereinstimmen.",
    email: "Bitte gültige E-Mail eingeben.",
    confirmEmail: "Die Bestätigungs-E-Mail sollte dieselbe sein wie die E-Mail.",
    societyName: "Name der Gesellschaft ist erforderlich",
    uidReq: "Benutzer-ID erforderlich.",
    username: "Benutzername ist erforderlich",
    subject: "Betreff erforderlich",
    msgReq: "Nachricht erforderlich",
    suid: "Benutzer-UID für den Verkauf ist erforderlich.",
    cuid: "Benutzer-ID des Kunden ist erforderlich.",
    priceReq: "Preis ist erforderlich.",
    validReq: "Gültig bis ist erforderlich.",
    ppidReq: "Ppid ist erforderlich.",
    nidReq: "Nid ist erforderlich.",
    classReq: "Klasse ist erforderlich.",
    domainReq: "Domain ist erforderlich.",
    weightReq: "Gewicht ist erforderlich.",
    bundleReq: "is_bundle ist erforderlich.",
    pdflinkReq: "pdflink ist erforderlich.",
    bookableReq: "is_bookable ist erforderlich.",
    alwayson: " is_alwayson ist erforderlich.",
    commissionableReq: "is_commissionable ist erforderlich.",
    firstName: "Vorname ist erforderlich",
    surName: "Nachname ist erforderlich",
    addressLine: "Addresse ist erforderlich",
    zipcode: "Postleitzahl erforderlich",
    placeReq: "Ort erforderlich",
    ridReq: "role id ist erforderlich.",
    roleReq: "Rolle ist erforderlich.",
    permissionsReq: "Berechtigungen sind erforderlich.",
    modulesReq: "Einzelmodul Name ist erforderlich.",
    titleReq: "Titel ist erforderlich",
    descriptionReq: "Beschreibung erforderlich",
    publishReq: "Veröffentlichen in ist erforderlich",
    contentReq: "Inhalt ist erforderlich.",
    authorReq: "Autor ist erforderlich.",
    urlReq: "News-Url ist erforderlich.",
    newsCreationSuccess: "Nachrichten erfolgreich erstellt",
    newsCreationFail: "Nachrichtenerstellung fehlgeschlagen",
    contactNumber: "Kontaktnummer ist erforderlich",
    domainReq: "Domänenname ist erforderlich",
    sitenameReq: "Seitenname ist erforderlich",
    machineNameReq: "Computername ist erforderlich",
    productName: "Produktname ist erforderlich",
    productPrice: "Produktpreis ist erforderlich",
    productComponent: "Produktkomponenten sind erforderlich",
    votingOptionsReq: "Bitte wählen Sie Abstimmungsoptionen",
    answerOptionReq: "Bitte wählen Sie die Antwortoptionen",
    activeUntilReq: "Enddatum ist erforderlich.",
    surveyIdReq: "Umfrage-ID ist erforderlich",
    userIdReq: "Umfrage-Benutzerkennung ist erforderlich",
    surveyAnswerIdReq: "Umfrageantworten ID ist erforderlich",
    surveyTypeReq: "Umfragetyp ist erforderlich",
    activeReq: "Startdatum der Umfrage ist erforderlich",
    templateTypeReq: "Bitte wählen Sie den Vorlagentyp",
    subjectReq: "Mail-Betreff ist erforderlich.",
    templateBodyReq: "Vorlagenkörper ist erforderlich",
    urlReq: "Vorlage url ist erforderlich",
    headerContentReq: "Inhalt der Kopfzeile ist erforderlich",
    footerContentReq: "Fußzeileninhalt ist erforderlich",
    nameReq: "Anfrage",
    statusReq: "Status wird benötigt",
    prodctDescription: "Produkt Beschreibung hinzufügen",
    QuestionReq: "Frage wird benötigt",
    AnswerReq: "Antwort wird benötigt",
    commentReq: "Kommentar wird benötigt",
    assignedToReq: "Anfrage muss zugewiesen werden",
    OldUserDataDeleted : "Old User Data Deleted Successfully",
    OldUserDataNotExist : "Old User Data Not Exist",
    PleaseSelectAssigneduser : "Please Select Assigned user for Ticket",

      //Tineon Info
    tineonInfoSuccess: "Tineon Info successfully Added",
    tineonInfoFail: "Tineon Info Creation Failed",
    tineonInfoDeleted : "Tineon Info Successfully Deleted",
    tineonInfoNotExist : "Tineon Info Not Exist"



}

export default germanMessageList = germanMessageList;
