import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class EmailTemplateModel {

    async CreateemailTemplate(req, emailTemplateImg) {
        const rows = await db.conn.promise().query('INSERT INTO `email_template`(`template_type`,`subject`,`template_body`,`header_content`,`footer_content`,`logo`,`url`)VALUES(?,?,?,?,?,?,?)', [
            req.body.templateType,
            req.body.subject,
            req.body.templateBody,
            req.body.headerContent,
            req.body.footerContent,
            emailTemplateImg,
            req.body.url,
        ]);
        return rows[0];
    }

    async getemailTemplateById(id) {
        const rows = await db.conn.promise().query(`SELECT id,template_type,subject,template_body,header_content, footer_content,logo,url FROM email_template WHERE id = ?`, [id]);
        return rows[0];
    }

    async getemailTemplateByType(type) {
        const rows = await db.conn.promise().query(`SELECT id,template_type,subject,template_body,header_content, footer_content,logo,url FROM email_template WHERE template_type = ?`, [type]);
        return rows[0];
    }

    async getAllemailTemplate() {
        const rows = await db.conn.promise().query(`SELECT id,template_type,subject,template_body,header_content, footer_content,logo,url FROM email_template`);
        return rows[0];
    }

    async deleteemailTemplatebyId(id) {
        const rows = await db.conn.promise().query("DELETE FROM `email_template` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getUpdateemailTemplate(req, emailTemplateImg) {
        const rows = await db.conn.promise().query("UPDATE email_template SET template_type = ?,subject= ?,template_body= ?,header_content= ?,footer_content= ?,logo= ?,url= ? WHERE id = ?", [
            req.body.templateType,
            req.body.subject,
            req.body.templateBody,
            req.body.headerContent,
            req.body.footerContent,
            emailTemplateImg,
            req.body.url,
            req.params.id
        ])
        return rows[0];
    }
}