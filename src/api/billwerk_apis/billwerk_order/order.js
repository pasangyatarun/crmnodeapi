import express   from 'express';
import BillwerkOrderController from '../../../controllers/billwerk_controllers/billwerk_order/order';

let router = express.Router();
let billwerkOrderController = new BillwerkOrderController();

router.get('/billwerk-orders', billwerkOrderController.getOrders.bind(billwerkOrderController));
router.get('/billwerk-orders-paginate/:page/pagesize/:pagesize', billwerkOrderController.getOrdersByPage.bind(billwerkOrderController));
router.get('/billwerk-orders/:id', billwerkOrderController.getOrdersByOrderId.bind(billwerkOrderController));
router.get('/billwerk-orders/bycustomer/:id', billwerkOrderController.getOrdersByCustomerId.bind(billwerkOrderController));
router.get('/billwerk-licences/bycustomer/:id', billwerkOrderController.getLicencesByCustomerId.bind(billwerkOrderController));
router.post('/billwerk-order', billwerkOrderController.orderSend.bind(billwerkOrderController));
router.post('/billwerk-order/approve/:id', billwerkOrderController.orderApprove.bind(billwerkOrderController));
router.post('/billwerk-order/decline/:id', billwerkOrderController.orderDecline.bind(billwerkOrderController));
router.post('/billwerk-order/commit/:id', billwerkOrderController.orderCommit.bind(billwerkOrderController));
router.post('/billwerk-order/preview', billwerkOrderController.orderPreview.bind(billwerkOrderController));
router.delete('/billwerk-order-delete/:id', billwerkOrderController.orderDelete.bind(billwerkOrderController));
router.post('/billwerk-order-bystatus', billwerkOrderController.getOrdersByStatus.bind(billwerkOrderController));
router.get('/billwerk-order-status', billwerkOrderController.getOrdersStatus.bind(billwerkOrderController));
router.post('/billwerk-bystatususer/:user_id', billwerkOrderController.getOrderByUserStatus.bind(billwerkOrderController));
router.post('/validate-iban', billwerkOrderController.validateIban.bind(billwerkOrderController));
router.post('/club-create-update/:vid', billwerkOrderController.orderClub.bind(billwerkOrderController));
router.get('/billwerk-orders/completed-other', billwerkOrderController.getOrdersInProgress.bind(billwerkOrderController));
router.get('/billwerk-download-mandate/:uid', billwerkOrderController.getBillwerkMandatePdf.bind(billwerkOrderController));
router.get('/billwerk-mandate-download/:uid', billwerkOrderController.getMandatePdf.bind(billwerkOrderController));
//order send api for 3 spg user type
router.post('/billwerk-order-spgtrial', billwerkOrderController.orderSendSPG.bind(billwerkOrderController));
router.post('/billwerk-userorder-afterspgtrial', billwerkOrderController.incomingOrderByUser.bind(billwerkOrderController));

//API for billwerk component product
router.post('/billwerk-componentProduct-order', billwerkOrderController.componentOrderSend.bind(billwerkOrderController));
router.post('/billwerk-componentProduct-order/commit/:id', billwerkOrderController.componentOrderCommit.bind(billwerkOrderController));


export default router;