import async from 'async';
import ProductDisplayModel from '../../models/product-display/product-display';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import axios from 'axios';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');
let productDisplayModel = new ProductDisplayModel();

export default class ProductDisplayController {

    /**
    * Function to add selected products 
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    selectedProduct(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                productDisplayModel.getAllSelectedProducts().then((results) => {
                    if (results.length > 0) {
                        productDisplayModel.deleteProductsExists().then((resu) => {
                            if (resu.affectedRows > 0) {
                                done();
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['Product:productDeletefail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (req.body.productId.length > 0) {
                    for (let i = 0; i < req.body.productId.length; i++) {
                        productDisplayModel.SelectedProduct(req.body.productId[i]).then((ProductCreated) => {
                            if (ProductCreated.affectedRows > 0) {
                                if (i + 1 == req.body.productId.length) {
                                    done();
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['Product:ProductAddFail'], { code: 417 });
                                res.status(200).json(response)
                            }
                        })
                    }
                }
                else {
                    done();
                }
            },
            function () {
                response = responseFormat.getResponseMessageByCodes(['Product:ProductAddSuccess']);
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get all selected products 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    getAllSelectedProduct(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        productDisplayModel.getAllSelectedProducts().then((results) => {
            if (results.length > 0) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['Product:DisplayProductNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to get product display for customer
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} product details
    */
    getProductDisplayForCustomer(req, res, next) {
        let allVariant;
        let productsall;
        let BillwerkProcudt;
        let SelectedProduct;
        let compre;
        let DisplayProduct = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'productInfo',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (response) {
                        productsall = response.data;
                        allVariant = productsall.PlanVariants.filter(o => o.Id != '627a7d68f6750711f0176297');
                        done();
                    })
                    .catch(function (error) {
                        if (error && error.isAxiosError == true && error.response && error.response.data && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                for (let i = 0; i < allVariant.length; i++) {
                    let detail = productsall.Plans.find(o => o.Id == allVariant[i].PlanId);
                    let detailGroup = productsall.PlanGroups.find(o => o.Id == allVariant[i].PlanGroupId);
                    allVariant[i]['planDetail'] = detail;
                    allVariant[i]['planGroup'] = detailGroup;
                    if (allVariant.length == i + 1) {
                        done();
                    }
                }
            },
            function (done) {
                let newArrData = [];
                allVariant.forEach(o => {
                    if (!(o.AllowWithoutPaymentData == true)) {
                        newArrData.push(o);
                    }
                });
                BillwerkProcudt = newArrData;
                done();
            },

            function (done) {
                productDisplayModel.getSelectedProducts().then((results) => {
                    if (results.length > 0) {
                        SelectedProduct = results;
                        done()
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['Product:DisplayProductNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < SelectedProduct.length; i++) {
                    compre = BillwerkProcudt.find(o => o.Id == SelectedProduct[i].product_id);
                    if(compre != undefined){
                    DisplayProduct.push(compre);
                    }
                    if (SelectedProduct.length == i + 1) {
                        done();
                    }
                }
            },
            function (done) {
                DisplayProduct.sort(function(a, b){return a.RecurringFee-b.RecurringFee});
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: DisplayProduct } });
                res.status(200).json(response);
            }
        ])
    }
}