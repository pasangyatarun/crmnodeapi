exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('field_data_field_s_verein_newsletter', function(t) {
        t.integer('uid',[10]).unsigned().notNull().alter();
        t.tinyint('status').unsigned().notNull().alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('field_data_field_s_verein_newsletter', function(t) {
       
    });
 };