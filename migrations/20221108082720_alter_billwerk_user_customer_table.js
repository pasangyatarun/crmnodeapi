exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('billwerk_user_customer', function(t) {
        t.longtext('cancel_response').after("commit_response");
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('billwerk_user_customer', function(t) {
        
    });
 };