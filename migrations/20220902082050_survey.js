exports.up = async function(knex) {
    return knex.schema.createTable('survey', function(t) {
        t.increments('id').primary();
        t.string('title',[255]).notNull();
        t.string('description',[255]).notNull();
        t.enu('survey_option',['0','1']).notNull().comment('0 -> single seletion 1-> multiple selection	');
        t.string('surveyType',[20]).notNull();
        t.dateTime('survey_start_date').notNull();
        t.dateTime('survey_end_date').notNull();
        t.enu('survey_view_option',['0', '1', '2', '']).notNull();
        t.string('additional_anonymous_voting',[10]);
        t.string('additional_cast_vote',[10]);
        t.string('survey_notification_option',[255]);
        t.integer('user_id').notNull();
        t.integer('status').notNull().defaultTo(1);
        t.string('created_at',[32]).notNull();
        t.string('updated_at',32);

         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('survey');
};
    