export default class SurveyValidation {

    surveyCreate(surveyData) {
        let err = new Array();
        if (!surveyData.title) {
            err.push('title:titleReq');
        }
        if (!surveyData.content) {
            err.push('content:contentReq');
        }
        if (!surveyData.author) {
            err.push('author:authorReq');
        }
        if (!surveyData.votingOption) {
            err.push('votingOption:votingOptionsReq');
        }
        if (!surveyData.surveyAnswers) {
            err.push('surveyAnswer:answerOptionReq');
        }
        if (!surveyData.surveyEndDate) {
            err.push('surveyEndDate:activeUntilReq');
        }
        if (!surveyData.surveyStartDate) {
            err.push('surveyStartDate:activeReq');
        }
        return err;
    }

    surveyAdd(surveyAdd) {
        let err = new Array();
        if (!surveyAdd.surveyId) {
            err.push('surveyId:surveyIdReq');
        }
        if (!surveyAdd.userId) {
            err.push('userId:userIdReq');
        }
        if (!surveyAdd.surveyAnswerId) {
            err.push('surveyAnswerId:surveyAnswerIdReq');
        }
        if (!surveyAdd.surveyType) {
            err.push('surveyType:surveyTypeReq');
        }      
        return err;
    }

    surveyAddTineon(surveyAdd) {
        let err = new Array();
        if (!surveyAdd.surveyId) {
            err.push('surveyId:surveyIdReq');
        }
        if (!surveyAdd.tineon_user_id) {
            err.push('userId:userIdReq');
        }
        if (!surveyAdd.surveyAnswerId) {
            err.push('surveyAnswerId:surveyAnswerIdReq');
        }
        if (!surveyAdd.surveyType) {
            err.push('surveyType:surveyTypeReq');
        }      
        return err;
    }
}