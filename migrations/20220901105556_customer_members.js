exports.up = async function(knex) {
    return knex.schema.createTable('customer_members', function(t) {
        t.increments('id').primary();
        t.string('salutation',[32]).notNull();
        t.integer('customer_id',[11]).notNull();
        t.string('firstname',[255]);
        t.string('lastname',[255]);
        t.string('company_name',[255]);
        t.string('responsible_person',[255]);
        t.string('gender',[32]).notNull();
        t.string('birth_date',[32]);
        t.string('established_date',[32]);
        t.integer('age',[11]);
        t.string('email',[254]).notNull();
        t.string('mobile',[20]);
        t.string('phone',[20]);
        t.string('city',[64]);
        t.string('zipcode',[255]);
        t.text('street');
        t.string('image',[500]);
        t.integer('status',[11]).notNull();
        t.string('function',[64]);
        t.longtext('department');
        t.string('department_entry_date',[64]);
        t.string('club_entry_date',[64]);
        t.string('club_exit_date',[64]);
        t.longtext('reason_for_exit');
        t.longtext('notes');
        t.string('app_in_use',[20]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('customer_members');
};

