exports.up = async function(knex) {
    return knex.schema.createTable('crm_product_order', function(t) {
        t.increments('id').primary();
        t.integer('user_id',[11]).notNull();
        t.integer('crm_product_id',[11]).notNull();
        t.string('account_holder_name',[255]).notNull();
        t.string('iban',[255]).notNull();
        t.string('coupon_code',[255]).notNull();
        t.string('chairman_name',[255]).notNull();
        t.string('association',[255]).notNull();
        t.string('payment_method',[255]).notNull();
        t.text('mandate_text').notNull().notNull();
        t.string('file',[255]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('crm_product_order');
};

