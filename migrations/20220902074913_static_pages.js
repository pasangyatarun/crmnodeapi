exports.up = async function(knex) {
    return knex.schema.createTable('static_pages', function(t) {
        t.increments('id').primary();
        t.integer('uid',[11]).notNull();
        t.string('title',[32]).notNull();
        t.longtext('content',[32]).notNull();
        t.string('url',[254]).notNull();
        t.string('created',[254]).defaultTo(0);
        t.string('modified',[254]);
        t.tinyint('status',[3]);
        t.string('image',[254]);
        t.string('video',[254]);
        t.string('type',[32]);

         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('static_pages');
};
    