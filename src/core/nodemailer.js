/**
 * -------Import all classes and packages -------------
 */
import nodemailer from 'nodemailer';
import configContainer from '../config/localhost';

var sentDataEmail;
let config = configContainer.loadConfig();
let mailOptions = configContainer.emailOptions;
let dispatcher = nodemailer.createTransport(configContainer.emailSenderOptions);

async function sendEmail(options) {
    mailOptions.subject = options.subject;
    mailOptions.to = options.to;
    mailOptions.html = options.html;
    mailOptions.attachments = options.attachments;
    await dispatcher.sendMail(mailOptions)
        .then((info) => {
            sentDataEmail = info.response;
        })
        .catch((error) => {
            sentDataEmail = 'error';
        });
    return sentDataEmail;
}

export default sendEmail;
