exports.up = async function(knex) {
    return knex.schema.createTable('survey_users', function(t) {
        t.increments('id').primary();
        t.integer('survey_id',[11]).notNull();
        t.integer('user_id',[11]).notNull();
        t.integer('approved_status',[11]).defaultTo(0);
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('survey_users');
};