import express   from 'express';
import AuthController from '../../controllers/auth/auth.js';

let router = express.Router();
let authController = new AuthController();

router.post('/signup-user', authController.userRegistration.bind(authController));
router.post('/signup', authController.signupUser.bind(authController));
router.post('/trial-register', authController.trialRegisterUser.bind(authController));
router.post('/emailverify',authController.verifyEmail.bind(authController));
router.post('/signin',authController.signin.bind(authController));
router.post('/signout', authController.signout.bind(authController));
router.post('/resetpasswordlink', authController.passwordReset.bind(authController));
router.post('/resetpassword-linkexpiration', authController.passwordResetLinkCheck.bind(authController));
router.post('/resetpassword', authController.resetPassword.bind(authController));
router.post('/changepassword', authController.changePassword.bind(authController));
router.post('/trialemailverify',authController.verifyTrialEmail.bind(authController));

export default router;