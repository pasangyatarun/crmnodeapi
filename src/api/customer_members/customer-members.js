import express   from 'express';
import CustomerMembersController  from '../../controllers/customer_members/customer-members';
import multerMW from '../../core/multer';

let router = express.Router();
let customerMembersController = new CustomerMembersController();

router.get('/customer-members', customerMembersController.customerMembers.bind(customerMembersController));
router.get('/members-graph/:customer_id', customerMembersController.customerMembersByGraphFilter.bind(customerMembersController));
router.get('/members-filter/:customer_id', customerMembersController.customerMembersByFilter.bind(customerMembersController));
router.get('/membersby-customer/:customer_id', customerMembersController.MemberByCustomerId.bind(customerMembersController));
router.get('/customer-members/:member_id', customerMembersController.MemberById.bind(customerMembersController));
router.get('/customer-members-byemail/:email', customerMembersController.MemberByEmail.bind(customerMembersController));
router.post('/customer-members-add',multerMW.single('file'), customerMembersController.addCustomerMember.bind(customerMembersController));
router.put('/customer-members-update/:id',multerMW.single('file'), customerMembersController.updateCustomerMember.bind(customerMembersController));
router.delete('/customer-members-delete/:id', customerMembersController.deleteCustomerMember.bind(customerMembersController));
router.delete('/members-delete-bycustomer/:customer_id', customerMembersController.deleteMemberByCustomer.bind(customerMembersController));
router.delete('/customer-members-delete', customerMembersController.deleteMultiCustomerMember.bind(customerMembersController));  //expected req.body ---- arr:[21,26,24]
router.put('/members-supportaccess/:uid', customerMembersController.supportAcessMembers.bind(customerMembersController));
router.post('/customer-department-add', customerMembersController.addCustomerDepartment.bind(customerMembersController));
router.put('/customer-department-update/:id', customerMembersController.updateCustomerDepartment.bind(customerMembersController));
router.get('/customer-departments/:customer_id', customerMembersController.getCustomerDepartment.bind(customerMembersController));
router.delete('/customer-departments-delete/:id', customerMembersController.deleteCustomerDepartment.bind(customerMembersController));
router.get('/customer-departmentbyid/:id', customerMembersController.getCustomerDepartmentById.bind(customerMembersController));
router.get('/customer-member-filter/:type/customer/:customer_id', customerMembersController.getCustomerMemberFilter.bind(customerMembersController));

//customer-tags API
router.post('/customer-tag-add', customerMembersController.addCustomerTag.bind(customerMembersController));
router.put('/customer-tag-update/:id', customerMembersController.updateCustomerTag.bind(customerMembersController));
router.get('/customer-tag/:customer_id', customerMembersController.getCustomerTag.bind(customerMembersController));
router.delete('/customer-tag-delete/:id', customerMembersController.deleteCustomerTag.bind(customerMembersController));
router.get('/customer-tagbyid/:id', customerMembersController.getCustomerTagById.bind(customerMembersController));

//Customer-member- search section APIs
router.post('/customer-members-search-add', customerMembersController.addCustomerMemberSearch.bind(customerMembersController));
router.get('/customer-members-search-details/user/:customer_id', customerMembersController.getCustomerMemberSearchRecordByCutomerId.bind(customerMembersController));
router.delete('/customer-members-search-delete/:id', customerMembersController.deleteCustomerMemberSearch.bind(customerMembersController));
router.get('/customer-members-search-byId/:id', customerMembersController.getCustomerMemberSearchById.bind(customerMembersController));



export default router;