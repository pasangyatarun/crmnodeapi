exports.up = async function (knex) {
    return knex.schema.createTable('users_domain', function (t) {
        t.increments('uid').primary();
        t.integer('domain_id ').notNull().defaultTo(0);

     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('users_domain');
};

