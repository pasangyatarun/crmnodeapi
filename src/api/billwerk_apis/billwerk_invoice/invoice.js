import express   from 'express';
import BillwerkInvoiceController from '../../../controllers/billwerk_controllers/billwerk_invoice/invoice';

let router = express.Router();
let billwerkInvoiceController = new BillwerkInvoiceController();

router.get('/billwerk-invoices', billwerkInvoiceController.getInvoices.bind(billwerkInvoiceController));
router.get('/billwerk-invoice-bycustomer/:customer_id/contract/:contract_id', billwerkInvoiceController.getInvoiceByCustomer.bind(billwerkInvoiceController));
router.get('/billwerk-invoice-bycustomer/:customer_id', billwerkInvoiceController.getCustomerInvoices.bind(billwerkInvoiceController));
router.get('/billwerk-invoice-downloadbycustomer/:customer_id', billwerkInvoiceController.getInvoiceDownloadByCustomer.bind(billwerkInvoiceController));
router.get('/billwerk-invoicebyid/:id', billwerkInvoiceController.getInvoiceById.bind(billwerkInvoiceController));
router.get('/billwerk-invoice/downloadbyid/:id', billwerkInvoiceController.getInvoiceDownload.bind(billwerkInvoiceController));
router.get('/billwerk-invoice-downloadlink/:id', billwerkInvoiceController.getInvoiceDownloadLink.bind(billwerkInvoiceController));
router.get('/billwerk-invoice/drafts/:id', billwerkInvoiceController.getInvoicesDraftById.bind(billwerkInvoiceController));
router.get('/billwerk-invoice/drafts', billwerkInvoiceController.getInvoicesDraft.bind(billwerkInvoiceController));
router.post('/billwerk-invoice/drafts/:id', billwerkInvoiceController.InvoicesDraftById.bind(billwerkInvoiceController));

export default router;