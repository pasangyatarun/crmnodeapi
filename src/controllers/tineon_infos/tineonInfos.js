import async from 'async';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import TineonInfosModel from '../../models/tineon_infos/tineonInfos';
import fetch from 'node-fetch';

let tineonInfosModel = new TineonInfosModel();

export default class TineonInfosController {

    /**
    * Function for Create Tineon infor on CRM
    * @author  MangoIt Solutions
    * @param   {object}
    * @return  {success message} 
    */
    tineonInfoCreate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                tineonInfosModel.AddTineonInfo(req).then((tineonInfoAdd) => {
                    if (tineonInfoAdd.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoSuccess']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
        ])

    }

    /**
    * Function to get Tineon Info by pagination
    * @author  MangoIt Solutions
    * @param   {page,pagesize}
    * @return  {object} survey details
    */
    getTineonInfosByPagination(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                tineonInfosModel.getAllTineonInfosByPage(req.params).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoNotExist'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }


    /**
  * Function to delete tineon info by id
  * @author  MangoIt Solutions
  * @param   {id}
  * @return  {message} success message
  */
    tineonInfoDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        tineonInfosModel.deleteTineonInfobyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoNotExist'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }


     /**
    * Function to get voted survey By Answer
    * @author  MangoIt Solutions
    * @param   {answerId}
    * @return  {object} survey details
    */
     getTineonInfobyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                tineonInfosModel.getTineonInfobyId(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['tineon:tineonInfoNotExist'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }
}


