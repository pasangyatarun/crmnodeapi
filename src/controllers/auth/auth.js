import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import async from 'async';
import AuthValidation from '../../validators/auth/auth.js';
import UserModel from '../../models/user/user.js';
import sendEmail from '../../core/nodemailer.js';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import CommonMethods from '../../core/common-methods';
import LicenceModel from '../../models/licence/licence';
import EmailTemplateModel from '../../models/email_template/email-template'
import { dashLogger } from '../../../logs/logger';

let userModel = new UserModel();
let licenceModel = new LicenceModel();
let authValidation = new AuthValidation();
let commomMethods = new CommonMethods();
let emailTemplateModel = new EmailTemplateModel();

export default class AuthController {
  constructor() {
  }

  /**
   * Function to register User
   * @author  MangoIt Solutions
   * @param   {object} with user details
   * @return  {message} Succes message
   */
  userRegistration(req, res, next) {
    let inserId;
    let gId;
    let file;
    let s3URLofImage;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = authValidation.userRegistrationValidation(req.body);

    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    }
    else {
      let templateData;
      let mailOptions;
      async.waterfall([
        function (done) {
          userModel.checkEmailExists(req.body.email).then((data) => {
            if (data) {
              response = responseFormat.getResponseMessageByCodes(['userAdd:emailExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.userRegister(req).then((resData) => {
            if (resData.affectedRows > 0) {
              inserId = resData.insertId;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['userAdd:registrationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.getAddressCreate(req, inserId).then((addressUpdated) => {
            if (addressUpdated.affectedRows > 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['userAdd:userAddressUpdationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Registration').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let link = configContainer.emailVerificationLink;
          let encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime()) + '/' + req.body.productId + '/' + req.body.productType;
          // if (templateData && templateData.length > 0) {
          //     mailOptions = {
          //         to: req.body.email,
          //         subject: templateData[0].subject,
          //         html: `<!doctype html>
          //         <html lang="en-US">

          //         <head>
          //         <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
          //         <title>Reset Password Email</title>
          //         <meta name="description" content="Reset Password Email Template.">
          //         <style type="text/css">
          //         a:hover {text-decoration: underline !important;}
          //         </style>
          //         </head>

          //         <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
          //         <!--100% body table-->
          //         <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
          //         style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
          //         <tr>
          //         <td>
          //         <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
          //         align="center" cellpadding="0" cellspacing="0">
          //         <tr>
          //         <td style="height:80px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td style="text-align:center;">
          //         <a href="#" title="logo" target="_blank">
          //         <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
          //         </a>
          //         </td>
          //         </tr>
          //         <tr>
          //         <td style="height:20px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td>
          //         <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
          //         style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
          //         <tr>
          //         <td style="height:40px;">&nbsp;</td>
          //         </tr>
          //         <tr>
          //         <td style="padding:0 35px;">
          //         <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
          //                                                 `+ templateData[0].template_body + `:<br>
          //                                                 <a href="`+ encrypted + `">` + encrypted + `</a>
          //                                                 </p><br><br>
          //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
          //                                                 <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
          //                                                 <!-- <a href="javascript:void(0);"
          //                                                 style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
          //                                                     Password</a> -->
          //                                                     </td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="height:40px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     </table>
          //                                                     </td>
          //                                                     <tr>
          //                                                     <td style="height:20px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="text-align:center;">
          //                                                     <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
          //                                                     </td>
          //                                                     </tr>
          //                                                     <tr>
          //                                                     <td style="height:80px;">&nbsp;</td>
          //                                                     </tr>
          //                                                     </table>
          //                     </td>
          //                 </tr>
          //             </table>
          //             <!--/100% body table-->
          //             </body>

          //             </html>`,
          //     }
          // } else {
          //     mailOptions = {
          //         to: req.body.email,
          //         subject: 'Bitte bestätigen Sie Ihre Email-Adresse (Please confirm your email address)',
          //         html: `<h3>Dear association member,</h3>

          //             <h4>Thank you for your interest in our S-Club |
          //             Online club management.</h4>

          //            <h5> Please confirm your email address via the following link: </h5>
          //          <a href="`+ encrypted + `">` + encrypted + `</a>
          //          <h3>If the forwarding does not work, please copy the link in
          //             the address bar of your browser.

          //             After you have successfully confirmed your e-mail address, the access data will be sent
          //             sent to the specified email address.</h3>`,
          //     }
          // }
          mailOptions = {
            to: req.body.email,
            subject: 'Bitte bestätigen Sie Ihre Email-Adresse',
            html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                      <head>
                      <!--[if (gte mso 9)|(IE)]>
                        <xml>
                          <o:OfficeDocumentSettings>
                          <o:AllowPNG/>
                          <o:PixelsPerInch>96</o:PixelsPerInch>
                        </o:OfficeDocumentSettings>
                      </xml>
                      <![endif]-->
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                      <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                      <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                      <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                      <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                      <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                      <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                      <meta name="color-scheme" content="only">
                      <title></title>
                      
                      <link rel="preconnect" href="https://fonts.gstatic.com">
                      <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                      <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                      
                      <style type="text/css">
                      /*Basics*/
                      body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                      table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                      table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                      td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                      td p {margin:0; padding:0;}
                      td div {margin:0; padding:0;}
                      td a {text-decoration:none; color: inherit;} 
                      /*Outlook*/
                      .ExternalClass {width: 100%;}
                      .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                      .ReadMsgBody {width:100%; background-color: #ffffff;}
                      /* iOS BLUE LINKS */
                      a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                      /*Gmail blue links*/
                      u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                      /*Buttons fix*/
                      .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                      .yshortcuts a {border-bottom:none !important;
                      
                      .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                      /*Responsive*/
                      @media screen and (max-width: 639px) {
                        table.row {width: 100%!important;max-width: 100%!important;}
                        td.row {width: 100%!important;max-width: 100%!important;}
                        .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                        .center-float {float: none!important;margin:auto!important;}
                        .center-text{text-align: center!important;}
                        .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                        .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                        .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                        .hide-mobile {display: none!important;}
                        .menu-container {text-align: center !important;}
                        .autoheight {height: auto!important;}
                        .m-padding-10 {margin: 10px 0!important;}
                        .m-padding-15 {margin: 15px 0!important;}
                        .m-padding-20 {margin: 20px 0!important;}
                        .m-padding-30 {margin: 30px 0!important;}
                        .m-padding-40 {margin: 40px 0!important;}
                        .m-padding-50 {margin: 50px 0!important;}
                        .m-padding-60 {margin: 60px 0!important;}
                        .m-padding-top10 {margin: 30px 0 0 0!important;}
                        .m-padding-top15 {margin: 15px 0 0 0!important;}
                        .m-padding-top20 {margin: 20px 0 0 0!important;}
                        .m-padding-top30 {margin: 30px 0 0 0!important;}
                        .m-padding-top40 {margin: 40px 0 0 0!important;}
                        .m-padding-top50 {margin: 50px 0 0 0!important;}
                        .m-padding-top60 {margin: 60px 0 0 0!important;}
                        .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                        .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                        .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                        .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                        .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                        .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                        .center-on-mobile {text-align: center!important;}
                      }
                      </style>
                      </head>
                      
                      <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                      
                      <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                      
                      <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                        <tr><!-- Outer Table -->
                          <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                        <!-- Preheader -->
                        <tr>
                          <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="center-text">
                            <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                          </td>
                        </tr>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <!-- Preheader -->
                      </table>
                      
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                        <!-- simpli-header-1 -->
                        <tr>
                          <td align="center">
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                        <!-- bg-image -->
                        <tr>
                          <td align="center" style="border-radius: 36px;">
                      
                      <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                      <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                      <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                      
                      <div>
                      <!-- simpli-header-bg-image -->
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                        <tr>
                          <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                      
                      <!-- Content -->
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                        <tr>
                          <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                      
                          <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                            <tr>
                              <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                            </tr>
                          </table>
                      
                          </td>
                        </tr>
                      </table>
                      <!-- Content -->
                      
                          </td>
                        </tr>
                      </table>
                      <!-- simpli-header-bg-image -->
                      </div>
                      
                      <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                      
                          </td>
                        </tr>
                        <!-- bg-image -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                        <!-- basic-info -->
                        <tr>
                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                            <!-- content -->
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          HERZLICH WILLKOMMEN!
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          Bestätigung E-Mailadresse
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          wir freuen uns, dass Sie Interesse an der cloudbasierten S-Verein Vereinsverwaltung haben.
                                          Bestätigen Sie zunächst über den nachfolgenden Link Ihre E-Mail Adresse. 
                                          Danach können Sie ein persönliches Passwort festlegen und erhalten einen sicheren Zugang zu unserem Bestellsystem.
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <!-- Button -->
                                  <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                    <tr>
                                      <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                  <!--[if (gte mso 9)|(IE)]>
                                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                                      <tr>
                                        <td align="center" width="35"></td>
                                        <td align="center" height="50" style="height:50px;">
                                        <![endif]-->
                                          <singleline>
                                            <a href="`+ encrypted + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>BESTÄTIGEN</span></a>
                                          </singleline>
                                        <!--[if (gte mso 9)|(IE)]>
                                        </td>
                                        <td align="center" width="35"></td>
                                      </tr>
                                    </table>
                                  <![endif]-->
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- Buttons -->
                                </td>
                              </tr>
                              <tr>
                                <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                              </tr>
                            </table>
                            <!-- content -->
                          </td>
                        </tr>
                        <!-- basic-info -->
                      </table>
                      
                          </td>
                        </tr>
                        <!-- simpli-header-1 -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                        <!-- content-1A -->
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                            <!-- content -->
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                              <tr>
                                <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                          Weitere Informationen!
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                    <singleline>
                                      <div mc:edit Simpli>
                                        Wie geht es weiter?
                                      </div>
                                    </singleline>
                                </td>
                              </tr>
                              <tr>
                                <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                
                                <!-- 2-columns -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center">
                      
                                    <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                      <tr>
                                        <td align="center">
                                          <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/mouseclick.png" border="0" editable="true" alt="icon">
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- gap -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                      <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;"></td>
                                      </tr>
                                    </table>
                                    <!-- gap -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                      <tr>
                                        <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                                  Ausführlich Testen
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                                  Die S-Verein Vereinsverwaltung können Sie mit Echtdaten testen.<br />
                                                  Nach der Testphase werden diese Daten entweder rückstandslos gelöscht oder, sofern Sie die Lizenz bis dahin verbindlich bestellen, bleiben sie erhalten.
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                      
                                    </td>
                                  </tr>
                                </table>
                                <!-- 2-columns -->
                      
                                </td>
                              </tr>
                              <tr>
                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                              </tr>
                            </table>
                            <!-- content -->
                          </td>
                        </tr>
                        <!-- content-1A -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                        <!-- content-1B -->
                        <tr>
                          <td align="center" Simpli bgcolor="#FFFFFF">
                            <!-- content -->
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                              <tr>
                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                
                                <!-- 2-columns -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center">
                      
                                    <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                      <tr>
                                        <td align="center">
                                          <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/usericon.png" border="0" editable="true" alt="icon">
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- gap -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                      <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;"></td>
                                      </tr>
                                    </table>
                                    <!-- gap -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                      <tr>
                                        <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                                  Datenimport
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                              Sie können die Daten über eine CSV-Schnittstelle unter Verwendung der Online-Hilfe (<a href="https://hilfe.s-verein.de/help/205">https://hilfe.s-verein.de/help/205</a>) selbst importieren oder durch uns als Dienstleistung kostenpflichtig importieren lassen. Wenn wir die Daten importieren sollen, erwarten wir, dass Sie den Einrichtungsassistent abgeschlossen haben. Das beinhaltet die Anlage von Abteilungen und Beiträgen. Hängen Sie in der Bestellung bitte den Export im CSV/TXT Format an.
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                      
                                    </td>
                                  </tr>
                                </table>
                                <!-- 2-columns -->
                      
                                </td>
                              </tr>
                              <tr>
                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                              </tr>
                            </table>
                            <!-- content -->
                          </td>
                        </tr>
                        <!-- content-1B -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                        <!-- content-1C -->
                        <tr>
                          <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                            <!-- content -->
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                              <tr>
                                <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center">
                                
                                <!-- 2-columns -->
                                <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                  <tr>
                                    <td align="center">
                      
                                    <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                      <tr>
                                        <td align="center">
                                          <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/fragezeichen.png" border="0" editable="true" alt="icon">
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- gap -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                      <tr>
                                        <td height="20" style="font-size:20px;line-height:20px;"></td>
                                      </tr>
                                    </table>
                                    <!-- gap -->
                      
                                    <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                      
                                    <!-- column -->
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                      <tr>
                                        <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                                  Noch Fragen?
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;padding-bottom: 25px;">
                                            <singleline>
                                              <div mc:edit Simpli>
                                                  Wenn Sie Hilfestellung benötigen, unterstützen wir Sie gerne mit Tutorials zu den einzelnen Funktionen oder gerne auch mit unserem E-Mail Support unter support@s-verein.de
                                              </div>
                                            </singleline>
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- column -->
                                    <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                      <tr>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                          <td align="center" width="35"></td>
                                          <td align="center" height="50" style="height:50px;">
                                          <![endif]-->
                                            <singleline>
                                              <a href="https://hilfe.s-verein.de/" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Hilfe-Seite</span></a>
                                            </singleline>
                                          <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                          <td align="center" width="35"></td>
                                        </tr>
                                      </table>
                                    <![endif]-->
                                        </td>
                                        <td>
                                          <pre>           </pre>
                                        </td>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                          <!--[if (gte mso 9)|(IE)]>
                                            <table border="0" cellpadding="0" cellspacing="0" align="center">
                                              <tr>
                                                <td align="center" width="35"></td>
                                                <td align="center" height="50" style="height:50px;">
                                                <![endif]-->
                                                  <singleline>
                                                    <a href="https://www.youtube.com/@supports-verein6588" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>TUTORIALS</span></a>
                                                  </singleline>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td align="center" width="35"></td>
                                              </tr>
                                            </table>
                                          <![endif]-->
                                              </td>
                                      </tr>
                                    </table>
                      
                                    <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                      
                                    </td>
                                  </tr>
                                </table>
                                <!-- 2-columns -->
                      
                                </td>
                              </tr>
                              <tr>
                                <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                              </tr>
                            </table>
                            <!-- content -->
                          </td>
                        </tr>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <!-- content-1C -->
                      </table>
                      
                      <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                        <!-- simpli-footer -->
                        <tr>
                          <td align="center">
                            
                      <!-- Content -->
                      
                        <tr>
                          <td align="center">
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                              <tr>
                                <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                  <multiline>
                                    <div mc:edit Simpli>
                                      Tineon Aktiengesellschaft <br />
                                      Uferpromenade 5, 88709 Meersburg <br />
                                      Registergericht Freiburg HRB 710927 <br />
                                      Vorstand: Jean-Claude Parent <br />
                                      Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                      USt.-IdNr. DE203912818 <br />
                                    </div>
                                  </multiline>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center" class="center-text">
                            <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                          </td>
                        </tr>
                        <tr>
                          <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                        </tr>
                      </table>
                      <!-- Content -->
                      
                          </td>
                        </tr>
                        <!-- simpli-footer -->
                      </table>
                      
                          </td>
                        </tr><!-- Outer-Table -->
                      </table>
                      
                      </body>
                      </html>
                      `,
          }

          sendEmail(mailOptions).then((sentdata) => {

            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['userAdd:registeredSuccessMailNotSent']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['userAdd:registeredSuccess']);
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
      ]);
    }
  }

  /**
   * Function for trial product register User
   * @author  MangoIt Solutions
   * @param   {object} with user details
   * @return  {message} Succes message
   */
  trialRegisterUser(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = authValidation.signupNewValidation(req.body);

    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    }
    else {
      let insId;
      let gId;
      let templateData;
      let mailOptions;
      async.waterfall([
        function (done) {
          userModel.checkEmailExists(req.body.email).then((data) => {
            if (data) {
              response = responseFormat.getResponseMessageByCodes(['Signup:emailExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.signupTrialUser(req).then((data) => {
            if (data.affectedRows > 0) {
              insId = data.insertId;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registrationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.signupUserSociety(req, insId).then((data) => {
            if (data.affectedRows > 0) {
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registrationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Trial Registration').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let link = configContainer.trialVerificationLink;
          let encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime());
          mailOptions = {
            to: req.body.email,
            subject: 'Bitte bestätigen Sie Ihre Email-Adresse',
            html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                        <head>
                        <!--[if (gte mso 9)|(IE)]>
                          <xml>
                            <o:OfficeDocumentSettings>
                            <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                          </o:OfficeDocumentSettings>
                        </xml>
                        <![endif]-->
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                        <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                        <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                        <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                        <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                        <meta name="color-scheme" content="only">
                        <title></title>
                        
                        <link rel="preconnect" href="https://fonts.gstatic.com">
                        <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                        
                        <style type="text/css">
                        /*Basics*/
                        body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                        table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                        table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                        td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                        td p {margin:0; padding:0;}
                        td div {margin:0; padding:0;}
                        td a {text-decoration:none; color: inherit;} 
                        /*Outlook*/
                        .ExternalClass {width: 100%;}
                        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                        .ReadMsgBody {width:100%; background-color: #ffffff;}
                        /* iOS BLUE LINKS */
                        a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                        /*Gmail blue links*/
                        u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                        /*Buttons fix*/
                        .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                        .yshortcuts a {border-bottom:none !important;
                        
                        .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                        /*Responsive*/
                        @media screen and (max-width: 639px) {
                          table.row {width: 100%!important;max-width: 100%!important;}
                          td.row {width: 100%!important;max-width: 100%!important;}
                          .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                          .center-float {float: none!important;margin:auto!important;}
                          .center-text{text-align: center!important;}
                          .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                          .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                          .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                          .hide-mobile {display: none!important;}
                          .menu-container {text-align: center !important;}
                          .autoheight {height: auto!important;}
                          .m-padding-10 {margin: 10px 0!important;}
                          .m-padding-15 {margin: 15px 0!important;}
                          .m-padding-20 {margin: 20px 0!important;}
                          .m-padding-30 {margin: 30px 0!important;}
                          .m-padding-40 {margin: 40px 0!important;}
                          .m-padding-50 {margin: 50px 0!important;}
                          .m-padding-60 {margin: 60px 0!important;}
                          .m-padding-top10 {margin: 30px 0 0 0!important;}
                          .m-padding-top15 {margin: 15px 0 0 0!important;}
                          .m-padding-top20 {margin: 20px 0 0 0!important;}
                          .m-padding-top30 {margin: 30px 0 0 0!important;}
                          .m-padding-top40 {margin: 40px 0 0 0!important;}
                          .m-padding-top50 {margin: 50px 0 0 0!important;}
                          .m-padding-top60 {margin: 60px 0 0 0!important;}
                          .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                          .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                          .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                          .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                          .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                          .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                          .center-on-mobile {text-align: center!important;}
                        }
                        </style>
                        </head>
                        
                        <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                        
                        <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                        
                        <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                          <tr><!-- Outer Table -->
                            <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- Preheader -->
                          <tr>
                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- Preheader -->
                        </table>
                        
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- simpli-header-1 -->
                          <tr>
                            <td align="center">
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                          <!-- bg-image -->
                          <tr>
                            <td align="center" style="border-radius: 36px;">
                        
                        <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                        <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                        <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                        
                        <div>
                        <!-- simpli-header-bg-image -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                          <tr>
                            <td align="center" data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px; background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png') ">
                        
                        <!-- Content -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                          <tr>
                            <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                        
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                              <tr>
                                <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                              </tr>
                            </table>
                        
                            </td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                        </table>
                        <!-- simpli-header-bg-image -->
                        </div>
                        
                        <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                        
                            </td>
                          </tr>
                          <!-- bg-image -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                          <!-- basic-info -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            HERZLICH WILLKOMMEN!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Bestätigung E-Mailadresse
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            wir freuen uns, dass Sie Interesse an der cloudbasierten S-Verein Vereinsverwaltung haben.
                                            Bestätigen Sie zunächst über den nachfolgenden Link Ihre E-Mail Adresse. 
                                            Danach können Sie ein persönliches Passwort festlegen und erhalten einen sicheren Zugang zu unserem Bestellsystem.
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <!-- Button -->
                                    <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                      <tr>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                          <td align="center" width="35"></td>
                                          <td align="center" height="50" style="height:50px;">
                                          <![endif]-->
                                            <singleline>
                                              <a href="` + encrypted + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>BESTÄTIGEN</span></a>
                                            </singleline>
                                          <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                          <td align="center" width="35"></td>
                                        </tr>
                                      </table>
                                    <![endif]-->
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Buttons -->
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- basic-info -->
                        </table>
                        
                            </td>
                          </tr>
                          <!-- simpli-header-1 -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1A -->
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:36px 36px 0 0;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Weitere Informationen!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:28px;line-height:34px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                          Folgen Sie diesen Schritten!
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/mouseclick.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Ausführlich Testen
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Die S-Verein Vereinsverwaltung können Sie mit Echtdaten testen.<br />
                                                    Nach der Testphase werden diese Daten entweder rückstandslos gelöscht oder, sofern Sie die Lizenz bis dahin verbindlich bestellen, bleiben sie erhalten.
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- content-1A -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1B -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/usericon.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Datenimport
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                Sie können die Daten über eine CSV-Schnittstelle unter Verwendung der Online-Hilfe (<a href="https://hilfe.s-verein.de/help/205">https://hilfe.s-verein.de/help/205</a>) selbst importieren oder durch uns als Dienstleistung kostenpflichtig importieren lassen. Wenn wir die Daten importieren sollen, erwarten wir, dass Sie den Einrichtungsassistent abgeschlossen haben. Das beinhaltet die Anlage von Abteilungen und Beiträgen. Hängen Sie in der Bestellung bitte den Export im CSV/TXT Format an.
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- content-1B -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;" Simpli>
                          <!-- content-1C -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                  
                                  <!-- 2-columns -->
                                  <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                                    <tr>
                                      <td align="center">
                        
                                      <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="80" style="width:80px;max-width:80px;">
                                        <tr>
                                          <td align="center">
                                            <img style="display:block;width:100%;max-width:80px;border:0px;" data-image-edit  width="80" src="https://www.s-verein.de/sites/crm/files/fragezeichen.png" border="0" editable="true" alt="icon">
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- gap -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="30" style="width:30px;max-width:30px;">
                                        <tr>
                                          <td height="20" style="font-size:20px;line-height:20px;"></td>
                                        </tr>
                                      </table>
                                      <!-- gap -->
                        
                                      <!--[if (gte mso 9)|(IE)]></td><td><![endif]-->
                        
                                      <!-- column -->
                                      <table border="0" align="left" cellpadding="0" cellspacing="0" role="presentation" class="row" width="410" style="width:410px;max-width:410px;">
                                        <tr>
                                          <td class="center-text" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:20px;line-height:22px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Noch Fragen?
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td height="5" style="font-size:5px;line-height:5px;" Simpli>&nbsp;</td>
                                        </tr>
                                        <tr>
                                          <td class="center-text container-padding" Simpli align="left" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:14px;line-height:20px;font-weight:400;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;padding-bottom: 25px;">
                                              <singleline>
                                                <div mc:edit Simpli>
                                                    Wenn Sie Hilfestellung benötigen, unterstützen wir Sie gerne mit Tutorials zu den einzelnen Funktionen oder gerne auch mit unserem E-Mail Support unter support@s-verein.de
                                                </div>
                                              </singleline>
                                          </td>
                                        </tr>
                                      </table>
                                      <!-- column -->
                                      <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                        <tr>
                                          <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                      <!--[if (gte mso 9)|(IE)]>
                                        <table border="0" cellpadding="0" cellspacing="0" align="center">
                                          <tr>
                                            <td align="center" width="35"></td>
                                            <td align="center" height="50" style="height:50px;">
                                            <![endif]-->
                                              <singleline>
                                                <a href="https://hilfe.s-verein.de/" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Hilfe-Seite</span></a>
                                              </singleline>
                                            <!--[if (gte mso 9)|(IE)]>
                                            </td>
                                            <td align="center" width="35"></td>
                                          </tr>
                                        </table>
                                      <![endif]-->
                                          </td>
                                          <td>
                                            <pre>           </pre>
                                          </td>
                                          <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                            <!--[if (gte mso 9)|(IE)]>
                                              <table border="0" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                  <td align="center" width="35"></td>
                                                  <td align="center" height="50" style="height:50px;">
                                                  <![endif]-->
                                                    <singleline>
                                                      <a href="https://www.youtube.com/@supports-verein6588" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>TUTORIALS</span></a>
                                                    </singleline>
                                                  <!--[if (gte mso 9)|(IE)]>
                                                  </td>
                                                  <td align="center" width="35"></td>
                                                </tr>
                                              </table>
                                            <![endif]-->
                                                </td>
                                        </tr>
                                      </table>
                        
                                      <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                        
                                      </td>
                                    </tr>
                                  </table>
                                  <!-- 2-columns -->
                        
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- content-1C -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                          <!-- simpli-footer -->
                          <tr>
                            <td align="center">
                              
                        <!-- Content -->
                        
                          <tr>
                            <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                    <multiline>
                                      <div mc:edit Simpli>
                                        Tineon Aktiengesellschaft <br />
                                        Uferpromenade 5, 88709 Meersburg <br />
                                        Registergericht Freiburg HRB 710927 <br />
                                        Vorstand: Jean-Claude Parent <br />
                                        Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                        USt.-IdNr. DE203912818 <br />
                                      </div>
                                    </multiline>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                          <!-- simpli-footer -->
                        </table>
                        
                            </td>
                          </tr><!-- Outer-Table -->
                        </table>
                        
                        </body>
                        </html>`,
          }

          sendEmail(mailOptions).then((sentdata) => {
            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['Signup:registeredSuccessAsGuest']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registeredSuccessLink']);
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }
      ]);
    }
  }

  /**
   * Function to change password
   * @author  MangoIt Solutions
   * @param   {object} with oldpassword,newpassword
   * @return  {message} Succes message
   */
  changePassword(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = authValidation.passwordValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          userModel.getUserPassword(req.body.id).then((data) => {
            if (data[0]) {
              bcrypt.compare(req.body.oldPassword, data[0].pass).then((passMatch) => {
                if (!passMatch) {
                  response = responseFormat.getResponseMessageByCodes(['reset:oldPassword'], { code: 417 });
                  res.status(200).json(response)
                } else if (req.body.oldPassword == req.body.password) {
                  response = responseFormat.getResponseMessageByCodes(['reset:oldNewPassSame'], { code: 417 });
                  res.status(200).json(response)
                } else {
                  done();
                }
              }).catch((error) => {
                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                res.status(500).json(response);
                return;
              })
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let hashPass;
          bcrypt.hash(req.body.password, 10, function (err, hash) {
            hashPass = hash;
            userModel.changeUserPassword(req.body.id, hashPass).then((emailVerify) => {
              if (emailVerify.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['reset:passwordChangedSuccess']);
                res.status(200).json(response);
              } else {
                response = responseFormat.getResponseMessageByCodes(['reset:passwordChangedFail'], { code: 417 });
                res.status(200).json(response)
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })

          })
        }
      ])
    }
  }

  /**
   * Function to reset password
   * @author  MangoIt Solutions
   * @param   {object} with oldpassword,newpassword
   * @return  {message} Succes message
   */
  resetPassword(req, res, next) {
    let inssid
    let splited;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        if (req.body.id) {
          commomMethods.decrypt(req.body.id).then((dec) => {
            splited = dec.split('||');
            var datetime = new Date();
            const dates = commomMethods.getDates(new Date(splited[1]), new Date(datetime));
            if (dates.length < 2) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['reset:linkExpired'], { code: 417 });
              res.status(200).json(response)
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        } else {
          response = responseFormat.getResponseMessageByCodes(['reset:invalidLink'], { code: 417 });
          res.status(200).json(response)
        }
      },
      function (done) {
        userModel.getUserByEmail(splited[0]).then((userfind) => {
          if (userfind.length > 0 && userfind[0].reset_password_token) {
            inssid = userfind[0].uid;
            done();
          } else if (!userfind[0].reset_password_token) {
            response = responseFormat.getResponseMessageByCodes(['reset:alreadyChangedPassword'], { code: 417 });
            res.status(200).json(response)
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['reset:emailNotExists'], { code: 417 });
            res.status(200).json(response)
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function () {
        let msgCode = authValidation.passwordValidation(req.body);
        if (msgCode && msgCode.length) {
          response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
          res.status(200).json(response);
        } else {
          let hashPass;
          bcrypt.hash(req.body.password, 10, function (err, hash) {
            hashPass = hash;
            let dt = new Date().getTime();
            let dateTime = dt;
            userModel.updateUserPassword(splited[0], hashPass).then((emailVerify) => {
              if (emailVerify.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['reset:passwordChangedSuccess']);
                res.status(200).json(response);
              } else {
                response = responseFormat.getResponseMessageByCodes(['reset:verificationFail'], { code: 417 });
                res.status(200).json(response)
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          })
        }
      }
    ])
  }

  /**
       * Function to  password reset link expiration check
       * @author  MangoIt Solutions
       * @param   {object} with id
       * @return  {message} Succes message
       */
  passwordResetLinkCheck(req, res, next) {
    let inssid
    let splited;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        if (req.body.id) {
          commomMethods.decrypt(req.body.id).then((dec) => {
            splited = dec.split('||');
            var datetime = new Date();
            const dates = commomMethods.getDates(new Date(splited[1]), new Date(datetime));
            if (dates.length < 2) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['reset:linkExpired'], { code: 417 });
              res.status(200).json(response)
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        } else {
          response = responseFormat.getResponseMessageByCodes(['reset:invalidLink'], { code: 417 });
          res.status(200).json(response)
        }
      },
      function (done) {
        userModel.getUserByEmail(splited[0]).then((userfind) => {
          if (userfind.length > 0 && userfind[0].reset_password_token) {
            response = responseFormat.getResponseMessageByCodes(['reset:changePassword']);
            res.status(200).json(response);
          } else if (!userfind[0].reset_password_token) {
            response = responseFormat.getResponseMessageByCodes(['reset:alreadyChangedPassword'], { code: 417 });
            res.status(200).json(response)
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['reset:emailNotExists'], { code: 417 });
            res.status(200).json(response)
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      }
    ])
  }

  /**
   * Function to  password reset
   * @author  MangoIt Solutions
   * @param   {object} with email
   * @return  {message} Succes message
   */
  passwordReset(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let templateData;
    let mailOptions;
    async.waterfall([
      function (done) {
        userModel.checkEmailExists(req.body.email).then((data) => {
          if (data) {
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['forget:emailNotExists'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          console.log("---1816--",error)
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        userModel.storeResetPasswordToken(req.body.email).then((data) => {
          if (data && data.affectedRows > 0) {
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['forget:emailNotExists'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          console.log("---1832--",error)
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        emailTemplateModel.getemailTemplateByType('Password Reset').then((data) => {
          if (data.length > 0) {
            templateData = data;
            done();
          }
          else {
            done();
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        let link = configContainer.resetPasswordLink;
        let encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime());
        // if (templateData && templateData.length > 0) {
        //     mailOptions = {
        //         to: req.body.email,
        //         subject: templateData[0].subject,
        //         html: `<!doctype html>
        //         <html lang="en-US">

        //         <head>
        //         <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        //         <title>Reset Password Email</title>
        //         <meta name="description" content="Reset Password Email Template.">
        //         <style type="text/css">
        //         a:hover {text-decoration: underline !important;}
        //         </style>
        //         </head>

        //         <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
        //         <!--100% body table-->
        //         <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
        //         style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
        //         <tr>
        //         <td>
        //         <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
        //         align="center" cellpadding="0" cellspacing="0">
        //         <tr>
        //         <td style="height:80px;">&nbsp;</td>
        //         </tr>
        //         <tr>
        //         <td style="text-align:center;">
        //         <a href="#" title="logo" target="_blank">
        //         <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
        //         </a>
        //         </td>
        //         </tr>
        //         <tr>
        //         <td style="height:20px;">&nbsp;</td>
        //         </tr>
        //         <tr>
        //         <td>
        //         <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
        //         style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
        //         <tr>
        //         <td style="height:40px;">&nbsp;</td>
        //         </tr>
        //         <tr>
        //         <td style="padding:0 35px;">
        //         <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

        //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

        //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
        //                                                 `+ templateData[0].template_body + `:<br>
        //                                                 <a href="`+ encrypted + `">` + encrypted + `</a>
        //                                                 </p><br><br>
        //                                                 <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
        //                                                 <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
        //                                                 <!-- <a href="javascript:void(0);"
        //                                                 style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
        //                                                     Password</a> -->
        //                                                     </td>
        //                                                     </tr>
        //                                                     <tr>
        //                                                     <td style="height:40px;">&nbsp;</td>
        //                                                     </tr>
        //                                                     </table>
        //                                                     </td>
        //                                                     <tr>
        //                                                     <td style="height:20px;">&nbsp;</td>
        //                                                     </tr>
        //                                                     <tr>
        //                                                     <td style="text-align:center;">
        //                                                     <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
        //                                                     </td>
        //                                                     </tr>
        //                                                     <tr>
        //                                                     <td style="height:80px;">&nbsp;</td>
        //                                                     </tr>
        //                                                     </table>
        //                     </td>
        //                 </tr>
        //             </table>
        //             <!--/100% body table-->
        //             </body>

        //             </html>`,

        //     };
        // } else {
        //     mailOptions = {
        //         to: req.body.email,
        //         subject: 'Please reset your password (Please reset your password)',
        //         html: `<h3>Dear association member,</h3>

        //             <h4>Thank you for your interest in our S-Club |
        //             Online club management.</h4>

        //            <h5> Please reset your password via the following link: </h5>
        //          <a href="`+ encrypted + `">` + encrypted + `</a>
        //          <h3>If the forwarding does not work, please copy the link in
        //             the address bar of your browser.</h3>`,
        //     };
        // }
        mailOptions = {
          to: req.body.email,
          subject: 'Passwort zurücksetzen',
          html: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
                        <head>
                        <!--[if (gte mso 9)|(IE)]>
                          <xml>
                            <o:OfficeDocumentSettings>
                            <o:AllowPNG/>
                            <o:PixelsPerInch>96</o:PixelsPerInch>
                          </o:OfficeDocumentSettings>
                        </xml>
                        <![endif]-->
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
                        <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
                        <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
                        <meta name="format-detection" content="date=no"> <!-- disable auto date linking in iOS -->
                        <meta name="format-detection" content="address=no"> <!-- disable auto address linking in iOS -->
                        <meta name="format-detection" content="email=no"> <!-- disable auto email linking in iOS -->
                        <meta name="color-scheme" content="only">
                        <title></title>
                        
                        <link rel="preconnect" href="https://fonts.gstatic.com">
                        <link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
                        <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
                        
                        <style type="text/css">
                        /*Basics*/
                        body {margin:0px !important; padding:0px !important; display:block !important; min-width:100% !important; width:100% !important; -webkit-text-size-adjust:none;}
                        table {border-spacing:0; mso-table-lspace:0pt; mso-table-rspace:0pt;}
                        table td {border-collapse: collapse;mso-line-height-rule:exactly;}
                        td img {-ms-interpolation-mode:bicubic; width:auto; max-width:auto; height:auto; margin:auto; display:block!important; border:0px;}
                        td p {margin:0; padding:0;}
                        td div {margin:0; padding:0;}
                        td a {text-decoration:none; color: inherit;} 
                        /*Outlook*/
                        .ExternalClass {width: 100%;}
                        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height:inherit;}
                        .ReadMsgBody {width:100%; background-color: #ffffff;}
                        /* iOS BLUE LINKS */
                        a[x-apple-data-detectors] {color:inherit !important; text-decoration:none !important; font-size:inherit !important; font-family:inherit !important; font-weight:inherit !important; line-height:inherit !important;} 
                        /*Gmail blue links*/
                        u + #body a {color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit;}
                        /*Buttons fix*/
                        .undoreset a, .undoreset a:hover {text-decoration:none !important;}
                        .yshortcuts a {border-bottom:none !important;
                        
                        .ios-footer a {color:#aaaaaa !important;text-decoration:none;}
                        /*Responsive*/
                        @media screen and (max-width: 639px) {
                          table.row {width: 100%!important;max-width: 100%!important;}
                          td.row {width: 100%!important;max-width: 100%!important;}
                          .img-responsive img {width:100%!important;max-width: 100%!important;height: auto!important;margin: auto;}
                          .center-float {float: none!important;margin:auto!important;}
                          .center-text{text-align: center!important;}
                          .container-padding {width: 100%!important;padding-left: 15px!important;padding-right: 15px!important;}
                          .container-padding10 {width: 100%!important;padding-left: 10px!important;padding-right: 10px!important;}
                          .container-padding25 {width: 100%!important;padding-left: 25px!important;padding-right: 25px!important;}
                          .hide-mobile {display: none!important;}
                          .menu-container {text-align: center !important;}
                          .autoheight {height: auto!important;}
                          .m-padding-10 {margin: 10px 0!important;}
                          .m-padding-15 {margin: 15px 0!important;}
                          .m-padding-20 {margin: 20px 0!important;}
                          .m-padding-30 {margin: 30px 0!important;}
                          .m-padding-40 {margin: 40px 0!important;}
                          .m-padding-50 {margin: 50px 0!important;}
                          .m-padding-60 {margin: 60px 0!important;}
                          .m-padding-top10 {margin: 30px 0 0 0!important;}
                          .m-padding-top15 {margin: 15px 0 0 0!important;}
                          .m-padding-top20 {margin: 20px 0 0 0!important;}
                          .m-padding-top30 {margin: 30px 0 0 0!important;}
                          .m-padding-top40 {margin: 40px 0 0 0!important;}
                          .m-padding-top50 {margin: 50px 0 0 0!important;}
                          .m-padding-top60 {margin: 60px 0 0 0!important;}
                          .m-height10 {font-size:10px!important;line-height:10px!important;height:10px!important;}
                          .m-height15 {font-size:15px!important;line-height:15px!important;height:15px!important;}
                          .m-height20 {font-size:20px!important;line-height:20px!important;height:20px!important;}
                          .m-height25 {font-size:25px!important;line-height:25px!important;height:25px!important;}
                          .m-height30 {font-size:30px!important;line-height:30px!important;height:30px!important;}
                          .rwd-on-mobile {display: inline-block!important;padding: 5px!important;}
                          .center-on-mobile {text-align: center!important;}
                        }
                        </style>
                        </head>
                        
                        <body Simpli style="margin-top: 0; margin-bottom: 0; padding-top: 0; padding-bottom: 0; width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;" bgcolor="#ffffff">
                        
                        <span class="preheader-text" Simpli style="color: transparent; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; visibility: hidden; width: 0; display: none; mso-hide: all;"></span>
                        
                        <div  style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></div>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;max-width:100%;">
                          <tr><!-- Outer Table -->
                            <td align="center" Simpli bgcolor="#F0F0F0" data-composer>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- Preheader -->
                          <tr>
                            <td height="20" style="font-size:20px;line-height:20px;" Simpli>&nbsp;</td>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://www.s-verein.de/sites/all/themes/sverein_graco_bootstrap/logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <!-- Preheader -->
                        </table>
                        
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row" role="presentation" width="640" style="width:640px;max-width:640px;" Simpli>
                          <!-- simpli-header-1 -->
                          <tr>
                            <td align="center">
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="row container-padding10" role="presentation" width="640" style="width:640px;max-width:640px;">
                          <!-- bg-image -->
                          <tr>
                            <td align="center" style="border-radius: 36px;">
                        
                        <!--[if (gte mso 9)|(IE)]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                        <v:fill type="frame" src="images/header-1.jpg" color="#F4F4F4" />
                        <v:textbox style="mso-fit-shape-to-text:true;" inset="0,0,0,0"><![endif]-->
                        
                        <div>
                        <!-- simpli-header-bg-image -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                          <tr>
                            <td align="center"  data-bg-image   style="background-size:cover;background-position:center top; border-radius: 36px;  background-image:url('https://www.s-verein.de/sites/crm/files/produktwelt.png')">
                        
                        <!-- Content -->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="600" style="width:600px;max-width:600px;">
                          <tr>
                            <td height="300" valign="bottom" class="container-padding" style="font-size:640px;line-height:640px;" Simpli>
                        
                            <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;">
                              <tr>
                                <td align="center" Simpli bgcolor="#FFFFFF" height="40"  style="height:40px;font-size:40px;line-height:36px;border-radius:36px 36px 0 0;" class="container-padding">&nbsp;</td>
                              </tr>
                            </table>
                        
                            </td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                        </table>
                        <!-- simpli-header-bg-image -->
                        </div>
                        
                        <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                        
                            </td>
                          </tr>
                          <!-- bg-image -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding25" width="600" style="width:600px;max-width:600px;">
                          <!-- basic-info -->
                          <tr>
                            <td align="center" Simpli bgcolor="#FFFFFF"  style="border-radius:0 0 36px 36px; border-bottom:solid 6px #DDDDDD;">
                              <!-- content -->
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row container-padding" width="520" style="width:520px;max-width:520px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:12px;line-height:24px;font-weight:900;font-style:normal;color:red;text-decoration:none;letter-spacing:2px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            PASSWORT VERGESSEN?
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:44px;line-height:54px;font-weight:700;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Passwort zurücksetzen
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="15" style="font-size:15px;line-height:15px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:26px;font-weight:300;font-style:normal;color:#333333;text-decoration:none;letter-spacing:0px;">
                                      <singleline>
                                        <div mc:edit Simpli>
                                            Guten Tag,
                                            es gab eine Aufforderung, Ihr Passwort zurückzusetzen.<br>
                                            Wenn Sie diese Anfrage nicht gestellt haben, ignorieren Sie bitte diese E-Mail.<br>
                                            Andernfalls klicken Sie bitte auf diesen Link, um Ihr Passwort zurückzusetzen.<br>
                        
                                        </div>
                                      </singleline>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="25" style="font-size:25px;line-height:25px;" Simpli>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td align="center">
                                    <!-- Button -->
                                    <table border="0" cellspacing="0" cellpadding="0" role="presentation" align="center" class="center-float">
                                      <tr>
                                        <td align="center"  Simpli bgcolor="red" style="border-radius: 6px;">
                                    <!--[if (gte mso 9)|(IE)]>
                                      <table border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                          <td align="center" width="35"></td>
                                          <td align="center" height="50" style="height:50px;">
                                          <![endif]-->
                                            <singleline>
                                              <a href="`+ encrypted + `" target="_blank" mc:edit  style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:16px;line-height:20px;font-weight:700;font-style:normal;color:#FFFFFF;text-decoration:none;letter-spacing:0px;padding: 15px 35px 15px 35px;display: inline-block;"><span>Passwort zurücksetzen</span></a>
                                            </singleline>
                                          <!--[if (gte mso 9)|(IE)]>
                                          </td>
                                          <td align="center" width="35"></td>
                                        </tr>
                                      </table>
                                    <![endif]-->
                                        </td>
                                      </tr>
                                    </table>
                                    <!-- Buttons -->
                                  </td>
                                </tr>
                                <tr>
                                  <td height="40" style="font-size:40px;line-height:40px;" Simpli>&nbsp;</td>
                                </tr>
                              </table>
                              <!-- content -->
                            </td>
                          </tr>
                          <!-- basic-info -->
                        </table>
                        
                            </td>
                          </tr>
                          <!-- simpli-header-1 -->
                        </table>
                        
                        <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="width:100%;max-width:100%;" Simpli>
                          <!-- simpli-footer -->
                          <tr>
                            <td align="center">
                              
                        <!-- Content -->
                        
                          <tr>
                            <td align="center">
                              <table border="0" align="center" cellpadding="0" cellspacing="0" role="presentation" class="row" width="480" style="width:480px;max-width:480px;">
                                <tr>
                                  <td class="center-text" Simpli align="center" style="font-family:'Catamaran',Arial,Helvetica,sans-serif;font-size:13px;line-height:18px;font-weight:480;font-style:normal;color:#666666;text-decoration:none;letter-spacing:0px;">
                                    <multiline>
                                      <div mc:edit Simpli>
                                        <br />
                                        Tineon Aktiengesellschaft <br />
                                        Uferpromenade 5, 88709 Meersburg <br />
                                        Registergericht Freiburg HRB 710927 <br />
                                        Vorstand: Jean-Claude Parent <br />
                                        Aufsichtsrat: Hans-Peter Haubold (Vorsitzender), Eugen Schindler, Hans-Peter Kolb <br />
                                        USt.-IdNr. DE203912818 <br />
                                      </div>
                                    </multiline>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td height="30" style="font-size:30px;line-height:30px;" Simpli>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center" class="center-text">
                              <img style="width:120px;border:0px;display: inline!important;" src="https://tineon.de/wp-content/themes/tineon/images/black_logo.png" width="120" border="0" editable="true" Simpli data-image-edit  Simpli alt="logo">
                            </td>
                          </tr>
                          <tr>
                            <td height="50" style="font-size:50px;line-height:50px;" Simpli>&nbsp;</td>
                          </tr>
                        </table>
                        <!-- Content -->
                        
                            </td>
                          </tr>
                          <!-- simpli-footer -->
                        </table>
                        
                            </td>
                          </tr><!-- Outer-Table -->
                        </table>
                        
                        </body>
                        </html>
                    ` }
        sendEmail(mailOptions).then((sentdata) => {
          if (sentdata == 'error') {
            response = responseFormat.getResponseMessageByCodes(['forget:mailSentFail'], { code: 417 });
            res.status(200).json(response);
          } else {
            response = responseFormat.getResponseMessageByCodes(['forget:resetPasswordMail']);
            res.status(200).json(response);
          }
        }).catch((error) => {
          console.log("---error002272--",error)
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      }
    ])
  }

  /**
   * Function to  verify trial email
   * @author  MangoIt Solutions
   * @param   {object} with email
   * @return  {message} Succes message
   */
  verifyTrialEmail(req, res, next) {
    let insId;
    let gId;
    let splited;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        if (req.body.id) {
          commomMethods.decrypt(req.body.id).then((dec) => {
            splited = dec.split('||');
            var datetime = new Date();
            const dates = commomMethods.getDates(new Date(splited[1]), new Date(datetime));
            if (dates.length < 2) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['verify:linkExpired'], { code: 417 });
              res.status(200).json(response)
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        } else {
          response = responseFormat.getResponseMessageByCodes(['verify:invalidLink'], { code: 417 });
          res.status(200).json(response)
        }
      },
      function (done) {
        userModel.getUserByEmail(splited[0]).then((userfind) => {
          if (userfind.length > 0) {
            insId = userfind[0].uid;
            if (userfind[0].access == 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['verify:alreadyVerifiedEmail'], { code: 417 });
              res.status(200).json(response)
            }
          } else {
            response = responseFormat.getResponseMessageByCodes(['verify:emailNotExists'], { code: 417 });
            res.status(200).json(response)
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        let dt = new Date().getTime();
        let dateTime = dt;
        userModel.updateTrialUserStatus(splited[0], dateTime).then((emailVerify) => {
          if (emailVerify.affectedRows > 0) {
            done();
          } else {
            response = responseFormat.getResponseMessageByCodes(['verify:verificationFail'], { code: 417 });
            res.status(200).json(response)
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        userModel.getGuestRole().then((guestId) => {
          if (guestId) {
            gId = guestId;
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['verify:roleNotExists'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        userModel.registerUserRole(insId, gId.rid).then((resData) => {
          if (resData.affectedRows > 0) {
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['verify:registeredRoleFail'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        licenceModel.registerLicenceInformation(insId).then((licenceData) => {
          if (licenceData.affectedRows > 0) {
            response = responseFormat.getResponseMessageByCodes(['verify:userLicenceRegistrationSuccess']);
            res.status(200).json(response);
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['verify:userLicenceRegistrationFail'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      }
    ])
  }

  /**
   * Function to  verify  email
   * @author  MangoIt Solutions
   * @param   {object} with id,email
   * @return  {message} Succes message
   */
  verifyEmail(req, res, next) {
    let insId;
    let gId;
    let splited;
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    async.waterfall([
      function (done) {
        if (req.body.id) {
          commomMethods.decrypt(req.body.id).then((dec) => {
            splited = dec.split('||');
            var datetime = new Date();
            const dates = commomMethods.getDates(new Date(splited[1]), new Date(datetime));
            if (dates.length < 2) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['verify:linkExpired'], { code: 417 });
              res.status(200).json(response)
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        } else {
          response = responseFormat.getResponseMessageByCodes(['verify:invalidLink'], { code: 417 });
          res.status(200).json(response)
        }
      },
      function (done) {
        userModel.getUserByEmail(splited[0]).then((userfind) => {
          if (userfind.length > 0) {
            insId = userfind[0].uid;
            if (userfind[0].access == 0) {
              done();
            } else {
              response = responseFormat.getResponseMessageByCodes(['verify:alreadyVerified'], { code: 417 });
              res.status(200).json(response)
            }
          } else {
            response = responseFormat.getResponseMessageByCodes(['verify:emailNotExists'], { code: 417 });
            res.status(200).json(response)
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        let msgCode = authValidation.passwordValidation(req.body);
        if (msgCode && msgCode.length) {
          response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
          res.status(200).json(response);
        } else {
          let hashPass;
          bcrypt.hash(req.body.password, 10, function (err, hash) {
            hashPass = hash;
            let dt = new Date().getTime();
            let dateTime = dt;
            userModel.updateUserStatus(splited[0], hashPass, dateTime, req.body.processStatus).then((emailVerify) => {
              if (emailVerify.affectedRows > 0) {
                done();
              } else {
                response = responseFormat.getResponseMessageByCodes(['verify:verificationFail'], { code: 417 });
                res.status(200).json(response)
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          })
        }
      },
      function (done) {
        userModel.getGuestRole().then((guestId) => {
          if (guestId) {
            gId = guestId;
            done();
          }
          else {
            response = responseFormat.getResponseMessageByCodes(['verify:roleNotExists'], { code: 417 });
            res.status(200).json(response);
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        userModel.getUserRolesDataByAdmin(insId).then((resRol) => {
          if (resRol.length > 0) {
            done();
          }
          else {
            userModel.registerUserRole(insId, gId.rid).then((resData) => {
              if (resData.affectedRows > 0) {
                done();
              }
              else {
                response = responseFormat.getResponseMessageByCodes(['verify:registeredRoleFail'], { code: 417 });
                res.status(200).json(response);
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          }
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      },
      function (done) {
        let permissionArr = [];
        let roleA = [];
        let userA = [];
        let customerAssignA = [];
        let productA = [];
        let productComponentA = [];
        let SlicenseA = [];
        let domainA = [];
        let newsA = [];
        let productAssignA = [];
        let permissionA = [];
        let pagesA = [];
        let surveyA = [];
        let emailTemplateA = [];
        let salesPartnerA = [];
        let clubsA = [];
        let OldUserStatus;
        userModel.getOlduserStatus(insId).then((olduserData) => {
          if (olduserData.length > 0) {
            OldUserStatus = 1;
          }
          else {
            OldUserStatus = 0;
          }
          userModel.getUserDetail(insId).then((resData) => {
            if (resData.length > 0) {
              let token = jwt.sign({ data: { userId: insId, roleId: resData[0].rid } }, configContainer.jwtSecretKey, {
                expiresIn: '1d'
              });
              for (let n = 0; n < resData.length; n++) {

                userModel.getRolePermission(resData[n].rid).then((permissionData) => {
                  if (permissionData.length > 0) {
                    for (let k = 0; k < permissionData.length; k++) {
                      if (permissionData[k].module == 'user' && !userA.includes(permissionData[k].permission)) {
                        userA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'role' && !roleA.includes(permissionData[k].permission)) {
                        roleA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'customerAssign' && !customerAssignA.includes(permissionData[k].permission)) {
                        customerAssignA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'productComponent' && !productComponentA.includes(permissionData[k].permission)) {
                        productComponentA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'product' && !productA.includes(permissionData[k].permission)) {
                        productA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'domain' && !domainA.includes(permissionData[k].permission)) {
                        domainA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'Slicense' && !SlicenseA.includes(permissionData[k].permission)) {
                        SlicenseA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'news' && !newsA.includes(permissionData[k].permission)) {
                        newsA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'productAssign' && !productAssignA.includes(permissionData[k].permission)) {
                        productAssignA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'permission' && !permissionA.includes(permissionData[k].permission)) {
                        permissionA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'staticWebPage' && !pagesA.includes(permissionData[k].permission)) {
                        pagesA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'survey' && !surveyA.includes(permissionData[k].permission)) {
                        surveyA.push(permissionData[k].permission)
                      } else if (permissionData[k].module == 'emailTemplate' && !emailTemplateA.includes(permissionData[k].permission)) {
                        emailTemplateA.push(permissionData[k].permission)
                      }
                      else if (permissionData[k].module == 'salesPartner' && !salesPartnerA.includes(permissionData[k].permission)) {
                        salesPartnerA.push(permissionData[k].permission)
                      }
                      else if (permissionData[k].module == 'clubs' && !clubsA.includes(permissionData[k].permission)) {
                        clubsA.push(permissionData[k].permission)
                      }
                    }
                    if (n + 1 == resData.length) {

                      permissionArr = { user: userA, role: roleA, customerAssign: customerAssignA, productComponent: productComponentA, product: productA, domain: domainA, slicense: SlicenseA, news: newsA, productAssign: productAssignA, permission: permissionA, staticWebPage: pagesA, survey: surveyA, emailTemplate: emailTemplateA, salesPartner: salesPartnerA, clubs: clubsA };
                      let userdetails = [{ username: resData[0].name, processStatus: resData[0].process_status, picture: resData[0].picture, email: resData[0].mail, uid: insId, role: resData, society: resData[0].society, token: token, permission: permissionArr, OldUserStatus: OldUserStatus }];
                      response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userdetails } });
                      res.status(200).json(response);
                    }
                  } else {
                    if (n + 1 == resData.length) {
                      permissionArr = { user: userA, role: roleA, customerAssign: customerAssignA, productComponent: productComponentA, product: productA, domain: domainA, slicense: SlicenseA, news: newsA, productAssign: productAssignA, permission: permissionA, staticWebPage: pagesA, survey: surveyA, emailTemplate: emailTemplateA, salesPartner: salesPartnerA, clubs: clubsA };
                      let userdetails = [{ username: resData[0].name, processStatus: resData[0].process_status, picture: resData[0].picture, email: resData[0].mail, uid: insId, role: resData, society: resData[0].society, token: token, permission: permissionArr, OldUserStatus: OldUserStatus }];
                      response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userdetails } });
                      res.status(200).json(response);
                    }
                  }
                }).catch((error) => {
                  response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                  res.status(500).json(response);
                  return;
                })
              }
            } else {
              response = responseFormat.getResponseMessageByCodes(['verify:emailNotExists'], { code: 417 });
              res.status(200).json(response)
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }).catch((error) => {
          response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
          res.status(500).json(response);
          return;
        })
      }
    ])
  }

  /**
   * Function for sign in user
   * @author  MangoIt Solutions
   * @param   {object} with email,password
   * @return  {message} Succes message
   */
  signin(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = authValidation.signinValidation(req.body);
    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    } else {
      async.waterfall([
        function (done) {
          userModel.checkEmailExists(req.body.email).then((data) => {
            if (data) {
              done();
            }
            else {
              userModel.checkUserNameExists(req.body.email).then((data) => {
                if (data.length > 0) {
                  done();
                } else {
                  response = responseFormat.getResponseMessageByCodes(['Signin:emailUserNameNotExists'], { code: 417 });
                  dashLogger.error(`Error : ${response.content.messageList.Signin},Request : ${req.originalUrl}`);
                  res.status(200).json(response);
                }
              }).catch((error) => {
                dashLogger.error(`Error : ${error},Request : ${req.originalUrl}`);
                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                res.status(500).json(response);
                return;
              })
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.login(req).then((data) => {
            if (data[0]) {
              bcrypt.compare(req.body.password, data[0].pass).then((passMatch) => {
                if (!passMatch) {
                  response = responseFormat.getResponseMessageByCodes(['Signin:invalidPassword'], { code: 417 });
                  res.status(200).json(response)
                } else {
                  if (data[0].access == 0) {
                    response = responseFormat.getResponseMessageByCodes(['Signin:accountNotActivated'], { code: 417 });
                    res.status(200).json(response)
                  } else if (data[0].status != 1) {
                    response = responseFormat.getResponseMessageByCodes(['Signin:deActivateAccount'], { code: 417 });
                    res.status(200).json(response)
                  }
                  else {
                    done(null, data[0]);
                  }
                }
              }).catch((error) => {
                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                res.status(500).json(response);
                return;
              })
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (result, err) {
          let permissionArr = [];
          let roleA = [];
          let userA = [];
          let customerAssignA = [];
          let productA = [];
          let productComponentA = [];
          let SlicenseA = [];
          let domainA = [];
          let newsA = [];
          let productAssignA = [];
          let permissionA = [];
          let pagesA = [];
          let surveyA = [];
          let emailTemplateA = [];
          let salesPartnerA = [];
          let clubsA = [];
          let OldUserStatus;
          userModel.getOlduserStatus(result.uid).then((olduserData) => {
            if (olduserData.length > 0) {
              OldUserStatus = 1;
            }
            else {
              OldUserStatus = 0;
            }

            userModel.getRoleDetails(result.uid).then((resData) => {
              if (resData.length > 0) {
                let token = jwt.sign({ data: { userId: result.uid, roleId: resData[0].rid } }, configContainer.jwtSecretKey, {
                  expiresIn: '1d'
                });
                for (let n = 0; n < resData.length; n++) {
                  userModel.getRolePermission(resData[n].rid).then((permissionData) => {
                    if (permissionData.length > 0) {
                      for (let k = 0; k < permissionData.length; k++) {
                        if (permissionData[k].module == 'user' && !userA.includes(permissionData[k].permission)) {
                          userA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'role' && !roleA.includes(permissionData[k].permission)) {
                          roleA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'customerAssign' && !customerAssignA.includes(permissionData[k].permission)) {
                          customerAssignA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'productComponent' && !productComponentA.includes(permissionData[k].permission)) {
                          productComponentA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'product' && !productA.includes(permissionData[k].permission)) {
                          productA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'domain' && !domainA.includes(permissionData[k].permission)) {
                          domainA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'Slicense' && !SlicenseA.includes(permissionData[k].permission)) {
                          SlicenseA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'news' && !newsA.includes(permissionData[k].permission)) {
                          newsA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'productAssign' && !productAssignA.includes(permissionData[k].permission)) {
                          productAssignA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'permission' && !permissionA.includes(permissionData[k].permission)) {
                          permissionA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'staticWebPage' && !pagesA.includes(permissionData[k].permission)) {
                          pagesA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'survey' && !surveyA.includes(permissionData[k].permission)) {
                          surveyA.push(permissionData[k].permission)
                        } else if (permissionData[k].module == 'emailTemplate' && !emailTemplateA.includes(permissionData[k].permission)) {
                          emailTemplateA.push(permissionData[k].permission)
                        }
                        else if (permissionData[k].module == 'salesPartner' && !salesPartnerA.includes(permissionData[k].permission)) {
                          salesPartnerA.push(permissionData[k].permission)
                        }
                        else if (permissionData[k].module == 'clubs' && !clubsA.includes(permissionData[k].permission)) {
                          clubsA.push(permissionData[k].permission)
                        }
                      }
                      if (n + 1 == resData.length) {
                        permissionArr = { user: userA, role: roleA, customerAssign: customerAssignA, productComponent: productComponentA, product: productA, domain: domainA, slicense: SlicenseA, news: newsA, productAssign: productAssignA, permission: permissionA, staticWebPage: pagesA, survey: surveyA, emailTemplate: emailTemplateA, salesPartner: salesPartnerA, clubs: clubsA };
                        let userdetails = [{ username: result.name, processStatus: result.process_status, email: result.mail, uid: result.uid, picture: result.picture, role: resData, society: resData[0].society, token: token, permission: permissionArr, OldUserStatus: OldUserStatus }];
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userdetails } });
                        res.status(200).json(response);
                      }
                    } else {
                      if (n + 1 == resData.length) {
                        permissionArr = { user: userA, role: roleA, customerAssign: customerAssignA, productComponent: productComponentA, product: productA, domain: domainA, slicense: SlicenseA, news: newsA, productAssign: productAssignA, permission: permissionA, staticWebPage: pagesA, survey: surveyA, emailTemplate: emailTemplateA, salesPartner: salesPartnerA, clubs: clubsA };
                        let userdetails = [{ username: result.name, processStatus: result.process_status, email: result.mail, uid: result.uid, picture: result.picture, role: resData, society: resData[0].society, token: token, permission: permissionArr, OldUserStatus: OldUserStatus }];
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userdetails } });
                        res.status(200).json(response);
                      }
                    }
                  }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                  })
                }
              } else {
                response = responseFormat.getResponseMessageByCodes(['Signin:deActivateAccount'], { code: 417 });
                res.status(200).json(response)
              }
            }).catch((error) => {
              response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
              res.status(500).json(response);
              return;
            })
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }
      ])
    }
  }

  /**
   * Function for sign up user
   * @author  MangoIt Solutions
   * @param   {object}
   * @return  {message} Succes message
   */
  signupUser(req, res, next) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    let msgCode = authValidation.signupNewValidation(req.body);

    if (msgCode && msgCode.length) {
      response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
      res.status(200).json(response);
    }
    else {
      let insId;
      let gId;
      let templateData;
      let mailOptions;
      async.waterfall([
        function (done) {
          userModel.checkEmailExists(req.body.email).then((data) => {
            if (data) {
              response = responseFormat.getResponseMessageByCodes(['Signup:emailExists'], { code: 417 });
              res.status(200).json(response);
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.signupUser(req).then((data) => {
            if (data.affectedRows > 0) {
              insId = data.insertId;
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registrationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          userModel.signupUserSociety(req, insId).then((data) => {
            if (data.affectedRows > 0) {
              done();
            }
            else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registrationFail'], { code: 417 });
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          emailTemplateModel.getemailTemplateByType('Registration').then((data) => {
            if (data.length > 0) {
              templateData = data;
              done();
            }
            else {
              done();
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        },
        function (done) {
          let link = configContainer.emailVerificationLink;
          let encrypted = link + commomMethods.encrypt(req.body.email + '||' + new Date().getTime());
          if (templateData && templateData.length > 0) {
            mailOptions = {
              to: req.body.email,
              subject: 'Bitte bestätigen Sie Ihre Email-Adresse',
              html: `<!doctype html>
                            <html lang="en-US">
                            
                            <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Reset Password Email</title>
                            <meta name="description" content="Reset Password Email Template.">
                            <style type="text/css">
                            a:hover {text-decoration: underline !important;}
                            </style>
                            </head>
                            
                            <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <!--100% body table-->
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                            style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                            <tr>
                            <td>
                            <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                            align="center" cellpadding="0" cellspacing="0">
                            <tr>
                            <td style="height:80px;">&nbsp;</td>
                            </tr>
                            <tr>
                            <td style="text-align:center;">
                            <a href="#" title="logo" target="_blank">
                            <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                            </a>
                            </td>
                            </tr>
                            <tr>
                            <td style="height:20px;">&nbsp;</td>
                            </tr>
                            <tr>
                            <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                            style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                            <tr>
                            <td style="height:40px;">&nbsp;</td>
                            </tr>
                            <tr>
                            <td style="padding:0 35px;">
                            <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->
                            
                                                                    <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>
                                                                   
                                                                    <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    `+ templateData[0].template_body + `:<br>
                                                                    <a href="`+ encrypted + `">` + encrypted + `</a>
                                                                    </p><br><br>
                                                                    <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                                                                    <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                                                                    <!-- <a href="javascript:void(0);"
                                                                    style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                                                                        Password</a> -->
                                                                        </td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td style="height:40px;">&nbsp;</td>
                                                                        </tr>
                                                                        </table>
                                                                        </td>
                                                                        <tr>
                                                                        <td style="height:20px;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td style="text-align:center;">
                                                                        <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                                                                        </td>
                                                                        </tr>
                                                                        <tr>
                                                                        <td style="height:80px;">&nbsp;</td>
                                                                        </tr>
                                                                        </table>
                                        </td>
                                    </tr>
                                </table>
                                <!--/100% body table-->
                                </body>
                                
                                </html>`,
            };
          }
          else {

            mailOptions = {
              to: req.body.email,
              subject: 'Bitte bestätigen Sie Ihre Email-Adresse',
              html: `<h3>Dear association member,</h3>
                                
                                <h4>Thank you for your interest in our S-Club |
                                Online club management.</h4>
                                
                                <h5> Please confirm your email address via the following link: </h5>
                                <a href="`+ encrypted + `">` + encrypted + `</a>
                                <h3>If the forwarding does not work, please copy the link in
                                the address bar of your browser.
                                
                                After you have successfully confirmed your e-mail address, the access data will be sent
                                sent to the specified email address.</h3>`,
            };
          }
          sendEmail(mailOptions).then((sentdata) => {
            if (sentdata == 'error') {
              response = responseFormat.getResponseMessageByCodes(['Signup:registeredSuccessAsGuest']);
              res.status(200).json(response);
            } else {
              response = responseFormat.getResponseMessageByCodes(['Signup:registeredSuccessLink']);
              res.status(200).json(response);
            }
          }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
          })
        }
      ]);
    }
  }

  /**
   * Function for sign out user
   * @author  MangoIt Solutions
   * @param   {}
   * @return  {message} Succes message
   */
  signout = (req, res) => {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    res.clearCookie('token');
    response = responseFormat.getResponseMessageByCodes(['Signout:logOutSuccess']);
    res.status(200).json(response);
  }
}