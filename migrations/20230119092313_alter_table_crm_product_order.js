exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('crm_product_order', function(t) {
        t.tinyint('order_status').notNull().defaultTo(0).comment('0 -> Open, 1 -> Inprogress, 2 -> Complete').after("mandate_text");
             
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('crm_product_order', function(t) {
        
    });
 };