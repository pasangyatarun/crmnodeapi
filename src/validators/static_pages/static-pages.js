export default class PagesValidation {
    pagesCreate(pagesData){
        let err = new Array();
        if(!pagesData.title ){
            err.push('title:titleReq');
        }
        if(!pagesData.content ){
            err.push('content:contentReq');
        } 
        if(!pagesData.author ){
            err.push('author:authorReq');
        }       
        return err;
    }
}