exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('sverein_licence_summary', function(t) {
        t.string('customername',[255]).notNull().defaultTo('').comment('Name of customer club').alter();
        t.string('salespartnername',[255]).notNull().defaultTo('').comment('Name of sales partner').alter();
        t.string('postcode',[255]).notNull().defaultTo('').comment('Postal code customer club').alter();
        t.string('city',[255]).notNull().defaultTo('').comment('City customer club').alter();
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('sverein_licence_summary', function(t) {
        
    });
 };