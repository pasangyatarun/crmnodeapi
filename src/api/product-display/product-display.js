import express   from 'express';
import  ProductDisplayController from '../../controllers/product-display/product-display';

let router = express.Router();
let productDisplayController = new ProductDisplayController();

router.post('/selected-product', productDisplayController.selectedProduct.bind(productDisplayController));
router.get('/selected-product-display', productDisplayController.getAllSelectedProduct.bind(productDisplayController));
router.get('/productDisplay-for-customer', productDisplayController.getProductDisplayForCustomer.bind(productDisplayController));

export default router;