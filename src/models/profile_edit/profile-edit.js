import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class ProfileEditModel {

    async getAddressById(uid) {
        const rows = await db.query(`SELECT * FROM field_data_field_address  WHERE entity_id = ?`, [uid]);
        return rows[0];
    }

    async getAccountById(uid) {
        const rows = await db.query(`SELECT * FROM field_data_field_bank_iban  WHERE entity_id = ?`, [uid]);
        return rows[0];
    }

    async getupdatePicture(picId) {
        const rows = await db.query("UPDATE users SET picture = ? WHERE uid = ?", [
            picId,
            req.params.uid
        ]);
        return rows[0];
    }

    async getProfileUpdate(req, imgUrl) {
        const rows = await db.query("UPDATE users SET auther_id = ?, name = ?,mail= ?,status=?,picture = ?,contact_number = ?,process_status = ? WHERE uid = ?", [
            req.body.auther_id,
            req.body.userName,
            req.body.email,
            req.body.status,
            imgUrl,
            req.body.contactNumber,
            req.body.processStatus,
            req.params.uid
        ]);
        return rows[0];
    }

    async getAddressUpdate(req) {
        const rows = await db.query('INSERT INTO `field_data_field_address`(`entity_id`,`field_address_name_line`,`field_address_country`,`field_address_first_name`,`field_address_last_name`,`field_address_organisation_name`,`field_address_locality`,`field_address_postal_code`,`field_address_thoroughfare`,`field_address_second`,`entity_type`)VALUES(?,?,?,?,?,?,?,?,?,?,?)', [
            req.params.uid,
            req.body.firstName + req.body.lastName,
            req.body.country,
            req.body.firstName,
            req.body.lastName,
            req.body.society,
            req.body.place,
            req.body.zipcode,
            req.body.addressLine1,
            req.body.addressLine2,
            'user'
        ]);
        return rows[0];
    }

    async getAccountUpdate(req) {
        const rows = await db.query('INSERT INTO `field_data_field_bank_iban`(`entity_id`,`field_bank_iban_value`,`entity_type`)VALUES(?,?,?)', [
            req.params.uid,
            req.body.iban,
            'user'
        ]);
        return rows[0];
    }

    async getImageUpdate(req) {
        const rows = await db.query('INSERT INTO `file_managed`(`uid`,`filename`,`uri`,`filemime`,`filesize`,`status`,`timestamp`,`type`)VALUES(?,?,?,?,?,?,?,?)', [
            req.params.uid,
            req.body.account,
            'user'
        ]);
        return rows[0];
    }

    async ProfileUpdate(req) {
        const rows = await db.query("UPDATE users SET name = ?,mail= ?,status=?,contact_number=? WHERE uid = ?", [
            req.body.userName,
            req.body.email,
            1,
            req.body.contactNumber,
            req.params.uid
        ]);
        return rows[0];
    }

    async AddressUpdate(req) {
        const rows = await db.query("UPDATE field_data_field_address SET field_address_name_line = ?,field_address_country = ?,field_address_first_name = ?,field_address_last_name = ?,field_address_organisation_name = ?,field_address_locality = ?,field_address_postal_code = ?,field_address_thoroughfare = ?,field_address_second = ?,entity_type = ? WHERE entity_id = ?", [
            req.body.firstName + req.body.lastName,
            req.body.country,
            req.body.firstName,
            req.body.lastName,
            req.body.society,
            req.body.place,
            req.body.zipcode,
            req.body.addressLine1,
            req.body.addressLine2,
            'user',
            req.params.uid
        ]);
        return rows[0];
    }

    async AccountUpdate(req) {
        const rows = await db.query("UPDATE field_data_field_bank_iban SET field_bank_iban_value = ?,entity_type = ? WHERE entity_id = ?", [
            req.body.iban,
            'user',
            req.params.uid
        ]);
        return rows[0];
    }

    async ImageUpdate(req) {
        const rows = await db.query("UPDATE file_managed filename = ?,uri =?,filemime = ?,filesize = ?,status = ?,timestamp = ?,type = ? WHERE uid = ?", [

            req.params.uid
        ]);
        return rows[0];
    }
    
    async getDeleteDomainByUid(uid) {
        const rows = await db.query("DELETE FROM users_domain WHERE uid = ? ", [uid]);
        return rows[0];
    }
}