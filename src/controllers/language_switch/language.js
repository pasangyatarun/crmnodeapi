import async from 'async';
import responseFormat from '../../core/response-format';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');

export default class LanguageController {

    /**
    * Function to set language
    * @author  MangoIt Solutions
    * @param   {object} with language 
    * @return  {message} success message
    */
    languageSet(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                localStorage.setItem('language', req.body.language)
                response = responseFormat.getResponseMessageByCodes(['language:languageSuccess']);
                res.status(200).json(response);
            }
        ])
    }
}