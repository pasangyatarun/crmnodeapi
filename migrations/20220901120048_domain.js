exports.up = async function(knex) {
    return knex.schema.createTable('domain', function(t) {
        t.increments('domain_id').primary();
        t.string('subdomain',[255]).notNull();
        t.string('sitename',[255]).notNull();
        t.string('machine_name',[255]).notNull();
      
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('domain');
};