import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class TicketRequestModel {

    async createRequest(req, attachment,ticket_number) {
        const rows = await db.query('INSERT INTO `tickets_requests`(`ticket_number`,`name`,`description`,`attachment`,`status`,`assigned_to`,`user_id`,`category_id`)VALUES(?,?,?,?,?,?,?,?)', [
            ticket_number,
            req.body.name,
            req.body.description,
            attachment,
            req.body.status,
            req.body.assigned_to,
            req.params.req_user_id,
            req.body.category_id
        ]);
        return rows[0];
    }

    async checkRequestExist(req){
        const rows = await db.query("SELECT * FROM `tickets_requests` WHERE `user_id` = ? AND `name`=?  AND `status`!= ?", [req.params.req_user_id,req.body.name, 'Resolved']);
        return rows[0]; 
    }
    
    async checkticketNumberExist(ticket_number){
        const rows = await db.query("SELECT * FROM `tickets_requests` WHERE `ticket_number` = ? ", [ticket_number]);
        return rows[0]; 
    }

    async updateRequest(req,attachment){
        const rows = await db.query('UPDATE `tickets_requests` SET name=?,description=?,attachment=?,status=?,assigned_to=? WHERE id = ?', [
            req.body.name,
            req.body.description,
            attachment,
            req.body.status,
            req.body.assigned_to,
            req.params.id
        ]);
        return rows[0];
    }
    
    async updateRequestStatus(req){
        const rows = await db.query('UPDATE `tickets_requests` SET status=? WHERE id = ?', [
            req.body.status,
            req.params.id
        ]);
        return rows[0];
    }

    async deleteRequest(id){
        const rows = await db.query("DELETE FROM `tickets_requests` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getRequestByRequest(name){
        const rows = await db.query("SELECT * FROM `tickets_requests` WHERE `name`=? ", [name]);
        return rows[0]; 
    }
    
    async checksupportUserAssigned(id){
        const rows = await db.query("SELECT * from ticket_request_assignedTo WHERE request_id = ? ",[id]) ;
        return rows[0]; 
    }

    async getRequestById(id){
       const rows = await db.query("SELECT tickets_requests.*,faq_category.category , faq_status.sid as status_id,faq_status.status as selected_status FROM `tickets_requests` LEFT JOIN `faq_category` ON tickets_requests.category_id = faq_category.id LEFT JOIN `faq_status` ON tickets_requests.status = faq_status.sid WHERE tickets_requests.id = ?",[id]);  
       return rows[0]; 
    }
    
    async getDetailswithSupportuser(id){
      const rows = await db.query("SELECT tickets_requests.*,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,users.mail as email, faq_status.sid,faq_status.status FROM `tickets_requests` LEFT JOIN `faq_category` ON tickets_requests.category_id = faq_category.id LEFT JOIN `ticket_request_assignedTo` ON tickets_requests.id = ticket_request_assignedTo.request_id LEFT JOIN `users` ON ticket_request_assignedTo.assigned_user_id = users.uid LEFT JOIN `faq_status` ON tickets_requests.status = faq_status.sid WHERE tickets_requests.id = ? AND ticket_request_assignedTo.id=( SELECT MAX(ID) FROM ticket_request_assignedTo WHERE request_id = ?)",[id,id]);  
       return rows[0]; 
    }

    async getRequestByUserIdPage(uid,req){
        let param=req.params;
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
       if (req.query && req.query.search) {
        let search = req.query.search
        const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, tickets_requests.*,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,users.mail as email, faq_status.sid,faq_status.status as selected_status FROM tickets_requests LEFT JOIN faq_category ON tickets_requests.category_id = faq_category.id LEFT JOIN ticket_request_assignedTo ON tickets_requests.id = ticket_request_assignedTo.request_id AND ticket_request_assignedTo.id IN(SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) LEFT JOIN users ON ticket_request_assignedTo.assigned_user_id = users.uid LEFT JOIN faq_status ON tickets_requests.status = faq_status.sid WHERE tickets_requests.user_id = ? AND (users.mail LIKE '%" + search + "%' OR ticket_request_assignedTo.assigned_user_id LIKE '%" + search + "%' OR tickets_requests.ticket_number LIKE '%" + search + "%' OR tickets_requests.name LIKE '%" + search + "%' OR faq_category.category LIKE '%" + search + "%') ORDER BY id DESC LIMIT "+pageSize+" OFFSET "+offset+"", [uid]);
        return rows[0]; 
       }else{
           const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, tickets_requests.*,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,users.mail as email, faq_status.sid,faq_status.status as selected_status FROM tickets_requests LEFT JOIN faq_category ON tickets_requests.category_id = faq_category.id LEFT JOIN ticket_request_assignedTo ON tickets_requests.id = ticket_request_assignedTo.request_id AND ticket_request_assignedTo.id IN(SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) LEFT JOIN users ON ticket_request_assignedTo.assigned_user_id = users.uid LEFT JOIN faq_status ON tickets_requests.status = faq_status.sid WHERE tickets_requests.user_id = ? ORDER BY id DESC LIMIT "+pageSize+" OFFSET "+offset+"", [uid]);
           return rows[0]; 
        }
    }

    async getRequestByUserId(uid){
        const rows = await db.query("SELECT tickets_requests.*,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,users.mail as email, faq_status.sid,faq_status.status as selected_status FROM tickets_requests LEFT JOIN faq_category ON tickets_requests.category_id = faq_category.id LEFT JOIN ticket_request_assignedTo ON tickets_requests.id = ticket_request_assignedTo.request_id AND ticket_request_assignedTo.id IN(SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) LEFT JOIN users ON ticket_request_assignedTo.assigned_user_id = users.uid LEFT JOIN faq_status ON tickets_requests.status = faq_status.sid WHERE tickets_requests.user_id = ? ORDER BY id DESC", [uid]);
        return rows[0]; 
    }

    async getRequestUsersByPagination(req){
        let param=req.params
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
       if (req.query && req.query.search) {
        let search = req.query.search
        const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, tickets_requests.*,users.mail as request_email,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,u.mail as email,faq_status.sid as status_id, faq_status.status as selected_status FROM `tickets_requests` LEFT JOIN `users` on tickets_requests.user_id = users.uid LEFT JOIN `faq_category` ON tickets_requests.category_id = faq_category.id LEFT JOIN `ticket_request_assignedTo` ON tickets_requests.id = ticket_request_assignedTo.request_id LEFT JOIN `users` as u ON ticket_request_assignedTo.assigned_user_id = u.uid LEFT JOIN `faq_status` ON tickets_requests.status = faq_status.sid WHERE ticket_request_assignedTo.id IN( SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) OR ticket_request_assignedTo.id IS NULL AND (users.mail LIKE '%" + search + "%' OR ticket_request_assignedTo.assigned_user_id LIKE '%" + search + "%' OR tickets_requests.ticket_number LIKE '%" + search + "%' OR tickets_requests.name LIKE '%" + search + "%' OR faq_category.category LIKE '%" + search + "%') ORDER BY tickets_requests.id DESC  LIMIT "+pageSize+" OFFSET "+offset+"");
        return rows[0];
       }else{
           const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, tickets_requests.*,users.mail as request_email,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,u.mail as email,faq_status.sid as status_id, faq_status.status as selected_status FROM `tickets_requests` LEFT JOIN `users` on tickets_requests.user_id = users.uid LEFT JOIN `faq_category` ON tickets_requests.category_id = faq_category.id LEFT JOIN `ticket_request_assignedTo` ON tickets_requests.id = ticket_request_assignedTo.request_id LEFT JOIN `users` as u ON ticket_request_assignedTo.assigned_user_id = u.uid LEFT JOIN `faq_status` ON tickets_requests.status = faq_status.sid WHERE ticket_request_assignedTo.id IN( SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) OR ticket_request_assignedTo.id IS NULL ORDER BY tickets_requests.id DESC  LIMIT "+pageSize+" OFFSET "+offset+"");
           return rows[0]; 
        }
      }

    async getRequestUsers(){
      const rows = await db.query("SELECT tickets_requests.*,users.mail as request_email,faq_category.category,ticket_request_assignedTo.id as assigned_id,ticket_request_assignedTo.request_id as request_id,ticket_request_assignedTo.assigned_user_id as assigned_userId,u.mail as email,faq_status.sid as status_id, faq_status.status as selected_status FROM `tickets_requests` LEFT JOIN `users` on tickets_requests.user_id = users.uid LEFT JOIN `faq_category` ON tickets_requests.category_id = faq_category.id LEFT JOIN `ticket_request_assignedTo` ON tickets_requests.id = ticket_request_assignedTo.request_id LEFT JOIN `users` as u ON ticket_request_assignedTo.assigned_user_id = u.uid LEFT JOIN `faq_status` ON tickets_requests.status = faq_status.sid WHERE ticket_request_assignedTo.id IN( SELECT MAX(ID) FROM ticket_request_assignedTo GROUP BY request_id) OR ticket_request_assignedTo.id IS NULL ORDER BY tickets_requests.id DESC");
      return rows[0]; 
    }
    
    async getCategoryDetails(req){
        const rows = await db.query("SELECT faq_category.id as id , faq_category.category as Category FROM `faq_category` WHERE id = ?",[req.body.category_id]);
        return rows[0]; 
    }

    async getRequestDetails(userId){
       const rows = await db.query("SELECT users.name as name, users.mail as email, field_data_field_address.entity_id as entityId , field_data_field_address.field_address_organisation_name as ClubName FROM `users` LEFT JOIN `field_data_field_address`ON users.uid = field_data_field_address.entity_id  WHERE `uid`=? ", [userId]);
        return rows[0]; 
    }

    //Queries for ticket_comment table
    async addComment(req,attachment ) {
        const rows = await db.query('INSERT INTO `tickets_comments`(`request_id`,`comment`,`attachment`,`commented_user`)VALUES(?,?,?,?)', [
            req.params.ticket_id,
            req.body.comment,
            attachment,
            req.params.user_id
        ]);
        return rows[0];
    }
    
    async getRequestCreatedUser(id){
        const rows = await db.query("SELECT tickets_requests.*,users.uid,users.mail as email FROM `tickets_requests` LEFT JOIN `users` ON tickets_requests.user_id = users.uid WHERE `id` = ? ", [id]);
        return rows[0]; 
    }
    
    async getSupportuserasigned(id){
        const rows = await db.query("SELECT ticket_request_assignedTo.*,u.uid as assigned_user,u.mail as assigned_user_mail, users.uid as ticket_user,users.mail as ticket_user_mail,field_data_field_address.field_address_organisation_name as Clubname,tickets_requests.*,faq_category.category from ticket_request_assignedTo LEFT JOIN tickets_requests ON ticket_request_assignedTo.request_id = tickets_requests.id LEFT JOIN users as u ON ticket_request_assignedTo.assigned_user_id = u.uid LEFT JOIN faq_category ON tickets_requests.category_id =faq_category.id LEFT JOIN users ON tickets_requests.user_id = users.uid LEFT JOIN field_data_field_address ON tickets_requests.user_id = field_data_field_address.entity_id WHERE ticket_request_assignedTo.id = (SELECT MAX(id) FROM ticket_request_assignedTo WHERE request_id = ?);", [id]);
        return rows[0]; 
    }
    async getCommentByTicketId(ticket_id){
        const rows = await db.query("SELECT tickets_comments.*,users.name,users.mail, field_data_field_address.field_address_first_name as FirstName, field_data_field_address.field_address_last_name as LastName FROM `tickets_comments` LEFT JOIN `users` on tickets_comments.commented_user = users.uid LEFT JOIN `field_data_field_address` ON tickets_comments.commented_user = field_data_field_address.entity_id WHERE `request_id`=?  ", [ticket_id]);
        return rows[0]; 
    }

    async getCommentById(id){
        const rows = await db.query("SELECT * FROM `tickets_comments` WHERE `id`=? ", [id]);
        return rows[0]; 
    }

    async updateComment(req,attachment){
        const rows = await db.query('UPDATE `tickets_comments` SET comment=? , attachment=? WHERE id = ?', [
           req.body.comment,
           attachment,
            req.params.id
        ]);
        return rows[0];
    }

    async deleteComment(id){
        const rows = await db.query("DELETE FROM `tickets_comments` WHERE `id`=?", [id]);
        return rows[0];
    }
    
    //Queries for ticket comeent reply
    async addCommentReply(req) {
        const rows = await db.query('INSERT INTO `tickets_comments_reply`(`comment_id`,`ticket_id`,`replied_user_id`,`reply`)VALUES(?,?,?,?)', [
            req.params.comment_id,
            req.params.ticket_id,
            req.params.user_id,
            req.body.reply
        ]);
        return rows[0];
    }

    async requestTicketDetails(id){
        const rows = await db.query("SELECT tickets_requests.*,users.mail as request_user_email,users.uid FROM `tickets_requests` LEFT JOIN `users` ON tickets_requests.user_id = users.uid WHERE id = ? ",[id]);
        return rows[0]; 
    }

    //getAllSupportUsers
    async getAllSupportUsers(){
        const rows = await db.query("SELECT users_roles.*, users.uid as user_id, users.name as name,users.mail as Email,field_data_field_address.field_address_first_name as FirstName, field_data_field_address.field_address_last_name as LastName FROM `users_roles` LEFT JOIN `users` ON users_roles.uid = users.uid LEFT JOIN `field_data_field_address` ON users_roles.uid = field_data_field_address.entity_id WHERE `rid` IN (1,2,3) AND `mail` IS NOT NULL");
        return rows[0]; 
    }

    async addAssignedUser(req) {
        const rows = await db.query('INSERT INTO `ticket_request_assignedTo`(`request_id`,`assigned_user_id`)VALUES(?,?)', [
            req.body.request_id,
            req.body.assigned_user_id,
        ]);
        return rows[0];
    }

    async assignedUserTicketDetails(req){
        const rows = await db.query("SELECT ticket_request_assignedTo.*,u.uid as assigned_user,u.mail as assigned_user_mail, users.uid as ticket_user,users.mail as ticket_user_mail,field_data_field_address.field_address_organisation_name as Clubname,tickets_requests.*,faq_category.category from ticket_request_assignedTo LEFT JOIN tickets_requests ON ticket_request_assignedTo.request_id = tickets_requests.id LEFT JOIN users as u ON ticket_request_assignedTo.assigned_user_id = u.uid LEFT JOIN faq_category ON tickets_requests.category_id =faq_category.id LEFT JOIN users ON tickets_requests.user_id = users.uid LEFT JOIN field_data_field_address ON tickets_requests.user_id = field_data_field_address.entity_id WHERE  ticket_request_assignedTo.id = (SELECT MAX(id) FROM ticket_request_assignedTo WHERE request_id = ? AND assigned_user_id =?)",[req.body.request_id,req.body.assigned_user_id]);
        return rows[0]; 
    }
}