import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class SurveyModel {

    async readSurvey(data,users){
        const rows = await db.query("UPDATE crm_push_notification SET survey =?,author=?,to_user=? WHERE id = ?", [
            data.survey,
            data.author,
            JSON.stringify(users),
            data.id
        ])
        return rows[0];
    }

    async getSurveyNotification(){
        const rows = await db.query(`SELECT * FROM crm_push_notification`);
        return rows[0];
    }

    async getSurveyNotificationById(id){
        let x = id.join();
       const rows = await db.query("SELECT * FROM crm_push_notification WHERE id IN  (" + x + ") ");
        return rows[0];
    }

    async surveyPushNotification(req,allUser){
        const rows = await db.query('INSERT INTO `crm_push_notification`(`survey`,`author`,`to_user`)VALUES(?,?,?)', [
            req.body.title,
            req.body.author,
            JSON.stringify(allUser)
        ]);
        return rows[0];
    }

    async getSurveyNotify(req) {
        let surveyIds = req.body.join();
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,users.picture as picture,survey.title,survey.description,survey.survey_option as votingOption,survey.survey_type,survey.survey_view_option,survey.additional_anonymous_voting,survey.additional_cast_vote,survey.survey_notification_option,survey.status,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.id  IN (` + surveyIds + `)`);
        return rows[0];
    }

    async getSurveyByTitle(title) {
        const rows = await db.query(`SELECT survey.id,survey.survey_option,survey.survey_type,survey.survey_end_date,survey.survey_option,survey.survey_view_option,survey.additional_anonymous_voting,	survey.additional_cast_vote,survey.survey_notification_option,survey.user_id as author_id,users.mail as author,survey.title,survey.created_at,survey.description,survey.updated_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.title = ?`, [title]);
        return rows[0];
    }

    async CreateSurveyAnswer(id, answer) {
        const rows = await db.query('INSERT INTO `survey_answer`(`survey_id`,`survey_answer`)VALUES(?,?)', [
            id,
            answer
        ]);
        return rows[0];
    }

    async getAllUser() {
        const rows = await db.query(`SELECT uid,mail FROM users WHERE status = 1`);
        return rows[0];
    }

    async CreateSurveyUser(id, userId) {
        const rows = await db.query('INSERT INTO `survey_users`(`survey_id`,`user_id`)VALUES(?,?)', [
            id,
            userId
        ]);
        return rows[0];
    }

    async AddSurveyResult(req, answer) {
        const rows = await db.query('INSERT INTO `survey_result`(`user_id`,`survey_id`,`survey_answer_id`,`survey_type`)VALUES(?,?,?,?)', [
            req.body.userId,
            req.body.surveyId,
            answer,
            req.body.surveyType
        ]);
        return rows[0];
    }

    async AddSurveyResultTineon(req, answer) {
        const rows = await db.query('INSERT INTO `survey_result`(`tineon_user_id`,`survey_id`,`survey_answer_id`,`survey_type`)VALUES(?,?,?,?)', [
            req.body.tineon_user_id,
            req.body.surveyId,
            answer,
            req.body.surveyType
        ]);
        return rows[0];
    }

    async deleteVoteByUser(req) {
        const rows = await db.query("DELETE FROM `survey_result` WHERE `survey_id` = ? AND `user_id` = ?", [req.body.surveyId, req.body.userId]);
        return rows[0];
    }

    //API to delete previous vote for tineon user
    async deleteVoteByTineonUser(req) {
        const rows = await db.query("DELETE FROM `survey_result` WHERE `survey_id` = ? AND `tineon_user_id` = ?", [req.body.surveyId, req.body.tineon_user_id]);
        return rows[0];
    }

    async deleteSurveyAns(id) {
        const rows = await db.query("DELETE FROM `survey_answer` WHERE `survey_id` = ?", [id]);
        return rows[0];
    }
    async CreateSurvey(req, notificationOption) {
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query('INSERT INTO `survey`(`user_id`,`title`,`description`,`survey_option`,`survey_type`,`survey_start_date`,`survey_end_date`,`survey_view_option`,`additional_anonymous_voting`,`additional_cast_vote`,`survey_notification_option`,`created_at`)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)', [
            req.body.author,
            req.body.title,
            req.body.content,
            req.body.votingOption,
            req.body.surveyType,
            req.body.surveyStartDate,
            req.body.surveyEndDate,
            req.body.surveyViewOption,
            req.body.additionalAnonymousVoting,
            req.body.additionalCastVote,
            notificationOption,
            date,
        ]);
        return rows[0];
    }
    async getSurveyById(id) {
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_option as votingOption,survey.survey_type,survey.survey_view_option,survey.additional_anonymous_voting,survey.additional_cast_vote,survey.survey_notification_option,survey.status,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.id = ?`, [id]);
        return rows[0];
    }

    async getVotedSurveyBySurveyId(id) {
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_end_date,survey.created_at, survey_result.user_id, survey_result.survey_answer_id FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey_result.survey_id =?`, [id]);
        return rows[0];
    }

    //survey result by survey id for tineon user
    async getVotedSurveyBySurveyIdTineon(id) {
        const rows = await db.query(`SELECT survey_result.* FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id WHERE survey_result.survey_id =? `, [id]);
        return rows[0];
    }

    async voteBysurveyUser(sid, userid) {
        const rows = await db.query(`SELECT survey_result.survey_answer_id,survey_answer.survey_answer FROM survey_result   JOIN survey_answer ON survey_answer.id = survey_result.survey_answer_id  WHERE survey_result.survey_id =? AND survey_result.user_id =?`, [sid, userid]);
        return rows[0];
    }

    //function to get vote details by suvrey_id and Answer_id
    async voteBysurveyAnswer(sid, ansid) {
        const rows = await db.query(`SELECT survey_result.*,survey_answer.survey_answer FROM survey_result   JOIN survey_answer ON survey_answer.id = survey_result.survey_answer_id  WHERE survey_result.survey_id =? AND survey_result.survey_answer_id =?`, [sid, ansid]);
        return rows[0];
    }

    async voteBysurveyAnswerUserDetails(sid, ansid) {
        const rows = await db.query(`SELECT users.name,users.mail, users.uid , survey_result.survey_id,survey_result.survey_answer_id FROM survey_result JOIN users ON users.uid = survey_result.user_id WHERE survey_result.survey_id =? AND survey_result.survey_answer_id =?`, [sid, ansid]);
        return rows[0];
    }

    //function to get survey vote details by survey id and user Id for tineon
    async voteBysurveyTineonUser(sid, userid) {

        const rows = await db.query(`SELECT survey_result.survey_answer_id,survey_answer.survey_answer FROM survey_result   JOIN survey_answer ON survey_answer.id = survey_result.survey_answer_id  WHERE survey_result.survey_id =? AND survey_result.tineon_user_id =?`, [sid, userid]);
        return rows[0];
    }

    async VoteByAnsSurvey(sid, ansid) {
        const rows = await db.query(`SELECT survey_result.user_id,users.mail as voterName FROM survey_result LEFT JOIN users ON users.uid = survey_result.user_id WHERE survey_result.survey_id =? AND survey_result.survey_answer_id =?`, [sid, ansid]);
        return rows[0];
    }

    //for tineon survey to count votes by answer
    async VoteByAnsSurveyTineon(sid, ansid) {
        const rows = await db.query(`SELECT survey_result.user_id,survey_result.tineon_user_id,users.mail as voterName FROM survey_result LEFT JOIN users ON users.uid = survey_result.user_id WHERE survey_result.survey_id =? AND survey_result.survey_answer_id =?`, [sid, ansid]);
        return rows[0];
    }

    async getSurveyAnsById(id) {
        const rows = await db.query(`SELECT survey_answer.id,survey_answer.survey_id ,survey_answer.survey_answer FROM survey LEFT JOIN survey_answer ON survey_answer.survey_id = survey.id WHERE survey.id = ?`, [id]);
        return rows[0];
    }

    async activeCloseSurvey(id, status) {
        const rows = await db.query("UPDATE survey SET status = ? WHERE id = ?", [
            status,
            id
        ])
        return rows[0];
    }

    async getVotedSurveyByAnswerId(id) {
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey_result.survey_answer_id =?`, [id]);
        return rows[0];
    }

    async getActiveSurveyListPaginate(param){
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount, survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.status,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date >= ?  LIMIT `+pageSize+` OFFSET `+offset+``, [date]);
        return rows[0];
    }

    async getActiveSurveyList() {
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.status,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date >= ?`, [date]);
        return rows[0];
    }

    //function for Tineon CRM survey active survey
    async getActiveSurveyListCrm(param) {
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount, survey.*,users.mail as author,users.picture as picture FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE  survey.survey_end_date >= ? AND survey.survey_start_date <= ? ORDER BY id DESC  LIMIT `+pageSize+` OFFSET `+offset+``, [date, date]);
        return rows[0];
    }

    async getAllCompletedSurveyByPage(param) {
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount,  survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date < ?  LIMIT `+pageSize+` OFFSET `+offset+``, [date]);
        return rows[0];
    }

    async getAllCompletedSurvey(req) {
        let pageSize = req.params.pagesize?req.params.pagesize:10;
        let page= req.params.page?req.params.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount,survey.id,survey.user_id as author_id,users.mail as author,users.picture as picture,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date < ? LIMIT `+pageSize+` OFFSET `+offset+``, [date]);
        return rows[0];
    }

    async getAllCompletedSurveyUserPage(req){
        let pageSize = req.params.pagesize?req.params.pagesize:10;
        let page= req.params.page?req.params.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount, survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_users ON survey_users.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id  WHERE survey.survey_end_date < ? AND survey_users.user_id =?  LIMIT `+pageSize+` OFFSET `+offset+``, [date, req.params.user_id]);
        return rows[0];
    }

    async getAllCompletedSurveyUser(req) {
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_users ON survey_users.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id  WHERE survey.survey_end_date < ? AND survey_users.user_id =?`, [date, req.params.user_id]);
        return rows[0];
    }

    async getAllSurvey() {
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_end_date,survey.status,survey.created_at FROM survey LEFT JOIN users ON users.uid = survey.user_id`);
        return rows[0];
    }

    async getActiveSurveyPaginate(req) {
        let pageSize = req.params.pagesize?req.params.pagesize:10;
        let page= req.params.page?req.params.page:1
       let offset = (pageSize * page) - pageSize;
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount,  survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_users ON survey_users.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date >= ? AND survey.survey_start_date <= ? AND survey_users.user_id = ? LIMIT `+pageSize+` OFFSET `+offset+``, [date, date, req.params.user_id]);
        return rows[0];
    }

    async getActiveSurvey(req) {
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_users ON survey_users.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey.survey_end_date >= ? AND survey.survey_start_date <= ? AND survey_users.user_id = ?`, [date, date, req.params.user_id]);
        return rows[0];
    }

    async getVotedSurveyByIdPage(id,param) {
        let pageSize = param.pagesize?param.pagesize:10;
        let page= param.page?param.page:1
       let offset = (pageSize * page) - pageSize;
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount, survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey_result.user_id =? LIMIT `+pageSize+` OFFSET `+offset+``, [id]);
        return rows[0];
    }

    async getVotedSurveyById(id) {
        const rows = await db.query(`SELECT survey.id,survey.user_id as author_id,users.mail as author,survey.title,survey.description,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey_result.user_id =?`, [id]);
        return rows[0];
    }

    //funtion for geeting voted survey br Id for tineon user
    async getMyVotedSurveyById(req) {
        let id = req.params.user_id;
        let pageSize = req.params.pagesize?req.params.pagesize:10;
        let page= req.params.page?req.params.page:1
       let offset = (pageSize * page) - pageSize;
        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount,survey.id,survey.user_id as author_id,users.mail as author,users.picture as picture,survey.title,survey.description,survey.survey_start_date,survey.survey_end_date,survey.created_at FROM survey LEFT JOIN survey_result ON survey_result.survey_id = survey.id LEFT JOIN users ON users.uid = survey.user_id WHERE survey_result.tineon_user_id =? LIMIT `+pageSize+` OFFSET `+offset+``, [id]);
        return rows[0];
    }

    async deleteSurveybyId(id) {
        const rows = await db.query("DELETE FROM `survey` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getSurveyAnswer(id) {
        const rows = await db.query(`SELECT survey_answer FROM survey_answer WHERE survey_id = ?`, [id]);
        return rows[0];
    }

    async getUpdateSurvey(req, surveyNotificationOption) {
        let cudate = new Date()
        let cuday = cudate.getDate().toString().padStart(2, "0");
        let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
        let cuyear = cudate.getFullYear();
        var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
        const rows = await db.query("UPDATE survey SET user_id =?,title=?,description=?,survey_option=?,survey_type=?,survey_start_date=?,survey_end_date=?,survey_view_option=?,additional_anonymous_voting=?,additional_cast_vote=?,survey_notification_option=?,updated_at=? WHERE id = ?", [
            req.body.author,
            req.body.title,
            req.body.content,
            req.body.votingOption,
            req.body.surveyType,
            req.body.surveyStartDate,
            req.body.surveyEndDate,
            req.body.surveyViewOption,
            req.body.additionalAnonymousVoting,
            req.body.additionalCastVote,
            surveyNotificationOption,
            date,
            req.params.id
        ])
        return rows[0];
    }
}
