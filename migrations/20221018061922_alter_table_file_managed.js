exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('file_managed', function(t) {
        t.integer('uid').notNull().defaultTo(0).unsigned().comment('The users.uid of the user who is associated with the file.').alter();
        t.string('filename',[255]).notNull().defaultTo('').comment('	Name of the file with no path components. This may differ from the basename of the URI if the file is renamed to avoid overwriting an existing file.').alter();
        t.string('uri',[255]).notNull().defaultTo('').comment('The URI to access the file (either local or remote)').alter();
        t.string('filemime',[255]).notNull().defaultTo('').comment('The fileâ€™s MIME type.').alter();
        t.bigint('filesize').notNull().defaultTo(0).unsigned().comment('The size of the file in bytes').alter();
        t.integer('timestamp').notNull().defaultTo(0).unsigned().comment('UNIX timestamp for when the file was added.').alter();
        t.string('type',[50]).notNull().defaultTo('undefined').comment('The URI to access the file (either local or remote)').alter();
   
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('file_managed', function(t) {
       
    });
 };