import express from 'express';
import CrmProductController from '../../controllers/crm_products/crm_products';
import multerMW from '../../core/multer';

let router = express.Router();
let crmproductController =new CrmProductController();

router.post('/crmproductcreate', multerMW.single('file'), crmproductController.crmproductCreate.bind(crmproductController));
router.put('/crmproductedit/:id',multerMW.single('file'), crmproductController.crmproductUpdate.bind(crmproductController));
router.get('/crmproducts', crmproductController.getCrmProductAll.bind(crmproductController));
router.get('/crmproductsbyid/:id', crmproductController.getcrmproductsbyid.bind(crmproductController));
router.delete('/crmproductdelete/:id', crmproductController.crmproductDelete.bind(crmproductController));
router.post('/crmproductOrder/:user_id/:product_id',multerMW.single('file'), crmproductController.crmproductOrderCreate.bind(crmproductController));
router.get('/crmproductOrderList/:user_id', crmproductController.getcrmproductOrderList.bind(crmproductController));
router.get('/crmproduct-orders-admin', crmproductController.getCrmOrderAll.bind(crmproductController));
router.get('/crmorderdetail/:order_id', crmproductController.getcrmOrderDetail.bind(crmproductController));
router.get('/allcrmproducts', crmproductController.getAllCrmProduct.bind(crmproductController));
router.get('/crmproductsbyproductid/:id', crmproductController.getcrmproductsbyproductid.bind(crmproductController));
router.put('/crmproductStatusEdit/byAdmin/:order_id/:user_id',crmproductController.crmOrderProductStatusUpdate.bind(crmproductController));

export default router;