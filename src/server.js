import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import responseFormat from './core/response-format';
import authMiddleware from './core/authenticationMiddleware';
import handleRequestFormat from './core/handleRequestFormat';
import configContainer from './config/localhost';
import errorHandler from './core/errorHandler';
import customCors from './core/customCors';
import dbConnectionMiddleware from './core/dbConnectionMiddleware';
import versions from './versions/v1';
import webhook from '../src/api/billwerk_apis/billwerk_webhook/webhook'
import { Server } from 'socket.io';
import cronJob from '../src/core/cronJobs';
/**
 *  -------Initialize global variables-------------
 */
let config = configContainer.loadConfig();
let app = express();
app.use(cors());

/**
 * file upload limit extend body limit
 */
app.use(bodyParser.json({ limit: '500mb' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

/*
*   handle json syntax error in request 
*/
app.use(function (err, req, res, next) {

  if (err) {
    let response = responseFormat.createResponseTemplate(req.headers.lang);
    response = responseFormat.getResponseMessageByCodes(['common400'], { code: 400 });
    res.status(400).json(response);
  }
  else {
    next();
  }
});

/**
 *  Handle CORS request  
 */
app.use(customCors);

/**
 * check database is connected
 */
app.use(dbConnectionMiddleware);

/**
 *  Handle Authorization and  Authentication permission 
 */
app.use(webhook)
app.use(authMiddleware);

/**
 *  Handle Request is on proper format  
 */
app.use(handleRequestFormat);

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(versions);
app.use(cronJob);
/**
 *  catch 404 and forward to error handler
 */
app.use(function (err, req, res, next) {
  let response = responseFormat.createResponseTemplate(req.headers.lang);
  response = responseFormat.getResponseMessageByCodes(['common404'], { code: 404 });
  res.status(404).json(response);
});

/**
 *  error handler
 */

app.use(errorHandler);

/**
 * Callback function initialize port
 */

const sev = app.listen(configContainer.PORT, () => {
  console.log(`Using environment: ${config.node_env}...`);
  console.log(`Staffline API server running on port ${configContainer.PORT}...\n`);
});

const io = new Server(sev, {
  cors: {
    origin: '*',
    methods: ["GET", "POST"],
    credentials: true
  },
});

io.on("connection", (socket) => {
  socket.on("pushNotify", (noti) => {
    console.log('push-notify');
    socket.broadcast.emit('newnoti', noti)
  });
});  