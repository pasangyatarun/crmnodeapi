
/**
 *  -------Import all classes and packages -------------
 */
import lodash from 'lodash';
import CoreUtils from './core-utils';
import configContainer from '../config/localhost';

let config = configContainer.loadConfig();
/**
 *  Handle Exclude path which will only check Authorization key not Auth token 
 */
let excludeJwtPaths_base = [
    { url: '/signin', methods: ['POST'] },
    { url: '/signup', methods: ['POST'] },
    { url: '/signup-user', methods: ['POST'] },
    { url: '/emailverify', methods: ['POST'] },
    { url: '/resetpasswordlink', methods: ['POST'] },
    { url: '/resetpassword-linkexpiration', methods: ['POST'] },
    { url: '/resetpassword', methods: ['POST'] },
    { url: '/trial-register', methods: ['POST'] },
    { url: '/trialemailverify', methods: ['POST'] },
    { url: '/billwerk-recurringbilling', methods: ['POST'] },


];

/**
 *  Handle API Path which can be access before login and after login  
 */

let accessAndAuthUrl_base = [
    { url: '/accounts/verifycode', methods: ['POST'], regex: 0 },
    { url: '/accounts/createcode', methods: ['POST'], regex: 0 },
    { url: '/news', methods: ['GET'],regex: 0 },
    { url: '/newsbypagination/:page/pagesize/:pagesize', methods: ['GET'],regex: 1 ,pattern: /api\/newsbypagination\/[0-9a-z]/},
    { url: '/getAllAdvertisement', methods: ['GET'],regex: 0 },
    { url: '/billwerk-product', methods: ['GET'],regex: 0 },
    { url: '/billwerk-product-components', methods: ['GET'],regex: 0 },
    { url: '/userbyemail/:email', methods: ['GET'],regex: 1, pattern: /api\/userbyemail\/[0-9a-z]/ },
    { url: '/newsbyurl/:url', methods: ['GET'],regex: 1, pattern: /api\/newsbyurl\/[0-9a-z]/ },
    { url: '/pagesbyurl/:url', methods: ['GET'],regex: 1, pattern: /api\/pagesbyurl\/[0-9a-z]/ },
    { url: '/pages', methods: ['GET'],regex: 0 },
    { url: '/newsbyid/:id', methods: ['GET'],regex: 1, pattern: /api\/newsbyid\/[0-9a-z]/ },
    { url: '/newsby/newsids', methods: ['POST'],regex: 0},
    { url: '/allsurveylist', methods: ['GET'],regex: 0 },
    { url: '/crmproducts', methods: ['GET'],regex: 0 },
    { url: '/activesurvey', methods: ['GET'],regex: 0 },
    // { url: '/completedsurvey', methods: ['GET'],regex: 0 },
    { url: '/completedsurvey/:page/pagesize/:pagesize', methods: ['GET'],regex: 1 ,pattern: /api\/completedsurvey\/[0-9a-z]/ },
    { url: '/surveybyid/:id', methods: ['GET'],regex: 1, pattern: /api\/surveybyid\/[0-9a-z]/ },
    { url: '/billwerk-productbyid/:id', methods: ['GET'],regex: 1, pattern: /api\/billwerk-productbyid\/[0-9a-z]/ },
    { url: '/surveyby/surveyids', methods: ['POST'],regex: 0},
    { url: '/language', methods: ['POST'],regex: 0 },
    { url: '/productDisplay-for-customer', methods: ['GET'],regex: 0 },
    { url: '/allcrmproducts', methods: ['GET'],regex: 0 },
    { url: '/crmproductsbyproductid/:id', methods: ['GET'],regex: 1, pattern: /api\/crmproductsbyproductid\/[0-9a-z]/},

    
    //API path for tineon CRM survey
    { url: '/surveyaddvoteTineon', methods: ['POST'],regex: 0},
    { url: '/crm-surveyupdatevote', methods: ['PUT'],regex: 0},
    //{ url: '/crm-activesurvey', methods: ['GET'],regex: 0 },
    { url: ' /crm-activesurvey/:page/pagesize/:pagesize', methods: ['GET'],regex: 1 ,pattern: /api\/crm-activesurvey\/[0-9a-z]/},
    { url: '/crm-surveybyid/:id', methods: ['GET'],regex: 1 ,pattern: /crm-surveybyid\/[0-9a-z]/  },
    // { url: '/crm-myvotedsurvey/:user_id', methods: ['GET'],regex: 1 ,pattern: /api\/crm-myvotedsurvey\/[0-9a-z]/ },
    { url: '/crm-myvotedsurvey/:user_id/:page/pagesize/:pagesize', methods: ['GET'],regex: 1 ,pattern: /api\/crm-myvotedsurvey\/[0-9a-z]/ },
    { url: '/crm-surveyvotebyuser/:survey_id/user/:user_id', methods: ['GET'],regex: 1 ,pattern: /api\/crm-surveyvotebyuser\/[0-9a-z]/ } ,
    { url: '/crm-surveyvotebyAnswer/:survey_id/answer/:answer_id', methods: ['GET'],regex: 1 ,pattern: /api\/crm-surveyvotebyAnswer\/[0-9a-z]/ },
      
    
    //Api for Tineon Info on CRM
    { url: '/tineonInfoCreate', methods: ['POST'],regex: 0},
]; 


var api_versions = configContainer.api_versions;
let accessAndAuthUrl = [];
let excludeJwtPaths = [];

for (let i in api_versions) {
    let accessAndAuthUrl_f = accessAndAuthUrl_base.map(item => {
        return { url: (i + item.url), methods: item.methods, regex: item.regex, pattern: item.pattern };
    })
    Array.prototype.push.apply(accessAndAuthUrl, accessAndAuthUrl_f);

    let excludeJwtPaths_f = excludeJwtPaths_base.map(item => {
        return { url: (i + item.url), methods: item.methods };
    })
    Array.prototype.push.apply(excludeJwtPaths, excludeJwtPaths_f);
}


/**
 * All routes will be register here to validate url and request method
 */
let coreUtils = new CoreUtils(),
    prefix = "/api",
    expressRoutes = [],
    apiRoutes = coreUtils.parseRegisteredRoutes(expressRoutes);


/**
 * Return all routes with prefix
 */
function getRoutes() {

    return {
        prefix: prefix,
        apiRoutes: apiRoutes
    };
}



/**
 * Check route exists or not
 * @param {*} originalUrl 
 */
function routeExists(originalAPIUrl, reqMethod) {
    let originalUrl = originalAPIUrl.split('/api')[1];
    let appRoutes = getRoutes();
    const exists = lodash.find(appRoutes.apiRoutes, (route) => {
        let index = route.path.indexOf(":");
        let pathToCheck = route.path;

        if (index >= 0) {
            pathToCheck = route.path.substr(0, (index - 1));
            return ((originalUrl.indexOf(pathToCheck) >= 0) && (route.method == reqMethod));
        }
        return ((prefix + originalUrl == prefix + pathToCheck) && (route.method == reqMethod));
    });
    return exists;
}

export default {getRoutes,routeExists,excludeJwtPaths,accessAndAuthUrl}
