exports.up = async function(knex) {
    return knex.schema.createTable('advertisement', function(t) {
        t.increments('id').primary();
        t.text('name').notNull();
        t.text('text').notNull();
        t.string('picture',[600]).notNull();
        t.string('link',[500]).notNull();
        t.tinyint('status').notNull();
        t.date('start_date').notNull();
        t.date('end_date').notNull();
        t.datetime('created_at').notNull().defaultTo(knex.fn.now());
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('advertisement');
};

