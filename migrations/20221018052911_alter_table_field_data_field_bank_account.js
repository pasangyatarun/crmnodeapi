exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('field_data_field_bank_account', function(t) {
        t.string('entity_type',[128]).notNull().defaultTo('').comment('The entity type this data is attached to').alter();
        t.integer('entity_id').unsigned().comment('The entity type this data is attached to').alter();
        t.string('field_bank_account_value',[12]).alter();
        t.string('field_bank_account_format',[255]).alter();
        t.string('field_bank_code',[12]).alter();
      });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('field_data_field_bank_account', function(t) {
       
    });
 };