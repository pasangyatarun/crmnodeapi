exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('customer_members', function(t) {
        t.string('payment_type').after("app_in_use");
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('customer_members', function(t) {
        
    });
 };