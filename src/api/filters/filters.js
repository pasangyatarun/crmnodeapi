import express   from 'express';
import FilterController from '../../controllers/filters/filter.js'

let router = express.Router();
let filterController = new FilterController();

router.post('/filter', filterController.filterUser.bind(filterController));

export default router;