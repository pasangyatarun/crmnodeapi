import async from 'async';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import SurveyModel from '../../models/survey/survey';
import SurveyValidation from '../../validators/survey/survey.js';
import fetch from 'node-fetch';
import EmailTemplateModel from '../../models/email_template/email-template'
import { Json } from 'sequelize/dist/lib/utils';

let surveyModel = new SurveyModel();
let surveyValidation = new SurveyValidation();
let emailTemplateModel = new EmailTemplateModel();

export default class SurveyController {

    /**
    * Function for suvey Notification
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    surveyTineonNotify(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getSurveyNotify(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to create new survey
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    surveyCreate(req, res, next) {
        let sId;
        let allUser;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = surveyValidation.surveyCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            let templateData;
            let mailOptions;
            async.waterfall([
                function (done) {
                    surveyModel.getSurveyByTitle(req.body.title).then((surveyFind) => {
                        if (surveyFind && surveyFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    surveyModel.CreateSurvey(req, JSON.stringify(req.body.surveyNotificationOption)).then((surveyCreated) => {
                        if (surveyCreated.affectedRows > 0) {
                            sId = surveyCreated.insertId;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                },
                function (done) {
                    for (let i = 0; i < req.body.surveyAnswers.length; i++) {
                        surveyModel.CreateSurveyAnswer(sId, req.body.surveyAnswers[i]).then((surveyACreated) => {
                            if (surveyACreated.affectedRows > 0) {
                                if (i + 1 == req.body.surveyAnswers.length) {
                                    done();
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:surveyCreationFail'], { code: 417 });
                                res.status(200).json(response)
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    surveyModel.getAllUser().then((user) => {
                        if (user.length > 0) {
                            allUser = user;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                },
                function (done) {
                    for (let i = 0; i < allUser.length; i++) {
                        surveyModel.CreateSurveyUser(sId, allUser[i].uid).then((surveyUCreated) => {
                            if (surveyUCreated.affectedRows > 0) {
                                if (i + 1 == allUser.length) {
                                    done();
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:surveyCreationFail'], { code: 417 });
                                res.status(200).json(response)
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    var notific = {
                        "surveyId": sId
                    }
                    var tok = Math.floor(100000 + Math.random() * 900000) + 'CRMTOVCLOUDSECRET' + Math.floor(100000 + Math.random() * 900000);
                    let base64data = Buffer.from(tok).toString('base64');
                    fetch(configContainer.tineonNodeServerURL + 'api/crm-survey-created', {
                        'method': 'POST',
                        'headers': {
                            'secret': base64data,
                            'Content-Type': 'application/json'
                        },
                        'body': JSON.stringify(notific)
                    }).then(() => {
                        done();
                    }).catch(() => {
                        done();
                    })
                },
                function (done) {
                    emailTemplateModel.getemailTemplateByType('Survey Create').then((data) => {
                        if (data.length > 0) {
                            templateData = data;
                            done();
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })

                },
                // function (done) {
                //     let found = (req.body.surveyNotificationOption).includes('2');
                //     if (found == true) {
                //         for (let i = 0; i < allUser.length; i++) {
                //             allUser[i].status = 0;
                //             let link = configContainer.surveyVoteLink;
                //             let encrypted = link + commomMethods.encrypt(allUser[i].mail + '||' + new Date().getTime());
                //     if (templateData && templateData.length > 0) {
                //         mailOptions = {
                //             to: allUser[i].mail,
                //             subject: templateData[0].subject,
                //             html: `<!doctype html>
                //             <html lang="en-US">

                //             <head>
                //             <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                //             <title>Reset Password Email</title>
                //             <meta name="description" content="Reset Password Email Template.">
                //             <style type="text/css">
                //             a:hover {text-decoration: underline !important;}
                //             </style>
                //             </head>

                //             <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                //             <!--100% body table-->
                //             <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8"
                //             style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                //             <tr>
                //             <td>
                //             <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                //             align="center" cellpadding="0" cellspacing="0">
                //             <tr>
                //             <td style="height:80px;">&nbsp;</td>
                //             </tr>
                //             <tr>
                //             <td style="text-align:center;">
                //             <a href="#" title="logo" target="_blank">
                //             <img width="60" src=`+ templateData[0].logo + ` title="logo" alt="logo">
                //             </a>
                //             </td>
                //             </tr>
                //             <tr>
                //             <td style="height:20px;">&nbsp;</td>
                //             </tr>
                //             <tr>
                //             <td>
                //             <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                //             style="max-width:670px;background:#fff; border-radius:3px; text-align:left;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                //             <tr>
                //             <td style="height:40px;">&nbsp;</td>
                //             </tr>
                //             <tr>
                //             <td style="padding:0 35px;">
                //             <!--   <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:28px;font-family:'Rubik',sans-serif;">Survey Notification mail from<br> tineon AG</h1> -->

                //                                                     <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].header_content + `</p><br>

                //                                                     <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                //                                                     `+ templateData[0].template_body + `:<br>
                //                                                     <a href="`+ encrypted + `">` + encrypted + `</a>
                //                                                     </p><br><br>
                //                                                     <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">`+ templateData[0].footer_content + `,</p>
                //                                                     <!-- <p style="color:#455056; font-size:15px;line-height:24px; margin:0;font-weight: bold;">verein.cloud</p><br> -->
                //                                                     <!-- <a href="javascript:void(0);"
                //                                                     style="background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;">Reset
                //                                                         Password</a> -->
                //                                                         </td>
                //                                                         </tr>
                //                                                         <tr>
                //                                                         <td style="height:40px;">&nbsp;</td>
                //                                                         </tr>
                //                                                         </table>
                //                                                         </td>
                //                                                         <tr>
                //                                                         <td style="height:20px;">&nbsp;</td>
                //                                                         </tr>
                //                                                         <tr>
                //                                                         <td style="text-align:center;">
                //                                                         <p style="font-size:14px; color:rgba(69, 80, 86, 0.7411764705882353); line-height:18px; margin:0 0 0;">&copy; <strong>`+ templateData[0].url + `</strong></p>
                //                                                         </td>
                //                                                         </tr>
                //                                                         <tr>
                //                                                         <td style="height:80px;">&nbsp;</td>
                //                                                         </tr>
                //                                                         </table>
                //                         </td>
                //                     </tr>
                //                 </table>
                //                 <!--/100% body table-->
                //                 </body>

                //                 </html>`,
                //         }
                //     }else{
                //              mailOptions = {
                //                 to: allUser[i].mail,
                //                 subject: 'new survey is created on CRM',
                //                 html: `<h3>Dear association member,</h3>

                //                     <h4>Thank you for your interest in our S-Club |
                //                     Online club management.</h4>

                //                    <h5> Please vote via the following link: </h5>
                //                  <a href="`+ encrypted + `">` + encrypted + `</a>
                //                  <h3>If the forwarding does not work, please copy the link in
                //                     the address bar of your browser.
                //                     </h3>`,
                //             }
                //         }

                //             sendEmail(mailOptions).then((sentdata) => {

                //                 if (sentdata == 'error') {
                //                     if (allUser.length == i + 1) {
                //                         done();

                //                     }
                //                 } else {
                //                     if (allUser.length == i + 1) {
                //                         done();

                //                     }

                //                 }
                //             })

                //         }
                //     } else {
                //         done();
                //     }

                // },
                function (done) {
                    let found = (req.body.surveyNotificationOption).includes('1');
                    if (found == true) {
                        surveyModel.surveyPushNotification(req, allUser).then((surveyACreated) => {

                            response = responseFormat.getResponseMessageByCodes(['survey:surveyCreated']);
                            res.status(200).json(response);

                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyCreated']);
                        res.status(200).json(response);
                    }
                }

            ])
        }
    }

    /**
    * Function to read survey notification
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {message} Success message
    */
    surveyRead(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let data = [];
        let userTemp = [];
        async.waterfall([
            function (done) {
                surveyModel.getSurveyNotificationById(req.body.id).then((results) => {
                    if (results.length > 0) {
                        data = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let i = 0; i < data.length; i++) {
                    userTemp = JSON.parse(data[i].to_user);
                    for (let j = 0; j < userTemp.length; j++) {
                        if (userTemp[j].mail == req.body.loginEmail) {
                            userTemp[j].status = 1;
                        }
                    };
                    data[i].to_user = (userTemp);
                    if (data.length == i + 1) {
                        done();
                    }
                };
            },
            function (done) {
                for (let q = 0; q < data.length; q++) {
                    surveyModel.readSurvey(data[q], data[q].to_user).then((result) => {
                        if (result.affectedRows > 0) {
                            if (data.length == q + 1) {
                                response = responseFormat.getResponseMessageByCodes(['survey:surveyReaded']);
                                res.status(200).json(response);
                            }
                        } else {
                            if (data.length == q + 1) {
                                response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }
                    }).catch((error) => {
                        if (data.length == q + 1) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    })
                }
            }
        ])
    }

    /**
    * Function to get survey notification
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    surveyNotification(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        var filteredArray = [];
        var displayNotification = '';
        async.waterfall([
            function (done) {
                surveyModel.getSurveyNotification().then((result) => {
                    if (result.length > 0) {
                        for (let i = 0; i < result.length; i++) {
                            let x = JSON.parse(result[i].to_user)
                            for (let j = 0; j < x.length; j++) {
                                if (x[j].mail == req.body.mail && !x[j].status) {
                                    filteredArray.push(result[i]);
                                    filteredArray.sort((a, b) => b.timestamp - a.timestamp)
                                    displayNotification = filteredArray[0];
                                }
                                if ((result.length == i + 1) && (x.length == j + 1)) {
                                    done();
                                }
                            }
                        }
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: filteredArray } });
                res.status(200).json(response);
            }
        ])
    }

    /**
    * Function to get survey details by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} survey details
    */
    getSurveybyId(req, res, next) {
        let data;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getSurveyById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                surveyModel.getSurveyAnsById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data[0].surveyAnswerOption = results;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: data } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get survey  by it for tineon CRM survey section
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} survey details
    */
    getSurveybyIdTineon(req, res, next) {
        let data;
        let TotalVoteCount;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getSurveyById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }, function (done) {
                surveyModel.getSurveyAnsById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data[0].surveyAnswerOption = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                surveyModel.getVotedSurveyBySurveyIdTineon(req.params.id).then((results) => {
                    if (results) {
                        TotalVoteCount = results.length;
                        data[0].votes = results;
                        data[0].TotalVoteCount = results.length;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                var surveyAnswerOption = data[0].surveyAnswerOption
                for (let j = 0; j < surveyAnswerOption.length; j++) {
                    surveyAnswerOption[j].answerId = surveyAnswerOption[j].id;
                    surveyModel.VoteByAnsSurveyTineon(req.params.id, surveyAnswerOption[j].id).then((results) => {
                        if (results) {
                            surveyAnswerOption[j].votedUsers = results;
                            surveyAnswerOption[j].votedUsersCount = results.length;
                            if (surveyAnswerOption.length == j + 1) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: data, surveyAnswerOption } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])
    }

    /**
    * Function to get pagination for voted survey
    * @author  MangoIt Solutions
    * @param   {user_id,pagesize,page}
    * @return  {object} survey details
    */
    getVotedSurveyPagination(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getVotedSurveyByIdPage(req.params.user_id, req.params).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get  voted survey
    * @author  MangoIt Solutions
    * @param   {user_id}
    * @return  {object} survey details
    */
    getVotedSurvey(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getVotedSurveyById(req.params.user_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get Myvoted survey recods for tineon users 
    * @author  MangoIt Solutions
    * @param   {user_id}
    * @return  {object} survey details
    */
    getMyVotedSurvey(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getMyVotedSurveyById(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 }, 'results': results }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to set status of survey
    * @author  MangoIt Solutions
    * @param   {id, status}
    * @return  {message} success message
    */
    surveyActiveClose(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let data;
        async.waterfall([
            function (done) {
                surveyModel.getSurveyById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        data = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (data[0].status == 0) {
                    surveyModel.activeCloseSurvey(req.params.id, 1).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['survey:closedSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                } else {
                    surveyModel.activeCloseSurvey(req.params.id, 0).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['survey:closedSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])

    }

    /**
    * Function to set status of survey
    * @author  MangoIt Solutions
    * @param   {survey_id, user_id}
    * @return  {object} survey details
    */
    getVoteBySurveyUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.voteBysurveyUser(req.params.survey_id, req.params.user_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get survey vote details by survey id and user Id for CRM tineon
    * @author  MangoIt Solutions
    * @param   {survey_id, user_id}
    * @return  {object} survey details
    */
    getVoteBySurveyTineonUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.voteBysurveyTineonUser(req.params.survey_id, req.params.user_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get voted survey By Answer
    * @author  MangoIt Solutions
    * @param   {answerId}
    * @return  {object} survey details
    */
    getVotedSurveyByAnswer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getVotedSurveyByAnswerId(req.params.answer_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function for getting vote count by survey_id and Answer_id
    * @author  MangoIt Solutions
    * @param   {survey_id,answerId}
    * @return  {object} survey details
    */
    getVoteBySurveyAnswer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let data;
        async.waterfall([
            function (done) {
                surveyModel.voteBysurveyAnswer(req.params.survey_id, req.params.answer_id).then((results) => {

                    if (results) {
                        data = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                surveyModel.voteBysurveyAnswerUserDetails(req.params.survey_id, req.params.answer_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [data, { count: data.length }, results] } });
                        res.status(200).json(response);
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function for getting vote count by survey_id and Answer_id
    * @author  MangoIt Solutions
    * @param   {survey_id,answerId}
    * @return  {object} survey details
    */
    getVoteBySurveyAns(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let surveyAnswerOption;
        let TotalVoteCount;
        async.waterfall([
            function (done) {
                surveyModel.getSurveyAnsById(req.params.survey_id).then((results) => {
                    if (results.length > 0) {
                        surveyAnswerOption = results;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                surveyModel.getVotedSurveyBySurveyId(req.params.survey_id).then((results) => {
                    if (results.length > 0) {
                        TotalVoteCount = results.length;
                        done();
                    } else {
                        TotalVoteCount = 0;
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                for (let j = 0; j < surveyAnswerOption.length; j++) {
                    surveyAnswerOption[j].answerId = surveyAnswerOption[j].id;
                    surveyModel.VoteByAnsSurvey(req.params.survey_id, surveyAnswerOption[j].id).then((results) => {
                        if (results) {
                            surveyAnswerOption[j].votedUsers = results;
                            surveyAnswerOption[j].votedUsersCount = results.length;
                            if (surveyAnswerOption.length == j + 1) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'TotalVoteCount': TotalVoteCount }, surveyAnswerOption] } });
                                res.status(200).json(response);
                            }
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            }
        ])
    }

    /**
    * Function to get voted survey by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object} survey details
    */
    getVotedSurveyById(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getVotedSurveyBySurveyId(req.params.survey_id).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get active survey paginate
    * @author  MangoIt Solutions
    * @param   {user_id,pagesize,pag}
    * @return  {object} survey details
    */
    getActiveSurveyPaginate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getActiveSurveyPaginate(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get active survey details
    * @author  MangoIt Solutions
    * @param   {user_id}
    * @return  {object} survey details
    */
    getActiveSurvey(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getActiveSurvey(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get all survey
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    getAllSurvey(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getAllSurvey().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get active survey list by Page
    * @author  MangoIt Solutions
    * @param   {user_id,pagesize,page}
    * @return  {object} survey details
    */
    getActiveSurveyListByPage(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getActiveSurveyListPaginate(req.params).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get active survey list 
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    getActiveSurveyList(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getActiveSurveyList().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to controller for Tineon CRM survey Active survey
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    getActiveSurveyListCrm(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getActiveSurveyListCrm(req.params).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 }, 'result': results }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get completed survey by pagination
    * @author  MangoIt Solutions
    * @param   {page,pagesize}
    * @return  {object} survey details
    */
    getCompletedSurveyByPagination(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getAllCompletedSurveyByPage(req.params).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to get completed survey 
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} survey details
    */
    getCompletedSurvey(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getAllCompletedSurvey(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [{ 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 }, 'results': results }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get completed survey by user pagination
    * @author  MangoIt Solutions
    * @param   {user_id,pagesize,page}
    * @return  {object} survey details
    */
    getCompletedSurveyByUserPagination(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getAllCompletedSurveyUserPage(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get completed survey by user 
    * @author  MangoIt Solutions
    * @param   {user_id}
    * @return  {object} survey details
    */
    getCompletedSurveyByUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                surveyModel.getAllCompletedSurveyUser(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete survey by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    surveyDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        surveyModel.deleteSurveybyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['survey:surveyDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update survey by id
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {message} success message
    */
    surveyUpdate(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let a4;
        let msgCode = surveyValidation.surveyCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    surveyModel.getSurveyByTitle(req.body.title).then((surveyFind) => {
                        if (surveyFind.length > 0 && req.params.id != surveyFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    surveyModel.getUpdateSurvey(req, JSON.stringify(req.body.surveyNotificationOption)).then((result) => {
                        if (result.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    surveyModel.deleteSurveyAns(req.params.id).then((result) => {
                        // if (result.length > 0) {
                        //     a4 = req.body.surveyAnswers.filter(entry1 => !result.some(entry2 => entry1 === entry2.survey_answer));
                        //     done();
                        // } else {
                        //     a4 = req.body.surveyAnswers;
                        //     done();
                        // }
                        done();
                    })
                },
                function (done) {
                    for (let y = 0; y < req.body.surveyAnswers.length; y++) {
                        surveyModel.CreateSurveyAnswer(req.params.id, req.body.surveyAnswers[y]).then((result) => {
                            if (result.affectedRows > 0) {
                                if (req.body.surveyAnswers.length == y + 1) {
                                    done();
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                function (done) {
                    response = responseFormat.getResponseMessageByCodes(['survey:surveyUpdated']);
                    res.status(200).json(response);
                }
            ])
        }
    }

    /**
    * Function to add survey votes 
    * @author  MangoIt Solutions
    * @param   {object} with user_id,survey_id,answer_id
    * @return  {message} success message
    */
    surveyAddVote(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = surveyValidation.surveyAdd(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    // req.body.surveyAnswerId.forEach(element => {
                    //     surveyModel.AddSurveyResult(req, element).then((surveyAdded) => {
                    //         if (surveyAdded.affectedRows > 0) {
                    //             response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                    //             res.status(200).json(response);
                    //         } else {
                    //             response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                    //             res.status(200).json(response);
                    //         }
                    //     }).catch((error) => {
                    //         response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    //         res.status(500).json(response);
                    //         return;
                    //     })
                    // });
                    for (let y = 0; y < req.body.surveyAnswerId.length; y++) {
                        surveyModel.AddSurveyResult(req,req.body.surveyAnswerId[y]).then((surveyAdded) => { 
                            if (surveyAdded.affectedRows > 0) {
                                if (req.body.surveyAnswerId.length == y + 1) {
                                    response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }
            ])
        }
    }

    /**
    * Function to add votes on survey by Tineon web app user
    * @author  MangoIt Solutions
    * @param   {object} with user_id,survey_id,answer_id
    * @return  {message} success message
    */
    surveyAddVoteTineon(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = surveyValidation.surveyAddTineon(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    // req.body.surveyAnswerId.forEach(element => {
                    //     surveyModel.AddSurveyResultTineon(req, element).then((surveyAdded) => {
                    //         if (surveyAdded.affectedRows > 0) {
                    //             response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                    //             res.status(200).json(response);
                    //         } else {
                    //             response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                    //             res.status(200).json(response);
                    //         }
                    //     }).catch((error) => {
                    //         response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    //         res.status(500).json(response);
                    //         return;
                    //     })
                    // });

                        for (let y = 0; y < req.body.surveyAnswerId.length; y++) {
                            surveyModel.AddSurveyResultTineon(req,req.body.surveyAnswerId[y]).then((surveyAdded) => { 
                                if (surveyAdded.affectedRows > 0) {
                                    if (req.body.surveyAnswerId.length == y + 1) {
                                        response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                                        res.status(200).json(response);
                                    }
                                } else {
                                    response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                                    res.status(200).json(response);
                                }
                            }).catch((error) => {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            })
                        }
                    },
            ])
        }
    }

    /**
    * Function to update survey vote
    * @author  MangoIt Solutions
    * @param   {object} with user_id,survey_id,answer_id
    * @return  {message} success message
    */
    surveyUpdateVote(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = surveyValidation.surveyAdd(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    surveyModel.deleteVoteByUser(req).then((results) => {
                        if (results.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                // function (done) {
                //     req.body.surveyAnswerId.forEach(element => {
                //         surveyModel.AddSurveyResult(req, element).then((surveyAdded) => {
                //             if (surveyAdded.affectedRows > 0) {
                //                 response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                //                 res.status(200).json(response);
                //             } else {
                //                 response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                //                 res.status(200).json(response);
                //             }
                //         }).catch((error) => {
                //             response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                //             res.status(500).json(response);
                //             return;
                //         })
                //     });
                // }
                function (done){
                    for (let y = 0; y < req.body.surveyAnswerId.length; y++) {
                        surveyModel.AddSurveyResult(req,req.body.surveyAnswerId[y]).then((surveyAdded) => { 
                            if (surveyAdded.affectedRows > 0) {
                                if (req.body.surveyAnswerId.length == y + 1) {
                                    response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                }
                
            ])
        }
    }

    /**
    * Function to  Update survey vote by tineon user in CRM 
    * @author  MangoIt Solutions
    * @param   {object} with user_id,survey_id,answer_id
    * @return  {message} success message
    */
    surveyUpdateVoteTineonUser(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = surveyValidation.surveyAddTineon(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    surveyModel.deleteVoteByTineonUser(req).then((results) => {
                        if (results.affectedRows > 0) {
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['survey:surveyNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                // function (done) {
                //     req.body.surveyAnswerId.forEach(element => {
                //         surveyModel.AddSurveyResultTineon(req, element).then((surveyAdded) => {
                //             if (surveyAdded.affectedRows > 0) {
                //                 done();
                //             } else {
                //                 response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                //                 res.status(200).json(response);
                //             }
                //         }).catch((error) => {
                //             console.log("----error---",error)
                //             response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                //             res.status(500).json(response);
                //             return;
                //         })
                //     });
                // },
                // function (done) {
                //     response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                //                 res.status(200).json(response);
                // }
                function (done) {
                    for (let y = 0; y < req.body.surveyAnswerId.length; y++) {
                        surveyModel.AddSurveyResultTineon(req,req.body.surveyAnswerId[y]).then((surveyAdded) => { 
                            if (surveyAdded.affectedRows > 0) {
                                if (req.body.surveyAnswerId.length == y + 1) {
                                    response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                                    res.status(200).json(response);
                                }
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['survey:voteFail'], { code: 417 });
                                res.status(200).json(response);
                            }
                        }).catch((error) => {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    }
                },
                // function (done) {
                //     console.log("----done----")
                //     response = responseFormat.getResponseMessageByCodes(['survey:voteSuccess']);
                //     res.status(200).json(response);
                // }
            ])
        }
    }
}

