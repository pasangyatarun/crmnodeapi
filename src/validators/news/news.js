export default class AuthValidation {
    newsCreate(newsData){
        let err = new Array();
        if(!newsData.title ){
            err.push('title:titleReq');
        }
        if(!newsData.content ){
            err.push('content:contentReq');
        } 
        if(!newsData.author ){
            err.push('author:authorReq');
        }      
        return err;
    }
}