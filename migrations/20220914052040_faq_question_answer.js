exports.up = async function(knex) {
    return knex.schema.createTable('faq_question_answer', function(t) {
        t.increments('id').primary();
        t.integer('category_id').notNull();
        t.integer('author_id').notNull();
        t.longtext('question').notNull();
        t.longtext('answer').notNull();
        t.datetime('created_at').notNull().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('faq_question_answer');
};
