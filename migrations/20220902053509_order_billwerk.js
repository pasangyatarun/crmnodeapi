exports.up = async function(knex) {
    return knex.schema.createTable('order_billwerk', function(t) {
        t.increments('id').primary();
        t.string('order_id',[64]).notNull();
        t.integer('quantity',[11]);
        t.string('planId',[255]).notNull();
        t.string('planVariantId',[255]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.longtext('data').notNull();
     });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('order_billwerk');
};