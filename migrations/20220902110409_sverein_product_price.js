exports.up = async function (knex) {
    return knex.schema.createTable('sverein_product_price', function (t) {
        t.increments('pid').primary().comment('Product ID.');
        t.integer('suid',[11]).notNull().defaultTo(0).comment('User ID (sales partner).');
        t.integer('cuid',[11]).notNull().defaultTo(0).comment('User ID (customer) or 0 if default price.	');
        t.integer('price',[11]).notNull().defaultTo(0).comment('Price per month in euro cent.');
        t.integer('validuntil', [10]).defaultTo(0).comment('Unix timestamp until the price is valid.');
     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('sverein_product_price');
};

