import DBConnection from '../../core/db';
let db = new DBConnection();

export default class LicenceModel {

    async registerLicenceInformation(uid) {
        const rows = await db.conn.promise().query('INSERT INTO `sverein_licence_summary`(`cuid`,`updated`,`installed`)VALUES(?,?,?)', [
            uid,
            new Date().getTime(),
            new Date().getTime()
        ]);
        return rows[0];
    }

    async getlicenceById(lid) {
        const rows = await db.conn.promise().query("SELECT * FROM `sverein_licence` WHERE lid = ?", [lid]);
        return rows[0];
    }

    async getlicenceBylicence(title) {
        const rows = await db.conn.promise().query("SELECT * FROM `sverein_licence` WHERE title = ?", [title]);
        return rows[0];
    }

    async createlicence(req) {
        const rows = await db.conn.promise().query('INSERT INTO `sverein_licence`(`title`,`data`,`publish_to`,`domain`)VALUES(?,?,?,?)', [
            req.body.title,
            req.body.description,
            req.body.publish,
            req.body.domain
        ]);
        return rows[0];
    }
    
    async getAlllicence() {
        const rows = await db.conn.promise().query("SELECT * FROM `sverein_licence`");
        return rows[0];
    }

    async deletelicence(id) {
        const rows = await db.conn.promise().query("DELETE FROM `sverein_licence` WHERE `lid`=?", [id]);
        return rows[0];
    }
    
    async updatelicence(req) {
        const rows = await db.conn.promise().query("UPDATE sverein_licence SET title = ?,domain= ?,data =?,publish_to = ? WHERE lid = ?", [
            req.body.title,
            req.body.domain,
            req.body.description,
            req.body.publish,
            req.params.id
        ])
        return rows[0];
    }

}