import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import axios from 'axios';
import qs from 'qs';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');
import CustomerModel from '../../../models/billwerk_customer/billwerk_customer';
let customerModel = new CustomerModel();

export default class BillwerkCustomerController {
	constructor() {
	}
	/**
		 * Function for customer update
		 * @author  MangoIt Solutions
		 * @return  {object} customer details
		 */
	getBillwerkCustomer(req, res) {
		var a1 = [];
		async.waterfall([
			function (done) {
				var config = {
					method: 'get',
					url: configContainer.billwerkAccountUrl + 'customers',
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
					}
				};
				axios(config)
					.then(function (resPonse) {
						a1 = resPonse.data;
						done();
					})
					.catch(function (error) {
					});
			},
			function (done) {
				for (let i = 0; i < a1.length; i++) {
					customerModel.checkEmailExists(a1[i].EmailAddress).then(async (data) => {
						if (data.length <= 0) {
							await customerModel.insertUser(a1[i]).then(async (ress) => {
								if (ress.affectedRows > 0) {
									await customerModel.insertUserRole(ress.insertId).then(async (roleInserted) => {
										if (roleInserted.affectedRows > 0) {
											await customerModel.insertUserAddress(a1[i], ress.insertId).then(async (addinserted) => {
												if (addinserted.affectedRows > 0) {
													// 	if(a1[i].length == i + 1){
													//   Console.log("-----")
													// 		done();
													// 	}
												}
											}).catch(function (err) {
											})
										}
									}).catch(function (err) {

									})
								}
							}).catch(function (err) {

							})
						}
						else {
						}
					}).catch((error) => {
					})
					if (a1[i].length == i + 1) {
						done();
					}
				}

			},
			function (done) {
				console.log("User Imported")
			}
		])
	}

}