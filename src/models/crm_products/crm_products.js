import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class CrmProductModel {
	async CreateCrmProduct(req, Img) {
		let cudate = new Date()
		let cuday = cudate.getDate().toString().padStart(2, "0");
		let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
		let cuyear = cudate.getFullYear();
		var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
		const rows = await db.query('INSERT INTO `crm_product`(`product_name`,`image`,`description`,`price`,`component_type`,`tax_policy`,`non_visible_role_category`,`status`,`created_at`)VALUES(?,?,?,?,?,?,?,?,?)', [
			req.body.product_name,
			Img,
			req.body.description,
			req.body.price,
			req.body.component_type,
			req.body.tax_policy,
			req.body.nonVisibleRoleCategory,
			1,
			date,
		]);
		return rows[0];
	}

	async getCrmProductAll() {
		const rows = await db.query(`SELECT * from crm_product WHERE status=? ORDER BY price ASC`, [1]);
		return rows[0];

	}
	async getallcrmproducts() {
		const rows = await db.query(`SELECT * from crm_product`);
		return rows[0];

	}

	async getcrmproductsbyid(id) {
		const rows = await db.query(`SELECT * FROM crm_product WHERE id = ?`, [id]);
		return rows[0];
	}

	async getcrmproductsbyproductid(id) {
		const rows = await db.query(`SELECT * FROM crm_product WHERE id = ?`, [id]);
		return rows[0];
	}

	async getCrmProductByName(product_name) {
		const rows = await db.query(`SELECT * FROM crm_product WHERE product_name = ?`, [product_name]);
		return rows[0];
	}

	async crmproductUpdate(req, Img) {
		let cudate = new Date()
		let cuday = cudate.getDate().toString().padStart(2, "0");
		let cumonth = (cudate.getMonth() + 1).toString().padStart(2, "0");
		let cuyear = cudate.getFullYear();
		var date = cuyear + "-" + cumonth + "-" + cuday + "T00:0.000Z;";
		const rows = await db.query("UPDATE crm_product SET product_name= ?,image=?,description= ?,price= ?,component_type=?,tax_policy=?,non_visible_role_category=?,modified_at=? WHERE id = ?", [
			req.body.product_name,
			Img,
			req.body.description,
			req.body.price,
			req.body.component_type,
			req.body.tax_policy,
			req.body.nonVisibleRoleCategory,
			date,
			req.params.id
		])
		return rows[0];
	}

	async deleteCrmProductbyId(id) {
		const rows = await db.query("UPDATE crm_product SET status= ? WHERE id=?", [0, id]);
		return rows[0];
	}

	async getClubOrder(user_id) {
		const rows = await db.query("select * FROM `billwerk_user_customer` WHERE `user_id`=? AND `type` IN (?,?,?)", [user_id, 1, 3,4]);
		return rows[0];
	}

	async getCrmProductOrderCheck(user_id, product_id) {
		const rows = await db.query("select * FROM `crm_product_order` WHERE `user_id`=? AND `crm_product_id`=?", [user_id, product_id]);
		return rows[0];
	}

	async CreateCrmProductOrder(req, Img) {
		const rows = await db.query('INSERT INTO `crm_product_order`(`user_id`,`crm_product_id`,`account_holder_name`,`iban`,`coupon_code`,`chairman_name`,`association`,`payment_method`,`mandate_text`,`file`)VALUES(?,?,?,?,?,?,?,?,?,?)', [
			req.params.user_id,
			req.params.product_id,
			req.body.account_holder_name,
			req.body.iban,
			req.body.coupon_code,
			req.body.chairman_name,
			req.body.association,
			req.body.payment_method,
			req.body.mandate_text,
			Img
		]);
		return rows[0];
	}

	async getCrmOrderByUser(user_id) {
		const rows = await db.query("SELECT crm_product_order.id,crm_product_order.user_id,crm_product_order.crm_product_id,crm_product_order.account_holder_name,crm_product_order.iban,crm_product_order.coupon_code,crm_product_order.chairman_name,crm_product_order.association,crm_product_order.payment_method,crm_product_order.mandate_text,crm_product_order.file,crm_product_order.created_at,users.name as username,users.mail,users.status,users.picture,users.contact_number,crm_product.product_name,crm_product.image as productImage,field_data_field_address.field_address_first_name AS firstname,field_data_field_address.field_address_last_name AS lastname,field_data_field_address.field_address_organisation_name AS society,field_data_field_address.field_address_country AS country,field_data_field_address.field_address_locality AS city,field_data_field_address.field_address_postal_code As zipcode,field_data_field_address.field_address_thoroughfare AS address, crm_product.description,crm_product.price,crm_product.component_type,crm_product.tax_policy,crm_product.non_visible_role_category,crm_product.status FROM `crm_product_order` LEFT JOIN users ON crm_product_order.user_id = users.uid LEFT JOIN field_data_field_address ON crm_product_order.user_id = field_data_field_address.entity_id LEFT JOIN crm_product ON crm_product_order.crm_product_id = crm_product.id WHERE `user_id`=? ", [user_id]);
		return rows[0];
	}

	async getCrmAllOrders() {
		const rows = await db.query("SELECT crm_product_order.id,crm_product_order.user_id,crm_product_order.crm_product_id,crm_product_order.account_holder_name,crm_product_order.iban,crm_product_order.coupon_code,crm_product_order.chairman_name,crm_product_order.association,crm_product_order.payment_method,crm_product_order.mandate_text,crm_product_order.file,crm_product_order.order_status,crm_product_order.created_at,users.name as username,users.mail,users.status,users.picture,users.contact_number,crm_product.product_name,crm_product.image as productImage,field_data_field_address.field_address_first_name AS firstname,field_data_field_address.field_address_last_name AS lastname,field_data_field_address.field_address_organisation_name AS society,field_data_field_address.field_address_country AS country,field_data_field_address.field_address_locality AS city,field_data_field_address.field_address_postal_code As zipcode,field_data_field_address.field_address_thoroughfare AS address, crm_product.description,crm_product.price,crm_product.component_type,crm_product.tax_policy,crm_product.non_visible_role_category,crm_product.status FROM `crm_product_order` LEFT JOIN users ON crm_product_order.user_id = users.uid LEFT JOIN field_data_field_address ON crm_product_order.user_id = field_data_field_address.entity_id LEFT JOIN crm_product ON crm_product_order.crm_product_id = crm_product.id");
		return rows[0];
	}
	
	async getorderDetail(oid) {
		const rows = await db.query("SELECT crm_product_order.user_id,crm_product_order.crm_product_id,crm_product_order.account_holder_name,crm_product_order.iban,crm_product_order.coupon_code,crm_product_order.chairman_name,crm_product_order.association,crm_product_order.payment_method,crm_product_order.mandate_text,crm_product_order.file,crm_product_order.created_at,users.name as username,users.mail,users.status,users.picture,users.contact_number,crm_product.product_name,crm_product.image as productImage, field_data_field_address.field_address_first_name AS firstname,field_data_field_address.field_address_last_name AS lastname,field_data_field_address.field_address_organisation_name AS society,field_data_field_address.field_address_country AS country,field_data_field_address.field_address_locality AS city,field_data_field_address.field_address_postal_code As zipcode,field_data_field_address.field_address_thoroughfare AS address,crm_product.description,crm_product.price,crm_product.component_type,crm_product.tax_policy,crm_product.non_visible_role_category,crm_product.status FROM `crm_product_order` LEFT JOIN users ON crm_product_order.user_id = users.uid LEFT JOIN field_data_field_address ON crm_product_order.user_id = field_data_field_address.entity_id LEFT JOIN crm_product ON crm_product_order.crm_product_id = crm_product.id WHERE crm_product_order.id=?", [oid]);
		return rows[0];
	}

	async getCrmOrderProductById(order_id) {
		const rows = await db.query("select * FROM `crm_product_order` WHERE `id`=?",[order_id]);
		return rows[0];
	}

	
	async crmOrderProductStatusUpdate(req) {
		const rows = await db.query("UPDATE crm_product_order SET 	order_status= ? WHERE id=?", [req.body.order_status,req.params.order_id]);
		return rows[0];
	}
}