exports.up = async function(knex) {
    return knex.schema.createTable('club_support_user', function(t) {
        t.increments('id').primary();
        t.integer('user_id').notNull();
        t.string('user',[255]).notNull();
        t.string('password',[255]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
        // t.datetime('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('club_support_user');
};
