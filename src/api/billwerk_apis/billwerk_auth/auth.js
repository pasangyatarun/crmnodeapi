import express   from 'express';
import BillwerkAuthController from '../../../controllers/billwerk_controllers/billwerk_auth/auth';

let router = express.Router();
let billwerkAuthController = new BillwerkAuthController();

router.get('/billwerk-signin', billwerkAuthController.signin.bind(billwerkAuthController));
router.put('/billwerk-customer-update/:customer_id', billwerkAuthController.customerUpdate.bind(billwerkAuthController));
router.get('/billwerk-allpaymentbearer-bcustomer/:customer_id', billwerkAuthController.allPaymentDetailByCustomer.bind(billwerkAuthController));

export default router;