import dotenv from 'dotenv'
dotenv.config();
let configContainer = {

    /**
     * port setting
     */
    PORT: process.env.PORT,

    /**
     * authentication token key for
     */
    jwtSecretKey: process.env.jwtSecretKey,

    /**
     * authorization key for api access permission
     */
    apiAccessToken: process.env.apiAccessToken,

    awsCredentials: {
        accessKeyId: process.env.AKID,
        secretAccessKey: process.env.SAKEY
    },

    emailSenderOptions: {
        host: process.env.service,
        port: process.env.port,
        secure: process.env.secure, // use SSL
        auth: {
            user: process.env.user,
            pass: process.env.pass
        }
    },

    supportMail: process.env.supportMail,

    emailOptions: {
        from: process.env.from,
        to: '',
        subject: 'Sending Email from app!',
        html: '<h1>Welcome. Email Sent Successfully</h1>'
    },


    // crm server url for create club and login data
    crmClubUrl: process.env.crmClubUrl,

//crm redirect to FAQ section

crmFAQ : process.env.crmFAQ,

    //crm url
    crmDashboard : process.env.crmDashboard,
    //keyclock
    keyclock: process.env.keycloakhost,
    client_secret: process.env.secret,
    //link of frontend url, verification link on email 
    //https://crmstage.tineon.de/
    adminToCustomerProductAssignLink: process.env.adminToCustomerProductAssignLink,
    addUserVerificationLink: process.env.addUserVerificationLink,
    emailVerificationLink: process.env.emailVerificationLink,
    resetPasswordLink: process.env.resetPasswordLink,
    trialVerificationLink: process.env.trialVerificationLink,
    surveyVoteLink: process.env.surveyVoteLink,
    tineonNodeServerURL: process.env.tineonNodeServerURL,

    loadConfig: function loadConfig() {
        let node_env = (process.env.NODE_ENV),
            config = import('./' + node_env);
        config.node_env = config.NODE_ENV = node_env;
        return config;
    },

    headerParams: {
        'Authorization': 'required',
        'Platform': 'required'
    },

    //please use the billwerk account client id, secret, account url's as sandbox or production account
    billwerkAccountUrl: process.env.billwerkAccountUrl,
    billwerkAuthUrl: process.env.billwerkAuthUrl,
    billwerkClientId: process.env.billwerkClientId,
    billwerkClientSecret: process.env.billwerkClientSecret,

    api_versions: {
        '/api': { path: '/v1', inUse: true }
    },
}
export default configContainer = configContainer;
