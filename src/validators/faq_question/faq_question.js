export default class CrmFaqQuestionValidation {
    crmFaqQuestionCreate(questionData){
        let err = new Array();
        
        if (!questionData.question) {
            err.push('FaqQuestion:QuestionReq');
        }
        if (!questionData.answer) {
            err.push('FaqAnswer:AnswerReq');
        } 
        return err;
    }
}