import async from 'async';
import PagesValidation from '../../validators/static_pages/static-pages';
import responseFormat from '../../core/response-format';
import PagesModel from '../../models/static_pages/static-pages'
import AWSHandler from '../../core/AWSHander';

let pagesModel = new PagesModel();
let pagesValidation = new PagesValidation();

export default class PagesController {

    /**
    * Function to create static pages
    * @author  MangoIt Solutions
    * @param   {object} with page details
    * @return  {message} success message
    */
    pagesCreate(req, res, next) {
        let file;
        let s3URLofImage;
        let text;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = pagesValidation.pagesCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    pagesModel.getPagesByTitle(req.body.title).then((pagesFind) => {
                        if (pagesFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.body.url == "" || req.body.url == null) {
                        text = req.body.title.replace(/\s+/g, '-').toLowerCase();
                        done();
                    } else {
                        text = req.body.url.replace(/\s+/g, '-').toLowerCase();
                        done();
                    }
                },
                function (done) {
                    pagesModel.getPagesByUrl(text).then((pagesFind) => {
                        if (pagesFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesURLAliasExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        done();
                    }
                },
                function (done) {
                    pagesModel.CreatePages(req, text, s3URLofImage).then((pagesCreated) => {
                        if (pagesCreated.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesCreationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                }
            ])
        }
    }

    /**
    * Function to get pages by URL
    * @author  MangoIt Solutions
    * @param   {url} 
    * @return  {object} pages deatils
    */
    getPagesbyURL(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                pagesModel.getPagesByUrl(req.params.url).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get pages by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} pages deatils
    */
    getPagesbyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                pagesModel.getPagesById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get all pages 
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} pages deatils
    */
    getPagesAll(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                pagesModel.getAllPages().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to delete  page by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    pagesDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                pagesModel.deletePagesbyId(req.params.id).then((results) => {
                    if (results.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesDeleted']);
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to update page by id
    * @author  MangoIt Solutions
    * @param   {id} with update page details
    * @return  {message} success message
    */
    pagesUpdate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = pagesValidation.pagesCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    pagesModel.getPagesByTitle(req.body.title).then((pagesFind) => {
                        if (pagesFind.length > 0 && req.params.id != pagesFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.body.url == "" || req.body.url == null) {
                        text = req.body.title.replace(/\s+/g, '-').toLowerCase();
                        done();
                    } else {
                        text = req.body.url.replace(/\s+/g, '-').toLowerCase();
                        done();
                    }
                },
                function (done) {
                    pagesModel.getPagesByUrl(text).then((pagesFind) => {
                        if (pagesFind.length > 0 && req.params.id != pagesFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesURLAliasExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['pages:pagesImgRequired'], { code: 417 });
                        res.status(200).json(response);
                    }
                },
                function (done) {
                    pagesModel.getUpdatePages(req, text, s3URLofImage).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesUpdationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['pages:pagesNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }
}