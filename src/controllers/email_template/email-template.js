import async from 'async';
import EmailTemplateValidation from '../../validators/email_template/email-template';
import responseFormat from '../../core/response-format';
import EmailTemplateModel from '../../models/email_template/email-template';
import AWSHandler from '../../core/AWSHander';
let emailTemplateModel = new EmailTemplateModel();
let emailTemplateValidation = new EmailTemplateValidation();

export default class EmailTemplateController {

    /**
    * Function to get all email template
    * @author  MangoIt Solutions
    * @param   {}  
    * @return  {object} email template details
    */
    getemailTemplateAll(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                emailTemplateModel.getAllemailTemplate().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get email template by Id
    * @author  MangoIt Solutions
    * @param   {id}  
    * @return  {object} email template details
    */
    getemailTemplatebyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                emailTemplateModel.getemailTemplateById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to create new email template
    * @author  MangoIt Solutions
    * @param   {object}  email template details
    * @return  {message} success message
    */
    emailTemplateCreate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = emailTemplateValidation.emailTemplateCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    emailTemplateModel.getemailTemplateByType(req.body.templateType).then((emailTemplateFind) => {
                        if (emailTemplateFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        done();
                    }
                },
                function (done) {
                    emailTemplateModel.CreateemailTemplate(req, s3URLofImage).then((emailTemplateCreated) => {
                        if (emailTemplateCreated.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateCreationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                }
            ])
        }
    }

    /**
    * Function to delete email template 
    * @author  MangoIt Solutions
    * @param   {id}  
    * @return  {message} success message
    */
    emailTemplateDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        emailTemplateModel.deleteemailTemplatebyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function to update email template details by id
    * @author  MangoIt Solutions
    * @param   {id} with updated email template data
    * @return  {message} success message
    */
    emailTemplateUpdate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = emailTemplateValidation.emailTemplateCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    emailTemplateModel.getemailTemplateByType(req.body.templateType).then((emailTemplateFind) => {
                        if (emailTemplateFind.length > 0 && req.params.id != emailTemplateFind[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateImgRequired'], { code: 417 });
                        res.status(200).json(response);
                    }
                },
                function (done) {
                    emailTemplateModel.getUpdateemailTemplate(req, s3URLofImage).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateUpdationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['emailTemplate:emailTemplateNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }
}