
 let englishMessageList = {
    languageSuccess: "language set successfully",
    invalidAuthToken: "Invalid token or token has expired, please signin again",
    unauthorized401: "Unauthorized access",
    common401: "Unauthorized access",
    common404: "Not found",
    common402: "Incorrect data format",
    common500: "Something went wrong, please try after some time",
    common400: "Bad request",
    Common204: "No content",
    saved: "Saved successfully",
    deleted: "Data delete successfully",
    emailExists: "Provided email is already registered with us",
    emailNotExists: "Provided email is not registered with us",
    userNameExists: "Provided username is already registered with us",
    emailUserNameNotExists: "Provided email or username is not registered with us",
    blankRequest: "Can't process blank request",
    resumeFormat: "An error occurred while processing resume, please contact support",
    fileSizeError: "The file you are uploading exceeds the 20MB limit",


    //Account
    invalidUserNamePassword: "Invalid username and/or password",
    accountNotActivated: "Verification required. An email was sent to you with an activation link.",
    deActivateAccount: "Your account is disabled. Please contact support.",
    inactiveAccount: "Account is not active",
    accountDisabled: "Your account is disabled. Please contact support at support@tineon.de",
    unAuthorizedAccount: "Your account is not authorized",
    accFirstName: "Are you sure you entered your first name correctly?",
    accLastName: "Are you sure you entered your last name correctly?",
    accUserDomain: "Please select desired domain.",


    // password: "Use at least 8 characters, including 1 uppercase letter and 1 symbol", // "Password must have alpha-numeric with an special character with a 8-20 characters in length",
    reCaptchaCode: "A valid reCaptcha is required",
    termsAndConditions: "Please agree to our Privacy Policy and Terms of use to continue",
    invalidDocType: "Invalid file type",
    resumeFileName: "A valid file is required",
    registeredSuccess: "User registered successfully And the verification link is sent to the user account",
    registrationFail: "User registration failed",
    countryCode: "A valid country is required",
    invalidCode: "A valid code is required",
    code: "A valid code is required",
    expiredCode: "Your code has expired",
    codeORusername: "A valid code or user name is required",
    invalidUser: "Invalid user",
    validCode: "Code is valid",
    name: "A valid name is required",
    profilePicture: "Profile photos must be ",
    jobStatusUpdated: "Updated successfully",
    notLookingInvalidCode: "Unable to validate your request. Please try again later.",
    notLookingExpiredCode: "Your authorization code has expired",
    registeredSuccessLink: "Registration success and verification email sent successfully to your email account",
    mailNotAdmin : "Mail sending failed to admin",
    mailNotUser: "Mail sending failed to user",
    registeredSuccessMailNotSent: "User registered successfully But the verification mail sent is failed to the user account",
    invalidPassword: "Please enter a valid password",
    changePassword: "Please change your password",
    alreadyChangedPassword: "You have already reseted your password.",

    
    //Account Activate
    employeeDetailsId: "A valid employeeDetailsId is required",
    userName: "username is required",
    otpCode: "A valid OTP code is required",
    otpExpired: "OTP has been expired",
    accountAlreadyActivated: "Your account is already active",
    accountActivatedSuccess: "Your account is not active",
    accountActivatelink: "An email with the account activation link has been sent to your email ID",
    otpSentSuccess: "OTP has been sent successfully on your email ID",
    passwordMismatch: "Passwords must match",


    //change password
    passwordChangedSuccess: "Your password has been successfully changed",
    passwordCantBeSame: "Your new password must differ any previously used passwords",
    confirmPasswordNotMatched: "New password and confirm password does not match",
    invalidOldPassword: "A valid old password is required",
    resetPasswordMail: "An email with the reset password link has been sent to your email ID",
    confirmPassword: "Password and confirm password does not match",
    mailSentFail: "Mail sent failed, please try again later",
    mailSentSuccess: "Mail sent successfully",
    passwordChangedFail: "password change failed",


    //logout
    logOutSuccess: "User logged-out successfully",


    // email-verification
    verificationFail: "Email verification failed, please try again",
    invalidLink: "Link is invalid",
    linkExpired: "Your link has expired,Please do reset password again",
    alreadyVerified: "You have already verified your account password",
    alreadyVerifiedEmail: "You have already verified your email",


    // licence
    licenceNotExists: "licence does not exists",
    licenceRegisteredSuccess: "User licence registered successfully",
    licenceRegistrationFail: "User licence registration failed",
    licenceUpdationSuccess: "User licence updated successfully",
    licenceUpdationFail: "User licence updation failed",
    licenceIdExists: "licence-ID is already registered with us",
    licenceNameExists: "licence name is already registered with us",
    licenceDeleted: "licence deleted successfully",
    licenceCreated: "The user licence has been successfully created",
    licenceCreationFail: "Invalid licence creation",
    licenceUpdateSuccess: "The licence has been successfully updated",
    userLicenceRegistrationFail: "Licence registration failed",
    userLicenceRegistrationSuccess: "Licence registration success",
    licenceActivated: "Club licence activated",
    licenceBlocked: "Club licence blocked",

    //role
    roleNotExists: "Role does not exists",
    roleRegisteredSuccess: "User Role registered successfully",
    roleRegistrationFail: "User Role registration failed",
    roleUpdationSuccess: "User Role updated successfully",
    roleUpdationFail: "User Role updation failed",
    roleIdExists: "Role-ID is already registered with us",
    roleNameExists: "Role name is already registered with us",
    roleDeleted: "Role deleted successfully",
    roleCreated: "The User Role has been successfully created",
    roleCreationFail: "Invalid Role creation",
    roleUpdateSuccess: "The Role has been successfully updated",


    //productComponents
    productComponentsNotExists: "Product component does not exists",
    productComponentsRegisteredSuccess: "Product component registered successfully",
    productComponentsRegistrationFail: "Product component registration failed",
    productComponentsUpdationSuccess: "Product component updated successfully",
    productComponentsUpdationFail: "Product component updation failed",
    productComponentsIdExists: "Product component id is already registered with us",
    productComponentsNameExists: "Product component name is already registered with us",
    productComponentsDeleted: "Product component deleted successfully",
    productComponentsCreated: "The Product component has been successfully created",
    productComponentsCreationFail: "Invalid Product component creation",
    productComponentsUpdateSuccess: "The Product component has been successfully updated",


    //domain
    domainNotExists: "Domain does not exists",
    domainRegisteredSuccess: "Domain registered successfully",
    domainRegistrationFail: "Domain registration failed",
    domainUpdationSuccess: "Domain updated successfully",
    domainUpdationFail: "Domain updation failed",
    domainIdExists: "Domain-ID is already registered with us",
    domainNameExists: "Domain name is already registered with us",
    domainDeleted: "Domain deleted successfully",
    domainCreated: "The Domain has been successfully created",
    domainCreationFail: "Invalid domain creation",
    domainUpdateSuccess: "The domain has been successfully updated",


    //user
    userNotExists: "User does not exists",
    registeredSuccessAsGuest: "Registration success as a guest user.",
    registeredRoleFail: "User registered but role is not registered successfully",
    registeredDomainFail: "User registered but domain is not registered successfully",
    userUpdationSuccess: "User updated successfully",
    userUpdationFail: "User updation failed",
    userDeleted: "User deleted successfully",
    userSoftDeleted :  "can't delete, as invoice is present",
    userDelRoleNotExists: "The User has been deleted but its role does not exists",
    userAssigned: "Customer assigned successfully",
    userAssignedFail: "Customer assigned failed",
    commissionSet:'Commission setting saved successfully',
    commissionSetFail:'Commission setting failed',
    commExists:"you have already setted the commission for this product",
    comSetNotExists:"commission setting does not exists",
    comSetDeleted:"commission setting deleted successfully",

    //club
    supportUserSuccess:"Support user successfully stored",
    supportUserFail:"Support user failed to store",
    supportUserNotExists:"Support user does not exists",

    // profile-edit
    userAddressUpdationFail: "User address updation failed",
    userAccountUpdationFail: "User account updation failed",
    userImageUpdationFail: "User Image updation failed",


    //product-price
    productSuccess: "Product has been successfully created",
    productPriceFail: "Invalid product price creation.",

    //faqCateogry
    categoryExists: "Category already exists",
    categorySucess: "Category Successfully Created",
    categoryCreationFail : "Category Creation Fail",
    categoryUpdate : "Category Update Successfully",
    categoryUpdationFail : "Category Updation Failed",
    categoryDelete : "Category Successfully Deleted",
    categoryDeleteionFail : "Category Not Deleted",
    categoryquestionExist : "Question related to this category are exist, Not able to delete Category",
   
   //faqStatus
   StausAssigned : "Status Already Assigned to Some Request, Not able to delete it",
   statusDelete : "Status Deleted Successfully",
   StatusDeleteionFail : "Status Deletion Failed",

    //faqStatus
    StatusExists : "Status Already Exist",
    StatusNotExists : "Status Not Exist",
    StatusSucess : "Status Successfully Created",
    StatusCreationFail: " Status creation fail",
    statusUpdate : "Status Updated Successfully",

   //faqQuestion
   FaqQuestionCreated : "Question Created Successfully.",
   FaqQuestionCreationFail :"Question Cration Failed.",
   FaqQuestionExists : "Question already exists",
   FaqQuestionNotExists : "Question Not exists",
   FaqQuestionUpdate : "Question Update Successfully",
    FaqQuestionUpdationFail : "Question Updation Failed",
    FaqQuestionDelete : "Question Successfully Deleted",

    //faqQuestion feedback
    FaqFeedbakCreated : "Feedback Stored Sucessfully",
    FaqFeedbackFail : "Feedback Not stored",
    FaqFeedbackNotExist : "Feedback not exists",
    FaqFeedbackUpdated : "Feedback Updated Successfully",
//faq
faqNotExists:"Faq Not Exist",

    //billwerk
    AuthSuccess: "Auth-token generated successfully",
    orderFail: "Order success but order record insertion failed",
    orderAlreadyOrdered: "You have already ordered this product.",
    orderFirstPrimary: "To purchase the sub-products, please order the Club-Administration product first.",
    orderNotExist: "Order already deleted or does not exists",
    deletedOrder: "Order successfully deleted",
    alreadyOrderdVersion: "You have already ordered the another version of this product",
    orderStoreSuccess: "Your upcomming order is saved successfully, it will continue after free trial period",
    orderStoreFail: "Your upcomming order store is failed",
    InValidIBAN: "Please enter valid IBAN",

    //survey
    surveyExists:"Survey already exists",
    surveyCreationFail: "Survey creation failed",
    surveyCreated: "Survey created successfully",
    surveyNotExists: "Survey does not exists",
    surveyUpdated: "Survey updated successfully",
    voteSuccess: "Survey voted successfully",
    voteFail: "Survey voting failed",


    //contact
    contactFail: "Contact failed",

    //advertisement
    advertisementExist : "Advertisement already exists",
    AdvertisementCreatedsucess : "Advertisement Created Successfully",
    AdvertisementCreatedfail : "Advertisement Creation failed",
    advertisementUpdationSuccess : "Advertisement Updated Successfully",
   AdvertisementNotExists : "Advertisement does not exist",
    advertisementDeleted : "Advertisement Deleted Successfully",
    //product
    productCreationFail: "Product creation failed.",
    productNotExists: "Product does not exists",
    productIDNotExists: "Product ID does not exists",
    productDeleted: "Product deleted successfully",
    productUpdated: "Product updated successfully",
    productUpdationSuccess: "Product updated successfully",
    productPriceDeleted: "The product and its price has been successfully DELETED",
    productPriceUpdated: "The product and its price has been successfully updated",
    productDetailFail: "Product details creation failed",
    productAssigned: "Product assigned successfully",
    productAssignedFail: "Product assigned failed",
    ProductSucess: "Product Successfully Created",
    productExists: "Product already exists",
    productImgRequired : "Product Image Required",
    ProductOrderSucess : "Product Ordered Sucessfully",

    //crmProductOrder
    clubProduct : "Please order Club Product first",
    OrderProductExist : "You Already Ordered this product",
    OrderedNotExists: "No product is ordered yet",
    OrdereFailed: "product order failed",
    productOrderedButMailNot:"your product order process is incomplete,please contact to support team",
    OrderUpdationSuccess : "Order Status Successfully Updated",
    orderUpdationFail : "Order Status Updation Failed",

    //mandate
    MandateNotExists: "Mandate does not exists.",
    pdfSentFail: "PDF generation failed",

    //social login
    accessToken: "A valid accessToken is required",
    accessTokenExpired: "Access token has expired please try again",
    emailNotMatchWithSocailEmail: "Entered email does not match with social media email",
    socialEmailRequired: "User registration could not be completed due to email address in social media is required. Please update your social media and try again.",
    id: "A valid id is required",
    passwordCreatedSuccess: "Password for social login has been created successfully",
    passwordAlreadyCreated: "Password for your social login has already been created",
    passwordAlreadyExists: "Password already been created",
    passwordCreated: "Password created successfully",


    // role_permissions
    rolePermissionsNotExists: "Role permissions are not existing.",
    rolePermissionCreated: "Role permission created successfully.",
    rolePermissionCreationFail: "Role permission creation failed.",
    rolePermissionDeleted: "Role permission deleted successfully.",


    //news
    newsExists: "News title already exists, please use another.",
    newsNotExists: "News does not exists.",
    newsDeleted: "News deleted successfully",
    newsUpdationSuccess: "News updated successfully",


    //email-template
    emailTemplateNotExists:"Email template does not exists",
    emailTemplateExists: "Email template already exists",
    emailTemplateImgRequired: "Template image is required",
    emailTemplateCreationSuccess: "Email template created successfully",
    emailTemplateCreationFail: "Email template creation failed",
    emailTemplateDeleted: "Email template deleted successfully",
    emailTemplateUpdationSuccess: "Email template updated successfully",


    //pages
    pagesExists: "Page title already exists,please use another.",
    pagesNotExists: "Page does not exists.",
    pagesDeleted: "Page deleted successfully",
    pagesUpdationSuccess: "Page updated successfully",
    pagesURLAliasExists: "URL alias already used, please use another",


    //document upload
    documentUpdated: "Document is uploaded successfully",
    documentUpdationFail: "Document upload failed",
    documentNotExists: "User document are not exists",
    uploadDocDeleted: "Uploaded document is deleted successfully",


    //request ticket
    requestNameExists: "Request name is already exists please choose another one",
    requestCreated: "Request created successfully",
    requestCreatedAndEmailSend : "Request Created  and Email Send to Support",
    requestCreationFail: "Request creation failed",
    requestNotExists: "Request does not exists",
    requestUpdateSuccess:"Request updated successfully",
    requestDeleted: "Request deleted successfully",
 //reuest ticket assigned to support users
 requestAssigned : "Ticket Assigned to Support User",
 requestAssignedfail:"Ticket Assiged to Support user Failed",
    //comment ticket
    commentCreated: "Comment Created successfully",
    commentCreationFail: "comment creation failed",
    commentNotExists: "Comment does not exists",
    commentUpdateSuccess:"Comment updated successfully",
    commenttDeleted: "Comment deleted successfully",
    //customer membership

    memberNameExists: "member username is already used, please use another one",
    memberAddSuccess: "member added successfully",
    memberAddFail: "member add failed",
    memberNotExists: "member does not exists",
    memberUpdationSuccess: "member updated successfully",
    memberUpdationFail: "member updation failed",
    memberDeleted: "member deleted successfully",
    toogleSuccess: "toggle saved successfully",
    departmentExists: "customers department is already exists",
    departmentAddSuccess: "department added successfully",
    departmentAddFail: "department add failed",
    departmentNotExists: "department not exists",
    departmentUpdationSuccess: "department updated successfully",
    departmentUpdationFail: "department updation failed",
    departmentDeleted:"department deleted successfully", 
    deparmentNameUsed : "department Name is Used for Member",
    departmentassignedtomember : "department assigned to member, please change member department",

    //customer tag
    tagExists: "customers Tag is already exists",
    tagAddSuccess: "Tag added successfully",
    tagAddFail: "tag add failed",
    tagNotExists: "tag not exists",
    tagUpdationSuccess: "tag updated successfully",
    tagUpdationFail: "tag updation failed",
    tagDeleted:"tag deleted successfully", 

// customer member search
   memberSearchAddSuccess: "member search added successfully",
   memberSearchAddFail: "member search add failed",
   memberSearchNameExists: "search filter name already used, please use another one",

    memberSearchDeleted:"member search deleted successfully",
    memberSearchNotExists: "member Search does not exists",

    //FunctionInClub
    clubExists: "Club Name is already exists",
    clubAddSuccess: "Function Club Name added successfully",
    clubAddFail: "Function Club Name add failed",
    clubNotExists: "club name does not exists",
    clubUpdationSuccess: "Function Club Name updated successfully",
    clubUpdationFail: "Function Club Name updation failed",
    clubDeleted:"Function Club Name deleted successfully", 
    FunctionNameUsed : "Function Club Name is Used for Member",
    //ProductDisplay
    ProductAddSuccess: "Display Product Added Successfully",
    ProductAddFail : "Display Product Add Fail",
    DisplayProductNotExists: "Display Product  does not exists",
    
    //validation errors
    oldNewPassSame: "Old password and new password should not be same",
    oldPassword: "Please enter valid current password",
    password: "Password must contains uppercase, lowercase, number, symbol, length between 8 -30 characters.",
    confirmPassword: "Confirm password should be same as password.",
    email: "Please enter valid email.",
    confirmEmail: "Confirm email should be same as email.",
    societyName: "Society name is required",
    uidReq: "User-id required.",
    username: "Username is required",
    subject: "Subject required",
    msgReq: "Message required",
    suid: "Sales user uid is required.",
    cuid: "Customer user id is required.",
    priceReq: "Price is required.",
    validReq: "Validuntil is required.",
    ppidReq: "Ppid is required.",
    nidReq: "Nid is required.",
    classReq: "Class is required.",
    domainReq: "domain is required.",
    weightReq: "weight is required.",
    bundleReq: "is_bundle is required.",
    pdflinkReq: "pdflink is required.",
    bookableReq: "is_bookable is required.",
    alwayson: " is_alwayson is required.",
    commissionableReq: "is_commissionable is required.",
    firstName: "First name is required",
    surName: "Last name is required",
    addressLine: "addressLine is required",
    zipcode: "zipcode required",
    placeReq: "place required",
    ridReq: "role id is Required.",
    roleReq: "Role is required.",
    permissionsReq: "Permissions are required.",
    modulesReq: "Single module Name is required.",
    titleReq: "Title is required",
    descriptionReq: "Description required",
    publishReq: "Publish to is required",
    contentReq: "Content is required.",
    authorReq: "Author is required.",
    urlReq: "news url is required.",
    newsCreationSuccess: "news created successfully",
    newsCreationFail: "news creation failed",
    contactNumber: "contact number is required",
    domainReq: "Domain name is required",
    sitenameReq: "sitename is required",
    machineNameReq: "machine name is required",
    productName: "Product name is required",
    productPrice: "Product price is required",
    productComponent: "product components atre required",
    votingOptionsReq: "Please select voting options",
    answerOptionReq: "Please select answer options",
    activeUntilReq: "Active End date is required.",
    surveyIdReq: "survey id is required",
    userIdReq: "survey user id is required",
    surveyAnswerIdReq: "survey answers id is required",
    surveyTypeReq: "survey type is required",
    activeReq: "survey start date is required",
    templateTypeReq: "please select Template Type",
    subjectReq: "mail subject is required.",
    templateBodyReq: "Template body is required",
    urlReq: "Template url is required",
    headerContentReq: "header content is required",
    footerContentReq: "footer content is required",
    prodctDescription : "Product Description required",
    nameReq: "Request name is required",
    statusReq: "Request status is required",
    assignedToReq: "Request assigned_to is required",
    QuestionReq : "Question Required",
    AnswerReq : "Answer Required",
    commentReq : "Comment Required",
    OldUserDataDeleted : "Old User Data Deleted Successfully",
    OldUserDataNotExist : "Old User Data Not Exist",
    PleaseSelectAssigneduser : "Please Select Assigned user for Ticket",

    //Tineon Info
    tineonInfoSuccess: "Tineon Info successfully Added",
    tineonInfoFail: "Tineon Info Creation Failed",
    tineonInfoDeleted : "Tineon Info Successfully Deleted",
    tineonInfoNotExist : "Tineon Info Not Exist"


}

export default englishMessageList = englishMessageList;
