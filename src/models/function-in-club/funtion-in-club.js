import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class FunctionInModel {
    async checkClubNameExists(req) {
        const rows = await db.query("SELECT function_club_name from `function_in_club` WHERE function_club_name=? AND customer_id = ?", [req.body.function_club_name, req.body.customerId]);
        return rows[0];
    }

    async addFunctionInClub(req) {
        const rows = await db.query("INSERT INTO `function_in_club`(`customer_id`,`function_club_name`)VALUES(?,?)", [
            req.body.customer_id,
            req.body.function_club_name,
        ]);
        return rows[0];
    }

    async checkFunctionClubExists(req) {
        const rows = await db.query("SELECT function_club_name from `function_in_club` WHERE function_club_name=? AND customer_id = ?", [req.body.department, req.body.customerId]);
        return rows[0];
    }

    async updateFunctionClub(req) {
        const rows = await db.query("UPDATE function_in_club SET function_club_name = ? WHERE id = ?", [
            req.body.function_club_name,
            req.params.id
        ]);
        return rows[0];
    }

    async getAllClubName() {
        const rows = await db.query(`SELECT * from function_in_club`);
        return rows[0];
    }

    async getFunctionClubByCustomerId(customerId) {
        const rows = await db.query(`SELECT * FROM function_in_club WHERE customer_id = ? ORDER BY id DESC`, [customerId]);
        return rows[0];
    }

    async getClubById(id) {
        const rows = await db.query(`SELECT * FROM function_in_club WHERE id = ?`, [id]);
        return rows[0];
    }

    async checkFunctionInClub(id) {
        const rows = await db.query(`SELECT * FROM function_in_club WHERE id = ?`, [id]);
        return rows[0];

    }

    async getMemberAssignedtoFunction(customerId) {
        const rows = await db.query(`SELECT customer_id,functionClub FROM customer_members WHERE customer_id = ?`, [customerId]);
        return rows[0];

    }

    async getDeleteFunctionClub(id) {
        const rows = await db.query("DELETE  FROM `function_in_club` WHERE `id`= ?", [id]);
        return rows[0];
    }


}