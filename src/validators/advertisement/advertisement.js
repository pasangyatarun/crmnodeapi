export default class AdvertisementValidation {
    advertisementCreate(advertisementData) {

        let err = new Array();
        if (!advertisementData.name) {
            err.push('name: Advertisement Name Required');
        }
        if (!advertisementData.text) {
            err.push('text:Advertisement Description Required');
        }
        if (!advertisementData.link) {
            err.push('link:Advertisement link Required');
        }
        if (!advertisementData.status) {
            err.push('status:Advertisement status Required');
        }
        if (!advertisementData.start_date) {
            err.push('start_date:Advertisement start date Required');
        }
        if (!advertisementData.end_date) {
            err.push('end_date:Advertisement end date Required');
        }
        return err;
    }
}