import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class JsonScriptModel {
    async insertUser(data) {
        let dt = new Date().getTime();
        const rows = await db.query("INSERT INTO `users`(`name`,`pass`,`mail`,`created`,`access`,`login`)VALUES(?,?,?,?,?,?)", [
            data.EmailAddress,
            data.Password,
            data.EmailAddress,
            dt,
            dt,
            dt
        ]);
        return rows[0];
    }

    async insertUserRole(userid) {
        const roleSelect = await db.query("SELECT * FROM `role` WHERE name=?", ['Customer']);
        const rows = await db.query("INSERT INTO `users_roles`(`uid`,`rid`)VALUES(?,?)", [
            userid,
            roleSelect[0][0].rid,
        ]);
        return rows[0];
    }

    async insertUserAddress(customerData, userId) {
        const rows = await db.query('INSERT INTO `field_data_field_address`(`entity_id`,`field_address_name_line`,`field_address_country`,`field_address_first_name`,`field_address_last_name`,`field_address_organisation_name`,`field_address_locality`,`field_address_postal_code`,`field_address_thoroughfare`,`field_address_second`,`entity_type`)VALUES(?,?,?,?,?,?,?,?,?,?,?)', [
            userId,
            customerData.FirstName + customerData.Lastname,
            customerData.Country,
            customerData.FirstName,
            customerData.Lastname,
            customerData.Clubname,
            customerData.City,
            customerData['ZIP Code'],
            customerData['Address Line 1'],
            customerData['Address Line 2'],
            'user'
        ]);
        return rows[0];
    }

    async insertIBAN(payment, userId) {
        const rows = await db.query('INSERT INTO `field_data_field_bank_iban`(`entity_id`,`field_bank_iban_value`,`mandate`,`entity_type`)VALUES(?,?,?,?)', [
            userId,
            payment.IBAN,
            payment['Mandate?'],
            'user'
        ]);
        return rows[0];
    }

    async insertLicence(licenceData, userId) {
        const rows = await db.query('INSERT INTO `licence_history`(`plan_variant_id`,`lid`,`status`,`first_action`,`first_start_date`,`first_due_date`,`last_action`,`last_start_date`,`last_due_date`,`sales_partner`,`invoice`,`user_id`)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)', [
            licenceData.PlanVariantId,
            licenceData.lid,
            licenceData.Status,
            licenceData.FirstAction,
            licenceData.FirstStartDate,
            licenceData.FirstDueDate,
            licenceData.LastAction,
            licenceData.LastStartDate,
            licenceData.LastDueDate,
            licenceData.SalesPartner,
            JSON.stringify(licenceData.Invoice),
            userId
        ]);
        return rows[0];
    }

    async insertInvoice(invoice, userId, licId) {
        const rows = await db.query('INSERT INTO `licence_invoice`(`invoice_number`,`invoice_date`,`invoice_net_amount`,`invoice_tax_amount`,`invoice_gross_amount`,`invoice_period`,`licence_history_id`,`user_id`)VALUES(?,?,?,?,?,?,?,?)', [
            invoice.InvoiceNumber,
            invoice.InvoiceDate,
            invoice.InvoiceNetAmount,
            invoice.InvoiceTaxAmount,
            invoice.InvoiceGrossAmount,
            invoice.InvoicePeriod,
            licId,
            userId
        ]);
        return rows[0];
    }

    async allOldUserDataByPage(req) {
        let param = req.params;
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;
        let search;
        if (req.query && req.query.search) {
            search = req.query.search
            const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, users.*,field_data_field_address.*,licence_history.plan_variant_id,licence_history.user_id,licence_history.sales_partner FROM `users` LEFT JOIN `licence_history` ON users.uid = licence_history.user_id LEFT JOIN `field_data_field_address` ON users.uid = field_data_field_address.entity_id WHERE users.uid = licence_history.user_id AND (users.mail LIKE '%" + search + "%' OR users.name LIKE '%" + search + "%' OR field_data_field_address.field_address_organisation_name LIKE '%" + search + "%' OR licence_history.sales_partner LIKE '%" + search + "%') LIMIT " + pageSize + " OFFSET " + offset + "");
            return rows[0];
        } else {
            const rows = await db.query("SELECT COUNT(*) OVER() as totalCount, users.*,field_data_field_address.*,licence_history.plan_variant_id,licence_history.user_id,licence_history.sales_partner FROM `users` LEFT JOIN `licence_history` ON users.uid = licence_history.user_id LEFT JOIN `field_data_field_address` ON users.uid = field_data_field_address.entity_id WHERE users.uid = licence_history.user_id LIMIT " + pageSize + " OFFSET " + offset + "");
            return rows[0];
        }
    }

    async allOldUserData() {
        const rows = await db.query("SELECT users.*,field_data_field_address.*,licence_history.plan_variant_id,licence_history.user_id,licence_history.sales_partner FROM `users` LEFT JOIN `licence_history` ON users.uid = licence_history.user_id LEFT JOIN `field_data_field_address` ON users.uid = field_data_field_address.entity_id WHERE users.uid = licence_history.user_id");
        return rows[0];
    }

    async getUserDataById(uid) {
        const rows = await db.query("SELECT * FROM `users` LEFT JOIN `field_data_field_address` ON users.uid = field_data_field_address.entity_id LEFT JOIN `field_data_field_bank_iban` ON users.uid = field_data_field_bank_iban.entity_id LEFT JOIN `licence_history` ON users.uid = licence_history.user_id WHERE users.uid = ?", [uid]);
        return rows[0];
    }

    async getolduserid(uid) {
        const rows = await db.query("SELECT user_id FROM licence_history");
        return rows[0];
    }

    async deleteuserliceneceHistory(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query( "DELETE FROM `licence_history` WHERE user_id IN  (" + x + ") ");
        return rows[0];
    }
    
    async deleteuserInvoice(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query( "DELETE FROM `licence_invoice` WHERE user_id IN  (" + x + ") ");
        return rows[0];
    }

    
    async deleteOldUserBankIban(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query( "DELETE FROM `field_data_field_bank_iban` WHERE entity_id IN  (" + x + ") ");
        return rows[0];
    }

    async deleteOldUserAddress(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query("DELETE FROM `field_data_field_address` WHERE entity_id IN  (" + x + ") ");
        return rows[0];
    }

    async deleteOldUserRoles(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query("DELETE FROM `users_roles` WHERE uid IN  (" + x + ") ");
        return rows[0];
    }
    async deleteOldUsersData(oldUserId) {
        let x = oldUserId.join();
        const rows = await db.query("DELETE FROM `users` WHERE uid IN  (" + x + ") ");
        return rows[0];
    }


    /* Queries to delete all user data from relared tables */

    async getuserid() {
        const rows = await db.query("SELECT * FROM `users` WHERE `mail` IN ('admin@gmail.com','super-admin@mailinator.com')");
        return rows[0];
    }

    
    
    async deleteuserData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `users` WHERE uid NOT IN (" + x + ") ");
        return rows[0];
    }
    
    
    async deleteuserRoleData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `users_roles` WHERE uid NOT IN (" + x + ") ");
        return rows[0];
    }

    async deleteuserliceneceHistoryData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `licence_history` WHERE user_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteuserInvoiceData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `licence_invoice` WHERE user_id NOT IN (" + x + ") ");
        return rows[0];
    }

    async deleteuserOrderData() {
        const rows = await db.query( "DELETE FROM `order_billwerk` ");
        return rows[0];
    }

    
    async deleteBillerkCustomerData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `billwerk_user_customer` WHERE user_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteCustomerAddressData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `field_data_field_address` WHERE entity_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteCustomerBankIbanData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `field_data_field_bank_iban` WHERE entity_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteProductAssignedData(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `product_assign_customer` WHERE user_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteTicketAssignedData() {
        const rows = await db.query( "DELETE FROM `ticket_request_assignedTo` ");
        return rows[0];
    }

    async deleteTicketRequestData() {
        const rows = await db.query( "DELETE FROM `tickets_requests` ");
        return rows[0];
    }

    
    async deleteTicketCommentData() {
        const rows = await db.query( "DELETE FROM `tickets_comments` ");
        return rows[0];
    }

    
    async deleteCrmProductOrder(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `crm_product_order`  WHERE user_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteCustomerDepartment(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `customer_departments`  WHERE customer_id NOT IN (" + x + ") ");
        return rows[0];
    }

    
    async deleteCustomerMember(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `customer_members`  WHERE customer_id NOT IN (" + x + ") ");
        return rows[0];
    }

    async deleteCustomerTags(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `customer_tags`  WHERE customer_id NOT IN (" + x + ") ");
        return rows[0];
    }

    async deleteCustomerDistributer(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `customer_to_distributor`  WHERE distributor_id NOT IN (" + UserId + ") ");
        return rows[0];
    }


    async deleteFaqQuestionFeedback(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `faq_question_feedback`  WHERE user_id NOT IN (" + UserId + ") ");
        return rows[0];
    }

    async deleteFunctionInClub(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `function_in_club`  WHERE customer_id NOT IN (" + UserId + ") ");
        return rows[0];
    }

    
    async deleteSurveyUsers(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `survey_users`  WHERE user_id NOT IN (" + UserId + ") ");
        return rows[0];
    }

    
    async deleteSurveyResult(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `survey_result`  WHERE user_id NOT IN (" + UserId + ") ");
        return rows[0];
    }

    
    async deleteSalesPartnerCommission(UserId) {
        let x = UserId.join();
        const rows = await db.query( "DELETE FROM `sales_partner_commission`  WHERE sales_partner_id NOT IN (" + UserId + ") ");
        return rows[0];
    }
    
}