import responseFormat from '../../core/response-format';
import FunctionInClubModel from '../../models/function-in-club/funtion-in-club';
import async from 'async';

let functionInClubModel = new FunctionInClubModel();

export default class FunctionInClubController {


    /**
    * Function to add function in club names
    * @author  MangoIt Solutions
    * @param   {object} with status or role or both
    * @return  {object} user details
    */
    addFunctionClubName(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                functionInClubModel.checkClubNameExists(req).then((data) => {
                    if (data.length > 0) {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            },
            function (done) {
                functionInClubModel.addFunctionInClub(req).then((resData) => {
                    if (resData.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubAddSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubAddFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }


    /**
    * Function to update function in club details
    * @author  MangoIt Solutions
    * @param   {id} with club details
    * @return  {message} success message
    */
    updateFunctionClub(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                functionInClubModel.checkFunctionClubExists(req).then((data) => {
                    if (data.length > 0 && req.params.id != data[0].id) {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                functionInClubModel.updateFunctionClub(req).then((data) => {
                    if (data.affectedRows > 0) {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubUpdationSuccess']);
                        res.status(200).json(response);
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubUpdationFail'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ]);
    }

    /**
    * Function to get all club details
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} club details
    */
    functionClubName(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                functionInClubModel.getAllClubName().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get club details by customer id
    * @author  MangoIt Solutions
    * @param   {customer_id} 
    * @return  {object} club details
    */
    functionClubByustomerId(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                functionInClubModel.getFunctionClubByCustomerId(req.params.customer_id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get club details by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} club details
    */
    ClubById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                functionInClubModel.getClubById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to delete club details by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    getDeleteFunctionClubById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let customerId;;
        let FunctionName;
        let functionNameExist = [];
        async.waterfall([
            function (done) {
                functionInClubModel.checkFunctionInClub(req.params.id).then((results) => {
                    if (results.length > 0) {
                        customerId = results[0].customer_id;
                        FunctionName = results[0].function_club_name;
                        done();
                    }
                    else {
                        response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                functionInClubModel.getMemberAssignedtoFunction(customerId).then((results) => {
                    if (results.length > 0) {
                        for (let i = 0; i < results.length; i++) {
                            if (JSON.parse(results[i]['functionClub']).includes(FunctionName)) {
                                functionNameExist.push(JSON.parse(results[i]['functionClub']));
                                if (results.length == i + 1) {
                                    done();
                                }
                            }
                            else {
                                if (results.length == i + 1) {
                                    done();
                                }
                            }
                        }
                    }
                    else {
                        done();
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            },
            function (done) {
                if (functionNameExist.length == 0) {
                    functionInClubModel.getDeleteFunctionClub(req.params.id).then((results) => {
                        if (results.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubDeleted']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['FunctionClub:clubNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
                else {
                    response = responseFormat.getResponseMessageByCodes(['FunctionClub:FunctionNameUsed'], { code: 417 });
                    res.status(200).json(response);
                }
            }
        ])
    }
}

