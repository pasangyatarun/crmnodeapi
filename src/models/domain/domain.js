import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class DomainModel {

    async getDomainById(domain_id) {
        const rows = await db.query("SELECT * FROM `domain` WHERE domain_id = ?", [domain_id]);
        return rows[0];
    }

    async getDomainBydomain(domain) {
        const rows = await db.query("SELECT * FROM `domain` WHERE subdomain = ?", [domain]);
        return rows[0];
    }

    async createDomain(req) {
        const rows = await db.query('INSERT INTO `domain`(`subdomain`,`sitename`,`machine_name`)VALUES(?,?,?)', [
            req.body.domain,
            req.body.sitename,
            req.body.machineName
        ]);
        return rows[0];
    }

    async getAllDomain() {
        const rows = await db.query("SELECT * FROM `domain`");
        return rows[0];
    }

    async deleteDomain(domain_id) {
        const rows = await db.query("DELETE FROM `domain` WHERE `domain_id`=?", [domain_id]);
        return rows[0];
    }
    
    async updateDomain(req) {
        const rows = await db.query("UPDATE domain SET subdomain= ?,sitename=?,machine_name=? WHERE domain_id = ?", [
            req.body.domain,
            req.body.sitename,
            req.body.machineName,
            req.params.id
        ])
        return rows[0];
    }
}