exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('user_documents', function(t) {
        t.longtext('path').alter();
        t.string('doc_name',[500]).notNull().after("path");
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('user_documents', function(t) {
        
    });
 };