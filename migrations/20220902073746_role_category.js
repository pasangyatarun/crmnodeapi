exports.up = async function(knex) {
    return knex.schema.createTable('role_category', function(t) {
        t.increments('id').primary();
        t.string('category_name',[255]).notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.string('modified_at',[255]);
         })
         .then(() =>
         knex("role_category").insert([
        { category_name: "Tineon Admin"},
        { category_name: "Tineon Employees"},
        { category_name: "Support Manager"},
        { category_name: "Customer"},
        { category_name: "Sales Partner"},
         ])
     )
     
};

exports.down = async function(knex) {
    await knex.schema.dropTable('role_category');
};