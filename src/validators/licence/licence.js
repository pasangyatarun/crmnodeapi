export default class licenceValidation {
    licenceValidation(details, allowedExt) {
        let signupBody = {
            title: details.title,
            description: details.description,
            publish: details.publish,
        };
        let err = new Array();
        if(!signupBody.title){
            err.push('title:titleReq');
            }
        if (!signupBody.description) {
            err.push('description:descriptionReq');
        }
        if (!signupBody.publish) {
            err.push('publish:publishReq');
        }
        return err;
    }
}