exports.up = async function(knex) {
    return knex.schema.createTable('customer_tags', function(t) {
        t.increments('id').primary();
        t.integer('customer_id').notNull();
        t.string('tag_name',[255]).notNull();
        t.datetime('created_at').notNull().defaultTo(knex.fn.now());
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('customer_tags');
};

