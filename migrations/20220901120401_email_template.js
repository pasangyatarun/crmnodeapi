exports.up = async function(knex) {
    return knex.schema.createTable('email_template', function(t) {
        t.increments('id').primary();
        t.string('template_type',[100]).notNull();
        t.string('subject',[255]).notNull();
        t.longtext('template_body').notNull();
        t.text('header_content').notNull();
        t.text('footer_content').notNull();
        t.longtext('logo').notNull();
        t.longtext('url').notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now());
        t.datetime('modified_at').defaultTo(knex.fn.now());
      
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('email_template');
};