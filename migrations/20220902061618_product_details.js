exports.up = async function(knex) {
    return knex.schema.createTable('product_details', function(t) {
        t.increments('id').primary();
        t.integer('cuid',[10]).notNull();
        t.integer('pid',[11]).notNull();
        t.string('data',[255]).notNull();
        t.tinyint('status',[1]).notNull().defaultTo(1);
        t.string('created_at',[32]).notNull().defaultTo(0);
        t.string('modified_at',[32]).notNull().defaultTo(0);
     });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('product_details');
};