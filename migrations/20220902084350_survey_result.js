exports.up = async function(knex) {
    return knex.schema.createTable('survey_result', function(t) {
        t.increments('id').primary();
        t.integer('survey_id',[11]).notNull();
        t.integer('user_id',[11]);
        t.integer('tineon_user_id',[11]);
        t.string('survey_answer_id',[50]).notNull();
        t.string('survey_type',[20]).notNull();
         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('survey_result');
};
    