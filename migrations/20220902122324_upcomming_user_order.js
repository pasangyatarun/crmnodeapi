exports.up = async function (knex) {
    return knex.schema.createTable('upcomming_user_order', function (t) {
        t.increments('id').primary();
        t.string('planVariantId',[255]).notNull();
        t.string('holder',[255]).notNull();
        t.string('iban',[255]).notNull();
        t.longtext('mandate_text').notNull();
        t.string('chairman_name',[255]).notNull();
        t.string('coupon',[255]).notNull();
        t.integer('user_id',[11]).notNull();

     });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('upcomming_user_order');
};

