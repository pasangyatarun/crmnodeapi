exports.up = async function(knex) {
    return knex.schema.createTable('licence_history', function(t) {
        t.increments('id').primary();
        t.string('plan_variant_id',[255]).notNull();
        t.integer('lid').notNull();
        t.string('status',[20]).notNull();
        t.string('first_action',[264]).notNull();
        t.string('first_start_date',[264]).notNull();
        t.string('first_due_date',[264]).notNull();
        t.string('last_action',[264]).notNull();
        t.string('last_start_date',[264]).notNull();
        t.string('last_due_date',[264]).notNull();
        t.string('sales_partner',[64]).notNull();
        t.longtext('invoice').notNull();
        t.integer('user_id').notNull();

         });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('licence_history');
};
    