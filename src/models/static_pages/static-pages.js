import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class PagesModel {

	async getPagesByTitle(title) {
		const rows = await db.conn.promise().query(`SELECT static_pages.id,static_pages.uid as author_id,users.mail as author,static_pages.title,static_pages.status,static_pages.created,static_pages.content,static_pages.url,static_pages.image,static_pages.video, static_pages.type,static_pages.modified as updated FROM static_pages LEFT JOIN users ON users.uid = static_pages.uid WHERE static_pages.title = ?`, [title]);
		return rows[0];
	}

	async getPagesByUrl(text) {
		const rows = await db.conn.promise().query(`SELECT static_pages.id,static_pages.uid as author_id,users.mail as author,static_pages.title,static_pages.status,static_pages.created,static_pages.content,static_pages.url,static_pages.image,static_pages.video, static_pages.type,static_pages.modified as updated FROM static_pages LEFT JOIN users ON users.uid = static_pages.uid WHERE static_pages.url = ?`, [text]);
		return rows[0];
	}

	async CreatePages(req, text, pageImg) {
		const rows = await db.conn.promise().query('INSERT INTO `static_pages`(`uid`,`title`,`content`,`url`,`created`,`status`,`image`,`video`,`type`)VALUES(?,?,?,?,?,?,?,?,?)', [
			req.body.author,
			req.body.title,
			req.body.content,
			text,
			new Date().getTime(),
			1,
			pageImg,
			req.body.video,
			req.body.type
		]);
		return rows[0];
	}

	async getPagesById(id) {
		const rows = await db.conn.promise().query(`SELECT static_pages.id,static_pages.uid as author_id,users.mail as author,static_pages.title,static_pages.status,static_pages.created,static_pages.content,static_pages.url,static_pages.image,static_pages.video, static_pages.type,static_pages.modified as updated FROM static_pages LEFT JOIN users ON users.uid = static_pages.uid WHERE static_pages.id = ?`, [id]);
		return rows[0];
	}

	async getAllPages() {
		const rows = await db.conn.promise().query(`SELECT static_pages.id,static_pages.uid as author_id,users.mail as author,static_pages.title,static_pages.status,static_pages.created,static_pages.content,static_pages.url,static_pages.image,static_pages.video, static_pages.type,static_pages.modified as updated FROM static_pages LEFT JOIN users ON users.uid = static_pages.uid`);
		return rows[0];
	}

	async deletePagesbyId(id) {
		const rows = await db.conn.promise().query("DELETE FROM `static_pages` WHERE `id`=?", [id]);
		return rows[0];
	}
	
	async getUpdatePages(req, text, pageImg) {
		const rows = await db.conn.promise().query("UPDATE static_pages SET uid = ?,title= ?,content= ?,url= ?,status= ?,image= ?,video= ?,type= ?,modified=? WHERE id = ?", [
			req.body.author,
			req.body.title,
			req.body.content,
			text,
			1,
			pageImg,
			req.body.video,
			req.body.type,
			new Date().getTime(),
			req.params.id
		])
		return rows[0];
	}
}