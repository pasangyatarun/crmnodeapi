import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class ProductModel {

    async getAllProductToCustomer(){
        const rows = await db.query(`SELECT product_assign_customer.product_name,product_assign_customer.pid,product_assign_customer.admin_id as author_id,product_assign_customer.author,users.mail as userName FROM product_assign_customer LEFT JOIN  users ON users.uid = product_assign_customer.user_id`);
        return rows[0];
    }

    async assignProductToDistributor(req, pId) {
        const rows = await db.query("INSERT INTO `product_to_distributor`(`pid`,`distributor_id`)VALUES(?,?)", [
            pId,
            req.body.distributor_id
        ]);
        return rows[0];
    }

    async assignProductAdminToCustomer(req, pro) {
        const rows = await db.query("INSERT INTO `product_assign_customer`(`pid`,`user_id`,`admin_id`,`product_name`,`author`)VALUES(?,?,?,?,?)", [
            pro.id,
            req.body.userId,
            req.body.adminId,
            pro.name,
            req.body.author
        ]);
        return rows[0];
    }

    async assignProductToSales(req, pId) {
        const rows = await db.query("INSERT INTO `product_to_sales`(`pid`,`sales_id`)VALUES(?,?)", [
            pId,
            req.body.sales_id
        ]);
        return rows[0];
    }

    async getDeleteAdminProduct(id) {
        const rows = await db.query("DELETE FROM `product_assign_customer` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getDeleteSalesProduct(id) {
        const rows = await db.query("DELETE FROM `product_to_sales` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getDeleteDistributorProduct(id) {
        const rows = await db.query("DELETE FROM `product_to_distributor` WHERE `id`=?", [id]);
        return rows[0];
    }

    async selectProductToSales(id) {
        const rows = await db.query(`SELECT * FROM product_to_sales WHERE pid = ?`, [id]);
        return rows[0];
    }

    async selectProductFromAdmin(id) {
        const rows = await db.query(`SELECT * FROM product_assign_customer WHERE pid = ?`, [id]);
        return rows[0];
    }

    async selectProductToDistributor(id) {
        const rows = await db.query(`SELECT * FROM product_to_distributor WHERE pid = ?`, [id]);
        return rows[0];
    }

    async getAllProductsBySales(req){
        const rows = await db.query("SELECT sverein_product.pid,product_to_sales.id, sverein_product.cuid,sverein_product.domain,sverein_product.data as description,sverein_product.publish_to as publish, sverein_product.title as productName,sverein_product.product_components as productComponent,sverein_product.created_at,sverein_product.modified_at,sverein_product_price.price,product_details.data as productDetails FROM sverein_product LEFT JOIN product_to_sales ON product_to_sales.pid = sverein_product.pid LEFT JOIN sverein_product_price ON sverein_product_price.pid = sverein_product.pid LEFT JOIN product_details ON product_details.id = sverein_product.pid WHERE product_to_sales.sales_id = ?", [req.params.id]);
        return rows[0];
    }

    async getAllProductBydId(req){
        const rows = await db.query("SELECT sverein_product.pid,product_to_distributor.id, sverein_product.cuid,sverein_product.domain,sverein_product.data as description,sverein_product.publish_to as publish, sverein_product.title as productName,sverein_product.product_components as productComponent,sverein_product.created_at,sverein_product.modified_at,sverein_product_price.price,product_details.data as productDetails FROM sverein_product LEFT JOIN product_to_distributor ON product_to_distributor.pid = sverein_product.pid LEFT JOIN sverein_product_price ON sverein_product_price.pid = sverein_product.pid LEFT JOIN product_details ON product_details.id = sverein_product.pid WHERE product_to_distributor.distributor_id = ?", [req.params.id]);
        return rows[0];
    }

    async getProductById(pid) {
        const rows = await db.query("SELECT sverein_product.pid, sverein_product.cuid,sverein_product.domain,sverein_product.data as description,sverein_product.publish_to as publish, sverein_product.title as productName,sverein_product.product_components as productComponent,sverein_product.created_at,sverein_product.modified_at,sverein_product_price.price,product_details.data as productDetails FROM sverein_product LEFT JOIN sverein_product_price ON sverein_product_price.pid = sverein_product.pid LEFT JOIN product_details ON product_details.id = sverein_product.pid WHERE sverein_product.pid = ?", [pid]);
        return rows[0];
    }

    async getProductByProduct(name) {
        const rows = await db.query("SELECT * FROM `sverein_product` WHERE name = ?", [name]);
        return rows[0][0];
    }

    async createProduct(req) {
        const rows = await db.query('INSERT INTO `sverein_product`(`cuid`,`domain`,`data`,`publish_to`,`state`,`title`,`product_components`,`created_at`)VALUES(?,?,?,?,?,?,?,?)', [
            25,
            req.body.domain,
            req.body.description,
            req.body.publish,
            1,
            req.body.productName,
            req.body.productComponent,
            new Date().getTime()
        ]);
        return rows[0];
    }

    async createProDetail(req, id, data) {
        const rows = await db.query('INSERT INTO `product_details`(`pid`,`cuid`,`data`,`status`,`created_at`)VALUES(?,?,?,?,?)', [
            id,
            25,
            data,
            1,
            new Date().getTime()
        ]);
        return rows[0];
    }

    async updateProductDetail(req, data) {
        const rows = await db.query("UPDATE product_details SET cuid = ?,data = ?,status = ?,modified_at = ? WHERE id = ?", [
            25,
            req.body.data,
            req.body.description,
            1,
            new Date().getTime(),
            req.params.pid
        ])
        return rows[0];
    }

    async getAllProducts(req) {
        const rows = await db.query("SELECT sverein_product.pid, sverein_product.cuid,sverein_product.domain,sverein_product.data as description,sverein_product.publish_to as publish, sverein_product.title as productName,sverein_product.product_components as productComponent,sverein_product.created_at,sverein_product.modified_at,sverein_product_price.price,product_details.data as productDetails FROM sverein_product LEFT JOIN sverein_product_price ON sverein_product_price.pid = sverein_product.pid LEFT JOIN product_details ON product_details.id = sverein_product.pid");
        return rows[0];
    }

    async deleteProduct(pid) {
        const rows = await db.query("DELETE FROM `sverein_product` WHERE `pid`=?", [pid]);
        return rows[0];
    }

    async deleteProDetail(pid){
        const rows = await db.query("DELETE FROM `product_details` WHERE `pid`=?", [pid]);
        return rows[0];
    }
    
    async updateProduct(req) {
        const rows = await db.query("UPDATE sverein_product SET cuid = ?,domain = ?,data = ?,publish_to = ?,state = ?,title = ?,product_components = ?,modified_at = ? WHERE pid = ?", [
            25,
            req.body.domain,
            req.body.description,
            req.body.publish,
            1,
            req.body.productName,
            req.body.productComponent,
            new Date().getTime(),
            req.params.pid
        ])
        return rows[0];
    }

}