exports.up = async function(knex) {
    return knex.schema.createTable('role', function(t) {
        t.increments('rid').primary();
        t.integer('cat_id');
        t.string('name',[64]).notNull();
        t.integer('weight').notNull;
        t.string('description',[255]);
        //t.timestamp('created_at').defaultTo(knex.fn.now());
        // t.datetime('updated_at').defaultTo(knex.fn.now());
    })
    .then(() =>
    knex("role").insert([
        {rid:1,cat_id: 1, name: "Administrator", weight : 1, description : "Dies ist die Administrationsrolle der Tineon AG."},
        {rid:2,cat_id: 2, name: "Sales-Administrator", weight : 0, description : "Verkaufsleiter Rolle der Tineon AG. test"},
        {rid:3,cat_id: 2, name: "Support admin", weight : 2, description : "Support-Admin f&#252;r das Support Team."},
        {rid:4,cat_id: 3, name: "Support manager", weight : 3, description : "Support-Mitarbeiter der Tineon AG."},
        {rid:6,cat_id: 5, name: "Sales Partner", weight : 4, description : "Vertriebspartner der Tineon AG.&#160;"},
        {rid:8,cat_id: 4, name: "Customer", weight : 4, description : "Kunde&#160; &#160;der Tineon AG."},
        {rid:16,cat_id: 4, name: "SGP-Wechsler", weight : 4, description : "Rolle SPG-Wechsler. Zugang zu Sonderkonditionen."},
        {rid:29,cat_id: 0, name: "Super-Admin", weight : 0, description : "Super-Admin der Tineon AG.&#160;"},
        {rid:30,cat_id: 2, name: "Employee", weight : 0, description : "Test description for the role is 32 character 50 m"},
    ])
)

};

exports.down = async function(knex) {
    await knex.schema.dropTable('role');
};
