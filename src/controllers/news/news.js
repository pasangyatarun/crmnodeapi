import async from 'async';
import NewsValidation from '../../validators/news/news';
import responseFormat from '../../core/response-format';
import configContainer from '../../config/localhost';
import NewsModel from '../../models/news/news';
import AWSHandler from '../../core/AWSHander';

import fetch from 'node-fetch';
let newsModel = new NewsModel();
let newsValidation = new NewsValidation();

export default class NewsController {

    /**
    * Function to create news 
    * @author  MangoIt Solutions
    * @param   {object}  with news details
    * @return  {message} success message
    */
    newsCreate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = newsValidation.newsCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    newsModel.getNewsByTitle(req.body.title).then((newsFind) => {
                        if (newsFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['news:newsExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.body.url == "" || req.body.url == null) {
                        text = req.body.title.replace(/\s+/g, '-').toLowerCase();
                        done();
                    } else {
                        text = req.body.url.replace(/\s+/g, '-').toLowerCase();
                        done();
                    }
                },
                function (done) {
                    newsModel.getNewsByUrl(text).then((newsFind) => {
                        if (newsFind.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['news:pagesURLAliasExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsImgRequired'], { code: 417 });
                        res.status(200).json(response);
                    }
                },
                function (done) {
                    newsModel.CreateNews(req, text, s3URLofImage).then((newsCreated) => {
                        if (newsCreated.affectedRows > 0) {
                            var notific = {
                                "newsId": newsCreated.insertId
                            }

                            // var tok = Math.floor(100000 + Math.random() * 900000) + 'CRMTOVCLOUDSECRET' + Math.floor(100000 + Math.random() * 900000);
                            // let base64data = Buffer.from(tok).toString('base64');
                            // fetch(configContainer.tineonNodeServerURL + 'api/crm-news-created', {
                            //     'method': 'POST',
                            //     'headers': {
                            //         'secret': base64data,
                            //         'Content-Type': 'application/json'
                            //     },
                            //     'body': JSON.stringify(notific)
                            // }).then(() => {
                            // }).catch(() => {
                            // })
                            response = responseFormat.getResponseMessageByCodes(['news:newsCreationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['news:newsCreationFail'], { code: 417 });
                            res.status(200).json(response)
                        }
                    })
                }
            ])
        }
    }

    /**
    * Function to get news by url
    * @author  MangoIt Solutions
    * @param   {url} 
    * @return  {object} News details
    */
    getNewsbyURL(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                newsModel.getNewsByUrl(req.params.url).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get news by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {object} News details
    */
    getNewsbyId(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                newsModel.getNewsById(req.params.id).then((results) => {
                    if (results.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
    * Function to get news by id
    * @author  MangoIt Solutions
    * @param   {object} with page,pagesize 
    * @return  {object} News details
    */
    getNewsByPagination(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                newsModel.getNewsPagination(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [results, { 'pagination': { 'page': req.params.page, 'pageSize': req.params.pagesize, 'rowCount': results[0] ? results[0].totalCount : 0 } }] } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }


    /**
    * Function to get all news
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {object} News details
    */
    getNewsAll(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                newsModel.getAllNews().then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to delete news by id
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message}success message
    */
    newsDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        newsModel.deleteNewsbyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['news:newsDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }

    /**
    * Function for Notification of news
    * @author  MangoIt Solutions
    * @param   {object} with news id
    * @return  {object} news details
    */
    newsTineonNotify(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                newsModel.getNewsNotify(req).then((results) => {
                    if (results) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: results } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })

            }
        ])
    }

    /**
    * Function to update seleted news
    * @author  MangoIt Solutions
    * @param   {id} 
    * @return  {message} success message
    */
    newsUpdate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = newsValidation.newsCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    newsModel.getNewsByTitle(req.body.title).then((newsFind) => {
                        if (newsFind.length > 0 && req.params.id != newsFind[0].news_id) {
                            response = responseFormat.getResponseMessageByCodes(['news:newsExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.body.url == "" || req.body.url == null) {
                        text = req.body.title.replace(/\s+/g, '-').toLowerCase();
                        done();
                    } else {
                        text = req.body.url.replace(/\s+/g, '-').toLowerCase();
                        done();
                    }
                },
                function (done) {
                    newsModel.getNewsByUrl(text).then((newsFind) => {
                        if (newsFind.length > 0 && req.params.id != newsFind[0].news_id) {
                            response = responseFormat.getResponseMessageByCodes(['news:pagesURLAliasExists'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.imageUrl) {
                        s3URLofImage = req.body.imageUrl;
                        done();
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['news:newsImgRequired'], { code: 417 });
                        res.status(200).json(response);
                    }
                },
                function (done) {
                    newsModel.getUpdateNews(req, text, s3URLofImage).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['news:newsUpdationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['news:newsNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }
}