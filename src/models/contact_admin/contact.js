import DBConnection from '../../core/db.js';
let db = new DBConnection();

export default class ContactModel {

    async adminContact(req) {
        const rows = await db.conn.promise().query("INSERT INTO `contact_admin`(`uid`,`name`,`mail`,`subject`,`message`,`status`)VALUES(?,?,?,?,?,?)", [
            req.body.uid,
            req.body.username,
            req.body.email,
            req.body.subject,
            req.body.message,
            req.body.status,
        ]);
        return rows[0];
    }

    async getUserDetail(uid) {
        const rows = await db.conn.promise().query("SELECT name,mail from `users` WHERE uid = ?", [uid]);
        return rows[0][0];
    }
}