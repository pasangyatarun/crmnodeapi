import express from 'express';
import ProductController from '../../controllers/products/product.js';

let router = express.Router();
let productController = new ProductController;

router.get('/products', productController.products.bind(productController));
router.get('/productbyid/:pid', productController.productById.bind(productController));
router.post('/productcreate', productController.productCreate.bind(productController));
router.put('/productupdate/:pid', productController.updateProduct.bind(productController));
router.delete('/productdelete/:pid', productController.deleteProduct.bind(productController));
router.delete('/productsdelete', productController.deleteMultiProduct.bind(productController));  //expected req.body ---- arr:[21,26,24]
router.post('/productassignto/distributor', productController.assignToDistributor.bind(productController));
router.post('/productassignto/sales', productController.assignToSales.bind(productController));
router.get('/productby/distributor/:id', productController.productByDistributor.bind(productController));
router.get('/productby/sales/:id', productController.productBySales.bind(productController));
router.delete('/sales/productdelete/:id', productController.deleteSalesProduct.bind(productController));
router.delete('/distributor/productdelete/:id', productController.deleteDistributorProduct.bind(productController));
router.post('/billwerk-product-adminassigntocustomer', productController.getProductAdmintoCustomer.bind(productController));
router.get('/billwerk-product-adminassigned', productController.getProductCustomerByAdmin.bind(productController));

export default router;