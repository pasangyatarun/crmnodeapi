import async from 'async';
import responseFormat from '../../../core/response-format';
import sendEmail from '../../../core/nodemailer';

export default class BillwerkWebhookController {
    constructor() {
    }
    /**
    * Function for payment Escalated billing
    * @author  MangoIt Solutions
    * @param   {} 
    * @return  {}  email send
    */
    paymentEscalatedBilling(req, res, next) {
        var a1 = [];
        let response = responseFormat.createResponseTemplate(req.headers.lang);

        async.waterfall([
            function (done) {
                let ev = req.body.Event ? req.body.Event : ' --';
                let cid = req.body.ContractId ? req.body.ContractId : ' --';
                let cuid = req.body.CustomerId ? req.body.CustomerId : ' --';
                let yid = req.body.YourCustomerId ? req.body.YourCustomerId : ' --';
                let tdays = req.body.TriggerDays ? req.body.TriggerDays : ' --';
                let ddate = req.body.DueDate ? req.body.DueDate : ' --';
                let pp = req.body.PaymentProvider ? req.body.PaymentProvider : ' --';
                let psp = req.body.PaymentEscalationProcessId ? req.body.PaymentEscalationProcessId : ' --';
                let mailOptions = {
                    to: ['testmywork112@gmail.com', 'testwork121@gmail.com', 'tarun.mangoitsolutions@gmail.com', 'support@s-verein.de'],
                    subject: 'Zahlungserinnerung',
                    html: `<h3>Mitteilung aus Billwerk, Kunde hat dem Lastschrifteinzug widersprochen/ Zahlungsdaten haben sich geändert</h3>

                       <p></p>
                        <h5> Event:  `+ ev + `</h5><br>
                        <h5> ContractId:  `+ cid + `</h5><br>
                        <h5> CustomerId:  `+ cuid + `</h5><br>
                        <h5> YourCustomerId:  `+ yid + `</h5><br>
                        <h5> TriggerDays:  `+ tdays + `</h5><br>
                        <h5> DueDate:  `+ ddate + `</h5><br>
                        <h5> PaymentProvider:  `+ pp + `</h5><br>
                        <h5> PaymentEscalationProcessId:  `+ psp + `</h5><br>
                        `
                }

                sendEmail(mailOptions).then((sentdata) => {
                    if (sentdata == 'error') {
                    } else {
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }
}