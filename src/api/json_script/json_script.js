import express from 'express';
import JsonScriptController from '../../controllers/json_script/json_script';
import multerMW from '../../core/multer';

let router = express.Router();
let jsonScriptController = new JsonScriptController;

router.get('/getAllOldUserData', jsonScriptController.getAllOldUserData.bind(jsonScriptController));
router.get('/getAllOldUserDataByPaginate/:page/pagesize/:pagesize', jsonScriptController.getAllOldUserDataByPaginate.bind(jsonScriptController));
router.get('/getUserdetailsByUserId/:uid', jsonScriptController.getUserDataByUserId.bind(jsonScriptController));
router.post('/json_script',multerMW.single('file'), jsonScriptController.getJson.bind(jsonScriptController));
router.delete('/deleteAllOldUserData', jsonScriptController.deleteAllOldUserData.bind(jsonScriptController));
 
router.delete('/deleteAllUserData', jsonScriptController.deleteAllUserData.bind(jsonScriptController));

export default router;