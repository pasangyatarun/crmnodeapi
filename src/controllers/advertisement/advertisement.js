import async from 'async';
import responseFormat from '../../core/response-format';
import AdvertisementModel from "../../models/advertisement/advertisement";
import AdvertisementValidation from "../../validators/advertisement/advertisement"
import AWSHandler from '../../core/AWSHander';
import sendEmail from '../../core/nodemailer.js';
import jwt from 'jsonwebtoken';

let advertisementModel = new AdvertisementModel();
let advertisementValidation = new AdvertisementValidation();
export default class AdvertisementController {


    /**
   * Function to create advertisement
   * @author  MangoIt Solutions
   * @param   {object} with advertisement details
   * @return  {message} success message
   */
    advertisementCreate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = advertisementValidation.advertisementCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    advertisementModel.getAdvertisement(req.body.name).then((data) => {
                        if (data.length > 0) {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:advertisementExist'], { code: 417 });
                            res.status(200).json(response)
                        }
                        else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else {
                        done();
                    }
                },
                function (done) {
                    advertisementModel.CreateAdvertisement(req, s3URLofImage).then((results) => {
                        if (results) {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementCreatedsucess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementCreatedfail'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
 * Function to update advertisement by id
 * @author  MangoIt Solutions
 * @param   {id} with updated advertisement details
 * @return  {success} success message
 */
    advertisementUpdate(req, res, next) {
        let text;
        let file;
        let s3URLofImage;
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let msgCode = advertisementValidation.advertisementCreate(req.body);
        if (msgCode && msgCode.length) {
            response = responseFormat.getResponseMessageByCodes(msgCode, { code: 417 });
            res.status(200).json(response);
        } else {
            async.waterfall([
                function (done) {
                    advertisementModel.getAdvertisement(req.body.name).then((data) => {
                        if (data.length > 0 && req.params.id != data[0].id) {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:advertisementExist'], { code: 417 });
                            res.status(200).json(response)
                        } else {
                            done();
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                },
                function (done) {
                    if (req.file) {
                        file = req.file;
                        AWSHandler.uploadFile('Leipzig', file.originalname, file.path).then(function (result) {
                            s3URLofImage = result.Location;
                            done();
                        }).catch(function (err) {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        })
                    } else if (req.body.picture) {
                        s3URLofImage = req.body.picture;
                        done();
                    } else {
                        done();
                    }
                },
                function (done) {
                    advertisementModel.advertisementUpdate(req, s3URLofImage).then((result) => {
                        if (result.affectedRows > 0) {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:advertisementUpdationSuccess']);
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementNotExists'], { code: 417 });
                            res.status(200).json(response);
                        }
                    }).catch((error) => {
                        response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                        res.status(500).json(response);
                        return;
                    })
                }
            ])
        }
    }

    /**
   * Function to get all advertisements
   * @author  MangoIt Solutions
   * @param   {} 
   * @return  {object} Advertisement list
   */
    getAllAdvertisement(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                advertisementModel.getAllAdvertisement().then((data) => {
                    if (data.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: data } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])

    }

    /**
   * Function to get all advertisements
   * @author  MangoIt Solutions
   * @param   {id} 
   * @return  {object} Advertisement list
   */
    getAdvertisementById(req, res) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                advertisementModel.getAdvertisementById(req.params.id).then((data) => {
                    if (data.length > 0) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: data } });
                        res.status(200).json(response);
                    } else {
                        response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementNotExists'], { code: 417 });
                        res.status(200).json(response);
                    }
                }).catch((error) => {
                    response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                    res.status(500).json(response);
                    return;
                })
            }
        ])
    }

    /**
   * Function to delete advertisement by id
   * @author  MangoIt Solutions
   * @param   {id} 
   * @return  {success} success message
   */
    advertisementDelete(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        advertisementModel.deleteAdvertisementbyId(req.params.id).then((results) => {
            if (results.affectedRows > 0) {
                response = responseFormat.getResponseMessageByCodes(['advertisement:advertisementDeleted']);
                res.status(200).json(response);
            } else {
                response = responseFormat.getResponseMessageByCodes(['advertisement:AdvertisementNotExists'], { code: 417 });
                res.status(200).json(response);
            }
        }).catch((error) => {
            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
            res.status(500).json(response);
            return;
        })
    }
}
