import jwt from 'jsonwebtoken';

export default class AuthMiddleware{

    requireSignin(req,res,next){
        if(req.headers.authorization){
            const token = req.headers.authorization.split(" ")[1];
            const user = jwt.verify(token, 'up-secret')
            req.user = user;        
        }
        else{
            return res.status(400).json({message: 'Authorization required'});
        }    
        next();
    }

    userMiddleware(req,res,next){
        if(req.user.role !== 'user'){
            return res.status(400).json({error: 'User Access Denied'});
        }
        next();
    }

    adminMiddleware(req,res,next){
        if(req.user.role !== 'admin'){
            return res.status(400).json({error: 'Admin Access Denied'});
        }
        next();
    }
}