import express   from 'express';
import ClubController from '../../controllers/club/club';

let router = express.Router();
let clubController = new ClubController();

router.post('/support-user/:uid', clubController.getSupportUser.bind(clubController));
router.get('/support-user-userpass/:uid', clubController.getSupportUserPass.bind(clubController));

export default router;