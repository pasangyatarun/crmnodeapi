import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import axios from 'axios';
import qs from 'qs';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');

export default class BillwerkAuthController {
	constructor() {
	}

	/**
	* Function for customer payment details
	* @author  MangoIt Solutions
	* @param   {object}
	* @return  {message} Succes message
	*/
	allPaymentDetailByCustomer(req, res, next) {
		var a1;
		var a2 = [];
		var uniqueValues;
		var a4 = [];
		var a5 = [];
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		async.waterfall([
			function (done) {
				var config = {
					method: 'get',
					url: configContainer.billwerkAccountUrl + 'customers/' + req.params.customer_id + '/contracts',
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
					}
				};
				axios(config)
					.then(function (resPonse) {
						a1 = resPonse.data;
						let x = a1.filter(o => o.PaymentBearer)
						x.forEach(element => {
							a2.push(element.PaymentBearer.IBAN)
						});
						const unique = (value, index, self) => {
							return self.indexOf(value) === index
						}

						uniqueValues = a2.filter(unique)
						uniqueValues.forEach(ele => {
							a4.push(x.find(o => o.PaymentBearer.IBAN == ele))
						});
						a4.forEach(e => {
							a5.push(e.PaymentBearer)
						});
						response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a5 } });
						res.status(200).json(response);
					})
					.catch(function (error) {
						if (error.isAxiosError == true && error.response.data.Message) {
							response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
							res.status(200).json(response);
						} else {

							response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
							res.status(500).json(response);
							return;
						}
					});
			}
		])
	}

	/**
	 * Function for customer update
	 * @author  MangoIt Solutions
	 * @param   {customer_id}
	 * @return  {object} customer details
	 */
	customerUpdate(req, res, next) {
		var a1 = [];
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		async.waterfall([
			function (done) {
				var config = {
					method: 'put',
					url: configContainer.billwerkAccountUrl + 'customers/' + req.params.customer_id,
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
					},
					data: req.body
				};
				axios(config)
					.then(function (resPonse) {
						a1[0] = resPonse.data;
						response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
						res.status(200).json(response);
					})
					.catch(function (error) {
						if (error.isAxiosError == true && error.response.data.Message) {
							response = responseFormat.getResponseMessageByCodes(['order:' + error.response.data.Message + ''], { code: 417 });
							res.status(200).json(response);
						} else {

							response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
							res.status(500).json(response);
							return;
						}
					});
			}
		])
	}

	/**
	 * Function for sign in billwerk
	 * @author  MangoIt Solutions
	 * @param   {}
	 * @return  {success} success message
	 */
	signinBillwerk() {
		return new Promise((resolve, reject) => {
			let clientId = configContainer.billwerkClientId;
			let clientsecret = configContainer.billwerkClientSecret;
			let base64data = Buffer.from(clientId + ':' + clientsecret).toString('base64');

			var data = qs.stringify({
				'grant_type': 'client_credentials'
			});
			var config = {
				method: 'post',
				url: configContainer.billwerkAuthUrl,
				headers: {
					'Authorization': 'Basic ' + base64data,
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: data
			};

			axios(config)
				.then(function (response) {
					localStorage.setItem('billwerkAuth', response.data.access_token);
					return resolve({ success: true, message: null })

				})
				.catch(function (error) {
					if (error.isAxiosError == true && error.response.data.Message) {
						return reject(error.response.data.Message);
					} else {
						return reject(error.response.data.Message);
					}
				});
		})
	}

	/**
	 * Function to sign in
	 * @author  MangoIt Solutions
	 * @param   {}
	 * @return  {success} success message
	 */
	signin(req, res, next) {
		let response = responseFormat.createResponseTemplate(req.headers.lang);
		let clientId = configContainer.billwerkClientId;
		let clientsecret = configContainer.billwerkClientSecret;
		let base64data = Buffer.from(clientId + ':' + clientsecret).toString('base64');

		async.waterfall([
			function (done) {
				var data = qs.stringify({
					'grant_type': 'client_credentials'
				});
				var config = {
					method: 'post',
					url: configContainer.billwerkAuthUrl,
					headers: {
						'Authorization': 'Basic ' + base64data,
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: data
				};
				axios(config)
				.then(function (response) {
						localStorage.setItem('billwerkAuth', response.data.access_token);
						response = responseFormat.getResponseMessageByCodes(['billwerk:AuthSuccess']);
						res.status(200).json(response);
					})
					.catch(function (error) {
						if (error.isAxiosError == true && error.response.data) {
							response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
							res.status(200).json(response);
						} else {

							response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
							res.status(500).json(response);
							return;
						}
					});
			}
		])
	}

}