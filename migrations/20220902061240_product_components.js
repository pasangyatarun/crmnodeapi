exports.up = async function(knex) {
    return knex.schema.createTable('product_components', function(t) {
        t.increments('id').primary();
        t.integer('cuid',[11]).notNull().defaultTo(0);
        t.string('name',[255]).notNull();
        t.string('description',[255]).notNull();
        t.string('created_at',[32]).notNull().defaultTo(0);
        t.string('modified_at',[32]).notNull().defaultTo(0);
     });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('product_components');
};