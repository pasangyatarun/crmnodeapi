import express   from 'express';
import UserController  from '../../controllers/users/users.js';
import multerMW from '../../core/multer';

let router = express.Router();
let userController = new UserController();

router.get('/usersbypage/:page/pagesize/:pagesize', userController.usersByPageSize.bind(userController));
router.get('/usersbysearch/:page/pagesize/:pagesize/search/:search', userController.usersBySearch.bind(userController));
router.get('/users', userController.users.bind(userController));
router.get('/userByAuthor', userController.userByAuthor.bind(userController));
router.get('/userbyid/:id', userController.userById.bind(userController));
router.get('/userbycustomerid/:id', userController.userByCustomerId.bind(userController));
router.get('/getuserlogininfo/:user_id', userController.userLoginInfo.bind(userController));
router.get('/userbyemail/:email', userController.userByEmail.bind(userController));
router.post('/useradd',multerMW.single('file'), userController.addUser.bind(userController));
router.put('/userupdate/:id', userController.updateUser.bind(userController));
router.delete('/userdelete/:id', userController.deleteUser.bind(userController));
router.delete('/usersdelete', userController.deleteMultiUser.bind(userController));  //expected req.body ---- arr:[21,26,24]
router.get('/distributors', userController.distributorUsers.bind(userController));
router.get('/salespartnerbypage/:page/pagesize/:pagesize', userController.salesPartnerUsersByPage.bind(userController));
router.get('/salesusers', userController.salesUsers.bind(userController));
router.get('/customerlist', userController.customerList.bind(userController));
router.post('/assignto/distributor', userController.assignToDistributor.bind(userController));
router.post('/assignto/sales', userController.assignToSales.bind(userController));
router.get('/customerby/distributor/:id', userController.customerByDistributor.bind(userController));
router.get('/customerby/distributorbypage/:id/page/:page/pagesize/:pagesize', userController.customerByDistributorByPaginate.bind(userController));
router.get('/customerby/sales/:id', userController.customerBySales.bind(userController));
router.get('/salesby/customer/:id', userController.salesPartnerByCustomer.bind(userController));
router.delete('/sales/customerdelete/:id', userController.deleteSalesCustomer.bind(userController));
router.delete('/distributor/customerdelete/:id', userController.deleteDistributorCustomer.bind(userController));
router.put('/updateIbanbyUserId/:user_id', userController.updateIbanByUser.bind(userController));
router.get('/clubsCustomer', userController.clubsCustomer.bind(userController));
router.get('/clubsCustomerbypagination/:page/pagesize/:pagesize', userController.clubsCustomerBypagination.bind(userController));

router.post('/commission-dateintervalbyspid/:id', userController.getCommissionByDateInterval.bind(userController));

router.post('/commissions-set/:spid', userController.setupCommissions.bind(userController));
router.get('/commission/calculatebyspid/:id', userController.getCommission.bind(userController));
router.get('/commission-settingbyspid/:id', userController.getCommSettingBySpId.bind(userController));
router.delete('/commission-setting-deletebyspid/:id', userController.deleteSetting.bind(userController));




export default router;