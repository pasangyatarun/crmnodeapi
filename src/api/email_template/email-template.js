import express   from 'express';
import EmailTemplateController from '../../controllers/email_template/email-template';
import multerMW from '../../core/multer';

let router = express.Router();
let emailTemplateController = new EmailTemplateController();

router.get('/emailtemplates', emailTemplateController.getemailTemplateAll.bind(emailTemplateController));
router.get('/emailtemplatebyid/:id', emailTemplateController.getemailTemplatebyId.bind(emailTemplateController));
router.post('/emailtemplatecreate', multerMW.single('file'), emailTemplateController.emailTemplateCreate.bind(emailTemplateController));
router.delete('/emailtemplatedelete/:id', emailTemplateController.emailTemplateDelete.bind(emailTemplateController));
router.put('/emailtemplateedit/:id',multerMW.single('file'), emailTemplateController.emailTemplateUpdate.bind(emailTemplateController));

export default router;