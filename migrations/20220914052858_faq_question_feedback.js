exports.up = async function(knex) {
    return knex.schema.createTable('faq_question_feedback', function(t) {
        t.increments('id').primary();
        t.integer('user_id').notNull();
        t.integer('category_id').notNull();
        t.integer('question_id').notNull();
        t.integer('status').notNull().comment('1:like, 0-dislike;');
        t.datetime('created_at').notNull().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
        t.datetime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('faq_question_feedback');
};
