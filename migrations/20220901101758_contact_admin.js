exports.up = async function(knex) {
    return knex.schema.createTable('contact_admin', function(t) {
        t.increments('cid').primary();
        t.integer('uid').notNull();
        t.string('name',[64]);
        t.string('mail',[255]).notNull();
        t.string('subject',[255]);
        t.string('message',[255]);
        t.tinyint('status',[1]).notNull().defaultTo(0);
      
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('contact_admin');
};

