exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('users', function(t) {
        t.tinyint('status').notNull().defaultTo(0).comment('Whether the user is active(1) or blocked(0).').alter();
             
    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('users', function(t) {
        
    });
 };