import express from 'express';
import ProductPriceController from '../../controllers/product_price/product-price.js';

let router = express.Router();
let propriceController = new ProductPriceController;

router.get('/product/prices', propriceController.productPrices.bind(propriceController));
router.get('/product/pricebyid/:pid', propriceController.proPriceById.bind(propriceController));
router.get('/product/pricebysuid/:suid', propriceController.proPriceBysuid.bind(propriceController));
router.get('/product/pricebycuid/:cuid', propriceController.proPriceBycuid.bind(propriceController));
router.put('/product/price/update/:pid', propriceController.updateProPrice.bind(propriceController));
router.put('/product-price/update/:pid', propriceController.updateProPriceBypid.bind(propriceController)); //only price update

export default router;