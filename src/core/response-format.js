/**
 *  -------Import all classes and packages -------------
 */
import deepmerge from 'deepmerge';
import messageCode from './message-code';
import englishMessageList from './message-list';
import germanMessageList from './message-list_dutch'
import express from 'express';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');
let app = express();
let messageList;

function switchLan(language) {
    // if(localStorage.getItem('language') == 'en'){
    //     messageList = englishMessageList;
    // }else if(localStorage.getItem('language') == 'de') {
    //     messageList = germanMessageList;
    // }
    if (language == 'en') {
        messageList = englishMessageList;
    } else if (language == 'de') {
        messageList = germanMessageList;
    }
}

function createResponseTemplate(lang, code, params) {
    switchLan(lang);
    let template = {
        success: true,
        code: 200,
        message: "Processed",
        description: "Request has been processed successfully.",
        content: {
            dataList: null,
            messageList: null,
        }
    };
    let merged, message = undefined;

    if (code && code.length) {
        template.content.messageList = [];
        for (let i = 0; i < code.length; i++) {
            message = messageCode.messageCode(code[i]);
            template.content.messageList.push({ messageCode: code[i], messageDescription: message });
        }
    }

    if (params) {
        merged = deepmerge(template, params);
    } else {
        merged = template;
    }
    return merged;
}

function getResponseMessageByCodeList(lang, code, params) {
    switchLan(lang);


    let template = {
        success: true,
        code: 200,
        message: "Processed",
        description: "OK",
        content: {
            dataList: null,
            messageList: null,
        }
    };
    let merged;
    template.content.messageList = {};
    if (code && code.length) {
        for (let i = 0; i < code.length; i++) {
            let fieldName = '', codeParams = '';
            if (code[i].indexOf(':') > -1) {
                fieldName = code[i].split(':')[0], codeParams = code[i].split(':')[1];
            } else {
                fieldName = code[i]; codeParams = code[i];
            }
            let msg = messageCode.messageCode(codeParams);
            if (template.content.messageList[fieldName]) {
                template.content.messageList[fieldName] = template.content.messageList[fieldName] + msg;
            } else {
                template.content.messageList[fieldName] = msg;
            }
        }
    }
    if (params) {
        merged = deepmerge(template, params);
        if (params.code == 400 || params.code == 401 || params.code == 404 || params.code == 417 || params.code == 500) {
            merged.success = false;
            merged.message = "Unprocessed";
        }
        if (params.code == 400) {
            merged.description = "Bad Request";
        }
        if (params.code == 401) {
            merged.description = "Unauthorized";
        }
        if (params.code == 404) {
            merged.description = "Not Found";
        }
        if (params.code == 500) {
            merged.message = "Error";
            merged.description = "Internal Server Error";
        }
        if (params.code == 417) {
            merged.message = "Processed";
            merged.description = "Expectation Failed";
        }
    } else {
        merged = template;
    }

    /**
     * replace null to empty string  from data
     */
    if (merged.content.dataList && merged.content.dataList.length) {
        let stringResp = JSON.stringify(merged.content.dataList).replace(/null/g, "\"\"").replace(/"\"\""/g, "\"\"");
        merged.content.dataList = JSON.parse(stringResp);
    } else {
        merged.content.dataList = [];
    }
    return merged;
}

function getResponseMessageByMsgList(code, params) {
    switchLan();

    let template = {
        success: true,
        code: 200,
        message: "Processed",
        description: "OK",
        content: {
            dataList: null,
            messageList: null,
        }
    };
    let merged;
    template.content.messageList = {};
    if (code && code.length) {
        for (let i = 0; i < code.length; i++) {
            let fieldName = '';
            fieldName = code[i].path;
            let msg = code[i].message;
            if (template.content.messageList[fieldName]) {

                template.content.messageList[fieldName] = template.content.messageList[fieldName] + msg;
            } else {
                template.content.messageList[fieldName] = msg;
            }
        }
    }
    if (params) {
        merged = deepmerge(template, params);
        if (params.code == 400 || params.code == 401 || params.code == 404 || params.code == 417 || params.code == 500) {
            merged.success = false;
            merged.message = "Unprocessed";
        }
        if (params.code == 400) {
            merged.description = "Bad Request";
        }
        if (params.code == 401) {
            merged.description = "Unauthorized";
        }
        if (params.code == 404) {
            merged.description = "Not Found";
        }
        if (params.code == 500) {
            merged.message = "Error";
            merged.description = "Internal Server Error";
        }
        if (params.code == 417) {
            merged.description = "Expectation Failed";
        }
    } else {
        merged = template;
    }
    /**
     * replace null to empty string  from data
     */
    if (merged.content.dataList && merged.content.dataList.length) {
        let stringResp = JSON.stringify(merged.content.dataList).replace(/null/g, "\"\"").replace(/"\"\""/g, "\"\"");
        merged.content.dataList = JSON.parse(stringResp);
    } else {
        merged.content.dataList = [];
    }
    return merged;
}


function getResponseMessageByCodes(code, params) {

    let template = {
        success: true,
        code: 200,
        message: "Processed",
        description: "OK",
        content: {
            dataList: null,
            messageList: null,
        }
    };


    if (app.locals.info && app.locals.info != '') {
        template.content['info'] = app.locals.info;
        template.content['mandatory'] = app.locals.mandatory;
    }


    if (params && params.content && typeof params.content['info'] != undefined) {
        app.locals.info = params.content['info'];
        app.locals.mandatory = params.content['mandatory'];
    }


    let merged;
    template.content.messageList = {};
    if (code && code.length) {
        for (let i = 0; i < code.length; i++) {

            let fieldName = '', codeParams = '';
            if (code[i].indexOf('~') > -1) {
                fieldName = code[i].split('~')[0], codeParams = code[i].split('~')[1];
            }
            else if (code[i].indexOf(':') > -1) {
                fieldName = code[i].split(':')[0], codeParams = code[i].split(':')[1];
            } else {
                fieldName = code[i]; codeParams = code[i];
            }

            let fldNm = template.content.messageList[fieldName] ? template.content.messageList[fieldName] : fieldName;
            let codPrm = messageList[codeParams] ? messageList[codeParams] : codeParams;
            template.content.messageList[fldNm] = codPrm;
        }
    }
    if (params) {
        merged = deepmerge(template, params);
        if (params.code == 400 || params.code == 401 || params.code == 404 || params.code == 417 || params.code == 500) {
            merged.success = false;
            merged.message = "Unprocessed";
        }
        if (params.code == 400) {
            merged.description = "Bad Request";
        }
        if (params.code == 401) {
            merged.description = "Unauthorized";
        }
        if (params.code == 404) {
            merged.description = "Not Found";
        }
        if (params.code == 500) {
            merged.message = "Error";
            merged.description = "Internal Server Error";
        }
        if (params.code == 417) {
            merged.message = "Processed";
            merged.description = "Expectation Failed";
        }
    } else {
        merged = template;
    }

    /**
     * replace null to empty string  from data
     */
    if (merged.content.dataList && merged.content.dataList.length) {

        let stringResp = JSON.stringify(merged.content.dataList, (k, v) => {
            if (v == null || v == 'null') {
                return '';
            }
            return v;
        })
        merged.content.dataList = JSON.parse(stringResp);

    } else {
        merged.content.dataList = [];
    }
    return merged;
}

export default { createResponseTemplate, getResponseMessageByCodeList, getResponseMessageByMsgList, getResponseMessageByCodes }
