import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class CustomerMembersModel {

    async checkDepartmentExists(req) {
        const rows = await db.query("SELECT department_name from `customer_departments` WHERE department_name=? AND customer_id = ?", [req.body.department, req.body.customerId]);
        return rows[0];
    }

    async checkTagExists(req) {
        const rows = await db.query("SELECT * from `customer_tags` WHERE tag_name = ? AND customer_id = ?", [req.body.tag, req.body.customerId]);
        return rows[0];
    }
    async getDepartmentByCustomerId(customerId) {
        const rows = await db.query(`SELECT * FROM customer_departments WHERE customer_id = ? ORDER BY id DESC`, [customerId]);
        return rows[0];
    }

    async getTagByCustomerId(customerId) {
        const rows = await db.query(`SELECT * FROM customer_tags WHERE customer_id = ? ORDER BY id DESC`, [customerId]);
        return rows[0];
    }
    async getMemberFilterByCompanyCustomerId(customerId) {
        const rows = await db.query(`SELECT id,salutation,company_name,responsible_person,email,gender,mobile,phone,established_date,status FROM customer_members WHERE customer_id = ? AND salutation = ? ORDER BY id DESC`, [customerId, 'Company']);
        return rows[0];
    }

    async getMemberFilterByCustomerId(customerId) {
        const rows = await db.query(`SELECT id,salutation,firstname,lastname,email,gender,mobile,phone,birth_date,status FROM customer_members WHERE customer_id = ? AND salutation != ? ORDER BY id DESC`, [customerId, 'Company']);
        return rows[0];
    }

    async getMemberByCustomerId(customerId) {
        const rows = await db.query(`SELECT * FROM customer_members WHERE customer_id = ? ORDER BY id DESC`, [customerId]);

        return rows[0];
    }

    async getDepartmentById(id) {
        const rows = await db.query(`SELECT * FROM customer_departments WHERE id = ?`, [id]);
        return rows[0];
    }


    async getTagById(id) {
        const rows = await db.query(`SELECT * FROM customer_tags WHERE id = ?`, [id]);
        return rows[0];
    }
    async checkEmailExists(email) {
        const rows = await db.query("SELECT email from `customer_members` WHERE email=?", [email]);
        return rows[0][0];
    }

    async checkUserNameExists(userName) {
        const rows = await db.query("SELECT username,id from `customer_members` WHERE username = ?", [userName]);
        return rows[0];
    }

    async addDepartment(req) {
        const rows = await db.query("INSERT INTO `customer_departments`(`customer_id`,`department_name`)VALUES(?,?)", [
            req.body.customerId,
            req.body.department,
        ]);
        return rows[0];
    }

    async addTag(req) {
        const rows = await db.query("INSERT INTO `customer_tags`(`customer_id`,`tag_name`)VALUES(?,?)", [
            req.body.customerId,
            req.body.tag,
        ]);
        return rows[0];
    }

    async addMemberTag(tag, customerId) {
        const rows = await db.query("INSERT INTO `customer_tags`(`customer_id`,`tag_name`)VALUES(?,?)", [
            customerId,
            tag,
        ]);
        return rows[0];
    }
    async addMember(req, tags1, userImg, age) {
        req.body.type = JSON.parse(req.body.type).map(function (x) {
            return parseInt(x, 10);
        });
        const rows = await db.query("INSERT INTO `customer_members`(`salutation`,`email`,`firstname`,`lastname`,`company_name`,`responsible_person`,`customer_id`,`gender`,`age`,`birth_date`,`established_date`,`mobile`,`phone`,`image`,`street`,`city`,`zipcode`,`status`,`type`,`functionClub`,`department`,`department_entry_date`,`club_entry_date`,`club_exit_date`,`reason_for_exit`,`notes`,`app_in_use`,`payment_type`,`tags`)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [
            req.body.salutation,
            req.body.email,
            req.body.firstName,
            req.body.lastName,
            req.body.companyName,
            req.body.responsiblePerson,
            req.body.customerId,
            req.body.gender,
            age,
            req.body.DOB,
            req.body.establishedDate,
            req.body.mobile,
            req.body.phone,
            userImg,
            req.body.street,
            req.body.city,
            req.body.zipcode,
            req.body.status,
            JSON.stringify(req.body.type),
            req.body.functionClub,
            req.body.department,
            req.body.departmentEntryDate,
            req.body.clubEntryDate,
            req.body.clubExitDate,
            req.body.reasonForExit,
            req.body.notes,
            req.body.appUse,
            req.body.payment_type,
            JSON.stringify(tags1)
        ]);
        return rows[0];
    }

    async addMemberTags(member_id, tags) {

        const rows = await db.query("INSERT INTO `customer_member_tags`(`member_id`,`tags`)VALUES(?,?)", [
            member_id,
            tags
        ]);
        return rows[0];
    }

    async getAllMembers() {
        const rows = await db.query(`SELECT * from customer_members`);
        return rows[0];
    }

    async getMembersByStatus(req) {
        const rows = await db.query(`SELECT * FROM customer_members WHERE status = ?`, [req.params.status]);
        return rows[0];
    }

    async getMemberById(id) {
        const rows = await db.query(`SELECT * FROM customer_members WHERE id = ?`, [id]);
        return rows[0];
    }

    async getMemberByEmail(email) {
        const rows = await db.query("SELECT * FROM `customer_members` WHERE email = ?", [email]);
        return rows[0];
    }

    async getMemberAcess(req) {
        const rows = await db.query("UPDATE users SET support_access = ? WHERE uid = ?", [
            req.body.toggleAccess,
            req.params.uid
        ]);
        return rows[0];
    }

    async updateDepartment(req) {
        const rows = await db.query("UPDATE customer_departments SET department_name = ? WHERE id = ?", [
            req.body.department,
            req.params.id
        ]);
        return rows[0];
    }

    async updateTag(req) {
        const rows = await db.query("UPDATE customer_tags SET tag_name = ? WHERE id = ?", [
            req.body.tag,
            req.params.id
        ]);
        return rows[0];
    }

    async updateMember(req, userImg, age, tags1) {
        req.body.type = JSON.parse(req.body.type).map(function (x) {
            return parseInt(x, 10);
        });
        const rows = await db.query("UPDATE customer_members SET salutation = ?,email= ?,firstname= ?,lastname= ?,company_name = ?,responsible_person = ?,gender= ?,age= ?,birth_date= ?,established_date= ?,mobile= ?,phone= ?,image= ?,street= ?,city= ?,zipcode= ?,status= ?,type= ?,functionClub= ?,department= ?,department_entry_date= ?,club_entry_date= ?,club_exit_date= ?,reason_for_exit= ?,notes= ?,app_in_use= ? , payment_type = ?,tags=? WHERE id = ?", [
            req.body.salutation,
            req.body.email,
            req.body.firstName,
            req.body.lastName,
            req.body.companyName,
            req.body.responsiblePerson,
            req.body.gender,
            age,
            req.body.DOB,
            req.body.establishedDate,
            req.body.mobile,
            req.body.phone,
            userImg,
            req.body.street,
            req.body.city,
            req.body.zipcode,
            req.body.status,
            JSON.stringify(req.body.type),
            req.body.functionClub,
            req.body.department,
            req.body.departmentEntryDate,
            req.body.clubEntryDate,
            req.body.clubExitDate,
            req.body.reasonForExit,
            req.body.notes,
            req.body.appUse,
            req.body.payment_type,
            JSON.stringify(tags1),
            req.params.id
        ]);
        return rows[0];
    }

    async getDeleteMemberByCustomer(id) {
        const rows = await db.query("DELETE  FROM customer_members WHERE `customer_id`=?", [id]);
        return rows[0];
    }

    async getDeparmentAssignedtoMemebr(customerId, req) {
        // const rows = await db.query(`SELECT customer_id,department FROM customer_members WHERE customer_id = ?`, [customerId]);
        const rows = await db.query("SELECT customer_id,department FROM customer_members WHERE customer_id = ? and department LIKE '%" + req.params.id + "%'", [customerId]);
        return rows[0];

    }

    async getDeleteDepartment(id) {
        const rows = await db.query("DELETE  FROM `customer_departments` WHERE `id`= ?", [id]);
        return rows[0];
    }
    async checkTagUsed(id) {
        const rows = await db.query("SELECT id,tags  FROM `customer_members` WHERE tags LIKE '%" + id + "%'");
        return rows[0];
    }

    async updatemembertags(id, arr) {
        const rows = await db.query("UPDATE customer_members SET tags=? WHERE id = ?", [
            JSON.stringify(arr),
            id
        ]);
        return rows[0];
    }

    async getDeleteTag(id) {
        const rows = await db.query("DELETE  FROM `customer_tags` WHERE `id`= ?", [id]);
        return rows[0];
    }
    async getDeleteMember(id) {
        const rows = await db.query("DELETE  FROM `customer_members` WHERE `id`= ?", [id]);
        return rows[0];
    }

    async checkFilterName(req) {
        const rows = await db.query(`SELECT * FROM customer_member_search WHERE filter_name = ? and customer_id=?`, [req.body.filter_name, req.body.customer_id]);
        return rows[0];
    }
    async addMemberSearch(req) {
        req.body.department = req.body.department.map(function (x) {
            return parseInt(x, 10);
        });
        const rows = await db.query("INSERT INTO `customer_member_search`(`customer_id`,`filter_name`,`search_in`,`type`,`department`,`link_the_search`)VALUES(?,?,?,?,?,?)", [
            req.body.customer_id,
            req.body.filter_name,
            JSON.stringify(req.body.search_in),
            JSON.stringify(req.body.type),
            JSON.stringify(req.body.department),
            req.body.link_the_search
        ]);
        return rows[0];
    }

    async getMemberSearchByCustomerId(id) {
        const rows = await db.query(`SELECT customer_member_search.*,customer_departments.department_name,customer_departments.id as depat_id  FROM customer_member_search LEFT JOIN customer_departments ON customer_member_search.department = customer_departments.id WHERE customer_member_search.customer_id = ? ORDER BY id DESC`, [id]);
        return rows[0];
    }

    async getMemeberSearchById(id) {
        const rows = await db.query(`SELECT * FROM customer_member_search WHERE id = ?`, [id]);
        return rows[0];
    }
    async getDeleteMemberSearch(id) {
        const rows = await db.query("DELETE  FROM `customer_member_search` WHERE `id`= ?", [id]);
        return rows[0];
    }
    async getSearchResultForSearchIn(status, data) {
        let status1 = status.join();
        if (status == 3) {
            const rows = await db.query("SELECT * FROM customer_members WHERE  customer_id =  (" + data[0].customer_id + ") ORDER BY id DESC ");
            return rows[0];
        }
        else {
            const rows = await db.query("SELECT * FROM customer_members WHERE status IN (" + status1 + ") and customer_id =  (" + data[0].customer_id + ") ORDER BY id DESC");
            return rows[0];
        }
    }

    async getSearchResultForType(type, data) {
        let result = []
        const rows = await db.query("SELECT * FROM customer_members WHERE  customer_id =  (" + data[0].customer_id + ") ORDER BY id DESC");
        let resu = rows[0].map(ele => {
            let isFounded = type.some(ai => (ele.type).includes(ai))
            if (isFounded) {
                result.push(ele)
            }
        })
        return result;

    }
    async getSearchResultForDepartment(department, data) {
        let result = [];
        const rows = await db.query("SELECT * FROM customer_members WHERE customer_id =  ? ORDER BY id DESC", [data[0].customer_id]);
        let resu = rows[0].map(ele => {
            let isFounded = department.some(ai => (ele.department).includes(ai))
            if (isFounded) {
                result.push(ele)
            }
        })
        return result;

    }
    async getSearchResultForStatusAndDepartment(status, data) {
        let department = JSON.parse(data[0].department)
        let status1 = status.join();
        if (data[0].link_the_search.toLowerCase() == 'and') {
            const rows = await db.query("SELECT * FROM customer_members WHERE customer_id =  ? and department LIKE '%" + data[0].department + "%' and status IN (" + status1 + ") ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }
        else {
            const rows = await db.query("SELECT * FROM customer_members WHERE  customer_id =  ? and (department LIKE '%" + data[0].department + "%' OR status IN (" + status1 + ")) ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }

    }
    async getSearchResultForStatusAndtype(status, type, data) {
        let department = data[0].department
        let status1 = status.join();
        if (data[0].link_the_search.toLowerCase() == 'and') {
            const rows = await db.query("SELECT * FROM customer_members WHERE customer_id =  ? and  type LIKE   '%" + JSON.stringify(type) + "%' and status IN (" + status1 + ")  ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }
        else {
            const rows = await db.query("SELECT * FROM customer_members WHERE  customer_id =  ? and (type LIKE '%" + JSON.stringify(type) + "%' OR  status  IN (" + status1 + ")) ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }


    }
    async getSearchResultForStatusAndtypeAndDepartment(status, type, data) {
        let department = data[0].department
        let status1 = status.join();
        if (data[0].link_the_search.toLowerCase() == 'and') {
            const rows = await db.query("SELECT * FROM customer_members WHERE customer_id =  ? and  type LIKE  '%" + JSON.stringify(type) + "%' and status IN (" + status1 + ") and department LIKE '%" + data[0].department + "%' ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }
        else {
            const rows = await db.query("SELECT * FROM customer_members WHERE  customer_id =  ? and ( type LIKE '%" + JSON.stringify(type) + "%'  OR status IN (" + status1 + ") OR department LIKE '%" + data[0].department + "%') ORDER BY id DESC", [data[0].customer_id]);
            return rows[0];
        }


    }
}