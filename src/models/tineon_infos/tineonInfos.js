import DBConnection from '../../core/db.js';
let db = new DBConnection().conn.promise();

export default class TineonInfosModel {

    async AddTineonInfo(req) {
        const rows = await db.query('INSERT INTO `tineon_infos`(`name`,`club_name`,`email`,`association_purpose`,`number_of_members`,`club_Website`)VALUES(?,?,?,?,?,?)', [
            req.body.name,
            req.body.club_name,
            req.body.email,
            req.body.association_purpose,
            req.body.number_of_members,
            req.body.club_Website
        ]);
        return rows[0];
    }

    async getAllTineonInfosByPage(param) {
        let pageSize = param.pagesize ? param.pagesize : 10;
        let page = param.page ? param.page : 1
        let offset = (pageSize * page) - pageSize;

        const rows = await db.query(`SELECT COUNT(*) OVER() as totalCount, tineon_infos.*  FROM tineon_infos  order by id desc LIMIT ` + pageSize + ` OFFSET ` + offset + ``);
        return rows[0];
    }

    async deleteTineonInfobyId(id) {
        const rows = await db.query("DELETE FROM `tineon_infos` WHERE `id`=?", [id]);
        return rows[0];
    }

    async getTineonInfobyId(id) {
        const rows = await db.query(`SELECT * FROM tineon_infos WHERE id =?`, [id]);
        return rows[0];
    }

}