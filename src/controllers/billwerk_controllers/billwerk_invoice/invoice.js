import async from 'async';
import responseFormat from '../../../core/response-format';
import configContainer from '../../../config/localhost';
import axios from 'axios';
import { LocalStorage } from 'node-localstorage';
let localStorage = new LocalStorage('./scratch');

export default class BillwerkInvoiceController {

    constructor() {
    }


    /**
     * Function to get customer cancel contract details from billwerk 
     * @author  MangoIt Solutions
     * @param   {contract_id}
     * @return  {object} customer contract details
     */
    InvoicesDraftById(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'invoiceDrafts/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get all Invoice draft data from billwerk 
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} Draft Invoice data 
    */
    getInvoicesDraft(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoiceDrafts',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get Invoice draft data by customer id from billwerk 
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object} Draft Invoice data 
    */
    getInvoicesDraftById(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoiceDrafts/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get Invoice download link
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  Invoice data 
    */
    getInvoiceDownloadLink(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'invoices/' + req.params.id + '/downloadLink',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])

    }

    /**
    * Function to get Invoice download by customer
    * @author  MangoIt Solutions
    * @param   {customer_id}
    * @return  {object}  Invoice data 
    */
    getInvoiceDownloadByCustomer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let invoiceId;
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'customers/' + req.params.customer_id + '/ledgerEntries',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        invoiceId = response.data[0].InvoiceId;
                        done();
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var config = {
                    method: 'post',
                    url: configContainer.billwerkAccountUrl + 'invoices/' + invoiceId + '/downloadLink',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get Invoice download 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  Invoice data 
    */
    getInvoiceDownload(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoices/' + req.params.id + '/download',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }

    /**
    * Function to get Invoice by id 
    * @author  MangoIt Solutions
    * @param   {id}
    * @return  {object}  Invoice data 
    */
    getInvoiceById(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoices/' + req.params.id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };
                axios(config)
                    .then(function (resPonse) {
                        var a1 = [];
                        a1[0] = resPonse.data;
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: a1 } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }


    /**
    * Function to get customer Invoice by customer_id 
    * @author  MangoIt Solutions
    * @param   {customer_id}
    * @return  {object}  Invoice data 
    */
    getCustomerInvoices(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let invoiceId;
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoices?customerId=' + req.params.customer_id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        if (response.data.length > 0) {
                            invoiceId = response.data;
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [] } });
                            res.status(200).json(response);
                        }
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                let userInvoices = [];
                for (let i = 0; i < invoiceId.length; i++) {
                    var config = {
                        method: 'get',
                        url: configContainer.billwerkAccountUrl + 'invoices/' + invoiceId[i].Id,
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        }
                    };

                    axios(config)
                        .then(function (response) {
                            userInvoices.push(response.data);
                            if (i + 1 == invoiceId.length) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: userInvoices } });
                                res.status(200).json(response);
                            }
                        })
                        .catch(function (error) {
                            if (error.isAxiosError == true && error.response.data.Message) {
                                response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            }
                        });
                }
            }
        ])
    }

    /**
    * Function to get customer Invoice by customer_id 
    * @author  MangoIt Solutions
    * @param   {customer_id}
    * @return  {object}  Invoice data 
    */
    getInvoiceByCustomer(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        let invoiceId;
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoices?customerId=' + req.params.customer_id,
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        if (response.data.length > 0) {
                            invoiceId = response.data.filter(obj => obj.ContractId === req.params.contract_id);
                            done();
                        } else {
                            response = responseFormat.getResponseMessageByCodes('', { content: { dataList: [] } });
                            res.status(200).json(response);
                        }
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {

                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            },
            function (done) {
                var invoiceArr = [];
                for (let i = 0; i < invoiceId.length; i++) {

                    var config = {
                        method: 'get',
                        url: configContainer.billwerkAccountUrl + 'invoices/' + invoiceId[i].Id,
                        headers: {
                            'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                        }
                    };

                    axios(config)
                        .then(function (response) {
                            invoiceArr.push(response.data);
                            if (invoiceId.length == i + 1) {
                                response = responseFormat.getResponseMessageByCodes('', { content: { dataList: invoiceArr } });
                                res.status(200).json(response);
                            }
                        })
                        .catch(function (error) {
                            if (error.isAxiosError == true && error.response.data.Message) {
                                response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                                res.status(200).json(response);
                            } else {
                                response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                                res.status(500).json(response);
                                return;
                            }
                        });
                }

            }

        ])

    }

    /**
    * Function to get Invoices
    * @author  MangoIt Solutions
    * @param   {}
    * @return  {object}  Invoice data 
    */
    getInvoices(req, res, next) {
        let response = responseFormat.createResponseTemplate(req.headers.lang);
        async.waterfall([
            function (done) {
                var config = {
                    method: 'get',
                    url: configContainer.billwerkAccountUrl + 'invoices',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('billwerkAuth')
                    }
                };

                axios(config)
                    .then(function (response) {
                        response = responseFormat.getResponseMessageByCodes('', { content: { dataList: response.data } });
                        res.status(200).json(response);
                    })
                    .catch(function (error) {
                        if (error.isAxiosError == true && error.response.data.Message) {
                            response = responseFormat.getResponseMessageByCodes(['billwerk:' + error.response.data.Message + ''], { code: 417 });
                            res.status(200).json(response);
                        } else {
                            response = responseFormat.getResponseMessageByCodes(['common500'], { code: 500 });
                            res.status(500).json(response);
                            return;
                        }
                    });
            }
        ])
    }
}