import express from 'express';
import RoleController from '../../controllers/roles/roles.js';

let router = express.Router();
let roleController = new RoleController();

router.get('/roles', roleController.roles.bind(roleController));
router.get('/rolebyid/:id', roleController.roleById.bind(roleController));
router.get('/rolebyrole/:role', roleController.roleByRole.bind(roleController));
router.get('/rolesByroleWeight', roleController.rolesByroleWeight.bind(roleController));
router.post('/rolecreate', roleController.roleCreate.bind(roleController));
router.put('/roleupdate/:id', roleController.updateRole.bind(roleController));
router.delete('/roledelete/:id', roleController.deleteRole.bind(roleController));
router.get('/roleCategory', roleController.roleCategory.bind(roleController)); //role_category section API's

export default router;