exports.up = async function (knex) {
    return knex.schema.createTable('sverein_product', function (t) {
        t.increments('pid').primary();
        t.integer('cuid').notNull();
        t.string('domain', [100]);
        t.text('data').notNull();
        t.text('publish_to').notNull();
        t.string('title', [64]).notNull();
        t.tinyint('state',[1]).notNull();
        t.text('product_components');
        t.string('created_at',[32]).notNull().defaultTo(0);
        t.string('modified_at',[32]).notNull().defaultTo(0);

    });
};

exports.down = async function (knex) {
    await knex.schema.dropTable('sverein_product');
};

