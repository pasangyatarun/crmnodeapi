exports.up = async function(knex, Promise) {
    await knex.schema.alterTable('field_data_field_address', function(t) {
        t.string('field_address_country',[32]).defaultTo('').comment('Two letter ISO country code of this address.').alter();
        t.string('field_address_name_line',[255]).defaultTo('').comment('Contents of a primary NameLine element in the xNL XML.').alter();
        t.string('field_address_first_name',[255]).defaultTo('').comment('Contents of the FirstName element of a primary PersonName element in the xNL XML.').alter();
        t.string('field_address_last_name',[255]).defaultTo('').comment('Contents of the LastName element of a primary PersonName element in the xNL XML.').alter();
        t.string('field_address_organisation_name',[255]).defaultTo('').comment('Contents of a primary OrganisationName element in the xNL XML.').alter();
        t.string('field_address_locality',[255]).defaultTo('').comment('The locality of this address.').alter();
        t.string('field_address_dependent_locality',[255]).defaultTo('').comment('The dependent locality of this address.').alter();
        t.string('field_address_postal_code',[255]).defaultTo('').comment('The postal code of this address.').alter();
        t.string('field_address_thoroughfare',[255]).defaultTo('').comment('The thoroughfare of this address.').alter();
        t.string('field_address_second',[255]).defaultTo('').alter();
        t.string('entity_type',[128]).defaultTo('').comment('The entity type this data is attached to.').notNull().alter()


    });
}
exports.down = async function(knex, Promise) { 
    await knex.schema.alterTable('field_data_field_address', function(t) {
       
    });
 };